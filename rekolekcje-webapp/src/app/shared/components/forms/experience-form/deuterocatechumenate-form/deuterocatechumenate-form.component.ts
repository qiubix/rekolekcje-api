import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Deuterocatechumenate, Talents } from '@shared/models';
import { Config } from '@config';

const resolvedPromise = Promise.resolve(null);

@Component({
  selector: 'reko-deuterocatechumenate-form',
  templateUrl: './deuterocatechumenate-form.component.html',
  styleUrls: ['./deuterocatechumenate-form.component.scss']
})
export class DeuterocatechumenateFormComponent implements OnInit {
  form: FormGroup;

  @Input() data: Deuterocatechumenate;
  @Output() formReady = new EventEmitter<FormGroup>();

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.form = this.fb.group({
      year: [
        this.data ? this.data.year : null,
        [Validators.min(Config.minYear), Validators.max(new Date().getFullYear())]
      ],
      stepsTaken: this.data ? this.data.stepsTaken : null,
      stepsPlannedThisYear: this.data ? this.data.stepsPlannedThisYear : null,
      celebrationsTaken: this.data ? this.data.celebrationsTaken : null,
      celebrationsPlannedThisYear: this.data ? this.data.celebrationsPlannedThisYear : null,
      callByNameCelebrationYear: this.data ? this.data.callByNameCelebrationYear : null,
      missionCelebrationYear: this.data ? this.data.missionCelebrationYear : null,
      holyTriduumYear: this.data ? this.data.holyTriduumYear : null
    });
    resolvedPromise.then(() => {
      this.formReady.emit(this.form);
    });
  }

  getYearValidationErrorMessage(): string {
    if (this.form)
      if (this.form.get('year').hasError('min')) {
        return 'Value cannot be lower than ' + Config.minYear + '.';
      } else if (this.form.get('year').hasError('max')) {
        return 'Value cannot be above ' + new Date().getFullYear() + '.';
      }
  }
}
