package pl.oaza.waw.rekolekcje.api.notification.dto

data class NotificationSummary(
    val id: Long,
    val applicationId: Long,
    val reason: NotificationReason,
    val status: NotificationStatus
)

