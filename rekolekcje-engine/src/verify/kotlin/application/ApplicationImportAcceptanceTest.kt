package pl.oaza.waw.rekolekcje.api.application

import org.junit.jupiter.api.Test
import org.springframework.security.test.context.support.WithMockUser
import pl.oaza.waw.rekolekcje.api.application.fixture.BaseApplicationAcceptanceTest
import java.nio.file.Files
import java.nio.file.Paths

internal class ApplicationImportAcceptanceTest : BaseApplicationAcceptanceTest() {

  @Test
  @WithMockUser
  fun `should import sample animator applications from file`() {
    // given
    val path = Paths.get("src/test/resources/Animatorzy.csv")
    val content = String(Files.readAllBytes(path), Charsets.UTF_8)

    // when
    val response = whenInApi.fileIsUploadedForAnimators(content)

    // then
    response.hasStatusOk()
    thenInApi.thereIsSpecificNumberOfApplications(3)
  }

  @Test
  @WithMockUser
  fun `should import sample participant applications from file`() {
    // given
    val path = Paths.get("src/test/resources/ONZ-pełnoletni.csv")
    val content = String(Files.readAllBytes(path), Charsets.UTF_8)

    // when
    val response = whenInApi.fileIsUploadedForParticipants(content)

    // then
    response.hasStatusOk()
    thenInApi.thereIsSpecificNumberOfApplications(2)
  }

  @Test
  @WithMockUser
  fun `should import sample underage participant applications from file`() {
    // given
    val path = Paths.get("src/test/resources/ONZ-niepełnoletni.csv")
    val content = String(Files.readAllBytes(path), Charsets.UTF_8)

    // when
    val response = whenInApi.fileIsUploadedForParticipants(content)

    // then
    response.hasStatusOk()
    thenInApi.thereIsSpecificNumberOfApplications(2)
  }
}