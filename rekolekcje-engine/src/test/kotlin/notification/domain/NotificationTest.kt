package notification.domain

import pl.oaza.waw.rekolekcje.api.notification.domain.InMemoryNotificationRepository
import pl.oaza.waw.rekolekcje.api.notification.domain.NotificationCommandFacade
import pl.oaza.waw.rekolekcje.api.notification.domain.NotificationCommandFacadeImpl

abstract class NotificationTest {

  private val notificationRepository = InMemoryNotificationRepository()
  protected val commandFacade: NotificationCommandFacade = NotificationCommandFacadeImpl(notificationRepository)
}