package pl.oaza.waw.rekolekcje.api.members.endpoint

import pl.oaza.waw.rekolekcje.api.members.domain.Member
import pl.oaza.waw.rekolekcje.api.shared.value.Sex

data class MemberSummary(
  val id: Long,
  val firstName: String,
  val lastName: String,
  val age: Int,
  val sex: Sex,
  val parishId: Long? = null,
  val communityId: Long? = null
) {
  constructor(member: Member) : this(
    id = member.getId() ?: throw IllegalStateException("Member should have ID."),
    firstName = member.personalData.contactInfo.firstName ?: throw IllegalStateException(),
    lastName = member.personalData.contactInfo.lastName ?: throw IllegalStateException(),
    parishId = member.personalData.parishId,
    communityId = member.personalData.communityId,
    age = member.personalData.age(),
    sex = member.personalData.sex()
  )
}

data class MemberPublicData(
  val fullName: String
)