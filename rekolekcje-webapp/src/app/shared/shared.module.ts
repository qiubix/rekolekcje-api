import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@shared/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
// import { NgxUploaderModule } from 'ngx-uploader';
import {
  AlternativeImportDialogComponent,
  DeleteConfirmAlertDialog,
  // ImportDialogComponent
} from './components';
import { RouterModule } from '@angular/router';
import * as fromDirectives from './directives';
import * as fromTransformations from './transformations';
import * as fromComponents from './components';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    // NgxUploaderModule,
    FormsModule,
    HttpClientModule,
    MaterialModule,
    ReactiveFormsModule,
  ],
  declarations: [
    ...fromComponents.components,
    ...fromTransformations.transformations,
    ...fromDirectives.directives,
  ],
  entryComponents: [
    DeleteConfirmAlertDialog,
    // ImportDialogComponent,
    AlternativeImportDialogComponent,
  ],
  exports: [
    CommonModule,
    RouterModule,
    FormsModule,
    // NgxUploaderModule,
    MaterialModule,
    ReactiveFormsModule,
    TranslateModule,
    ...fromComponents.components,
    ...fromTransformations.transformations,
    ...fromDirectives.directives,
  ],
})
export class SharedModule {}
