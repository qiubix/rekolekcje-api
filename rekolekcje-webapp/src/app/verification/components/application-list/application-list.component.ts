import { Component, OnInit } from '@angular/core';
import { ApplicationSummary } from '@verification/model';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Verification } from '../../state/verification-reducer';
import { Observable } from 'rxjs/internal/Observable';
import { AppSelectors } from '@core/store/app-selectors';
import { VerificationActions } from '../../state/verification-actions';
import {
  AlternativeImportDialogComponent,
  DeleteConfirmAlertDialog,
  // ImportDialogComponent,
} from '@shared/components';
import { AnimatorVsParticipantDialogComponent } from '@verification/components';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'reko-application-list',
  templateUrl: './application-list.component.html',
  styleUrls: ['./application-list.component.scss'],
})
export class ApplicationListComponent implements OnInit {
  displayedColumns = [
    'firstName',
    'lastName',
    'memberStatus',
    'paymentStatus',
    'isAnimator',
    'selectedRetreat',
    'options',
  ];

  pendingApplications$: Observable<ApplicationSummary[]>;
  verifiedApplications$: Observable<ApplicationSummary[]>;

  constructor(
    private router: Router,
    private store: Store<Verification.State>,
    private dialog: MatDialog
  ) {}

  ngOnInit() {
    this.store.dispatch(new VerificationActions.GetPendingApplications());
    this.store.dispatch(new VerificationActions.GetVerifiedApplications());
    this.pendingApplications$ = this.store.pipe(select(AppSelectors.getPendingApplications));
    this.verifiedApplications$ = this.store.pipe(select(AppSelectors.getVerifiedApplications));
  }

  onSelect(application: ApplicationSummary) {
    this.router.navigate(['verification', `${application.id}`], {
      queryParams: { animator: application.isAnimator },
    });
  }

  onAdd() {
    this.dialog.open(AnimatorVsParticipantDialogComponent);
  }

  onEdit(application: ApplicationSummary) {
    this.router.navigate(['verification', `${application.id}`, 'edit'], {
      queryParams: { animator: application.isAnimator },
    });
  }

  onDelete(id: number) {
    this.dialog
      .open(DeleteConfirmAlertDialog, { disableClose: false })
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          console.log('Trying to delete application with id: ', id);
          this.store.dispatch(new VerificationActions.DeleteApplication(id));
        }
      });
  }

  // onImport() {
  //   this.dialog.open(ImportDialogComponent, {
  //     data: { importUrl: '/application/animators/import' },
  //   });
  // }

  onImport2() {
    this.dialog.open(AlternativeImportDialogComponent, {
      data: { importUrl: '/application/animators/import' },
    });
  }
}
