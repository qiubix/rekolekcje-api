package pl.oaza.waw.rekolekcje.api.user.query

import pl.oaza.waw.rekolekcje.api.user.domain.UserNotFoundException
import pl.oaza.waw.rekolekcje.api.user.domain.UserReadRepository
import pl.oaza.waw.rekolekcje.api.user.endpoint.UserSecurityDetails
import pl.oaza.waw.rekolekcje.api.user.endpoint.UserSummary

class UserQueryService(private val userReadRepository: UserReadRepository) {

  fun findAll(): List<UserSummary> = userReadRepository.findAll().map { it.toDto() }

  fun findByUsername(username: String): UserSummary? = userReadRepository.findByUsername(username)?.toDto()

  fun loadSecurityDetails(username: String): UserSecurityDetails {
    val foundUser = userReadRepository.findByUsername(username) ?: throw UserNotFoundException(username)
    return foundUser.securityDetails()
  }

}
