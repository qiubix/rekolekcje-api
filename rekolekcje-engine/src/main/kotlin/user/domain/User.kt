package pl.oaza.waw.rekolekcje.api.user.domain

import pl.oaza.waw.rekolekcje.api.user.endpoint.UserSecurityDetails
import pl.oaza.waw.rekolekcje.api.user.endpoint.UserRole
import pl.oaza.waw.rekolekcje.api.user.endpoint.UserSummary
import java.util.Date
import javax.persistence.CollectionTable
import javax.persistence.Column
import javax.persistence.ElementCollection
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.Table
import javax.persistence.Temporal
import javax.persistence.TemporalType

@Entity
@Table(name = "USERS")
data class User(

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  val id: Long? = null,
  val username: String,
  val password: String,
  val email: String,
  val enabled: Boolean = true,

  @Column(name = "LASTPASSWORDRESETDATE")
  @Temporal(TemporalType.TIMESTAMP)
  val lastPasswordResetDate: Date = Date(),

  @ElementCollection(targetClass = Role::class, fetch = FetchType.EAGER)
  @CollectionTable(name = "USER_ROLE", joinColumns = [JoinColumn(name = "USER_ID")])
  @Enumerated(EnumType.STRING)
  @Column(name = "ROLE")
  val roles: Set<Role> = emptySet()
) {

  fun toDto(): UserSummary {
    return UserSummary(
      username = username,
      enabled = enabled,
      email = email,
      roles = roles.map { UserRole.valueOf(it.name) }.toSet()
    )
  }

  fun securityDetails(): UserSecurityDetails {
    return UserSecurityDetails(
      username = username,
      lastPasswordResetDate = lastPasswordResetDate,
      roles = roles.map { UserRole.valueOf(it.name) }.toSet()
    )
  }

}
