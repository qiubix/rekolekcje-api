package pl.oaza.waw.rekolekcje.api.application

import pl.oaza.waw.rekolekcje.api.application.domain.AnimatorRole
import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationStatus
import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationType
import pl.oaza.waw.rekolekcje.api.application.domain.RetreatApplication
import pl.oaza.waw.rekolekcje.api.shared.domain.Contact
import pl.oaza.waw.rekolekcje.api.shared.value.PaymentStatus

fun sampleApplication(
  memberId: Long = 123L,
  applicationStatus: ApplicationStatus = ApplicationStatus.WAITING,
  firstName: String = "Mike",
  lastName: String = "Lowrey",
  paymentStatus: PaymentStatus = PaymentStatus.DOWN_PAYMENT,
  suretyName: String = "Surety name",
  suretyEmail: String? = "surety@mail.com",
  animatorRoles: Set<AnimatorRole> = setOf(AnimatorRole.GROUP, AnimatorRole.MUSICIAN),
  retreatId: Long = 44L
) = RetreatApplication(
  applicationType = ApplicationType.ANIMATOR,
  memberId = memberId,
  firstName = firstName,
  lastName = lastName,
  animatorRoles = animatorRoles,
  availability = setOf(12L, 14L),
  surety = Contact(
    fullName = suretyName,
    email = suretyEmail
  ),
  status = applicationStatus,
  paymentStatus = paymentStatus,
  retreatId = retreatId
)

fun animatorApplication(
  memberId: Long = 123L,
  applicationStatus: ApplicationStatus = ApplicationStatus.WAITING,
  firstName: String = "Mike",
  lastName: String = "Lowrey",
  paymentStatus: PaymentStatus = PaymentStatus.DOWN_PAYMENT,
  suretyName: String = "Surety name",
  animatorRoles: Set<AnimatorRole> = setOf(AnimatorRole.GROUP, AnimatorRole.MUSICIAN)
) = RetreatApplication(
  applicationType = ApplicationType.ANIMATOR,
  memberId = memberId,
  firstName = firstName,
  lastName = lastName,
  animatorRoles = animatorRoles,
  availability = setOf(12L, 14L),
  surety = Contact(
    fullName = suretyName,
    email = "surety@mail.com"
  ),
  status = applicationStatus,
  paymentStatus = paymentStatus
)

fun participantApplication(
  memberId: Long = 223L,
  status: ApplicationStatus = ApplicationStatus.WAITING,
  retreatId: Long = 432L,
  firstName: String = "Marcus",
  lastName: String = "Burnett"
) = RetreatApplication(
  applicationType = ApplicationType.PARTICIPANT,
  memberId = memberId,
  firstName = firstName,
  lastName = lastName,
  status = status,
  paymentStatus = PaymentStatus.NOTHING,
  retreatId = retreatId
)
