import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Address } from '@shared/models';
import { FormBuilder, FormGroup } from '@angular/forms';

const resolvedPromise = Promise.resolve(null);

@Component({
  selector: 'reko-address-form',
  templateUrl: './address-form.component.html',
  styleUrls: ['./address-form.component.scss']
})
export class AddressFormComponent implements OnInit {
  @Input() address: Address;
  @Output() formReady = new EventEmitter<FormGroup>();

  public form: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      streetName: this.address ? this.address.streetName : null,
      streetNumber: this.address ? this.address.streetNumber : null,
      flatNumber: this.address ? this.address.flatNumber : null,
      postalCode: this.address ? this.address.postalCode : null,
      city: this.address ? this.address.city : ''
    });
    resolvedPromise.then(() => {
      this.formReady.emit(this.form);
    });
  }
}
