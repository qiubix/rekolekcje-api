export type AuthState = 'authenticated' | 'anonymous' | 'unknown';
