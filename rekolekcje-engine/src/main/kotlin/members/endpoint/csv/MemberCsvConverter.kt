package pl.oaza.waw.rekolekcje.api.members.endpoint.csv

import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import pl.oaza.waw.rekolekcje.api.members.endpoint.MemberDetails
import pl.oaza.waw.rekolekcje.api.shared.endpoint.csv.CsvConversionError


fun convertMembersFromCsv(content: String): CsvConversionResult {
  val rawData: List<Map<String, String>> = csvReader().readAllWithHeader(content)
  val csvReader = MemberCsvRowConverter(memberFieldToHeaders)
  val convertedMembers = rawData.map { csvReader.read(it) }
  return CsvConversionResult(
    convertedMembers = convertedMembers,
    // TODO: return errors for invalid members:
    //  https://betterprogramming.pub/do-you-even-try-functional-error-handling-in-kotlin-ad562b3b394f
    errors = emptyList()
  )
}

data class CsvConversionResult(
  val convertedMembers: List<MemberDetails>,
  val errors: List<CsvConversionError>
)