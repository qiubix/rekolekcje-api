package pl.oaza.waw.rekolekcje.api.retreats.infrastructure

import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import pl.oaza.waw.rekolekcje.api.retreats.domain.RetreatCommandFacade
import pl.oaza.waw.rekolekcje.api.retreats.domain.RetreatCommandFacadeImpl
import pl.oaza.waw.rekolekcje.api.retreats.domain.RetreatEventPublisher
import pl.oaza.waw.rekolekcje.api.retreats.domain.RetreatReadRepository
import pl.oaza.waw.rekolekcje.api.retreats.query.RetreatQueryFacade
import pl.oaza.waw.rekolekcje.api.retreats.query.RetreatQueryFacadeImpl
import pl.oaza.waw.rekolekcje.api.retreats.domain.RetreatWriteRepository
import pl.oaza.waw.rekolekcje.api.retreats.infrastructure.events.RetreatEventPublisherImpl

@Configuration
internal class RetreatConfiguration {

  @Bean
  fun retreatQueryFacade(
    retreatReadRepository: RetreatReadRepository
  ): RetreatQueryFacade = RetreatQueryFacadeImpl(retreatReadRepository)

  @Bean
  fun retreatCommandFacade(
    retreatReadRepository: RetreatReadRepository,
    retreatWriteRepository: RetreatWriteRepository,
    retreatEventPublisher: RetreatEventPublisher
  ): RetreatCommandFacade = RetreatCommandFacadeImpl(retreatWriteRepository, retreatReadRepository, retreatEventPublisher)

  @Bean
  fun retreatNotifier(eventPublisher: ApplicationEventPublisher): RetreatEventPublisher = RetreatEventPublisherImpl(eventPublisher)
}
