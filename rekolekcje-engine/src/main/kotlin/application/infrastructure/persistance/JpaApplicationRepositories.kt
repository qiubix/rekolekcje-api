package pl.oaza.waw.rekolekcje.api.application.infrastructure.persistance

import org.springframework.data.repository.Repository
import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationReadRepository
import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationWriteRepository
import pl.oaza.waw.rekolekcje.api.application.domain.RetreatApplication

class ApplicationReadRepositoryImpl(private val jpaReadRepository: JpaApplicationReadRepository) :
  ApplicationReadRepository {

  override fun findAll(): List<RetreatApplication> = jpaReadRepository.findAll().map { it.toDomainEntity() }

  override fun findById(id: Long): RetreatApplication? = jpaReadRepository.findById(id)?.toDomainEntity()

  override fun existsById(id: Long) = jpaReadRepository.existsById(id)
}

class ApplicationWriteRepositoryImpl(private val jpaWriteRepository: JpaApplicationWriteRepository) :
  ApplicationWriteRepository {

  override fun save(entity: RetreatApplication): RetreatApplication =
    jpaWriteRepository.save(PersistedRetreatApplication(entity)).toDomainEntity()

  override fun deleteById(id: Long) {
    jpaWriteRepository.deleteById(id)
  }

}

interface JpaApplicationReadRepository : Repository<PersistedRetreatApplication, Long> {

  fun findAll(): List<PersistedRetreatApplication>

  fun findById(id: Long): PersistedRetreatApplication?

  fun existsById(id: Long): Boolean
}

interface JpaApplicationWriteRepository : Repository<PersistedRetreatApplication, Long> {

  fun save(entity: PersistedRetreatApplication): PersistedRetreatApplication

  fun deleteById(id: Long)

}