package pl.oaza.waw.rekolekcje.api.tools.validation

import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.transaction.TransactionSystemException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@ControllerAdvice
internal class JPAExceptionHandler {

  private val log = LoggerFactory.getLogger(this.javaClass.name)

  // ConstraintViolationException can not be caught by @ExceptionHandler,
  // because it occurs during JPA transaction and it causes TransactionSystemException
  @ExceptionHandler(TransactionSystemException::class)
  fun transactionException(ex: TransactionSystemException): ResponseEntity<Any> {
    log.debug(ex.message, ex)

    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
      .body("Something went wrong on our side...")
  }
}
