import { Component, Inject } from '@angular/core';
import { Community } from '../../../models/community.model';
import { Observable } from 'rxjs';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

export interface CommunityDialogData {
  dialogTitle: Observable<string>;
  community: Community;
}

@Component({
  selector: 'reko-community-dialog',
  templateUrl: './community-dialog.component.html',
  styleUrls: ['./community-dialog.component.scss'],
})
export class CommunityDialogComponent {
  community: Community;
  dialogTitle: Observable<string>;

  constructor(
    private dialogRef: MatDialogRef<CommunityDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private data: CommunityDialogData
  ) {
    this.dialogTitle = data.dialogTitle;
    this.community = data.community ? data.community : {};
  }

  onFormOutput(community: Community): void {
    if (community) {
      community.id = this.community.id;
      this.dialogRef.close(community);
    } else {
      this.dialogRef.close();
    }
  }
}
