package pl.oaza.waw.rekolekcje.api.user.domain

import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.crypto.password.PasswordEncoder
import pl.oaza.waw.rekolekcje.api.user.endpoint.AuthenticationRequest
import pl.oaza.waw.rekolekcje.api.user.endpoint.UserSecurityDetails

class UserAuthenticationService(
  private val userReadRepository: UserReadRepository,
  private val passwordEncoder: PasswordEncoder
) {

  fun authenticate(authenticationRequest: AuthenticationRequest): UserSecurityDetails {
    val foundUser = userReadRepository.findByUsername(authenticationRequest.username)
    return foundUser?.let {
      if (passwordIsCorrect(authenticationRequest, foundUser) && foundUser.enabled) {
        foundUser.securityDetails()
      } else {
        null
      }
    } ?: throw BadCredentialsException("Bad credentials")
  }

  private fun passwordIsCorrect(
    authenticationRequest: AuthenticationRequest,
    foundUser: User
  ) = passwordEncoder.matches(authenticationRequest.password, foundUser.password)

}
