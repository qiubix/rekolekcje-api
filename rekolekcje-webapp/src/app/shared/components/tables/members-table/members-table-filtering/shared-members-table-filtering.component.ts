import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { MembersFilter } from './shared-members-table-filter.util';
import { Subject } from 'rxjs';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MemberStatus, Stage, Turns } from '@shared/models';
import { PositionDisplayOption } from '@members/components/list/position-display-options.model';
import { debounceTime, distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { Config } from '@config';

@Component({
  selector: 'reko-shared-members-table-filtering',
  templateUrl: './shared-members-table-filtering.component.html',
  styleUrls: ['./shared-members-table-filtering.component.scss']
})
export class SharedMembersTableFilteringComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  @Input() visibleFilters: string[];

  @Output() filterChange = new EventEmitter<MembersFilter>();

  form: FormGroup;
  _visibleFilters: string[];
  memberStatusKeys = Object.keys(MemberStatus);
  stagesKeys = Object.keys(Stage);
  turnKeys = Object.keys(Turns);

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.initForm();
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  private initForm() {
    this.form = this.fb.group({
      selectedDisplayOption: PositionDisplayOption.ALL_MEMBERS,
      memberStatus: null,
      firstOrLastName: '',
      stage: '',
      turn: '',
      community: ''
    });
    this.form.valueChanges
      .pipe(
        debounceTime(Config.inputDebounceTime),
        distinctUntilChanged(),
        takeUntil(this.ngUnsubscribe)
      )
      .subscribe((value: MembersFilter) => this.filterChange.emit(value));
  }

  shouldDisplayFilter(filterName: string) {
    return (
      !this.visibleFilters ||
      this.visibleFilters.length === 0 ||
      this.visibleFilters.includes(filterName)
    );
  }
}
