import { Injectable } from '@angular/core';
import { Observable, of, forkJoin } from 'rxjs';
import { MemberPublicData, MemberSummary } from '@members/models';
import { HttpClient } from '@angular/common/http';
import { Config } from '@config';
import { MemberDetails } from '@shared/models';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class MembersApiService {
  constructor(private http: HttpClient) {}

  getAll(): Observable<MemberSummary[]> {
    return this.http.get<MemberSummary[]>(Config.endpoints.membersModule);
  }

  getMembersToVerify(): Observable<MemberSummary[]> {
    return this.http.get<MemberSummary[]>(Config.endpoints.membersModule + '/verify');
  }

  getOne(id: number): Observable<MemberDetails> {
    return this.http.get<MemberDetails>(Config.endpoints.membersModule + '/' + id);
  }

  // getHistoricalRetreatsForMember(id: number): Observable<number[]> {
  //   return this.http.get<number[]>(Config.endpoints.retreatsModule + '/historical/' + id);
  // }

  getOneMember(id: number): Observable<MemberDetails> {
    return this.getOne(id);
    // const memberWithoutRetreats$ = this.getOne(id);
    // const historicalRetreats$ = this.getHistoricalRetreatsForMember(id);

    // return forkJoin(historicalRetreats$, memberWithoutRetreats$).pipe(
    //   map((data: [number[], MemberDetails]) => {
    //     const historicalRetreats = data[0];
    //     const memberWithoutRetreats = data[1];
    //     const experienceWithRetreats = Object.assign(memberWithoutRetreats.experience, {
    //       historicalRetreats: historicalRetreats
    //     });
    //     return Object.assign({}, memberWithoutRetreats, { experience: experienceWithRetreats });
    //   })
    // );
  }

  getMemberFullName(id: number): Observable<MemberPublicData> {
    return this.http.get<MemberPublicData>(Config.endpoints.membersModule + '/full-name/' + id);
  }

  getMemberFullNameFromEncrypted(encryptedId: string): Observable<MemberPublicData> {
    return this.http.get<MemberPublicData>(
      Config.endpoints.membersModule + '/full-name-encrypted/' + encryptedId
    );
  }

  update(details: MemberDetails): Observable<MemberSummary> {
    return this.http.put<MemberDetails>(Config.endpoints.membersModule, details);
  }

  addNew(details: MemberDetails): Observable<MemberSummary> {
    return this.http.post<MemberDetails>(Config.endpoints.membersModule, details);
  }

  deleteOne(id: number): Observable<any> {
    return this.http.delete<MemberDetails>(Config.endpoints.membersModule + '/' + id);
  }

  changeMemberStatus(id: number, status: string): Observable<MemberSummary> {
    return this.http.put<MemberSummary>(Config.endpoints.membersModule + '/' + id + '/status', {
      status: status
    });
  }

  changePaymentStatus(id: number, newStatus: string): Observable<MemberSummary> {
    return this.http.put<MemberSummary>(
      Config.endpoints.membersModule + '/' + id + '/payment',
      newStatus
    );
  }
}
