import { environment } from '@env';

export const Config = {
  defaultExportFileName: 'export.xls',
  endpoints: {
    applicationModule: environment.apiUrl + '/application',
    membersModule: environment.apiUrl + '/members',
    parishModule: environment.apiUrl + '/parish',
    retreatsModule: environment.apiUrl + '/retreats',
    communityModule: environment.apiUrl + '/communities',
    usersModule: environment.apiUrl + '/users',
    opinionsModule: environment.apiUrl + '/opinions'
  },

  inputDebounceTime: 250,
  minYear: 1900,
  notificationDuration: 10000
};
