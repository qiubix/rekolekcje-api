import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'delete-confirm-alert',
  templateUrl: './delete-confirm-alert.component.html',
  styleUrls: ['./delete-confirm-alert.component.scss'],
})
export class DeleteConfirmAlertDialog {
  constructor(
    public dialogRef: MatDialogRef<DeleteConfirmAlertDialog>,
    @Inject(MAT_DIALOG_DATA) public data
  ) {}
}
