package pl.oaza.waw.rekolekcje.api.community.domain

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import pl.oaza.waw.rekolekcje.api.community.dto.CommunityNotFoundException
import pl.oaza.waw.rekolekcje.api.community.dto.CommunityPayload

internal class CommunityTest {

  private val first = CommunityPayload(1L, "First", "City")
  private val second = CommunityPayload(2L, "Second", "Village")

  private val facade = CommunityConfiguration().communityFacade()

  @AfterEach
  fun tearDown() {
    facade.delete(first.id!!, second.id!!)
  }

  @Test
  fun shouldAddNew() {
    facade.save(first)

    val savedCommunities = facade.findAll()
    assertThat(savedCommunities).hasSize(1)
    assertThat(savedCommunities).contains(first)
  }

  @Test
  fun shouldAssignIdOnCreation() {
    val communityWithoutId = CommunityPayload(name = "Some name")
    val savedCommunity = facade.save(communityWithoutId)

    assertThat(savedCommunity.id)
      .isNotNull()
      .isNotZero()
    val savedCommunities = facade.findAll()
    assertThat(savedCommunities)
      .usingElementComparatorIgnoringFields("id")
      .contains(savedCommunity)
  }

  @Test
  fun shouldGetAll() {
    facade.save(first)
    facade.save(second)

    val foundCommunities = facade.findAll()
    assertThat(foundCommunities).hasSize(2)
    assertThat(foundCommunities).containsOnlyOnce(first, second)
  }

  @Test
  fun shouldGetOne() {
    facade.save(first)
    facade.save(second)

    val foundCommunity = facade.findOne(second.id!!)

    assertThat(foundCommunity).isEqualTo(second)
  }

  @Test
  fun shouldThrowExceptionWhenNoneIsFound() {
    facade.save(first)

    assertThatExceptionOfType(CommunityNotFoundException::class.java)
      .isThrownBy { facade.findOne(second.id!!) }
      .withMessageContaining("id " + second.id!!)
  }

  @Test
  fun shouldDelete() {
    facade.save(first)
    facade.save(second)

    facade.delete(second.id!!)

    val remainingCommunities = facade.findAll()
    assertThat(remainingCommunities).hasSize(1)
    assertThat(remainingCommunities).doesNotContain(second)
  }

  @Test
  fun shouldUpdate() {
    facade.save(first)
    facade.save(second)
    val communityWithUpdatedData =
      CommunityPayload(
        id = first.id,
        name = "New name"
      )

    val updatedCommunity = facade.save(communityWithUpdatedData)

    assertThat(updatedCommunity).isEqualTo(communityWithUpdatedData)
    val communities = facade.findAll()
    assertThat(communities).hasSize(2)
    assertThat(communities).contains(communityWithUpdatedData)
  }
}
