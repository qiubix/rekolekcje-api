package pl.oaza.waw.rekolekcje.api.community.storage

import org.springframework.jdbc.core.JdbcTemplate

class CommunityDatabase(private val jdbcTemplate: JdbcTemplate) {

  fun clear() {
    jdbcTemplate.execute("DELETE FROM communities")
  }
}
