import { Component, Input, OnInit } from '@angular/core';
import { RetreatApplication } from '@verification/model';

@Component({
  selector: 'reko-participant-application-details',
  templateUrl: './participant-application-details.component.html',
  styleUrls: ['./participant-application-details.component.scss']
})
export class ParticipantApplicationDetailsComponent implements OnInit {
  @Input() application: RetreatApplication;

  constructor() {}

  ngOnInit() {}
}
