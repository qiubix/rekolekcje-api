package pl.oaza.waw.rekolekcje.api.application.domain

interface ApplicationNotifier {

  fun newApplicationSubmitted(applicationId: Long)

  fun applicationVerified(id: Long)

  fun applicationRejected(id: Long, reason: String)

}