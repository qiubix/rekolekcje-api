package pl.oaza.waw.rekolekcje.api.retreats.api

import pl.oaza.waw.rekolekcje.api.shared.BaseApiBehaviour
import pl.oaza.waw.rekolekcje.api.shared.Response
import pl.oaza.waw.rekolekcje.api.shared.RestApi
import pl.oaza.waw.rekolekcje.api.retreats.endpoint.RetreatDetails
import pl.oaza.waw.rekolekcje.api.retreats.domain.RetreatNotFoundException
import pl.oaza.waw.rekolekcje.api.retreats.endpoint.RetreatSummary

class RetreatsApiBehaviour(restApi: RestApi, baseUri: String) :
  BaseApiBehaviour<RetreatDetails>(restApi, baseUri) {

  fun resourceAmountIsRequested(): Int {
    return restApi.performGet(baseUri)
      .getContentAsList<RetreatSummary>()
      .size
  }

  fun existsElementWithTheSameData(payload: RetreatDetails): RetreatDetails {
    val response = restApi.performGet(baseUri)
    val allElements = response.getContentAsList<RetreatDetails>()
    val foundElement = allElements.find { haveTheSameData(payload, it) }
    return foundElement ?: throw getNotFoundException(payload)
  }

  fun addAndGetId(payload: RetreatDetails): Long {
    return singleIsAdded(payload)
      .getContent<RetreatSummary>()
      .id ?: throw IllegalStateException()
  }

  @Throws(Exception::class)
  fun existsRetreatWithTheSameData(payload: RetreatDetails): Long {
    val response = restApi.performGet(baseUri)
    val allRetreats = response.getContentAsList<RetreatSummary>()
    val foundRetreat = allRetreats.find { haveTheSameData(payload, it) }
    return foundRetreat?.id ?: throw RetreatNotFoundException()
  }

  override fun haveTheSameData(
    first: RetreatDetails,
    second: RetreatDetails
  ): Boolean {
    return first.stage == second.stage && first.turn == second.turn
  }

  private fun haveTheSameData(
    first: RetreatDetails,
    second: RetreatSummary
  ): Boolean {
    return first.stage == second.stage && first.turn == second.turn
  }

  fun historicalRetreatsForMemberAreRequested(memberId: Long): Response {
    return restApi.performGet("$baseUri/historical/$memberId")
  }


  override fun getNotFoundException(payload: RetreatDetails): RuntimeException {
    return RetreatNotFoundException()
  }
}
