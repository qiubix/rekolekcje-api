import { RetreatTurnSummaryPipe } from './formatters/retreat-turn-summary.pipe';
import { ColorForMemberStatusPipe } from './formatters/color-for-member-status.pipe';
import { ColorForPaymentStatusPipe } from './formatters/color-for-payment-status.pipe';
import { MemberStatusTranslatePipe } from './formatters/member-status-translate.pipe';
import { PaymentStatusTranslatePipe } from './formatters/payment-status-translate.pipe';
import { RetreatTitlePipe } from './formatters/retreat-title.pipe';
import { FindParishPipe } from './find-parish.pipe';
import { FindCommunityPipe } from './find-community.pipe';
import { FormatParishPipe } from './formatters/format-parish.pipe';
import { FormatCommunityPipe } from './formatters/format-community.pipe';
import { FindRetreatPipe } from './find-retreat.pipe';
import { FindMemberFullNamePipe } from '@shared/transformations/find-member-full-name.pipe';
import { MemberFullNamePipe } from '@shared/transformations/formatters/member-full-name.pipe';
import { FindMemberFullNameFromEncryptedPipe } from '@shared/transformations/find-member-full-name-from-encrypted.pipe';

export const transformations: any[] = [
  RetreatTurnSummaryPipe,
  ColorForMemberStatusPipe,
  ColorForPaymentStatusPipe,
  MemberStatusTranslatePipe,
  PaymentStatusTranslatePipe,
  RetreatTitlePipe,
  FindParishPipe,
  FindCommunityPipe,
  FormatParishPipe,
  FormatCommunityPipe,
  FindRetreatPipe,
  FindMemberFullNamePipe,
  FindMemberFullNameFromEncryptedPipe,
  MemberFullNamePipe
];
