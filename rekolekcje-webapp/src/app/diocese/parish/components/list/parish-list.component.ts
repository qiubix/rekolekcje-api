import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DeleteConfirmAlertDialog } from '@shared/components';
import { TranslateService } from '@ngx-translate/core';
import { Parish } from '../../models/parish.model';
import { ParishAddEditDialogComponent } from '@diocese/parish/components';
import { Observable } from 'rxjs/internal/Observable';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'parish-list',
  templateUrl: './parish-list.component.html',
  styleUrls: ['./parish-list.component.scss'],
})
export class ParishListComponent {
  @Output() addParish: EventEmitter<Parish> = new EventEmitter<Parish>();
  @Output() deleteParish: EventEmitter<number> = new EventEmitter<number>();
  @Output() editParish: EventEmitter<Parish> = new EventEmitter<Parish>();

  dataSource: MatTableDataSource<Parish> = new MatTableDataSource<Parish>();
  displayedColumns = ['name', 'address', 'options'];

  addDialogTitle$: Observable<string>;
  editDialogTitle$: Observable<string>;

  constructor(public dialog: MatDialog, private translate: TranslateService) {
    this.addDialogTitle$ = this.translate.get('parish.dialog.add');
    this.editDialogTitle$ = this.translate.get('parish.dialog.edit');
  }

  @Input()
  set parishes(parishes: Parish[]) {
    this.dataSource.data = parishes;
  }

  openAddEditParishDialog(parish: Parish = {}): void {
    this.dialog
      .open(ParishAddEditDialogComponent, {
        data: {
          dialogTitle: parish.id ? this.editDialogTitle$ : this.addDialogTitle$,
          formData: parish,
        },
        disableClose: true,
      })
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          if (result.id) {
            this.editParish.emit(result);
          } else {
            this.addParish.emit(result);
          }
        }
      });
  }

  openConfirmDeleteAlert(id: number): void {
    this.dialog
      .open(DeleteConfirmAlertDialog, { disableClose: false })
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.deleteParish.emit(id);
        }
      });
  }
}
