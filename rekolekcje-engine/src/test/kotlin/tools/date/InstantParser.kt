package pl.oaza.waw.rekolekcje.api.tools.date

import java.time.Instant

fun String.toInstant(): Instant = Instant.parse(this)