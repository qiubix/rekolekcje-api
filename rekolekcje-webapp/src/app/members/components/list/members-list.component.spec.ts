import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MembersListComponent } from './members-list.component';
import { membersTestingModule } from '../../members-testing.module';

describe('MembersListComponent', () => {
  let component: MembersListComponent;
  let fixture: ComponentFixture<MembersListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule(membersTestingModule).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MembersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  //TODO: fix after resolving problems with pagination
  // it('should be created', () => {
  //   expect(component).toBeTruthy();
  // });

  /*
   * TODO: Implement tests for changing state. This might also be done in members.component.spec.ts.
   * See https://github.com/ngrx/platform/blob/master/docs/store/testing.md for details on testing
   */
});
