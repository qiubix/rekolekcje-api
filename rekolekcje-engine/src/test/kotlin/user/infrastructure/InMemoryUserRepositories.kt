package pl.oaza.waw.rekolekcje.api.user.infrastructure

import pl.oaza.waw.rekolekcje.api.user.domain.User
import pl.oaza.waw.rekolekcje.api.user.domain.UserReadRepository
import pl.oaza.waw.rekolekcje.api.user.domain.UserWriteRepository
import java.util.concurrent.ConcurrentHashMap

internal class InMemoryUserWriteRepository: UserWriteRepository {

  val storage = ConcurrentHashMap<Long, User>()

  private var nextId = 1L

  override fun save(entity: User): User {
    val userId = entity.id
    return if (userId != null && storage.containsKey(userId)) {
      storage[userId] = entity
      entity
    } else {
      val newEntity = entity.copy(id = nextId)
      storage[nextId] = newEntity
      nextId++
      newEntity
    }
  }

  override fun deleteById(id: Long) {
    storage.remove(id)
  }

}

internal class InMemoryUserReadRepository(private val storage: Map<Long, User>): UserReadRepository {

  override fun findByUsername(username: String): User? {
    return storage.values.firstOrNull { it.username == username }
  }

  override fun findAll(): List<User> {
    return storage.values.map { it }
  }

}
