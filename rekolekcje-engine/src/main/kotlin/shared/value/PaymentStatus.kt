package pl.oaza.waw.rekolekcje.api.shared.value

enum class PaymentStatus {
  NOTHING,
  DOWN_PAYMENT,
  FULL
}