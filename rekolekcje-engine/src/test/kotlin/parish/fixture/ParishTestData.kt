package pl.oaza.waw.rekolekcje.api.parish.fixture

import pl.oaza.waw.rekolekcje.api.parish.dto.ParishPayload

fun buildParish(name: String, address: String) = ParishPayload(name = name, address = address)
