package pl.oaza.waw.rekolekcje.api.members.domain


internal open class MembersCommandsFacadeImpl internal constructor(
  private val memberReadRepository: MemberReadRepository,
  private val memberWriteRepository: MemberWriteRepository
) : MembersCommandFacade {

  override fun save(member: Member): Member {
    return memberWriteRepository.save(member)
  }

  override fun delete(id: Long) {
    memberWriteRepository.deleteById(id)
  }

  override fun saveAll(members: List<Member>): List<Member> {
    return members
      .map { save(it) }
  }

}
