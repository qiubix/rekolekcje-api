import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GroupErrorStateMatcher } from '../group-error-state-matcher';
import { ChangePasswordRequest } from '../../../model/change-password-request.model';
import { SaveUserRequest } from '../../../model/save-user-request.model';

@Component({
  selector: 'reko-change-password-form',
  templateUrl: './change-password-form.component.html',
  styleUrls: ['./change-password-form.component.scss']
})
export class ChangePasswordFormComponent implements OnInit {
  @Input() username: string;

  @Output() formOutput: EventEmitter<ChangePasswordRequest> = new EventEmitter();

  public form: FormGroup;
  public passwordFormGroup: FormGroup;

  public groupStateMatcher: GroupErrorStateMatcher = new GroupErrorStateMatcher();

  constructor(private readonly fb: FormBuilder) {}

  ngOnInit() {
    this.passwordFormGroup = this.fb.group(
      {
        newPassword: this.fb.control('', Validators.required),
        confirmPassword: this.fb.control('')
      },
      {
        validators: this.checkPasswords
      }
    );
    this.form = this.fb.group({
      oldPassword: this.fb.control(''),
      newPassword: this.passwordFormGroup
    });
  }

  private checkPasswords(group: FormGroup): any | null {
    const pass = group.get('newPassword').value;
    const confirmPass = group.get('confirmPassword').value;

    return pass === confirmPass ? null : { notSame: true };
  }

  cancel(): void {
    this.formOutput.emit();
  }

  submit(): void {
    if (this.form.invalid) {
      this.form.markAsDirty();
      return;
    }

    const changePasswordRequest: ChangePasswordRequest = {
      username: this.username,
      oldPassword: this.form.get('oldPassword').value,
      newPassword: this.passwordFormGroup.get('newPassword').value,
      admin: true
    };

    this.formOutput.emit(changePasswordRequest);
  }
}
