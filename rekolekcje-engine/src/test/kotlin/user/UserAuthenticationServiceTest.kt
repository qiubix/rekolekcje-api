package pl.oaza.waw.rekolekcje.api.user

import io.mockk.every
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.Test
import org.springframework.security.authentication.BadCredentialsException
import pl.oaza.waw.rekolekcje.api.user.domain.UserNotFoundException
import pl.oaza.waw.rekolekcje.api.user.endpoint.AuthenticationRequest
import pl.oaza.waw.rekolekcje.api.user.endpoint.SignUpRequest
import pl.oaza.waw.rekolekcje.api.user.infrastructure.UserTestFixture

class UserAuthenticationServiceTest : UserTestFixture() {

  @Test
  fun `should authenticate existing user`() {
    // given
    userRegisterService.registerUser(signUpRequest)

    // when
    val userSecurityDetails = userAuthenticationService.authenticate(AuthenticationRequest(username, password))

    // then
    assertThat(userSecurityDetails.username).isEqualTo(username)
  }

  @Test
  fun `should reject user with incorrect password`() {
    // given
    val incorrectPassword = "incorrect-pass"
    every { passwordEncoderMock.matches(incorrectPassword, any()) } returns false
    userRegisterService.registerUser(signUpRequest)

    // when & then
    assertThatExceptionOfType(BadCredentialsException::class.java)
      .isThrownBy { userAuthenticationService.authenticate(AuthenticationRequest(username, incorrectPassword)) }
  }

  @Test
  fun `should reject disabled user with correct password`() {
    // given
    userRegisterService.registerUser(signUpRequest)
    userUpdateService.disableUser(username)

    // when & then
    assertThatExceptionOfType(BadCredentialsException::class.java)
      .isThrownBy { userAuthenticationService.authenticate(AuthenticationRequest(username, password)) }
  }

  @Test
  fun `should reject non-existing user`() {
    assertThatExceptionOfType(BadCredentialsException::class.java)
      .isThrownBy { userAuthenticationService.authenticate(AuthenticationRequest("non-existing-user", password)) }
  }
}
