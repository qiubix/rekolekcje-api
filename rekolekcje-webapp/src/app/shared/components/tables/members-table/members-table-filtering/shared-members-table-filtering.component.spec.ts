import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedMembersTableFilteringComponent } from './shared-members-table-filtering.component';

describe('SharedMembersTableFilteringComponent', () => {
  let component: SharedMembersTableFilteringComponent;
  let fixture: ComponentFixture<SharedMembersTableFilteringComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SharedMembersTableFilteringComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedMembersTableFilteringComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
