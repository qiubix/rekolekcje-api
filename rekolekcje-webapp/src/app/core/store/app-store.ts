import { Members } from '../../members/state/members-reducer';
import { Parishes } from '../../diocese/parish/store/parish-reducer';
import { Retreats } from '../../retreats/store/retreat-reducer';
import { Communities } from '../../diocese/community/store/community-reducer';
import { Users } from '../../users/store/users-reduces';
import { Verification } from '../../verification/state/verification-reducer';

export namespace App {
  export interface State {
    membersModule: Members.State;
    verificationModule: Verification.State;
    parishModule: Parishes.State;
    retreatsModule: Retreats.State;
    communitiesModule: Communities.State;
    usersModule: Users.State;
  }
}

export namespace AppReducer {
  export function reducer(state: App.State, action): App.State {
    switch (action.type) {
      default:
        return { ...state };
    }
  }
}
