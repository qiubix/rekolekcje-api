package pl.oaza.waw.rekolekcje.api.user.infrastructure

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder
import pl.oaza.waw.rekolekcje.api.user.domain.UserAuthenticationService
import pl.oaza.waw.rekolekcje.api.user.domain.UserReadRepository
import pl.oaza.waw.rekolekcje.api.user.domain.UserRegisterService
import pl.oaza.waw.rekolekcje.api.user.domain.UserUpdateService
import pl.oaza.waw.rekolekcje.api.user.domain.UserWriteRepository
import pl.oaza.waw.rekolekcje.api.user.query.UserQueryService

@Configuration
class UserConfiguration {

  @Bean
  fun userRegisterService(
    userReadRepository: UserReadRepository,
    userWriteRepository: UserWriteRepository,
    passwordEncoder: PasswordEncoder
  ): UserRegisterService = UserRegisterService(userReadRepository, userWriteRepository, passwordEncoder)

  @Bean
  fun userUpdateService(
    userReadRepository: UserReadRepository,
    userWriteRepository: UserWriteRepository,
    passwordEncoder: PasswordEncoder
  ): UserUpdateService = UserUpdateService(userReadRepository, userWriteRepository, passwordEncoder)

  @Bean
  fun userAuthenticationService(
    userReadRepository: UserReadRepository,
    passwordEncoder: PasswordEncoder
  ): UserAuthenticationService = UserAuthenticationService(userReadRepository, passwordEncoder)

  @Bean
  fun userQueryService(userReadRepository: UserReadRepository): UserQueryService = UserQueryService(userReadRepository)

  @Bean
  fun passwordEncoder(): PasswordEncoder = BCryptPasswordEncoder()

}
