package pl.oaza.waw.rekolekcje.api.notification.endpoint.events

import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationListener
import org.springframework.stereotype.Component
import pl.oaza.waw.rekolekcje.api.application.endpoint.events.ApplicationVerifiedEvent
import pl.oaza.waw.rekolekcje.api.members.query.MembersQueryFacade
import pl.oaza.waw.rekolekcje.api.notification.CreateNotificationRequest
import pl.oaza.waw.rekolekcje.api.notification.domain.NotificationCommandFacade
import pl.oaza.waw.rekolekcje.api.notification.dto.NotificationReason

@Component
internal class ApplicationVerifiedNotificationEventListener(
  private val membersQueryFacade: MembersQueryFacade,
  private val notificationCommandFacade: NotificationCommandFacade
) : ApplicationListener<ApplicationVerifiedEvent> {

  private val log = LoggerFactory.getLogger(this.javaClass)

  override fun onApplicationEvent(event: ApplicationVerifiedEvent) {
    val email = membersQueryFacade.getEmail(event.memberId)
    email?.let {
      notificationCommandFacade.create(CreateNotificationRequest(
        applicationId = event.application.id,
        reason = NotificationReason.APPLICATION_VERIFIED,
        targetEmail = it
      ))
    } ?: log.error("Member ${event.memberId} has no email, not sending verification email")
  }
}