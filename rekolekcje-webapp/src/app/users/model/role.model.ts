export enum UserRole {
  ROLE_ADMIN = 'Admin',
  ROLE_DOR = 'Dor',
  ROLE_MODERATOR = 'Moderator',
  ROLE_USER = 'User'
}
