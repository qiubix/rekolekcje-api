FROM amazoncorretto:17 as build
WORKDIR /app

# Add gradle files
#COPY --chown=gradle:gradle build.gradle.kts .
COPY build.gradle.kts .
COPY gradle.properties .
COPY settings.gradle.kts .
COPY gradlew .
RUN chmod +x gradlew
COPY gradle gradle

# Add modules
RUN mkdir build-logic
COPY build-logic/build.gradle.kts build-logic/build.gradle.kts
COPY build-logic/settings.gradle.kts build-logic/settings.gradle.kts
COPY build-logic/src build-logic/src

RUN mkdir rekolekcje-engine
COPY rekolekcje-engine/gradle rekolekcje-engine/gradle
COPY rekolekcje-engine/build.gradle.kts rekolekcje-engine/build.gradle.kts
COPY rekolekcje-engine/settings.gradle.kts rekolekcje-engine/settings.gradle.kts
COPY rekolekcje-engine/src rekolekcje-engine/src

RUN mkdir rekolekcje-webapp
COPY rekolekcje-webapp/build.gradle.kts rekolekcje-webapp/build.gradle.kts
COPY rekolekcje-webapp/settings.gradle.kts rekolekcje-webapp/settings.gradle.kts
COPY rekolekcje-webapp/angular.json rekolekcje-webapp/angular.json
COPY rekolekcje-webapp/karma.conf.js rekolekcje-webapp/karma.conf.js
COPY rekolekcje-webapp/package.json rekolekcje-webapp/package.json
COPY rekolekcje-webapp/package-lock.json rekolekcje-webapp/package-lock.json
COPY rekolekcje-webapp/proxy.conf.json rekolekcje-webapp/proxy.conf.json
COPY rekolekcje-webapp/protractor.conf.js rekolekcje-webapp/protractor.conf.js
COPY rekolekcje-webapp/tslint.json rekolekcje-webapp/tslint.json
COPY rekolekcje-webapp/tsconfig.json rekolekcje-webapp/tsconfig.json
COPY rekolekcje-webapp/src rekolekcje-webapp/src
COPY rekolekcje-webapp/e2e rekolekcje-webapp/e2e

RUN yum install -y tar bzip2
RUN ./gradlew bootJar -x check --stacktrace --no-daemon -i
RUN mkdir -p rekolekcje-engine/build/dependency && (cd rekolekcje-engine/build/dependency; jar -xf ../libs/*.jar)
#RUN ls -R build/dependency

FROM openjdk:17-jdk-slim

VOLUME /tmp

ARG DEPENDENCY=/app/rekolekcje-engine/build/dependency
COPY --from=build ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY --from=build ${DEPENDENCY}/META-INF /app/META-INF
COPY --from=build ${DEPENDENCY}/BOOT-INF/classes /app
ENTRYPOINT ["java","-cp","app:app/lib/*","pl.oaza.waw.rekolekcje.api.ApiApplicationKt"]
