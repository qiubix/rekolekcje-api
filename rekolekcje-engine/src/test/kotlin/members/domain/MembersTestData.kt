package pl.oaza.waw.rekolekcje.api.members.domain

import pl.oaza.waw.rekolekcje.api.members.domain.PersonalData.SchoolType
import pl.oaza.waw.rekolekcje.api.shared.domain.Contact
import pl.oaza.waw.rekolekcje.api.shared.value.Stage
import java.time.LocalDate

fun minimalMember() = Member(
  personalData = PersonalData(
    contactInfo = MemberContact(
      firstName = "Minimal",
      lastName = "Member"
    ),
    pesel = Pesel("92042312348")
  )
)

fun sampleMember(
  firstName: String = "Sample",
  lastName: String = "Participant",
  pesel: String = "98101012349",
  email: String? = null
) = Member(
  personalData = PersonalData(
    contactInfo = MemberContact(
      firstName = firstName,
      lastName = lastName,
      email = email
    ),
    pesel = Pesel(pesel)
  )
)

fun fullMember() = Member(
  personalData = PersonalData(
    contactInfo = MemberContact(
      firstName = "Full",
      lastName = "Member",
      phoneNumber = "111-222-333",
      email = "full.member@mail.com"
    ),
    pesel = Pesel("90042300004"),
    address = Address(
      street = "Broadway",
      streetNumber = "987/989",
      flatNumber = "13c",
      postalCode = "12-654",
      city = "New York"
    ),
    parishId = 1L,
    communityId = 2L,
    mother = Contact(fullName = "Mary Smith"),
    father = Contact(fullName = "Jake Smith Sr."),
    placeOfBirth = "Los Angeles",
    christeningDate = LocalDate.of(1981, 2, 13),
    emergencyContact = Contact(
      fullName = "Uncle Bob",
      phoneNumber = "444555666",
      email = "fake@dev.io"
    ),
    schoolYear = "1 LO",
    schoolType = SchoolType.HIGH_SCHOOL,
    nameDay = "24.04"
  ),
  healthReport = HealthReport(
    currentTreatment = "Standard treatment for diabetes",
    medications = "Insuline",
    allergies = "Peanuts, lactose",
    cannotHike = true,
    hasMotionSickness = true,
    illnessHistory = "Hib implant injected",
    mentalDisorders = "None",
    other = "May be very weird sometimes"
  ),
  experience = Experience(
    kwc = Kwc(
      kwcStatus = KwcStatus.CANDIDATE,
      kwcSince = LocalDate.of(1995, 5, 4)
    ),
    numberOfCommunionDays = 3,
    numberOfPrayerRetreats = 5,
    leadingGroupToFormationStage = "ONŻ I",
    formationInSmallGroup = true,
    animator = Contact(fullName = "Jason Anime"),
    formationMeetingsInMonth = 3,
    deuterocatechumenate = Deuterocatechumenate(
      celebrationsTaken = 5,
      celebrationsPlannedThisYear = 5,
      stepsTaken = 4,
      stepsPlannedThisYear = 6,
      year = 2016,
      missionCelebrationYear = 2016,
      callByNameCelebrationYear = 2017,
      holyTriduumYear = 2017
    ),
    numberOfODB = 1,
    numberOfOND = 0,
    numberOfRetreatsWithParents = 3,
    choir = false,
    musicalTalents = "none so far",
    altarBoy = true,
    flowerGirl = false,
    wasAnimatorBefore = false,
    animatorsSchool = false,
    animatorsBlessing = false,
    koda = false,
    numberOfAnimatorDays = 0,
    diakonia = "DOR",
    historicalRetreats = listOf(
      HistoricalRetreat(id = 5, stage = Stage.ODB, year = "2010"),
      HistoricalRetreat(id = 7, stage = Stage.OND, year = "2011")
    ),
    otherSkills = "SINGS,DRIVERS_LICENCE",
    animatorsExperience = "ODB: 1,OND: 2",
    otherRetreats = "KODA,ORAE",
    communityFunctions = "Odpowiedzialny"
  )
)
