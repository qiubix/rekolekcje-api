package pl.oaza.waw.rekolekcje.api.user.domain

import java.lang.RuntimeException


interface UserReadRepository {

  fun findByUsername(username: String): User?

  fun findAll(): List<User>

}

interface UserWriteRepository {

  fun save(entity: User): User

  fun deleteById(id: Long)

}

class UserNotFoundException(username: String): RuntimeException("User with username $username not found.")

class UserAlreadyExistsException(username: String): RuntimeException("User with username $username already exists.")
