package pl.oaza.waw.rekolekcje.api.parish.api

import org.assertj.core.api.Assertions.assertThat
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import pl.oaza.waw.rekolekcje.api.parish.PARISH_API_URI
import pl.oaza.waw.rekolekcje.api.parish.dto.ParishPayload
import pl.oaza.waw.rekolekcje.api.shared.BaseApiExpectations
import pl.oaza.waw.rekolekcje.api.shared.Response
import pl.oaza.waw.rekolekcje.api.shared.RestApi

class ParishApiExpectations(restApi: RestApi) : BaseApiExpectations(restApi) {

  private val baseUri = PARISH_API_URI

  private val all: List<ParishPayload>
    get() = restApi.performGet(baseUri).getContentAsList()

  fun responseHasExactPayload(response: Response, expectedPayload: ParishPayload) {
    val expectedJsonContent = restApi.jsonMapper.writeValueAsString(expectedPayload)
    response
      .andExpect(status().is2xxSuccessful)
      .andExpect(content().json(expectedJsonContent))
  }

  fun responseHasCorrectDataIgnoringId(response: Response, expectedPayload: ParishPayload) {
    response.andExpect(status().is2xxSuccessful)
    val responseContent = response.getContent<ParishPayload>()
    assertThat(responseContent)
      .isEqualToIgnoringGivenFields(expectedPayload, "id")
  }

  fun responseHasCorrectDataIgnoringId(response: Response, expectedPayload: List<ParishPayload>) {
    response.andExpect(status().is2xxSuccessful)
    val returnedPayloadAsList = response.getContentAsList<ParishPayload>()
    assertThat(returnedPayloadAsList)
      .usingRecursiveFieldByFieldElementComparator()
      .usingElementComparatorIgnoringFields("id")
      .containsAll(expectedPayload)
  }

  fun exists(payload: ParishPayload) {
    assertThat(all).contains(payload)
  }

  fun doesNotExist(payload: ParishPayload) {
    assertThat(all).doesNotContain(payload)
  }

  fun thereIsAnExactNumberOfEntities(size: Int) {
    assertThat(all.size).isEqualTo(size)
  }
}
