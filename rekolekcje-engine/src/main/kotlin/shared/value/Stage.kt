package pl.oaza.waw.rekolekcje.api.shared.value

import com.fasterxml.jackson.annotation.JsonCreator

enum class Stage(val description: String) {

  // Oaza Dzieci Bozych
  ODB("ODB"),

  // Oaza Nowej Drogi
  OND("OND"),

  // The New Life Oasis Retreat
  ONZ_I("ONŻ I"),
  ONZ_II("ONŻ II"),
  ONZ_III("ONŻ III"),

  // The Family Oasis Retreat
  OR_I("OR I"),
  OR_II("OR II"),
  OR_III("OR III"),

  ORAE("ORAE"),
  ORD("ORD"),
  Triduum("Triduum"),

  KODA("KODA");

  companion object {

    @JvmStatic
    @JsonCreator
    fun fromValue(text: String?): Stage? {
      return if (text == null || text.isEmpty()) {
        null
      } else valueOf(text)
    }
  }
}
