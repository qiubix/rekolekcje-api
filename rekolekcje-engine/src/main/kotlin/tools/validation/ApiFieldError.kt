package pl.oaza.waw.rekolekcje.api.tools.validation

internal data class ApiFieldError(
  val field: String? = null,
  val code: String? = null,
  val message: String? = null,
  val rejectedObject: Any? = null
)
