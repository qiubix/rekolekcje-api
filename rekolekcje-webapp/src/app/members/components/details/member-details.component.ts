import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DeleteConfirmAlertDialog } from '@shared/components';
import { select, Store } from '@ngrx/store';
import { Members } from '../../state/members-reducer';
import { MembersSharedActions } from '../../state/members-actions';
import { Observable, Subject } from 'rxjs';
import { AppSelectors } from '@core/store/app-selectors';
import { Parishes } from '@diocese/parish/store/parish-reducer';
import { Location } from '@angular/common';
import { Communities } from '@diocese/community/store/community-reducer';
import { takeUntil } from 'rxjs/operators';
import { MemberDetails, OpinionDetails } from '@shared/models';
import { Retreats } from 'app/retreats/store/retreat-reducer';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'member-details',
  templateUrl: './member-details.component.html',
  styleUrls: ['./member-details.component.scss'],
})
export class MemberDetailsComponent implements OnInit, OnDestroy {
  member: MemberDetails;

  member$: Observable<MemberDetails>;

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(
    private dialog: MatDialog,
    private router: Router,
    private membersStore: Store<Members.State>,
    private parishStore: Store<Parishes.State>,
    private communityStore: Store<Communities.State>,
    private retreatStore: Store<Retreats.State>,
    private translate: TranslateService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.member$ = this.membersStore.pipe(
      select(AppSelectors.getSelectedMember),
      takeUntil(this.ngUnsubscribe)
    );
    this.member$.subscribe((member: MemberDetails) => (this.member = member));
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  delete(): void {
    this.dialog
      .open(DeleteConfirmAlertDialog, { disableClose: false })
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.router.navigate(['members']);
          this.membersStore.dispatch(new MembersSharedActions.DeleteMember(this.member.id));
        }
      });
  }

  edit(): void {
    this.router.navigate(['members', `${this.member.id}`, 'edit']);
  }

  goBack(): void {
    this.location.back();
  }

  get opinions(): OpinionDetails[] {
    return [
      {
        id: 12,
        submitter: { fullName: 'Alex First' },
        created: '2009-09-01',
        content:
          'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vitae nunc fringilla, dapibus lorem non, varius ante. Aliquam nibh ipsum, condimentum ac justo ac, aliquet luctus ipsum. Praesent pharetra ante quis lorem posuere, in tristique felis luctus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed ornare urna sed elit convallis ornare nec aliquam ipsum. Nunc massa nulla, imperdiet eget enim eu, molestie ultrices odio. Vestibulum ullamcorper nibh at libero ultricies tempor. Nullam faucibus metus at turpis posuere euismod. Integer sodales tristique nunc vel vehicula. Nunc varius dapibus nibh eu ullamcorper. In leo ligula, tincidunt nec nunc quis, varius aliquam odio. Phasellus tincidunt leo vel cursus lacinia. ',
      },
      {
        id: 13,
        submitter: { fullName: 'Ben Second' },
        created: '2015-05-01',
        content:
          ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vitae nunc fringilla, dapibus lorem non, varius ante. Aliquam nibh ipsum, condimentum ac justo ac, aliquet luctus ipsum. Praesent pharetra ante quis lorem posuere, in tristique felis luctus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed ornare urna sed elit convallis ornare nec aliquam ipsum. Nunc massa nulla, imperdiet eget enim eu, molestie ultrices odio. Vestibulum ullamcorper nibh at libero ultricies tempor. Nullam faucibus metus at turpis posuere euismod. Integer sodales tristique nunc vel vehicula. Nunc varius dapibus nibh eu ullamcorper. In leo ligula, tincidunt nec nunc quis, varius aliquam odio. Phasellus tincidunt leo vel cursus lacinia.\n' +
          '\n' +
          'Mauris venenatis at augue nec vestibulum. Vivamus at semper sapien, vitae imperdiet elit. Curabitur dignissim euismod sem, eu maximus sapien tempus non. Proin erat nunc, egestas at massa et, dapibus convallis est. Donec ligula magna, malesuada ut elementum nec, vehicula commodo lectus. Ut ligula metus, luctus sit amet faucibus vel, accumsan a dui. Curabitur eu laoreet felis. Suspendisse lorem nibh, pharetra eu molestie sed, suscipit a nibh. Praesent feugiat, magna at tincidunt porta, lectus ex commodo ipsum, nec tempor elit mi sit amet purus.\n' +
          '\n' +
          'Etiam ligula dolor, venenatis in pellentesque pulvinar, placerat eu lorem. Cras sit amet tempus est, in scelerisque nunc. Maecenas sem nunc, sagittis ut erat ut, efficitur molestie metus. Integer tempor eleifend vehicula. Integer in rhoncus est. Cras cursus massa ut arcu interdum pharetra. Integer quis mollis neque, sed dapibus sapien. Quisque vel metus vitae justo varius cursus. Suspendisse volutpat lorem vitae aliquam consectetur.\n' +
          '\n' +
          'Fusce dapibus tortor eu orci bibendum, sed volutpat justo iaculis. Aliquam enim leo, accumsan nec elit non, convallis gravida magna. Aenean a tempus augue. Etiam eu lorem ac metus rhoncus tincidunt nec vel urna. Suspendisse at risus est. Curabitur pretium ultricies posuere. Aliquam mollis ex eget libero eleifend pharetra. Morbi sit amet leo at quam ornare malesuada. Phasellus eget maximus diam, ac feugiat est. Ut consectetur felis ut facilisis tempor. Nunc sed posuere purus. Nulla non dui ac arcu sagittis auctor. Aenean at gravida diam, ac blandit turpis. Vivamus sit amet purus feugiat, gravida elit non, egestas nulla. Donec non pharetra ligula, id interdum odio. Morbi sit amet placerat eros, sit amet placerat leo. ',
      },
      {
        id: 14,
        submitter: { fullName: 'Chris Third' },
        created: '2019-03-01',
        content:
          ' Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vitae nunc fringilla, dapibus lorem non, varius ante. Aliquam nibh ipsum, condimentum ac justo ac, aliquet luctus ipsum. Praesent pharetra ante quis lorem posuere, in tristique felis luctus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed ornare urna sed elit convallis ornare nec aliquam ipsum. Nunc massa nulla, imperdiet eget enim eu, molestie ultrices odio. Vestibulum ullamcorper nibh at libero ultricies tempor. Nullam faucibus metus at turpis posuere euismod. Integer sodales tristique nunc vel vehicula. Nunc varius dapibus nibh eu ullamcorper. In leo ligula, tincidunt nec nunc quis, varius aliquam odio. Phasellus tincidunt leo vel cursus lacinia.\n' +
          '\n' +
          'Mauris venenatis at augue nec vestibulum. Vivamus at semper sapien, vitae imperdiet elit. Curabitur dignissim euismod sem, eu maximus sapien tempus non. Proin erat nunc, egestas at massa et, dapibus convallis est. Donec ligula magna, malesuada ut elementum nec, vehicula commodo lectus. Ut ligula metus, luctus sit amet faucibus vel, accumsan a dui. Curabitur eu laoreet felis. Suspendisse lorem nibh, pharetra eu molestie sed, suscipit a nibh. Praesent feugiat, magna at tincidunt porta, lectus ex commodo ipsum, nec tempor elit mi sit amet purus. ',
      },
    ];
  }
}
