package pl.oaza.waw.rekolekcje.api.application.endpoint

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pl.oaza.waw.rekolekcje.api.application.query.ApplicationQueryFacade
import pl.oaza.waw.rekolekcje.api.members.endpoint.MembersFetchService

@Transactional
@Service
internal class ApplicationFetchService(
  private val applicationQueryFacade: ApplicationQueryFacade,
  private val membersFetchService: MembersFetchService
) {

  fun getSingleApplicationDetails(id: Long): RetreatApplicationDto {
    val foundApplication = applicationQueryFacade.findApplicationById(id)
      ?: throw ApplicationNotFoundException("No application with id $id found")
    val member = membersFetchService.getSingleMemberDetails(foundApplication.memberId)
    return foundApplication.toDto(member)
  }

  fun getAllApplications(): List<RetreatApplicationDto> {
    return applicationQueryFacade.findAllApplications().map {
      val member = membersFetchService.getSingleMemberDetails(it.memberId)
      it.toDto(member)
    }
  }

  fun getAllAnimatorApplications(): List<RetreatApplicationDto> {
    return applicationQueryFacade.findAnimatorApplications().map {
      val member = membersFetchService.getSingleMemberDetails(it.memberId)
      it.toDto(member)
    }
  }

  fun getAllParticipantApplications(): List<RetreatApplicationDto> {
    return applicationQueryFacade.findParticipantApplications().map {
      val member = membersFetchService.getSingleMemberDetails(it.memberId)
      it.toDto(member)
    }
  }

  fun getAllReadyApplications(): List<ApplicationSummary> =
    applicationQueryFacade.findReadyApplications().map { it.toSummary() }

  fun getAllPendingApplications(): List<ApplicationSummary> =
    applicationQueryFacade.findPendingApplications().map { it.toSummary() }

  fun getVerificationStatistics(): VerificationStatisticsDto =
    VerificationStatisticsDto(applicationQueryFacade.getVerificationStatistics())
}