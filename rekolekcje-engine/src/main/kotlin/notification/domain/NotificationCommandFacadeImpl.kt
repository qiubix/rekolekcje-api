package pl.oaza.waw.rekolekcje.api.notification.domain

import pl.oaza.waw.rekolekcje.api.notification.CreateNotificationRequest
import pl.oaza.waw.rekolekcje.api.notification.dto.NotificationSummary

internal class NotificationCommandFacadeImpl internal constructor(
    private val notificationRepository: NotificationRepository
) : NotificationCommandFacade {

  private val notificationMapper: NotificationMapper = NotificationMapper()

  override fun create(request: CreateNotificationRequest): NotificationSummary {
    val notification = notificationMapper.toNotification(request)
    return notificationRepository.save(notification).summary()
  }

  override fun deleteByApplication(applicationId: Long) {
    notificationRepository.deleteByApplicationId(applicationId)
  }
}