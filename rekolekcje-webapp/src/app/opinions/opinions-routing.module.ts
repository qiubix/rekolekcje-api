import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
  OpinionsComponent,
  OpinionSubmissionPageComponent,
  OpinionThankYouPageComponent
} from '@opinions/components';
import { AuthGuard } from '@auth/guards/auth-guard.service';
import { HasCorrectRoleGuard } from '@auth/guards/has-correct-role.guard';

const routes: Routes = [
  {
    path: '',
    component: OpinionsComponent,
    canActivate: [AuthGuard, HasCorrectRoleGuard]
  },
  {
    path: 'thanks',
    component: OpinionThankYouPageComponent
  },
  {
    path: ':encryptedMemberId',
    component: OpinionSubmissionPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OpinionsRoutingModule {}
