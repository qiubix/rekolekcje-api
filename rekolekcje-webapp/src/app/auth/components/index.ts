import { CorrectAuthRolesDirective } from './directives/correct-auth-roles.directive';
import { LoginComponent } from './login/login.component';

export const components: any[] = [CorrectAuthRolesDirective, LoginComponent];

export * from './directives/correct-auth-roles.directive';
