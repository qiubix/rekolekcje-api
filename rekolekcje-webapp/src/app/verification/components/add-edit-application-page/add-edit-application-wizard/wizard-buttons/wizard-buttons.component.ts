import { Component, Input } from '@angular/core';
import { MatStepper } from '@angular/material/stepper';

@Component({
  selector: 'reko-wizard-buttons',
  templateUrl: './wizard-buttons.component.html',
  styleUrls: ['./wizard-buttons.component.scss'],
})
export class WizardButtonsComponent {
  @Input() stepper: MatStepper;
  @Input() hasPrevious: boolean = true;
  @Input() hasNext: boolean = true;
  @Input() hasReset: boolean = false;
}
