package pl.oaza.waw.rekolekcje.api.retreats.api

import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.Matchers.`is`
import org.hamcrest.Matchers.notNullValue
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import pl.oaza.waw.rekolekcje.api.retreats.endpoint.RetreatDetails
import pl.oaza.waw.rekolekcje.api.retreats.endpoint.RetreatSummary
import pl.oaza.waw.rekolekcje.api.shared.BaseApiExpectations
import pl.oaza.waw.rekolekcje.api.shared.Response
import pl.oaza.waw.rekolekcje.api.shared.RestApi

class RetreatsApiExpectations(restApi: RestApi, private val baseUri: String )
  : BaseApiExpectations(restApi) {

  private val all: List<RetreatSummary>
    get() = restApi.performGet(baseUri).getContentAsList()

  fun responseHasCorrectDataIgnoringId(response: Response, expectedPayload: RetreatSummary) {
    response.andExpect(status().is2xxSuccessful)
    val responseContent = response.getContent<RetreatSummary>()
    assertThat(responseContent)
      .isEqualToIgnoringGivenFields(expectedPayload, "id")
  }

  fun responseHasCorrectDataIgnoringId(response: Response, expectedPayload: List<RetreatSummary>) {
    response.andExpect(status().is2xxSuccessful)
    val returnedPayloadAsList = response.getContentAsList<RetreatSummary>()
    assertThat(returnedPayloadAsList)
      .usingRecursiveFieldByFieldElementComparator()
      .usingElementComparatorIgnoringFields("id")
      .containsAll(expectedPayload)
  }

  fun exists(payload: RetreatSummary) {
    assertThat(all).contains(payload)
  }

  fun summariesAre(expected: List<RetreatSummary>) {
    assertThat(all).isEqualTo(expected)
  }

  fun exists(details: RetreatDetails) {
    assertThat(details.id).isNotNull()
    val response = restApi.performGet("$baseUri/${details.id}")
    val actualDetails = response.getContent<RetreatDetails>()
    assertThat(actualDetails).isEqualTo(details)
  }

  fun doesNotExist(payload: RetreatSummary) {
    assertThat(all).doesNotContain(payload)
  }

  fun thereIsAnExactNumberOfEntities(size: Int) {
    assertThat(all.size).isEqualTo(size)
  }

  fun responseHasCorrectJsonValues(response: Response, payload: RetreatSummary) {
    response
      .andExpect(jsonPath("$.id", notNullValue()))
      .andExpect(jsonPath("$.stage").value(payload.stage.toString()))
      .andExpect(jsonPath("$.turn").value(payload.turn.toString()))
      .andExpect(jsonPath("$.startDate", `is`(payload.startDate)))
      .andExpect(jsonPath("$.endDate", `is`(payload.endDate)))
      .andExpect(jsonPath("$.localization", `is`(payload.localization)))
      .andExpect(jsonPath("$.status", `is`(payload.status.toString())))
  }

  fun responseHasRetreatSummaryWithCorrectAmountOfParticipants(
    response: Response,
    participantIds: Set<Long>
  ) {
    val retreatSummary = response.getContent<RetreatSummary>()
    assertThat(retreatSummary.participantsAmount).isEqualTo(participantIds.size)
  }

  fun responseHasRetreatDetailsWithCorrectParticipants(
    response: Response,
    expectedParticipantIds: Set<Long>
  ) {

    val actualIds = response
      .getContent<RetreatDetails>()
      .participants

    assertThat(actualIds).isEqualTo(expectedParticipantIds)
  }

  fun responseHasCorrectRetreatDetails(response: Response, expectedPayload: RetreatDetails) {
    val responseContent = response.getContent<RetreatDetails>()
    assertThat(responseContent).isEqualTo(expectedPayload)
  }

  fun responseHasCorrectRetreatIds(response: Response, expectedRetreats: Set<Long>) {
    val responseContent = response.getContentAsList<Long>().toSet()
    assertThat(responseContent).isEqualTo(expectedRetreats)
  }
}
