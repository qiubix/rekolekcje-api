import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MemberStatus } from '@shared/models';
import { StatusWithReason } from '@verification/model';
import { RejectionReasonDialogComponent } from '@verification/components';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'reko-application-status-actions',
  templateUrl: './application-status-actions.component.html',
  styleUrls: ['./application-status-actions.component.scss'],
})
export class ApplicationStatusActionsComponent implements OnInit {
  @Input() applicationStatus: MemberStatus;
  @Output() onChangeStatus = new EventEmitter<StatusWithReason>();

  constructor(private dialog: MatDialog) {}

  ngOnInit() {}

  currentMemberIsWaiting(): boolean {
    return this.applicationStatus === MemberStatus.WAITING;
  }

  currentMemberIsVerified(): boolean {
    return this.applicationStatus === MemberStatus.VERIFIED;
  }

  isProblemWithCurrentMember(): boolean {
    return this.applicationStatus === MemberStatus.PROBLEM;
  }

  currentMemberIsRegistered(): boolean {
    return this.applicationStatus === MemberStatus.REGISTERED;
  }

  currentMemberIsRejected(): boolean {
    return this.applicationStatus === MemberStatus.REJECTED;
  }

  changeStatus(status: string) {
    this.onChangeStatus.emit({ status: status });
  }

  changeStatusWithReason(status: string) {
    this.dialog
      .open(RejectionReasonDialogComponent, { disableClose: false })
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          // console.log('trying to reject with reason: ', result.reason);
          this.onChangeStatus.emit({ status: status, reason: result.reason });
        }
      });
  }
}
