package pl.oaza.waw.rekolekcje.api.security

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.builders.WebSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import pl.oaza.waw.rekolekcje.api.security.jwt.JwtAuthenticationEntryPoint
import pl.oaza.waw.rekolekcje.api.security.jwt.JwtAuthenticationTokenFilter

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
class WebSecurityConfig
/**
 * Create an instance with a default configuration.
 *
 * @param unauthorizedHandler responsible for dealing with unauthorized requests
 * @param jwtAuthenticationTokenFilter filter for handing token-based authentication
 */
@Autowired
constructor(
  private val unauthorizedHandler: JwtAuthenticationEntryPoint,
  private val jwtAuthenticationTokenFilter: JwtAuthenticationTokenFilter
) : WebSecurityConfigurerAdapter() {

  private val logger = LoggerFactory.getLogger(this.javaClass.name)

  private val openApiEndpoints = listOf(
    "/api/auth/**", "/api/users/sign-up", "/api/members/full-name-encrypted/**", "/api/opinions/submit"
  )
  private val staticResources = listOf("/*.html", "/favicon.ico", "/**/*.html", "/**/*.css", "/**/*.js", "/*.css",
    "/assets/**", "/*.js"
  )
  private val devTools = listOf("/v3/api-docs/**", "/swagger-ui/**", "/swagger-ui.html", "actuator/**")
  private val other = listOf("/csrf", "/webjars/**")
  private val uiRouting = listOf(
    "/", "/auth/**", "/members/**", "/home/**", "/retreats/**", "/users/**", "/registration/**", "/verification/**",
    "/parish/**", "/community/**", "/opinions/**"
  )
  private val openEndpoints = openApiEndpoints + staticResources + devTools + other + uiRouting

  override fun configure(httpSecurity: HttpSecurity) {
    logger.info("Configuring http security...")
    httpSecurity
      .csrf().disable()
      .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
      .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
      .authorizeRequests()
      .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
      .antMatchers(*openEndpoints.toTypedArray()).permitAll()
      .anyRequest().authenticated()

    httpSecurity
      .addFilterBefore(
        jwtAuthenticationTokenFilter,
        UsernamePasswordAuthenticationFilter::class.java
      )

    httpSecurity
      .headers()
      .frameOptions().sameOrigin()
      .cacheControl()
  }

  override fun configure(web: WebSecurity) {
    web
      .ignoring()
      .antMatchers(HttpMethod.POST, "/api/auth/**", "/api/opinions/submit").and()
      .ignoring()
      .antMatchers(
        HttpMethod.GET,
        "/",
        "/*.html",
        "/favicon.ico",
        "/**/*.html",
        "/**/*.css",
        "/**/*.js",
        "/*.css",
        "/assets/**",
        "/*.js"
      )

  }
}
