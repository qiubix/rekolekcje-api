package pl.oaza.waw.rekolekcje.api.notification.endpoint.events

import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationListener
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import pl.oaza.waw.rekolekcje.api.application.endpoint.events.ApplicationRejectedEvent
import pl.oaza.waw.rekolekcje.api.application.query.ApplicationQueryFacade
import pl.oaza.waw.rekolekcje.api.members.query.MembersQueryFacade
import pl.oaza.waw.rekolekcje.api.notification.CreateNotificationRequest
import pl.oaza.waw.rekolekcje.api.notification.domain.NotificationCommandFacade
import pl.oaza.waw.rekolekcje.api.notification.dto.NotificationReason

@Component
internal class ApplicationRejectedNotificationEventListener(
  private val notificationCommandFacade: NotificationCommandFacade,
  private val applicationQueryFacade: ApplicationQueryFacade,
  private val membersQueryFacade: MembersQueryFacade
) : ApplicationListener<ApplicationRejectedEvent> {

  private val log = LoggerFactory.getLogger(this.javaClass)

  @Transactional
  override fun onApplicationEvent(event: ApplicationRejectedEvent) {
    val application = applicationQueryFacade.findApplicationById(event.applicationId)
      ?: throw IllegalStateException()
    val email = membersQueryFacade.getEmail(application.memberId)
    email?.let {
      notificationCommandFacade.create(CreateNotificationRequest(
          applicationId = application.getId() ?: throw IllegalStateException(),
          reason = NotificationReason.APPLICATION_REJECTED,
          targetEmail = it,
          additionalInfo = event.reason
      ))
    } ?: log.info("Member ${application.memberId} has no email, not sending rejection email")
  }
}
