package pl.oaza.waw.rekolekcje.api.application.infrastructure.events

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationEventPublisher
import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationNotifier
import pl.oaza.waw.rekolekcje.api.application.endpoint.events.ApplicationRejectedEvent
import pl.oaza.waw.rekolekcje.api.application.endpoint.events.ApplicationSubmittedEvent
import pl.oaza.waw.rekolekcje.api.application.endpoint.events.ApplicationVerifiedEvent
import pl.oaza.waw.rekolekcje.api.application.endpoint.toSummary
import pl.oaza.waw.rekolekcje.api.application.query.ApplicationQueryFacade

class ApplicationNotifierImpl(
  private val eventPublisher: ApplicationEventPublisher,
  private val queryFacade: ApplicationQueryFacade
) : ApplicationNotifier {

  private val logger: Logger = LoggerFactory.getLogger(this.javaClass.name)

  override fun newApplicationSubmitted(applicationId: Long) {
    logger.info("New application submitted: $applicationId")
    eventPublisher.publishEvent(ApplicationSubmittedEvent(applicationId))
  }

  override fun applicationVerified(id: Long) {
    logger.info("Application $id was verified")
    val foundApplication = queryFacade.findApplicationById(id) ?: throw IllegalStateException()
    val applicationVerifiedEvent = ApplicationVerifiedEvent(
      application = foundApplication.toSummary(),
      memberId = foundApplication.memberId
    )
    eventPublisher.publishEvent(applicationVerifiedEvent)
  }

  override fun applicationRejected(id: Long, reason: String) {
    logger.info("Application $id was rejected")
    val applicationRejectedEvent = ApplicationRejectedEvent(
      applicationId = id,
      reason = reason
    )
    eventPublisher.publishEvent(applicationRejectedEvent)
  }
}
