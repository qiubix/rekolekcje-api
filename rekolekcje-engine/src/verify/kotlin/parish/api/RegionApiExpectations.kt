package pl.oaza.waw.rekolekcje.api.parish.api

import org.assertj.core.api.Assertions.assertThat
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import pl.oaza.waw.rekolekcje.api.parish.REGIONS_API_URI
import pl.oaza.waw.rekolekcje.api.parish.dto.RegionPayload
import pl.oaza.waw.rekolekcje.api.shared.BaseApiExpectations
import pl.oaza.waw.rekolekcje.api.shared.Response
import pl.oaza.waw.rekolekcje.api.shared.RestApi

class RegionApiExpectations(restApi: RestApi) : BaseApiExpectations(restApi) {

  private val baseUri = REGIONS_API_URI

  private val all: List<RegionPayload>
    get() = restApi.performGet(baseUri).getContentAsList()

  fun responseHasExactPayload(response: Response, expectedPayload: RegionPayload) {
    val expectedJsonContent = restApi.jsonMapper.writeValueAsString(expectedPayload)
    response
      .andExpect(status().is2xxSuccessful)
      .andExpect(content().json(expectedJsonContent))
  }

  fun responseHasCorrectDataIgnoringId(response: Response, expectedPayload: RegionPayload) {
    response.andExpect(status().is2xxSuccessful)
    val responseContent = response.getContent<RegionPayload>()
    assertThat(responseContent)
      .isEqualToIgnoringGivenFields(expectedPayload, "id")
  }

  fun responseHasCorrectDataIgnoringId(response: Response, expectedPayload: List<RegionPayload>) {
    response.andExpect(status().is2xxSuccessful)
    val returnedPayloadAsList = response.getContentAsList<RegionPayload>()
    assertThat(returnedPayloadAsList)
      .usingRecursiveFieldByFieldElementComparator()
      .usingElementComparatorIgnoringFields("id")
      .containsAll(expectedPayload)
  }

  fun exists(payload: RegionPayload) {
    assertThat(all).contains(payload)
  }

  fun doesNotExist(payload: RegionPayload) {
    assertThat(all).doesNotContain(payload)
  }

  fun thereIsAnExactNumberOfEntities(size: Int) {
    assertThat(all.size).isEqualTo(size)
  }
}
