import { Component, Input, OnInit } from '@angular/core';
import { Talents } from '@shared/models';

@Component({
  selector: 'reko-talent-details',
  templateUrl: './talent-details.component.html',
  styleUrls: ['./talent-details.component.scss']
})
export class TalentDetailsComponent implements OnInit {
  @Input() data: Talents;

  ngOnInit() {}
}
