package pl.oaza.waw.rekolekcje.api.community.domain

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
internal class CommunityConfiguration {

  fun communityFacade(): CommunityFacade {
    return communityFacade(InMemoryCommunityRepository())
  }

  @Bean
  fun communityFacade(communityRepository: CommunityRepository): CommunityFacade {
    return CommunityFacade(communityRepository)
  }
}
