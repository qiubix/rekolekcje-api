import { Component, Input } from '@angular/core';
import { Kwc, KwcStatus } from '@shared/models';

@Component({
  selector: 'reko-kwc-details',
  templateUrl: './kwc-details.component.html',
  styleUrls: ['./kwc-details.component.scss']
})
export class KwcDetailsComponent {
  @Input() data: Kwc;

  kwcStatuses = KwcStatus;

  get kwcStatusValue() {
    return this.data ? this.kwcStatuses[this.data.status] : '';
  }

  get kwcSince() {
    return this.data ? this.data.year : '';
  }
}
