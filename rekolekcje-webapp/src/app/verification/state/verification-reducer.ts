import { VerificationActions } from './verification-actions';
import {
  ApplicationSummary,
  RetreatApplication,
  VerificationStatistics
} from '@verification/model';

export namespace Verification {
  export interface State {
    statistics: VerificationStatistics;
    pendingApplications: ApplicationSummary[];
    verifiedApplications: ApplicationSummary[];
    selectedApplication: RetreatApplication;
  }

  export const initialState: Verification.State = {
    statistics: null,
    pendingApplications: [],
    verifiedApplications: [],
    selectedApplication: null
  };
}

export namespace VerificationReducer {
  export function reducer(
    state: Verification.State = Verification.initialState,
    action
  ): Verification.State {
    switch (action.type) {
      case VerificationActions.types.GetStatisticsSuccess: {
        return {
          ...state,
          statistics: action.payload
        };
      }

      case VerificationActions.types.GetPendingApplicationsSuccess: {
        return {
          ...state,
          pendingApplications: action.payload
        };
      }

      case VerificationActions.types.GetVerifiedApplicationsSuccess: {
        return {
          ...state,
          verifiedApplications: action.payload
        };
      }

      case VerificationActions.types.GetSelectedApplicationSuccess: {
        return {
          ...state,
          selectedApplication: action.payload
        };
      }

      case VerificationActions.types.SaveApplicationSuccess: {
        return {
          ...state,
          selectedApplication: action.payload
        };
      }

      case VerificationActions.types.DeleteApplicationSuccess: {
        console.log('Delete success: ', action.payload);
        return {
          ...state,
          pendingApplications: state.pendingApplications.filter(it => it.id !== action.payload),
          verifiedApplications: state.verifiedApplications.filter(it => it.id !== action.payload)
        };
      }

      case VerificationActions.types.ChangeApplicationStatusSuccess: {
        return {
          ...state,
          selectedApplication: Object.assign({}, state.selectedApplication, {
            status: action.payload.status
          })
        };
      }

      case VerificationActions.types.AssignAnimatorSuccess: {
        return {
          ...state,
          selectedApplication: Object.assign({}, state.selectedApplication, {
            retreatId: action.payload.selectedRetreat,
            selectedRole: action.payload.selectedRole
          })
        };
      }

      default:
        return state;
    }
  }
}
