import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Community } from './models/community.model';
import { Config } from '../../../config/config';

@Injectable()
export class CommunityService {
  constructor(private http: HttpClient) {}

  getAll(): Observable<Community[]> {
    return this.http.get<Community[]>(Config.endpoints.communityModule);
  }

  getOne(id: number): Observable<Community> {
    return this.http.get<Community>(Config.endpoints.communityModule + '/' + id);
  }

  update(details: Community): Observable<Community> {
    return this.http.put<Community>(Config.endpoints.communityModule, details);
  }

  addNew(details: Community): Observable<Community> {
    return this.http.post<Community>(Config.endpoints.communityModule, details);
  }

  deleteOne(id: number): Observable<any> {
    return this.http.delete<number>(Config.endpoints.communityModule + '/' + id);
  }
}
