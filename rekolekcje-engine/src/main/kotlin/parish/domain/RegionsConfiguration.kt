package pl.oaza.waw.rekolekcje.api.parish.domain

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
internal class RegionsConfiguration {

  fun regionsService(): RegionsFacade {
    return regionsService(InMemoryRegionsRepository())
  }

  @Bean
  fun regionsService(regionsRepository: RegionsRepository): RegionsFacade {
    return RegionsFacade(regionsRepository)
  }
}
