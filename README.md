# Rekolekcje

Aplikacja do zarządzania turnusami rekolekcyjnymi i danymi uczestników tych rekolekcji.

**Przed wprowadzaniem zmian w repozytorium przeczytaj [Contribution Guide](CONTRIBUTING.md).**

## Zależności

Do kompilacji i uruchomienia aplikacji lokalnie potrzebne są następujące zależności:

- Java JDK 17
- NodeJS (wersja >=16.x) + npm (wersja >= 8.x)
- [Docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
- [Docker-compose](https://docs.docker.com/compose/install/)

Jeśli chcesz uruchomić bazę danych lokalnie/nie masz Dockera, potrzebny jest:

- PostgreSQL (przynajmniej wersja 9.4). Należy stworzyć bazę o nazwie `rekolekcjedb`,
  która będzie działać na porcie `localhost:5430` lub skonfigurować aplikację do połączenia z inną bazą.

## Uruchomienie

Aplikację można uruchomić w dwóch trybach:

- developerskim - możliwe jest niezależne uruchomienie aplikacji backendowej i frontendowej, oraz użycie innej bazy danych, na przykład pamieciowej lub testowej
- produkcyjnym - system budowany jest jako pojedyncza aplikacja

### Tryb developerski

##### Uruchomienie backendu (engine)

Będąc w katalogu głównym projektu wpisz w terminalu:

```$xslt
./env.sh start
```

a następnie

```$xslt
./gradlew bootRun
```

Backend bedzie dostepny na porcie `5000`.
Domyślną konfigurację można zmienić poprzez utworzenie pliku `application-local.yml` w tej samej lokalizacji co `application.yml` i nadpisanie w nim odpowiednich properties.

#### Uruchomienie frontendu (webapp):

Przejdź do katalogu `./rekolekcje-webapp/`. Aby pobrać zależności:

```$xslt
npm install
```

Aby uruchomić:

```$xslt
npm start
```

Frontend będzie dostepny na porcie `4200`. <br/>

### Tryb produkcyjny

Aby uruchomić aplikację: <br/>
Uruchom terminal i po wejściu do głównego katalogu projektu wpisz:

```$xslt
./env.sh start
```

a następnie

```$xslt
./gradlew bootJar
java -jar rekolekcje-engine/build/libs/rekolekcje-engine-1.0.0-SNAPSHOT.jar
```

lub bez konieczności posiadania zainstalowanej lokalnie Javy

```$xslt
docker-compose -f docker-compose-build.yml up
docker-compose up
```

Skrypt `./env.sh start` zbuduje kontener dockerowy z bazą danych PostreSQL, który jest wymagany do prawidłowego działania aplikacji. <br/>
Produkcyjna baza danych będzie dostępna przez `localhost:5430` <br/>
Skrypt `./gradlew bootJar` zbuduje `fat jar`, który można uruchomić poleceniem `java -jar`. <br/>
Aplikacja bedzie dostepna na porcie wyswietlonym w konsoli (`http://localhost:5000`) <br/>
Po zakończonej pracy wykonaj skrypt:
`./env.sh wipe`, co spowoduje usunięcie powstałego wcześniej kontenera. <br/>

#### Uruchomienie z dockerem

Możliwe jest uruchomienie aplikacji w kontenerze dockerowym. <br/>
Aby to zrobić, będąc w katalogu głównym projektu wykonaj komendę:
`docker-compose up`. <br/>
Aplikacja dostępna będzie na `localhost:5000`, a jej baza na `localhost:5433`.
Po zakończonej pracy wykonaj `docker-compose down`.

### Testy automatyczne

Testy akceptacyjne w pierwszej kolejności budują kontener z bazą danych PostgreSql, która wykorzystywana jest podczas testów.
Domyślną konfigurację tej bazy można znaleźć w `application-test.yml` w module `verify`.
Konfigurację można zmienić poprzez utworzenie pliku `application-test-local.yml` i nadpisanie w nim odpowiednich wartości.

Wszystkie testy z konsoli:

```$xslt
./gradlew check
```

Same testy jednostkowe:

```$xslt
./gradlew test
```

Same testy integracyjne:

```$xslt
./gradlew verify
```

Testy webowe jednostkowe (uruchamiane z modulu `rekolekcje-webapp`):

```$xslt
ng test
```
