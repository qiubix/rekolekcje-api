package pl.oaza.waw.rekolekcje.api.shared

import com.jayway.jsonpath.JsonPath
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.Matchers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.io.UnsupportedEncodingException

abstract class BaseApiExpectations protected constructor(
  protected val restApi: RestApi
) {

  @Throws(Exception::class)
  fun responseStatusIsOk(response: Response) = response.andExpect(status().isOk)

  @Throws(Exception::class)
  fun responseStatusIsCreated(response: Response) = response.andExpect(status().isCreated)

  @Throws(Exception::class)
  fun responseStatusIsBadRequest(response: Response) = response.andExpect(status().isBadRequest)

  @Throws(Exception::class)
  fun responseStatusIsNotFound(response: Response) = response.andExpect(status().isNotFound)

  @Throws(UnsupportedEncodingException::class)
  fun responseHasDataWithId(response: Response) {
    val contentAsString = response.andReturn().response.contentAsString
    val parsedJson = JsonPath.parse(contentAsString)
    val parsedId = parsedJson.read("$.id", Long::class.java)
    assertThat(parsedId)
      .isNotNull()
      .isNotZero()
  }

  @Throws(Exception::class)
  fun responseHasExactNumberOfElements(response: Response, expectedNumberOfValues: Int) {
    response
      .andExpect(jsonPath("$.length()").value(Matchers.`is`(expectedNumberOfValues)))
  }
}
