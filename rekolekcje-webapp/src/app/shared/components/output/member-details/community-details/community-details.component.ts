import { Component, Input } from '@angular/core';
import { Community } from '@diocese/community/models/community.model';

@Component({
  selector: 'reko-community-details',
  templateUrl: './community-details.component.html',
  styleUrls: ['./community-details.component.scss']
})
export class CommunityDetailsComponent {
  @Input() community: Community;
}
