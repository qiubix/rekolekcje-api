export const PARTICIPANTS_NUMBER: number = 450;
export const ANIMATORS_NUMBER: number = 50;
export const COMMUNITIES_NUMBER: number = 12;
export const RETREATS_NUMBER: number = 25;
