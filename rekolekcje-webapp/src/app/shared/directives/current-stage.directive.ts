import { Directive, Input, OnInit, TemplateRef, ViewContainerRef, OnDestroy } from '@angular/core';
import { Members } from '@members/state/members-reducer';
import { Store, select } from '@ngrx/store';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AppSelectors } from '@core/store/app-selectors';
import { Stage } from '@shared/models/stage.model';

@Directive({
  selector: '[rekoAllowedStages]'
})
export class CurrentStageDirective implements OnInit, OnDestroy {
  @Input() public rekoAllowedStages: string[] = [];

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(
    private readonly templateRef: TemplateRef<any>,
    private readonly viewContainer: ViewContainerRef,
    private store: Store<Members.State>
  ) {}

  public ngOnInit(): void {
    this.store
      .pipe(
        select(AppSelectors.getCurrentApplicationStage),
        takeUntil(this.ngUnsubscribe)
      )
      .subscribe((currentStage: Stage) => {
        if (!currentStage || this.rekoAllowedStages.includes(Stage[currentStage])) {
          this.viewContainer.clear();
          this.viewContainer.createEmbeddedView(this.templateRef);
        } else {
          this.viewContainer.clear();
        }
      });
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
