package pl.oaza.waw.rekolekcje.api.application.endpoint

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationCommandFacade
import pl.oaza.waw.rekolekcje.api.members.endpoint.MembersCreateService
import pl.oaza.waw.rekolekcje.api.members.endpoint.toDetailsDto
import pl.oaza.waw.rekolekcje.api.members.query.MembersQueryFacade

@Service
@Transactional
internal class ApplicationSubmissionService(
  private val applicationCommandFacade: ApplicationCommandFacade,
  private val membersCreateService: MembersCreateService,
  private val membersQueryFacade: MembersQueryFacade
) {

  fun submitNewApplication(submissionDto: ApplicationSubmissionDto): RetreatApplicationDto {
    val createdMember = membersCreateService.createMember(submissionDto.memberDetails)
    val applicationToSubmit = submissionDto.toDomainEntity(
      memberId = createdMember.id,
      firstName = createdMember.firstName,
      lastName = createdMember.lastName
    )
    val submittedApplication = applicationCommandFacade.submitNewApplication(applicationToSubmit)
    val memberDetails = toDetailsDto(membersQueryFacade.find(createdMember.id))
    return submittedApplication.toDto(memberDetails)
  }

  fun removeApplication(id: Long): Long {
    applicationCommandFacade.removeApplication(id)
    return id
  }
}