package pl.oaza.waw.rekolekcje.api.members.endpoint.csv

import pl.oaza.waw.rekolekcje.api.members.endpoint.AddressValue
import pl.oaza.waw.rekolekcje.api.members.endpoint.ExperienceAsAnimatorValue
import pl.oaza.waw.rekolekcje.api.members.endpoint.DeuterocatechumenateValue
import pl.oaza.waw.rekolekcje.api.members.endpoint.HealthReportValue
import pl.oaza.waw.rekolekcje.api.members.endpoint.KwcDto
import pl.oaza.waw.rekolekcje.api.members.endpoint.FormationInCommunityValue
import pl.oaza.waw.rekolekcje.api.members.endpoint.PersonalDataValue
import pl.oaza.waw.rekolekcje.api.members.endpoint.TalentsValue

val memberFieldToHeaders: Map<String, List<String>> = mapOf(

  // TODO nie przewidujemy pola RODO
  // TODO nie przewidujemy pola "Czy będzie potrzebne oświadczenie o pobycie uczestnika na rekolekcjach i ich koszcie? (nie wystawiamy faktur)"
  // TODO nie przewidujemy pola "Czy chcesz poinformować o czymś DOR bądź Moderatorów rekolekcji, na co nie było miejsca wcześniej w formularzu?"
  // TODO nie przewidujemy pola "Podaj dane osoby, na którą ma być wystawione zaświadczenie o pobycie i koszcie rekolekcji:"

  // Personal data
  PersonalDataValue::firstName.name to listOf("Imię (imiona)", "Imię (imiona) uczestnika", "Imię (imiona) animatora"),
  PersonalDataValue::lastName.name to listOf("Nazwisko", "Nazwisko uczestnika", "Nazwisko animatora"),
  PersonalDataValue::pesel.name to listOf("PESEL"),
  PersonalDataValue::email.name to listOf("Kontaktowy adres e-mail"),
  PersonalDataValue::phoneNumber.name to listOf(
    "Numer telefonu",
    "Numer telefonu uczestnika"
  ),
  PersonalDataValue::placeOfBirth.name to listOf("Miejsce urodzenia"),
  PersonalDataValue::nameDay.name to listOf("Data imienin"),
  AddressValue::city.name to listOf("Miejscowość"),
  AddressValue::postalCode.name to listOf("Kod pocztowy"),
  AddressValue::streetName.name to listOf("Ulica"),
  AddressValue::streetNumber.name to listOf("Numer domu"),
  AddressValue::flatNumber.name to listOf("Numer mieszkania"),
  MemberCsvRowConverter.motherContactKey to listOf("Imię i nazwisko matki"),
  MemberCsvRowConverter.fatherContactKey to listOf("Imię i nazwisko ojca"),
  MemberCsvRowConverter.emergencyContactKey to listOf("Imię i nazwisko bliskiej osoby"),
  MemberCsvRowConverter.emergencyContactPhoneKey to listOf("Numer telefonu do bliskiej osoby"),
  MemberCsvRowConverter.emergencyContactEmailKey to listOf("Adres e-mail do bliskiej osoby"),
  PersonalDataValue::schoolYear.name to listOf("Klasa/rok studiów"),
  PersonalDataValue::schoolType.name to listOf("Sytuacja zawodowa"),

  // TODO Diecezja. Nie przewidujemy takiego pola w aplikacji
  // TODO Wspólnota oazowa, do której należy uczestnik (przy parafii). przewidujemy id wspólnoty, a nie wprowadzanie nazwy
  // TODO Podaj parafię, do której należy uczestnik (miejscowość i wezwanie)
  // TODO Podaj wspólnotę oazową, do której należy uczestnik (miejscowość i wezwanie parafii). // Pewnie tu nową parafię trzeba podać
  // TODO Uczestnik należy do: // Przewidujemy opcje ministrant // bielanki, a powinno być Set<String> / String


  // Experience
  KwcDto::status.name to listOf(
    "Krucjata Wyzwolenia Człowieka",
    "Status uczestnika w Krucjacie Wyzwolenia Człowieka"
  ),
  KwcDto::year.name to listOf("Rok wstąpienia do KWC"),
  ExperienceAsAnimatorValue::communityFunctions.name to listOf("Funkcja we wspólnocie"),
  ExperienceAsAnimatorValue::otherRetreats.name to listOf("Inne rekolekcje/kursy oazowe:"),
  ExperienceAsAnimatorValue::otherSkills.name to listOf("Dodatkowo:"),
  ExperienceAsAnimatorValue::diakonia.name to listOf("Posługa w Diakonii Diecezjalnej"),
  MemberCsvRowConverter.odbParticipantKey to listOf(
    "Animator był uczestnikiem na Oazie Dzieci Bożych w roku/latach:",
    "Uczestnik brał udział w Oazie Dzieci Bożych w roku/latach:"
  ),
  MemberCsvRowConverter.ondParticipantKey to listOf(
    "Animator był uczestnikiem na Oazie Nowej Drogi w roku/latach:",
    "Uczestnik brał udział w Oazie Nowej Drogi w roku/latach:"
  ),
  MemberCsvRowConverter.onzIParticipantKey to listOf(
    "Animator był uczestnikiem na Oazie Nowego Życia 1° w roku/latach:",
    "Uczestnik brał udział w Oazie Nowego Życia I w roku/latach:"
  ),
  MemberCsvRowConverter.onzIIParticipantKey to listOf(
    "Animator był uczestnikiem na Oazie Nowego Życia 2° w roku:",
    "Uczestnik brał udział w Oazie Nowego Życia II w roku/latach:"
  ),
  MemberCsvRowConverter.triduumParticipantKey to listOf(
    "Uczestnik brał udział w rekolekcjach w Triduum Paschalne w roku/latach:"
  ),
  MemberCsvRowConverter.onzIIIParticipantKey to listOf(
    "Animator był uczestnikiem na Oazie Nowego Życia 3° w roku:",
    "Uczestnik brał udział w Oazie Nowego Życia III w roku/latach:"
  ),
  MemberCsvRowConverter.odbAnimatorKey to listOf("Ile razy animator posługiwał na: [ODB]"),
  MemberCsvRowConverter.ondAnimatorKey to listOf("Ile razy animator posługiwał na: [OND]"),
  MemberCsvRowConverter.onzIAnimatorKey to listOf("Ile razy animator posługiwał na: [ONŻ I]"),
  MemberCsvRowConverter.onzIIAnimatorKey to listOf("Ile razy animator posługiwał na: [ONŻ II]"),
  MemberCsvRowConverter.onzIIIAnimatorKey to listOf("Ile razy animator posługiwał na: [ONŻ III]"),
  TalentsValue::musicalTalents.name to listOf("Czy uczestnik chętnie śpiewa i/lub gra na jakimś instrumencie?"),
  FormationInCommunityValue::formationInSmallGroup.name to listOf("Czy uczestnik formuje się w grupce oazowej?"),
  DeuterocatechumenateValue::stepsTaken.name to listOf("Liczba przyjętych przez uczestnika kroków ku dojrzałości chrześcijańskiej"),
  DeuterocatechumenateValue::stepsPlannedThisYear.name to listOf("Ile uczestnik planuje przyjąć kroków do wakacji (poza wymienionymi wyżej)"),
  // TODO formularz oazowy przewiduje wypisanie lat, w których uczestnik brał udział w ODB. My przewidujemy tylko ich liczbę
//      ExperienceValue::numberOfODB.name to "Uczestnik brał udział w Oazie Dzieci Bożych w roku/latach:",

  // TODO formularz oazowy przewiduje zakresy lat, w których uczestnik brał udział w reko Domowego Kościoła z rodzicami. My przewidujemy tylko ich liczbę
//      ExperienceValue::numberOfRetreatsWithParents.name to "Ile razy uczestnik brał udział w rekolekcjach Domowego Kościoła z rodzicami?",

//       TODO w formularzu oazowym jest wskazanie na 'wakacje 2019', u nas odpowiednikiem są 'historical retreats'. Co więcej, przyjmujemy Set<Long>.
//        Zmiana formularza oazowego / naszego nieunikniona
//      ExperienceValue::historicalRetreats.name to "W wakacje 2019 r. uczestnik brał udział w:"

  // TODO nie przewidujemy pola "Czy uczestnik formuje się w grupce oazowej?" (Boolean)
  // TODO nie przewidujemy pola "Imię i nazwisko animatora grupki" (Może nie dotyczyć)

//    ExperienceValue::musicalTalents.name to "Czy uczestnik chętnie śpiewa i/lub gra na jakimś instrumencie?"

  // Health report
  // TODO w formularzu mamy jedno pole o aktualnym leczeniu / lekach. U nas mamy ro rozdzielone na dwa pola: currentTreatment i medications
  HealthReportValue::currentTreatment.name to listOf(
    "Czy aktualnie się na coś leczy/przyjmuje leki? Jeśli TAK, to na co i jakie?",
    "Czy animator aktualnie się na coś leczy/przyjmuje leki? Jeśli TAK, to na co i jakie?",
    "Czy uczestnik aktualnie się na coś leczy/przyjmuje leki? Jeśli TAK, to na co i jakie?"
  ),
  HealthReportValue::mentalDisorders.name to listOf(
    "Czy ma zaburzenia psychiczne, fizyczne, emocjonalne, umysłowe lub społeczne? Jeśli TAK, to jakie? Czy podejmuje leczenie?",
    "Czy animator ma zaburzenia psychiczne, fizyczne, emocjonalne, umysłowe lub społeczne? Jeśli TAK, to jakie? Czy podejmuje leczenie?",
    "Czy uczestnik ma zaburzenia psychiczne, fizyczne, emocjonalne, umysłowe lub społeczne? Jeśli TAK, to jakie?"
  ),
  HealthReportValue::allergies.name to listOf(
    "Czy jest na coś uczulony? Jeśli TAK, to na co i jak reaguje? Co zrobić w razie reakcji alergicznej?",
    "Czy animator jest na coś uczulony? Jeśli TAK, to na co i jak reaguje? Co zrobić w razie reakcji alergicznej?",
    "Czy uczestnik jest na coś uczulony? Jeśli TAK, to na co i jak reaguje? Co zrobić w razie reakcji alergicznej?"
  ),

  // TODO zamiast boolean powinno być String, ponieważ można wypisać przeciwwskazania. Ewentualnie w formularzu powinno dojść kolejne pole
  HealthReportValue::hikingContraindications.name to listOf(
    "Czy są przeciwwskazania, żeby brał udział w wycieczkach w góry? Jeśli TAK, to jakie?",
    "Czy są przeciwwskazania, żeby animator brał udział w wycieczkach w góry? Jeśli TAK, to jakie?",
    "Czy są przeciwwskazania, żeby uczestnik brał udział w wycieczkach w góry? Jeśli TAK, to jakie?"
  ),
  HealthReportValue::hasMotionSickness.name to listOf(
    "Czy cierpi na chorobę lokomocyjną? ",
    "Czy animator cierpi na chorobę lokomocyjną?",
    "Czy uczestnik cierpi na chorobę lokomocyjną?"
  ),
  HealthReportValue::medicalDiet.name to listOf(
    "Czy ma specjalną DIETĘ? Jeśli TAK, to należy opisać jaką.",
    "Czy animator ma specjalną DIETĘ? Jeśli TAK, to należy opisać jaką.",
    "Czy uczestnik ma specjalną DIETĘ? Jeśli TAK, to należy opisać jaką. "
  ),
  HealthReportValue::other.name to listOf(
    "Inne uwagi o stanie zdrowia, które nie zostały zawarte wyżej i mogą mieć znaczenie w trakcie udziału w rekolekcjach:",
    "Inne uwagi o stanie zdrowia, które nie zostały zawarte wyżej i mogą mieć znaczenie w trakcie udziału w rekolekcjach:"
  )
)
