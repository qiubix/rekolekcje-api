package pl.oaza.waw.rekolekcje.api.application.domain

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import pl.oaza.waw.rekolekcje.api.application.ApplicationTestFixture
import pl.oaza.waw.rekolekcje.api.application.animatorApplication
import pl.oaza.waw.rekolekcje.api.application.participantApplication
import pl.oaza.waw.rekolekcje.api.shared.domain.Contact

internal class UpdateApplicationTest : ApplicationTestFixture() {

  @Test
  fun `should not modify application status when updating application's data`() {
    val applicationId = saveApplicationWithStatus(ApplicationStatus.VERIFIED)
    val modifiedApplication = participantApplication()
      .copy(
        status = ApplicationStatus.WAITING,
        firstName = "New name",
        surety = Contact(fullName = "Updated surety")
      )
      .withId(applicationId) as RetreatApplication

    commandFacade.updateApplication(modifiedApplication)

    val persistedApplication = findPersistedApplication(applicationId)
    assertThat(persistedApplication).isEqualTo(modifiedApplication.copy(status = ApplicationStatus.VERIFIED))
  }

  @Test
  fun `should not modify animator's application retreat when updating application`() {
    val retreatId = 543L
    val application = animatorApplication().copy(retreatId = retreatId)
    val id = persistApplication(application)
    val modifiedApplication = animatorApplication(
      memberId = 876L,
      firstName = "Updated name",
      animatorRoles = setOf(AnimatorRole.QUARTERMASTER)
    ).copy(
      id = id,
      retreatId = 999L
    )

    commandFacade.updateApplication(modifiedApplication)

    val persistedApplication = findPersistedApplication(id)
    assertThat(persistedApplication).isEqualTo(modifiedApplication.copy(retreatId = retreatId))
  }

  @Test
  fun `should allow modifying participant's retreat id`() {
    val retreatId = 543L
    val application = participantApplication().copy(retreatId = retreatId)
    val id = persistApplication(application)
    val newRetreatId = 987L
    val modifiedApplication = participantApplication(
      memberId = 876L,
      firstName = "Updated name"
    ).copy(
      id = id,
      retreatId = newRetreatId
    )

    commandFacade.updateApplication(modifiedApplication)

    val persistedApplication = findPersistedApplication(id)
    assertThat(persistedApplication).isEqualTo(modifiedApplication)
  }

  private fun saveApplicationWithStatus(status: ApplicationStatus): Long {
    val application = participantApplication(status = status)
    return persistApplication(application)
  }
}