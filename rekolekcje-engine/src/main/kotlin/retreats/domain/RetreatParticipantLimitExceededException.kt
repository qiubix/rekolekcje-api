package pl.oaza.waw.rekolekcje.api.retreats.domain

class RetreatParticipantLimitExceededException(retreatId: Long, limit: Int) :
  RuntimeException(createMessage(retreatId, limit)) {
  companion object {
    private const val MESSAGE = "Retreat [%d] reached participant limit of [%d]. Member can not apply."

    fun createMessage(retreatId: Long, limit: Int) =
      String.format(MESSAGE, retreatId, limit)
  }
}