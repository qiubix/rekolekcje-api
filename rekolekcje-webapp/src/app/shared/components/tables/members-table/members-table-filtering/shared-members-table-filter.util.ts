import { MemberSummary } from '@members/models';
import { RetreatSummary } from '@retreats/models/retreat-summary.model';
import { Community } from '@diocese/community/models/community.model';
import { MemberStatus } from '@shared/models';

export enum PositionDisplayOption {
  ALL_MEMBERS = 'ALL_MEMBERS',
  ONLY_PARTICIPANTS = 'ONLY_PARTICIPANTS',
  ONLY_ANIMATORS = 'ONLY_ANIMATORS'
}

export interface MembersFilter {
  selectedDisplayOption: PositionDisplayOption;
  memberStatus: MemberStatus;
  firstOrLastName: string;
  firstName: string;
  lastName: string;
  stage: string;
  turn: string;
  community: string;
}

export class SharedMembersTableFilterUtil {
  static filterMembers(
    members: MemberSummary[],
    retreats: RetreatSummary[],
    communities: Community[],
    filter: MembersFilter
  ): MemberSummary[] {
    let retreatsWithStage = [];
    let retreatsWithTurn = [];
    let communitiesWithName = [];

    if (filter.stage) {
      retreatsWithStage = retreats.filter(it => it.stage === filter.stage).map(it => it.id);
    }

    if (filter.turn) {
      retreatsWithTurn = retreats.filter(it => it.turn === filter.turn).map(it => it.id);
    }

    if (filter.community) {
      communitiesWithName = communities
        .filter(it => it.name.includes(this.normalizeString(filter.community)))
        .map(it => it.id);
    }

    return members.filter((member: MemberSummary) => {
      return (
        this.shouldMemberBeDisplayedByPosition(member.animator, filter.selectedDisplayOption) &&
        this.shouldMemberBeDisplayedByStatus(member.memberStatus, filter.memberStatus) &&
        this.shouldMemberBeDisplayedByStage(retreatsWithStage, member.applyingForRetreatTurn) &&
        this.shouldMemberBeDisplayedByTurn(retreatsWithTurn, member.applyingForRetreatTurn) &&
        this.shouldMemberBeDisplayedByFirstOrLastName(member, filter.firstOrLastName) &&
        this.shouldMemberBeDisplayedByCommunity(communitiesWithName, member.communityId)
      );
    });
  }

  private static shouldMemberBeDisplayedByFirstOrLastName(
    member: MemberSummary,
    filterValue: string
  ): boolean {
    const normalizedFilterValue = this.normalizeString(filterValue).split(' ');
    const normalizedFirstName = this.normalizeString(member.firstName);
    const normalizedLastName = this.normalizeString(member.lastName);
    if (normalizedFilterValue.length == 2) {
      return (
        normalizedFirstName.includes(normalizedFilterValue[0]) &&
        normalizedLastName.includes(normalizedFilterValue[1])
      );
    } else {
      return (
        normalizedFirstName.includes(normalizedFilterValue[0]) ||
        normalizedLastName.includes(normalizedFilterValue[0])
      );
    }
  }

  private static shouldMemberBeDisplayedByStage(
    retreatsWithStage: number[],
    memberRetreatId: number
  ): boolean {
    return retreatsWithStage.length === 0 || retreatsWithStage.includes(memberRetreatId);
  }

  private static shouldMemberBeDisplayedByTurn(
    retreatsWithTurn: number[],
    memberRetreatId: number
  ): boolean {
    return retreatsWithTurn.length === 0 || retreatsWithTurn.includes(memberRetreatId);
  }

  private static shouldMemberBeDisplayedByCommunity(
    communitiesWithName: number[],
    memberCommunityId: number
  ): boolean {
    return communitiesWithName.length === 0 || communitiesWithName.includes(memberCommunityId);
  }

  private static normalizeString(value: string): string {
    return value ? value.toLowerCase() : '';
  }

  private static shouldMemberBeDisplayedByPosition(
    isAnimator: boolean,
    displayOption: PositionDisplayOption
  ): boolean {
    switch (displayOption) {
      case PositionDisplayOption.ALL_MEMBERS:
        return true;
      case PositionDisplayOption.ONLY_ANIMATORS:
        return isAnimator;
      case PositionDisplayOption.ONLY_PARTICIPANTS:
        return !isAnimator;
    }
  }

  private static shouldMemberBeDisplayedByStatus(
    current: MemberStatus,
    selected: MemberStatus
  ): boolean {
    return !selected || current === selected;
  }
}
