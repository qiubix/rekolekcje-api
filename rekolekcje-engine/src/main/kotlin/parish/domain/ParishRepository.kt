package pl.oaza.waw.rekolekcje.api.parish.domain

import pl.oaza.waw.rekolekcje.api.core.ddd.DomainRepository

@DomainRepository
internal interface ParishRepository {

  fun findById(id: Long): Parish?

  fun findAll(): List<Parish>

  fun save(parish: Parish): Parish

  fun deleteById(id: Long)
}
