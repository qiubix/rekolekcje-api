package pl.oaza.waw.rekolekcje.api.notification.domain

import pl.oaza.waw.rekolekcje.api.notification.CreateNotificationRequest
import pl.oaza.waw.rekolekcje.api.notification.dto.NotificationSummary

interface NotificationCommandFacade {
  fun create(request: CreateNotificationRequest): NotificationSummary
  fun deleteByApplication(applicationId: Long)
}