package pl.oaza.waw.rekolekcje.api.user.storage

import org.assertj.core.api.Assertions.assertThat
import org.springframework.security.crypto.password.PasswordEncoder
import pl.oaza.waw.rekolekcje.api.user.domain.User
import pl.oaza.waw.rekolekcje.api.user.domain.UserNotFoundException
import pl.oaza.waw.rekolekcje.api.user.endpoint.UpdateUserRequest
import pl.oaza.waw.rekolekcje.api.user.endpoint.UserRole

class UserStorageExpectations(private val storage: UserStorage, private val passwordEncoder: PasswordEncoder) {

  fun numberOfExistingUsers(): Int = storage.findAllUsers().size

  fun isCorrectNumberOfUsers(expectedNumberOfUsers: Int) {
    assertThat(storage.findAllUsers()).hasSize(expectedNumberOfUsers)
  }

  fun correctUserIsPersisted(request: UpdateUserRequest) {
    val persistedUser = storage.findUser(request.username) ?: throw UserNotFoundException(request.username)
    assertThat(persistedUser.enabled).isEqualTo(request.enabled)
    userHasCorrectRoles(persistedUser, request)
//    userHasCorrectPassword(request, persistedUser)
  }

  fun userHasCorrectPassword(username: String, expectedPassword: String) {
    val persistedUser = storage.findUser(username) ?: throw UserNotFoundException(username)
    val passwordsMatch = passwordEncoder.matches(expectedPassword, persistedUser.password)
    assertThat(passwordsMatch).isTrue()
  }

  private fun userHasCorrectRoles(persistedUser: User, request: UpdateUserRequest) {
    val userRoles = storage
      .findUserRoles(persistedUser.id ?: throw IllegalStateException("User should have an ID"))
      .map { UserRole.valueOf(it.name) }
      .toSet()
    val expectedRoles = request.roles + setOf(UserRole.ROLE_USER).toSet()
    assertThat(userRoles).isEqualTo(expectedRoles)
  }

//  private fun userHasCorrectPassword(request: UpdateUserRequest, persistedUser: User) {
//    val passwordsMatch = passwordEncoder.matches(request.password, persistedUser.password)
//    assertThat(passwordsMatch).isTrue()
//  }
}
