package pl.oaza.waw.rekolekcje.api.notification.dto

enum class NotificationStatus {
  SCHEDULED,
  SUCCESS,
  FAILURE
}