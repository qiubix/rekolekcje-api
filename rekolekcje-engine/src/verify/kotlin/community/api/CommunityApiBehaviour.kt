package pl.oaza.waw.rekolekcje.api.community.api

import pl.oaza.waw.rekolekcje.api.community.dto.CommunityNotFoundException
import pl.oaza.waw.rekolekcje.api.community.dto.CommunityPayload
import pl.oaza.waw.rekolekcje.api.shared.BaseApiBehaviour
import pl.oaza.waw.rekolekcje.api.shared.RestApi

class CommunityApiBehaviour(
  restApi: RestApi, baseUri: String
) : BaseApiBehaviour<CommunityPayload>(restApi, baseUri) {

  fun resourceAmountIsRequested(): Int {
    return restApi.performGet(baseUri)
      .getContentAsList<CommunityPayload>()
      .size
  }

  fun existsElementWithTheSameData(payload: CommunityPayload): CommunityPayload {
    val response = restApi.performGet(baseUri)
    val allElements = response.getContentAsList<CommunityPayload>()
    val foundElement = allElements.find { haveTheSameData(payload, it) }
    return foundElement ?: throw getNotFoundException(payload)
  }

  override fun haveTheSameData(first: CommunityPayload, second: CommunityPayload): Boolean {
    return first.name == second.name
  }

  override fun getNotFoundException(payload: CommunityPayload): RuntimeException {
    return CommunityNotFoundException(payload)
  }
}
