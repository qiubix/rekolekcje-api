import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { Members } from '../../state/members-reducer';
import { MembersSharedActions } from '../../state/members-actions';
import { AppSelectors } from '@core/store/app-selectors';
import { Router } from '@angular/router';
import { Community } from '@diocese/community/models/community.model';
import { Parish } from '@diocese/parish/models/parish.model';
import { MemberSummary } from '@members/models';
import { RetreatSummary } from 'app/retreats/models/retreat-summary.model';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'members-root',
  templateUrl: './members.component.html',
})
export class MembersComponent implements OnInit {
  members$: Observable<MemberSummary[]>;
  membersLoading$: Observable<boolean>;
  parishes$: Observable<Parish[]>;
  communities$: Observable<Community[]>;
  retreats$: Observable<RetreatSummary[]>;

  constructor(
    private router: Router,
    private store: Store<Members.State>,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.members$ = this.store.pipe(select(AppSelectors.getMembersList));
    this.membersLoading$ = this.store.pipe(select(AppSelectors.getMembersLoading));
    this.parishes$ = this.store.pipe(select(AppSelectors.getParishList));
    this.communities$ = this.store.pipe(select(AppSelectors.getCommunityList));
    this.retreats$ = this.store.pipe(select(AppSelectors.getRetreatsList));
  }

  onAdd(): void {
    this.router.navigate(['members', 'add']);
    // const dialogRef = this.dialog.open(MemberAddEditDialogComponent, {
    //   data: {
    //     dialogTitle: 'Dodaj nowego'
    //   },
    //   disableClose: true
    // });
    //
    // dialogRef.afterClosed().subscribe(result => {
    //   if (result && result.edit) {
    //     const recentlyCreatedMemberId$ = this.store.select(AppSelectors.getRecentlyCreatedMemberId);
    //     recentlyCreatedMemberId$.subscribe(id => {
    //       // console.log('Navigating to edit: ', id);
    //       this.router.navigate(['members', `${id}`, 'edit']);
    //     });
    //   }
    // });
  }

  onDelete(memberId: number): void {
    this.store.dispatch(new MembersSharedActions.DeleteMember(memberId));
  }

  onEdit(member: MemberSummary): void {
    this.router.navigate(['members', `${member.id}`, 'edit']);
    // const dialogRef = this.dialog.open(MemberAddEditDialogComponent, {
    //   data: {
    //     dialogTitle: 'Edytuj',
    //     memberId: member.id
    //   },
    //   disableClose: true
    // });
    //
    // dialogRef.afterClosed().subscribe(result => {
    //   if (result && result.edit) {
    //     // console.log('Navigating to edit: ', member.id);
    //     this.router.navigate(['members', `${member.id}`, 'edit']);
    //   }
    // });
  }

  onSelect(member: MemberSummary): void {
    this.router.navigate(['members', `${member.id}`]);
  }
}
