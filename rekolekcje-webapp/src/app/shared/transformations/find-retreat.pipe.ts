import { Pipe, PipeTransform } from '@angular/core';
import { Subject } from 'rxjs';
import { RetreatSummary } from '@retreats/models/retreat-summary.model';
import { select, Store } from '@ngrx/store';
import { Retreats } from '@retreats/store/retreat-reducer';
import { AppSelectors } from '@core/store/app-selectors';
import { filter, map, take, takeUntil, tap } from 'rxjs/operators';
import { Observable } from 'rxjs/internal/Observable';
import { RetreatsSharedActions } from '@retreats/store/retreat-actions';

@Pipe({
  name: 'findRetreat'
})
export class FindRetreatPipe implements PipeTransform {
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  private _retreats$: Observable<RetreatSummary[]>;

  constructor(private _store: Store<Retreats.State>) {
    this._store.dispatch(new RetreatsSharedActions.LoadRetreatsList());
    this._retreats$ = this._store.pipe(
      select(AppSelectors.getRetreatsList),
      takeUntil(this.ngUnsubscribe),
      filter(retreats => retreats.length > 0),
      take(1)
    );
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  transform(retreatId: number, args?: any): Observable<RetreatSummary> {
    return this._retreats$.pipe(map(retreats => retreats.find(it => it.id === retreatId)));
  }
}
