package pl.oaza.waw.rekolekcje.api.opinions.domain

import pl.oaza.waw.rekolekcje.api.opinions.endpoint.SubmitOpinionRequest

interface OpinionsCommandFacade {
  fun submit(submitRequest: SubmitOpinionRequest): Opinion
}