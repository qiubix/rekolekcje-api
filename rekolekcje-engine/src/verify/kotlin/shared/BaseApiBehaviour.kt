package pl.oaza.waw.rekolekcje.api.shared

abstract class BaseApiBehaviour<T : Any> protected constructor(
  protected val restApi: RestApi,
  protected val baseUri: String
) {

  @Throws(Exception::class)
  fun allAreRequested(): Response {
    return restApi.performGet(baseUri)
  }

  @Throws(Exception::class)
  fun singleIsRequested(id: Long?): Response {
    return restApi.performGet("$baseUri/$id")
  }

  @Throws(Exception::class)
  fun singleIsAdded(payload: T): Response {
    return restApi.performPost(baseUri, payload)
  }

  @Throws(Exception::class)
  fun singleIsDeleted(id: Long): Response {
    return restApi.performDelete("$baseUri/$id")
  }

  @Throws(Exception::class)
  fun singleIsUpdated(updatedPayload: T): Response {
    return restApi.performPut(baseUri, updatedPayload)
  }

  @Throws(Exception::class)
  fun allAreAdded(payloadAsList: List<T>) {
    for (payload in payloadAsList) {
      restApi.performPost(baseUri, payload)
    }
  }

  protected abstract fun haveTheSameData(first: T, second: T): Boolean

  protected abstract fun getNotFoundException(payload: T): RuntimeException

}
