import { Component } from '@angular/core';

@Component({
  selector: 'reko-main-menu-bar',
  templateUrl: './main-menu-bar.component.html',
  styleUrls: ['./main-menu-bar.component.scss']
})
export class MainMenuBarComponent {}
