package pl.oaza.waw.rekolekcje.api.notification.domain.template.extractor

enum class ExtractorType(val key: String) {
	EMAIL("email"),
	NAME("name"),
	RETREAT("retreat"),
  OPINION_LINK("opinionLink"),
	REJECTION_REASON("rejectionReason"),
	ATTACHMENT_LINK("attachmentLink"),
}
