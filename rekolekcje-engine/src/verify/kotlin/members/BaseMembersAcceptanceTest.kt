package pl.oaza.waw.rekolekcje.api.members

import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.params.provider.Arguments
import org.springframework.beans.factory.annotation.Autowired
import pl.oaza.waw.rekolekcje.api.shared.BaseIntegrationTest
import pl.oaza.waw.rekolekcje.api.members.api.MembersApiBehaviour
import pl.oaza.waw.rekolekcje.api.members.api.MembersApiExpectations
import pl.oaza.waw.rekolekcje.api.members.storage.MembersDatabase
import pl.oaza.waw.rekolekcje.api.members.utils.memberWithMinimalData
import pl.oaza.waw.rekolekcje.api.members.utils.memberWithSampleData
import pl.oaza.waw.rekolekcje.api.members.utils.withEmail
import pl.oaza.waw.rekolekcje.api.members.utils.memberWithFullData
import pl.oaza.waw.rekolekcje.api.members.utils.withPesel
import pl.oaza.waw.rekolekcje.api.members.utils.withPhoneNumber
import pl.oaza.waw.rekolekcje.api.opinions.api.OpinionApiBehaviour
import pl.oaza.waw.rekolekcje.api.opinions.storage.OpinionsDatabase
import pl.oaza.waw.rekolekcje.api.parish.api.ParishApiBehaviours
import pl.oaza.waw.rekolekcje.api.parish.api.ParishApiExpectations
import pl.oaza.waw.rekolekcje.api.retreats.storage.RetreatsDatabase
import java.util.stream.Stream

abstract class BaseMembersAcceptanceTest : BaseIntegrationTest() {

  @Autowired
  protected lateinit var whenInApi: MembersApiBehaviour
  @Autowired
  protected lateinit var thenInApi: MembersApiExpectations

  @Autowired
  protected lateinit var thenInParishApi: ParishApiExpectations

  @Autowired
  protected lateinit var whenInParishApi: ParishApiBehaviours

  @Autowired
  private lateinit var database: MembersDatabase

  @Autowired
  private lateinit var retreatsDatabase: RetreatsDatabase

  @Autowired
  private lateinit var opinionsDatabase: OpinionsDatabase

  @Autowired
  protected lateinit var whenInOpinionsApi: OpinionApiBehaviour


  protected val minimalMember = memberWithMinimalData()
  protected val sampleMember = memberWithSampleData()
  protected val memberWithFullData = memberWithFullData()

  protected val members = listOf(minimalMember, sampleMember)

  @BeforeAll
  fun setupAll() {
    retreatsDatabase.clear()
    opinionsDatabase.clear()
  }

  @BeforeEach
  fun setup() {
    database.clear()
  }

  protected fun invalidMembers(): Stream<Arguments> {
    val sampleMember = memberWithSampleData()
    return Stream.of(
      Arguments.of(sampleMember.withPesel("1234567890123"), "PESEL"),
      Arguments.of(sampleMember.withPesel("asd23r"), "PESEL"),
      Arguments.of(sampleMember.withEmail("@invalidEmail."), "Email"),
      Arguments.of(sampleMember.withPhoneNumber("4321"), "Pattern"),
      Arguments.of(sampleMember.withPhoneNumber("1234567890123"), "Pattern")
    )
  }
}
