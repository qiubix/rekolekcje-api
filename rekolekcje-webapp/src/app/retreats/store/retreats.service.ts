import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RetreatSummary } from '../models/retreat-summary.model';
import { Config } from '../../../config/config';
import { RetreatDetails } from '../models/retreat-details.model';

@Injectable()
export class RetreatsService {
  constructor(private http: HttpClient) {}

  getAll(): Observable<RetreatSummary[]> {
    return this.http.get<RetreatSummary[]>(Config.endpoints.retreatsModule);
  }

  getOne(id: number): Observable<RetreatDetails> {
    return this.http.get<RetreatDetails>(Config.endpoints.retreatsModule + '/' + id);
  }

  update(details: RetreatDetails): Observable<RetreatSummary> {
    return this.http.put<RetreatSummary>(Config.endpoints.retreatsModule, details);
  }

  addNew(details: RetreatDetails): Observable<RetreatSummary> {
    return this.http.post<RetreatSummary>(Config.endpoints.retreatsModule, details);
  }

  deleteOne(id: number): Observable<any> {
    return this.http.delete<number>(Config.endpoints.retreatsModule + '/' + id);
  }
}
