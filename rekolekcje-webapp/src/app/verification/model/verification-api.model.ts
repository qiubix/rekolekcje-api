import { MemberStatus } from '@shared/models';

export interface ChangePaymentStatusRequest {
  applicationId: number;
  paymentStatus: string;
}

export interface ChangeApplicationStatusRequest {
  id: number;
  status: string;
  reason?: string;
}

export interface StatusWithReason {
  status: string;
  reason?: string;
}

export interface ChangeApplicationStatusResponse {
  status: MemberStatus;
}

export interface AssignAnimatorRequest {
  applicationId: number;
  selectedRetreat: number;
  selectedRole: string;
}

export interface AssignAnimatorResponse {
  selectedRetreat: number;
  selectedRole: string;
}
