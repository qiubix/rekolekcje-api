import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'reko-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent {
  @Input() label: string;
  @Input() visible: boolean = true;
  @Input() icon: string = '';
  @Input() primary: boolean = false;

  @Output() clicked: EventEmitter<any> = new EventEmitter<any>();

  onClick() {
    this.clicked.emit();
  }
}
