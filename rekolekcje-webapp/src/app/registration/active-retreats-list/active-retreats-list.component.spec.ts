import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiveRetreatsListComponent } from './active-retreats-list.component';

describe('ActiveRetreatsListComponent', () => {
  let component: ActiveRetreatsListComponent;
  let fixture: ComponentFixture<ActiveRetreatsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ActiveRetreatsListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiveRetreatsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
