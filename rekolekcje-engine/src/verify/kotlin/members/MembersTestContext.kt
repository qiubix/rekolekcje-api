package pl.oaza.waw.rekolekcje.api.members

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.jdbc.core.JdbcTemplate
import pl.oaza.waw.rekolekcje.api.shared.RestApi
import pl.oaza.waw.rekolekcje.api.members.api.MembersApiBehaviour
import pl.oaza.waw.rekolekcje.api.members.api.MembersApiExpectations
import pl.oaza.waw.rekolekcje.api.members.query.OpinionLinkQueryFacade
import pl.oaza.waw.rekolekcje.api.members.storage.MembersDatabase
import pl.oaza.waw.rekolekcje.api.members.storage.MembersStorageBehaviour
import pl.oaza.waw.rekolekcje.api.members.storage.MembersStorageExpectations

@Configuration
@Profile("test")
internal class MembersTestContext {

  @Bean
  fun membersDatabase(jdbcTemplate: JdbcTemplate): MembersDatabase {
    return MembersDatabase(jdbcTemplate)
  }

  @Bean
  fun membersApiBehaviour(restApi: RestApi, opinionLinkQueryFacade: OpinionLinkQueryFacade): MembersApiBehaviour {
    return MembersApiBehaviour(restApi, opinionLinkQueryFacade)
  }

  @Bean
  fun membersApiExpectations(restApi: RestApi): MembersApiExpectations {
    return MembersApiExpectations(restApi)
  }

  @Bean
  fun membersStorageBehaviour(database: MembersDatabase): MembersStorageBehaviour {
    return MembersStorageBehaviour(database)
  }

  @Bean
  fun participantsStorageExpectations(database: MembersDatabase): MembersStorageExpectations {
    return MembersStorageExpectations(database)
  }
}
