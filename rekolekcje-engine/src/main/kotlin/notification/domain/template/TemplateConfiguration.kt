package pl.oaza.waw.rekolekcje.api.notification.domain.template

import pl.oaza.waw.rekolekcje.api.core.ddd.ValueObject

@ValueObject
data class TemplateConfiguration(
    val subject: String,
    val template: String,
    val templateModel: Map<String, Any> = emptyMap()
)