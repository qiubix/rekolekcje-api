import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import jwt_decode from 'jwt-decode';
import { JwtUser } from './models/jwt-user.model';
import { Credentials } from './models/credentials.model';

@Injectable({ providedIn: 'root' })
export class AuthService {
  private isLoggedIn = false;
  redirectUrl: string;

  logout(): void {
    this.isLoggedIn = false;
    localStorage.removeItem('currentUser');
  }

  isAuthenticated(): boolean {
    return !!localStorage.getItem('currentUser');
  }

  private readonly apiBaseUrl = '/api';
  private readonly authUrl = this.apiBaseUrl + '/auth/sign-in';

  constructor(private http: HttpClient) {}

  authorize(credentials: Credentials) {
    const body = { username: credentials.username, password: credentials.password };

    return this.http.post<Credentials>(this.authUrl, body).pipe(
      map((response) => {
        const token = response['token'];
        if (token) {
          localStorage.setItem(
            'currentUser',
            JSON.stringify({
              username: credentials.username,
              token: token,
            })
          );
          this.isLoggedIn = true;
          return true;
        } else {
          return false;
        }
      })
    );
  }

  public getToken(): string {
    const currentUser: JwtUser = JSON.parse(localStorage.getItem('currentUser'));
    const token = currentUser && currentUser.token;

    return token ? token : '';
  }

  public getCurrentUserRoles(): string[] {
    const token: string = this.getToken();

    if (!token || token === '') {
      return [];
    }

    const decodedToken = jwt_decode(token);

    return decodedToken['roles'].split(',').map((role) => role.replace('ROLE_', ''));
  }

  public getCurrentUsername(): string {
    const currentUser: JwtUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser) {
      return currentUser.username;
    } else {
      return '';
    }
  }
}
