package pl.oaza.waw.rekolekcje.api.user

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import pl.oaza.waw.rekolekcje.api.user.domain.Role
import pl.oaza.waw.rekolekcje.api.user.endpoint.SignUpRequest
import pl.oaza.waw.rekolekcje.api.user.endpoint.UserRole
import pl.oaza.waw.rekolekcje.api.user.infrastructure.UserTestFixture

class UserRegisterServiceTest : UserTestFixture() {

  @Test
  fun `should register new user`() {
    // when
    userRegisterService.registerUser(signUpRequest)

    // then
    val user = userQueryService.findByUsername(username)
    assertThat(user).isNotNull
  }


  @Test
  fun `should encrypt password on registering a new user`() {
    // when
    userRegisterService.registerUser(signUpRequest)

    // then
    val storedPassword = findPasswordForUser(signUpRequest.username)
    assertThat(storedPassword)
      .isNotBlank()
      .isNotEqualTo(signUpRequest.password)
  }

  @Test
  fun `should assign 'USER' role when no role was provided`() {
    // given
    val requestWithoutAuthority = signUpRequest

    // when
    userRegisterService.registerUser(requestWithoutAuthority)

    // then
    val registeredUser = userQueryService.findByUsername(requestWithoutAuthority.username)
      ?: throw IllegalStateException()
    val roles = registeredUser.roles
    assertThat(roles).contains(UserRole.ROLE_USER)
  }

  @Test
  fun `should always add role 'USER' to provided roles`() {
    // given
    val requestWithoutRoleUser = signUpRequest.withRoles(setOf(UserRole.ROLE_ADMIN))

    // when
    userRegisterService.registerUser(requestWithoutRoleUser)

    // then
    val registeredUser = userQueryService.findByUsername(requestWithoutRoleUser.username)
      ?: throw IllegalStateException()
    val roles = registeredUser.roles
    assertThat(roles).contains(UserRole.ROLE_USER)
  }

  @Test
  fun `should register user with roles assigned`() {
    // given
    val requestWithRoles = signUpRequest.withRoles(setOf(UserRole.ROLE_ADMIN, UserRole.ROLE_USER))

    // when
    userRegisterService.registerUser(requestWithRoles)

    // then
    val registeredUser = userQueryService.findByUsername(requestWithRoles.username)
      ?: throw IllegalStateException()
    val roles = registeredUser.roles
    assertThat(roles).isEqualTo(requestWithRoles.roles)
  }
}
