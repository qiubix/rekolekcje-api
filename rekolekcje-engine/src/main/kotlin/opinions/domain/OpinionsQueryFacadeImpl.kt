package pl.oaza.waw.rekolekcje.api.opinions.domain

internal class OpinionsQueryFacadeImpl(
    private val readRepository: OpinionsReadRepository
) : OpinionsQueryFacade {
  override fun getById(id: Long): Opinion {
    return readRepository.findById(id) ?: throw OpinionNotFoundException(id)
  }

  override fun getAllOpinionsForMember(memberId: Long): List<Opinion> {
    return readRepository.findAll().filter { it.memberId == memberId }
  }
}