package pl.oaza.waw.rekolekcje.api.members.query

interface OpinionLinkQueryFacade {
  fun generateLink(memberId: Long): String
  fun retrieveMemberId(encryptedMemberId: String): Long
}
