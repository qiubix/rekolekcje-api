package pl.oaza.waw.rekolekcje.api.members.domain

import pl.oaza.waw.rekolekcje.api.shared.domain.Contact
import java.time.LocalDate
import javax.persistence.CascadeType
import javax.persistence.Embeddable
import javax.persistence.Embedded
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.OneToOne

@Embeddable
data class PersonalData(
  @Embedded
  val contactInfo: MemberContact,
  @Embedded
  val pesel: Pesel,
  val address: Address? = null,
  val christeningDate: LocalDate? = null,
  val placeOfBirth: String? = null,
  val nameDay: String? = null,
  val parishId: Long? = null,
  val communityId: Long? = null,
  val schoolYear: String? = null,
  @Enumerated(EnumType.STRING)
  val schoolType: SchoolType? = null,
  @OneToOne(cascade = [CascadeType.ALL], orphanRemoval = true)
  val father: Contact? = null,
  @OneToOne(cascade = [CascadeType.ALL], orphanRemoval = true)
  val mother: Contact? = null,
  @OneToOne(cascade = [CascadeType.ALL], orphanRemoval = true)
  val emergencyContact: Contact? = null
) {

  fun age() = pesel.calculateAge()

  fun sex() = pesel.sex

  fun birthDate() = pesel.birthDate

  enum class SchoolType {
    PRIMARY,
    HIGH_SCHOOL,
    UNIVERSITY,
    GRADUATED
  }

}
