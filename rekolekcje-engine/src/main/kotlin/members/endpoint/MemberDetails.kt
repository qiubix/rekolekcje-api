package pl.oaza.waw.rekolekcje.api.members.endpoint

import org.hibernate.validator.constraints.pl.PESEL
import pl.oaza.waw.rekolekcje.api.members.domain.KwcStatus
import pl.oaza.waw.rekolekcje.api.members.domain.OtherRetreats
import pl.oaza.waw.rekolekcje.api.members.domain.OtherSkills
import pl.oaza.waw.rekolekcje.api.members.domain.PersonalData.SchoolType
import pl.oaza.waw.rekolekcje.api.opinions.endpoint.OpinionDetailsResponse
import pl.oaza.waw.rekolekcje.api.shared.exceptions.InvalidPayloadException
import pl.oaza.waw.rekolekcje.api.shared.value.Sex
import pl.oaza.waw.rekolekcje.api.shared.value.Stage
import java.time.LocalDate
import javax.validation.Valid
import javax.validation.constraints.Email
import javax.validation.constraints.Pattern

data class MemberDetails(
  val id: Long? = null,
  @field:Valid val personalData: PersonalDataValue,
  val healthReport: HealthReportValue? = null,
  val experience: ExperienceValue = ExperienceValue(),
  val opinions: List<OpinionDetailsResponse> = emptyList()
)

data class PersonalDataValue (
  val firstName: String,
  val lastName: String,
  @field:PESEL val pesel: String,
  val sex: Sex? = null,
  val age: Int? = null,
  val birthDate: String? = null,
  val placeOfBirth: String? = null,
  val christeningDate: LocalDate? = null,
  val nameDay: String? = null,
  @field:Pattern(regexp = "^$|^\\d{9}$", message = "Phone number should have 9 digits.")
  val phoneNumber: String? = null,
  @field:Email val email: String? = null,
  val parishId: Long? = null,
  val communityId: Long? = null,
  @field:Valid val newParish: ParishDto? = null,
  val address : AddressValue? = null,
  val father: ContactInfo? = null,
  val mother: ContactInfo? = null,
  val emergencyContact: ContactInfo? = null,
  val schoolYear: String? = null,
  val schoolType: SchoolType? = null
)

data class ParishDto(
  val id: Long? = null,
  val name: String? = null,
  val address: String? = null,
  val region: Long? = null
) {
  init {
    if (id != null && name != null) {
      throw InvalidPayloadException()
    }
  }
}

data class AddressValue (
  val streetName: String? = null,
  val streetNumber: String? = null,
  val flatNumber: String? = null,
  val postalCode: String? = null,
  val city: String? = null
)

data class ContactInfo(
  val id: Long? = null,
  val fullName: String? = null,
  @field:Pattern(regexp = "^$|^\\d{9}$", message = "Phone number should have 9 digits.")
  val phoneNumber: String? = null,
  @field:Email
  val email: String? = null
)

data class HealthReportValue(
  val currentTreatment: String? = null,
  val mentalDisorders: String? = null,
  val medications: String? = null,
  val allergies: String? = null,
  val medicalDiet: String? = null,
  val cannotHike: Boolean? = null,
  val hikingContraindications: String? = null,
  val hasMotionSickness: Boolean? = null,
  val other: String? = null
)

data class ExperienceValue (
  val animator: ContactInfo? = null,
  val kwc: KwcDto? = null,
  val formationInCommunity: FormationInCommunityValue? = null,
  val deuterocatechumenate: DeuterocatechumenateValue? = null,
  val historicalRetreats: Set<RetreatTurnDto> = emptySet(),
  val talents: TalentsValue? = null,
  val experienceAsAnimator: ExperienceAsAnimatorValue? = null
)

data class FormationInCommunityValue(
  val numberOfCommunionDays: Int? = null,
  val numberOfPrayerRetreats: Int? = null,
  val formationInSmallGroup: Boolean? = null,
  val formationMeetingsInMonth: Int? = null
)

data class DeuterocatechumenateValue(
  val year: Int? = null,
  val stepsTaken: Int? = null,
  val stepsPlannedThisYear: Int? = null,
  val celebrationsTaken: Int? = null,
  val celebrationsPlannedThisYear: Int? = null,
  val missionCelebrationYear: Int? = null,
  val callByNameCelebrationYear: Int? = null,
  val holyTriduumYear: Int? = null
)

data class ExperienceAsAnimatorValue(
  val leadingGroupToFormationStage: String? = null,
  val otherSkills: Set<OtherSkills> = emptySet(),
  val numberOfAnimatorDays: Int? = null,
  val retreatsAsAnimator: Set<RetreatAsAnimatorDto> = emptySet(),
  val otherRetreats: Set<OtherRetreats> = emptySet(),
  val communityFunctions: String? = null,
  val diakonia: String? = null
)

data class TalentsValue(
  val choir: Boolean? = null,
  val musicalTalents: String? = null,
  val altarBoy: Boolean? = null,
  val flowerGirl: Boolean? = null
)

data class KwcDto(
  val status: KwcStatus? = null,
  val year: Int? = null
)

data class RetreatTurnDto(
  val stage: Stage,
  val year: Int
)

data class RetreatAsAnimatorDto(
  val stage: Stage,
  val quantity: Int
)