package pl.oaza.waw.rekolekcje.api.retreats.query

import pl.oaza.waw.rekolekcje.api.retreats.domain.Retreat
import pl.oaza.waw.rekolekcje.api.retreats.domain.RetreatReadRepository
import pl.oaza.waw.rekolekcje.api.retreats.domain.RetreatNotFoundException

open class RetreatQueryFacadeImpl internal constructor(
  private val retreatReadRepository: RetreatReadRepository
) : RetreatQueryFacade {

  override fun findAll(): List<Retreat> {
    return retreatReadRepository.findAll()
  }

  override fun findOneById(id: Long): Retreat {
    return retreatReadRepository.findById(id) ?: throw RetreatNotFoundException(id)
  }

}
