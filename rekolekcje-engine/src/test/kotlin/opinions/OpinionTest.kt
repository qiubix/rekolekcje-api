package pl.oaza.waw.rekolekcje.api.opinions

import io.mockk.clearAllMocks
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.BDDAssertions.then
import org.assertj.core.api.BDDAssertions.thenThrownBy
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import pl.oaza.waw.rekolekcje.api.members.domain.Member
import pl.oaza.waw.rekolekcje.api.shared.exceptions.MemberNotFoundException
import pl.oaza.waw.rekolekcje.api.members.domain.minimalMember
import pl.oaza.waw.rekolekcje.api.members.infrastructure.InMemoryMemberReadRepository
import pl.oaza.waw.rekolekcje.api.members.infrastructure.encryption.MembersEncryptionService
import pl.oaza.waw.rekolekcje.api.members.query.MembersQueryFacadeImpl
import pl.oaza.waw.rekolekcje.api.opinions.domain.Opinion
import pl.oaza.waw.rekolekcje.api.opinions.endpoint.SubmitOpinionRequest
import pl.oaza.waw.rekolekcje.api.opinions.fixture.OpinionsTestData
import pl.oaza.waw.rekolekcje.api.opinions.fixture.SubmitterTestData
import pl.oaza.waw.rekolekcje.api.opinions.infrastructure.InMemoryOpinionsReadRepository
import pl.oaza.waw.rekolekcje.api.opinions.infrastructure.InMemoryOpinionsWriteRepository
import pl.oaza.waw.rekolekcje.api.opinions.infrastructure.OpinionsConfiguration
import pl.oaza.waw.rekolekcje.api.tools.date.toInstant
import java.time.Clock
import java.time.Instant
import java.time.LocalDate
import java.time.ZoneId
import java.time.ZoneOffset.UTC

class OpinionTest {

  private val now: Instant = "2020-03-20T16:45:42.00Z".toInstant()
  private val fixedClock = Clock.fixed(now, ZoneId.of("UTC"))

  private val members = hashMapOf<Long, Member>()
  private val memberReadRepository = InMemoryMemberReadRepository(members)
  private val membersQueryFacade = MembersQueryFacadeImpl(memberReadRepository)
  private val membersEncryptionService = MembersEncryptionService("439AC7B3DCB9C7C6BB6F466C4CACCC39", "hakunamatata1234%^&*")

  private val opinions = hashMapOf<Long, Opinion>()
  private val writeRepository = InMemoryOpinionsWriteRepository(opinions)
  private val commandFacade = OpinionsConfiguration().opinionsCommandFacade(
      opinionsWriteRepository = writeRepository,
      clock = fixedClock,
      membersQueryFacade = membersQueryFacade,
      membersEncryptionService = membersEncryptionService
  )

  private val readRepository = InMemoryOpinionsReadRepository(opinions)
  private val queryFacade = OpinionsConfiguration().opinionsQueryFacade(readRepository)

  private val minimalMember = minimalMember()

  private fun arrange() = Arrange()

  private fun encryptMemberId(memberId: Long): String = membersEncryptionService.encryptMemberId(memberId)

  @BeforeEach
  fun setup() {
    clearStore()

    clearAllMocks()
  }

  private fun clearStore() {
    opinions.clear()
    members.clear()
  }

  @Test
  fun `should submit correctly`() {
    // given
    arrange().`exists member with id`(10L)

    val submitRequest = OpinionsTestData.createSubmitRequest(encryptMemberId(10L))

    // when
    commandFacade.submit(submitRequest)

    // then
    val savedOpinion = queryFacade.getById(1L)

    submitRequest `corresponds to` savedOpinion
  }

  @Test
  fun `should not submit when member not exist`() {
    // given
    val submitRequest = OpinionsTestData
        .createSubmitRequest(encryptMemberId(5L))

    // when && then
    thenThrownBy {
      commandFacade.submit(submitRequest)
    }.isInstanceOf(MemberNotFoundException::class.java)
  }

  @Test
  fun `should have correct creation date when submitted`() {
    // given
    arrange().`exists member with id`(5L)

    // when
    val submitRequest = OpinionsTestData
        .createSubmitRequest(encryptMemberId(5L))

    commandFacade.submit(submitRequest)

    // then
    val actualDate = opinions.values.first().createdDate

    then(actualDate).isEqualTo(LocalDate.ofInstant(now, UTC))
  }

  @Test
  fun `should find all opinions for a particular member`() {
    // given
    arrange().`exists member with id`(5L)
    arrange().`exists member with id`(6L)
    arrange().`exists opinion for member`(5L, "5: First opinion", "Animator 1")
    arrange().`exists opinion for member`(5L, "5: Second opinion", "Animator 2")
    arrange().`exists opinion for member`(6L, "6: First opinion", "Animator 1")

    // when
    val foundOpinions = queryFacade.getAllOpinionsForMember(5L)

    // then
    val expectedOpinions = opinions.values.filter { it.content.contains("5:") }
    assertThat(foundOpinions).isEqualTo(expectedOpinions)
  }

  inner class Arrange {
    fun `exists member with id`(id: Long): Arrange {
      members[id] = minimalMember.withId(id)
      return this
    }
    
    fun `exists opinion for member`(memberId: Long, content: String, submitterName: String) : Arrange {
      val submitRequest = OpinionsTestData.createSubmitRequest(
        encryptedMemberId = encryptMemberId(memberId),
        content = content,
        submitter = SubmitterTestData.createSubmitter(fullName = submitterName)
      )
      commandFacade.submit(submitRequest)
      return this
    }
  }

  private infix fun SubmitOpinionRequest.`corresponds to`(opinion: Opinion) {
    then(membersEncryptionService.decryptMemberId(this.encryptedMemberId)).isEqualTo(opinion.memberId)
    then(this.content).isEqualTo(opinion.content)
    then(this.submitter.toDomain()).isEqualTo(opinion.submitter)
  }
}
