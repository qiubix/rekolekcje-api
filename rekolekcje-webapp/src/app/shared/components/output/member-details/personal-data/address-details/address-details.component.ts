import { Component, Input } from '@angular/core';
import { Address } from '@shared/models/member-details.model';

@Component({
  selector: 'reko-address-details',
  templateUrl: './address-details.component.html',
  styleUrls: ['./address-details.component.scss']
})
export class AddressDetailsComponent {
  @Input() address: Address;
}
