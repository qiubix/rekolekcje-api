package pl.oaza.waw.rekolekcje.api.members.endpoint

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory
import pl.oaza.waw.rekolekcje.api.members.domain.Address
import pl.oaza.waw.rekolekcje.api.members.domain.Deuterocatechumenate
import pl.oaza.waw.rekolekcje.api.members.domain.Experience
import pl.oaza.waw.rekolekcje.api.members.domain.HealthReport
import pl.oaza.waw.rekolekcje.api.members.domain.HistoricalRetreat
import pl.oaza.waw.rekolekcje.api.members.domain.Kwc
import pl.oaza.waw.rekolekcje.api.members.domain.KwcStatus
import pl.oaza.waw.rekolekcje.api.members.domain.Member
import pl.oaza.waw.rekolekcje.api.members.domain.MemberContact
import pl.oaza.waw.rekolekcje.api.members.domain.OtherRetreats
import pl.oaza.waw.rekolekcje.api.members.domain.OtherSkills
import pl.oaza.waw.rekolekcje.api.members.domain.PersonalData
import pl.oaza.waw.rekolekcje.api.members.domain.PersonalData.SchoolType
import pl.oaza.waw.rekolekcje.api.members.domain.Pesel
import pl.oaza.waw.rekolekcje.api.shared.domain.Contact
import pl.oaza.waw.rekolekcje.api.shared.value.Sex
import pl.oaza.waw.rekolekcje.api.shared.value.Stage
import pl.oaza.waw.rekolekcje.api.tools.date.getAgeFromBirthDate
import java.time.LocalDate

class MemberDtoConverterTest {

  private val minimalDetails = MemberDetails(
    id = 321L,
    personalData = PersonalDataValue(
      firstName = "Minimal",
      lastName = "Member",
      pesel = "90042300004",
      birthDate = "23.04.1990",
      sex = Sex.FEMALE,
      age = getAgeFromBirthDate("1990-04-23")
    )
  )

  private val minimalMember = Member(
    id = 321L,
    personalData = PersonalData(
      contactInfo = MemberContact(
        firstName = "Minimal",
        lastName = "Member"
      ),
      pesel = Pesel("90042300004")
    )
  )


  data class DtoConversionTestCase(
    val testCaseName: String,
    val domainEntity: Member,
    val dto: MemberDetails
  )

  private val testCases = listOf(
    DtoConversionTestCase("minimal member", minimalMember, minimalDetails),
    DtoConversionTestCase("personal data",
      minimalMember.copy(personalData = PersonalData(
        contactInfo = MemberContact(
          firstName = "John",
          lastName = "Smith",
          email = "john.smith@mail.com",
          phoneNumber = "998877665"
        ),
        pesel = Pesel("90042300004"),
        address = Address(
          street = "Broadway",
          streetNumber = "987/989",
          flatNumber = "13c",
          postalCode = "12-654",
          city = "New York"
        ),
        parishId = 1L,
        communityId = 2L,
        mother = Contact(fullName = "Mary Smith"),
        father = Contact(fullName = "Jake Smith Sr."),
        placeOfBirth = "Los Angeles",
        christeningDate = LocalDate.of(1981, 2, 13),
        emergencyContact = Contact(
          fullName = "Uncle Bob",
          phoneNumber = "444555666"
        ),
        schoolYear = "1 LO",
        schoolType = SchoolType.HIGH_SCHOOL,
        nameDay = "24.04"
      )),
      minimalDetails.copy(personalData = PersonalDataValue(
        firstName = "John",
        lastName = "Smith",
        pesel = "90042300004",
        parishId = 1L,
        communityId = 2L,
        email = "john.smith@mail.com",
        phoneNumber = "998877665",
        address = AddressValue(
          streetName = "Broadway",
          streetNumber = "987/989",
          flatNumber = "13c",
          postalCode = "12-654",
          city = "New York"
        ),
        mother = ContactInfo(fullName = "Mary Smith"),
        father = ContactInfo(fullName = "Jake Smith Sr."),
        birthDate = "23.04.1990",
        sex = Sex.FEMALE,
        age = getAgeFromBirthDate("1990-04-23"),
        placeOfBirth = "Los Angeles",
        christeningDate = LocalDate.of(1981, 2, 13),
        emergencyContact = ContactInfo(
          fullName = "Uncle Bob",
          phoneNumber = "444555666"
        ),
        schoolYear = "1 LO",
        schoolType = SchoolType.HIGH_SCHOOL,
        nameDay = "24.04"
      ))
    ),
    DtoConversionTestCase("health report",
      minimalMember.copy(healthReport = HealthReport(
        currentTreatment = "Standard treatment for diabetes",
        medications = "Insuline",
        allergies = "Peanuts, lactose",
        cannotHike = true,
        hasMotionSickness = true,
        illnessHistory = "Hib implant injected",
        mentalDisorders = "None",
        other = "May be very weird sometimes"
      )),
      minimalDetails.copy(healthReport = HealthReportValue(
        currentTreatment = "Standard treatment for diabetes",
        medications = "Insuline",
        allergies = "Peanuts, lactose",
        cannotHike = true,
        hasMotionSickness = true,
        hikingContraindications = "Hib implant injected",
        mentalDisorders = "None",
        other = "May be very weird sometimes"
      ))
    ),
    DtoConversionTestCase("experience",
      minimalMember.copy(experience = Experience(
        kwc = Kwc(
          kwcSince = LocalDate.of(1995, 1, 1),
          kwcStatus = KwcStatus.CANDIDATE
        ),
        numberOfCommunionDays = 3,
        numberOfPrayerRetreats = 5,
        formationInSmallGroup = true,
        formationMeetingsInMonth = 3,
        leadingGroupToFormationStage = "ONŻ I",
        deuterocatechumenate = Deuterocatechumenate(
          year = 2016,
          stepsTaken = 4,
          stepsPlannedThisYear = 6,
          celebrationsTaken = 5,
          celebrationsPlannedThisYear = 5,
          missionCelebrationYear = 2016,
          callByNameCelebrationYear = 2017,
          holyTriduumYear = 2017
        ),
        choir = false,
        musicalTalents = "none so far",
        altarBoy = true,
        flowerGirl = false,
        otherSkills = listOf(
          OtherSkills.DRIVERS_LICENCE,
          OtherSkills.PLAYS_INSTRUMENT
        ).joinToString(",") { it.name },
        numberOfAnimatorDays = 0,
        diakonia = "DOR",
        animator = Contact(fullName = "Jason Animator"),
        historicalRetreats = listOf(
          HistoricalRetreat(stage = Stage.ONZ_II, year = "2001"),
          HistoricalRetreat(stage = Stage.OR_II, year = "2002"),
          HistoricalRetreat(stage = Stage.ORAE, year = "2003")
        ),
        animatorsExperience = setOf(
          RetreatAsAnimatorDto(Stage.ODB, 2),
          RetreatAsAnimatorDto(Stage.ONZ_I, 1)
        ).joinToString(",") { "${it.stage.name}:${it.quantity}" },
        otherRetreats = setOf(
          OtherRetreats.ORAE,
          OtherRetreats.ANIMATORS_SCHOOL
        ).joinToString(",") { it.name },
        communityFunctions = "Leader"
      )),
      minimalDetails.copy(experience = ExperienceValue(
        kwc = KwcDto(
          status = KwcStatus.CANDIDATE,
          year = 1995
        ),
        formationInCommunity = FormationInCommunityValue(
          numberOfCommunionDays = 3,
          numberOfPrayerRetreats = 5,
          formationInSmallGroup = true,
          formationMeetingsInMonth = 3
        ),
        animator = ContactInfo(fullName = "Jason Animator"),
        deuterocatechumenate = DeuterocatechumenateValue(
          celebrationsTaken = 5,
          celebrationsPlannedThisYear = 5,
          stepsTaken = 4,
          stepsPlannedThisYear = 6,
          year = 2016,
          missionCelebrationYear = 2016,
          callByNameCelebrationYear = 2017,
          holyTriduumYear = 2017
        ),
        talents = TalentsValue(
          choir = false,
          musicalTalents = "none so far",
          altarBoy = true,
          flowerGirl = false
        ),
        historicalRetreats = setOf(
          RetreatTurnDto(stage = Stage.ONZ_II, year = 2001),
          RetreatTurnDto(stage = Stage.OR_II, year = 2002),
          RetreatTurnDto(stage = Stage.ORAE, year = 2003)
        ),
        experienceAsAnimator = ExperienceAsAnimatorValue(
          numberOfAnimatorDays = 0,
          diakonia = "DOR",
          leadingGroupToFormationStage = "ONŻ I",
          retreatsAsAnimator = setOf(
            RetreatAsAnimatorDto(Stage.ODB, 2),
            RetreatAsAnimatorDto(Stage.ONZ_I, 1)
          ),
          otherSkills = setOf(
            OtherSkills.DRIVERS_LICENCE,
            OtherSkills.PLAYS_INSTRUMENT
          ),
          otherRetreats = setOf(
            OtherRetreats.ORAE,
            OtherRetreats.ANIMATORS_SCHOOL
          ),
          communityFunctions = "Leader"
        )
      ))
    )
  )

  @TestFactory
  fun `should convert dto to domain entity`() =
    testCases.map { testCase ->
      DynamicTest.dynamicTest(testCase.testCaseName) {
        val convertedDomainEntity = fromDto(testCase.dto)
        assertThat(convertedDomainEntity).isEqualTo(testCase.domainEntity)
      }
    }

  @TestFactory
  fun `should convert domain entity to dto`() =
    testCases.map { testCase ->
      DynamicTest.dynamicTest(testCase.testCaseName) {
        val dto = toDetailsDto(testCase.domainEntity)
        assertThat(dto).isEqualTo(testCase.dto)
      }
    }
}

