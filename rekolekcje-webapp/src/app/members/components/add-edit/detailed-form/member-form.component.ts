import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  AfterViewChecked,
  ChangeDetectorRef
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MemberDetails } from '@shared/models';

@Component({
  selector: 'participiant-form',
  templateUrl: './member-form.component.html',
  styleUrls: ['./member-form.component.scss']
})
export class MemberFormComponent implements OnInit, AfterViewChecked {
  @Input()
  set member(member: MemberDetails) {
    this._member = member;
  }

  @Input() isAdult: boolean = false;

  @Output() formOutput = new EventEmitter<MemberDetails>();

  form: FormGroup;

  currentStep = 0;

  private _member: MemberDetails;

  constructor(private fb: FormBuilder, private cd: ChangeDetectorRef) {}

  ngOnInit(): void {
    if (!this.member) {
      this._member = {};
    }

    // const stage = this.determineStage(this.member.currentApplication.retreatId);
    // this.store.dispatch(new MembersSharedActions.SetCurrentApplicationStage(null));

    this.form = this.fb.group({});
  }

  ngAfterViewChecked(): void {
    this.cd.detectChanges();
  }

  get member() {
    return this._member;
  }

  cancel(): void {
    this.formOutput.emit();
  }

  submit(): void {
    if (this.form.invalid) {
      this.form.markAsDirty();
      return;
    }
    const member: MemberDetails = Object.assign({}, this.member, this.form.value);
    // if (!member.experience.historicalRetreats) {
    //   member.experience.historicalRetreats = [];
    // }
    console.log('Emitting form value: ', member);
    this.formOutput.emit(member);
  }

  setCurrentStep(index: number) {
    this.currentStep = index;
  }

  onSubFormInitialized(name: string, subForm: FormGroup) {
    this.form.addControl(name, subForm);
  }
}
