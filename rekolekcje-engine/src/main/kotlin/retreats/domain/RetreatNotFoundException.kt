package pl.oaza.waw.rekolekcje.api.retreats.domain


class RetreatNotFoundException(id: Long = 0L) : RuntimeException(String.format(MESSAGE_ID, id)) {

  companion object {
    private const val MESSAGE_ID = "No retreat found with this id: %d."
  }
}
