import { Action } from '@ngrx/store';
import {
  ApplicationSubmission,
  ApplicationSummary,
  AssignAnimatorRequest,
  AssignAnimatorResponse,
  ChangeApplicationStatusRequest,
  ChangeApplicationStatusResponse,
  RetreatApplication,
  VerificationStatistics
} from '@verification/model';
import { PaymentStatus } from '@shared/models';

export namespace VerificationActions {
  export const types = {
    GetStatistics: '[Verification] Get statistics',
    GetStatisticsSuccess: '[Verification] Get statistics success',
    VerificationActionFail: '[Verification] Action failed',
    GetPendingApplications: '[Verification] Get all pending applications',
    GetPendingApplicationsSuccess: '[Verification] Get all pending applications success',
    GetVerifiedApplications: '[Verification] Get all verified applications',
    GetVerifiedApplicationsSuccess: '[Verification] Get all verified applications success',
    GetSelectedApplication: '[Verification] Get selected animator application',
    GetSelectedApplicationSuccess: '[Verification] Get selected animator application success',
    SaveApplication: '[Verification] Save application',
    SaveApplicationSuccess: '[Verification] Save application success',
    DeleteApplication: '[Verification] Delete application',
    DeleteApplicationSuccess: '[Verification] Delete application success',
    ChangeApplicationStatus: '[Verification] Change application status',
    ChangeApplicationStatusSuccess: '[Verification] Change application status success',
    ChangePaymentStatus: '[Verification] Change payment status',
    ChangePaymentStatusSuccess: '[Verification] Change payment status success',
    AssignAnimator: '[Verification] Assign animator',
    AssignAnimatorSuccess: '[Verification] Assign animator success'
  };

  export class VerificationActionFail implements Action {
    type = types.VerificationActionFail;
    constructor(public payload: any) {}
  }

  export class GetStatistics implements Action {
    type = types.GetStatistics;
    constructor() {}
  }

  export class GetStatisticsSuccess implements Action {
    type = types.GetStatisticsSuccess;
    constructor(public payload: VerificationStatistics) {}
  }

  export class GetPendingApplications implements Action {
    type = types.GetPendingApplications;
    constructor(public payload?: any) {}
  }

  export class GetPendingApplicationsSuccess implements Action {
    type = types.GetPendingApplicationsSuccess;
    constructor(public payload: ApplicationSummary[]) {}
  }

  export class GetVerifiedApplications implements Action {
    type = types.GetVerifiedApplications;
    constructor(public payload?: any) {}
  }

  export class GetVerifiedApplicationsSuccess implements Action {
    type = types.GetVerifiedApplicationsSuccess;
    constructor(public payload: ApplicationSummary[]) {}
  }

  export class GetSelectedApplication implements Action {
    type = types.GetSelectedApplication;
    constructor(public payload: { id: number }) {}
  }

  export class GetSelectedApplicationSuccess implements Action {
    type = types.GetSelectedApplicationSuccess;
    constructor(public payload: RetreatApplication) {}
  }

  export class SaveApplication implements Action {
    type = types.SaveApplication;
    constructor(public payload: { application: ApplicationSubmission; update: boolean }) {}
  }

  export class SaveApplicationSuccess implements Action {
    type = types.SaveApplicationSuccess;
    constructor(public payload: RetreatApplication) {}
  }

  export class DeleteApplication implements Action {
    type = types.DeleteApplication;
    constructor(public payload: number) {}
  }

  export class DeleteApplicationSuccess implements Action {
    type = types.DeleteApplicationSuccess;
    constructor(public payload: any) {}
  }

  export class ChangeApplicationStatus implements Action {
    type = types.ChangeApplicationStatus;
    constructor(public payload: ChangeApplicationStatusRequest) {}
  }

  export class ChangeApplicationStatusSuccess implements Action {
    type = types.ChangeApplicationStatusSuccess;
    constructor(public payload: ChangeApplicationStatusResponse) {}
  }

  export class ChangePaymentStatus implements Action {
    type = types.ChangePaymentStatus;
    constructor(public payload: PaymentStatus) {}
  }

  export class ChangePaymentStatusSuccess implements Action {
    type = types.ChangePaymentStatusSuccess;
    constructor(public payload: PaymentStatus) {}
  }

  export class AssignAnimator implements Action {
    type = types.AssignAnimator;
    constructor(public payload: AssignAnimatorRequest) {}
  }

  export class AssignAnimatorSuccess implements Action {
    type = types.AssignAnimatorSuccess;
    constructor(public payload: AssignAnimatorResponse) {}
  }
}
