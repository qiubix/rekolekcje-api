package pl.oaza.waw.rekolekcje.api.application.domain

enum class AnimatorRole {
  MUSICIAN,
  LITURGIST,
  GROUP,
  QUARTERMASTER
}