package pl.oaza.waw.rekolekcje.api.shared.domain

interface EntityWithId {

  fun getId(): Long?

  fun withId(id: Long): EntityWithId

}