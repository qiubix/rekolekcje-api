import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { MemberSummary } from '@members/models/member-summary.model';
import { select, Store } from '@ngrx/store';
import { Members } from '@members/state/members-reducer';
import { AppSelectors } from '@core/store/app-selectors';
import { Router } from '@angular/router';
import { MembersSharedActions } from '@members/state/members-actions';

@Component({
  selector: 'reko-members-waiting-for-opinion',
  templateUrl: './members-waiting-for-opinion.component.html',
  styleUrls: ['./members-waiting-for-opinion.component.scss']
})
export class MembersWaitingForOpinionComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  members$: Observable<MemberSummary[]>;
  membersLoading$: Observable<boolean>;

  displayedColumns = ['firstName', 'lastName', 'community', 'options'];

  constructor(private router: Router, private store: Store<Members.State>) {}

  ngOnInit() {
    this.store.dispatch(new MembersSharedActions.LoadMembersList());
    this.members$ = this.store.pipe(select(AppSelectors.getMembersList));
    this.membersLoading$ = this.store.pipe(select(AppSelectors.getMembersLoading));
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  onSelect(member: MemberSummary): void {
    this.router.navigate(['opinions', `${member.id}`]);
  }
}
