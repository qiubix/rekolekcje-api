package pl.oaza.waw.rekolekcje.api.parish.domain

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import pl.oaza.waw.rekolekcje.api.parish.dto.RegionNotFoundException
import pl.oaza.waw.rekolekcje.api.parish.dto.RegionPayload

internal class RegionTest {

  private val firstRegion = RegionPayload(12L, "First region")
  private val secondRegion = RegionPayload(32L, "Second region")

  private val facade = RegionsConfiguration().regionsService()

  @AfterEach
  internal fun tearDown() {
    facade.delete(firstRegion.id!!, secondRegion.id!!)
  }

  @Test
  fun `should add new`() {
    facade.save(firstRegion)

    val savedRegions = facade.findAll()
    assertThat(savedRegions).hasSize(1)
    assertThat(savedRegions).contains(firstRegion)
  }

  @Test
  fun `should assign id on creation`() {
    val regionWithoutId = RegionPayload(name = "Some region")
    val savedRegion = facade.save(regionWithoutId)

    assertThat(savedRegion.id)
      .isNotNull()
      .isNotZero()
    val allSavedRegions = facade.findAll()
    assertThat(allSavedRegions)
      .hasSize(1)
      .contains(savedRegion)
      .usingElementComparatorIgnoringFields("id")
      .contains(regionWithoutId)
  }

  @Test
  fun `should get all`() {
    facade.save(firstRegion)
    facade.save(secondRegion)

    val foundRegions = facade.findAll()
    assertThat(foundRegions).hasSize(2)
    assertThat(foundRegions).containsOnlyOnce(firstRegion, secondRegion)
  }

  @Test
  fun `should get one`() {
    facade.save(firstRegion)
    facade.save(secondRegion)

    val foundRegion = facade.findOne(secondRegion.id!!)

    assertThat(foundRegion).isEqualTo(secondRegion)
  }

  @Test
  fun `should throw exception when none is found`() {
    facade.save(firstRegion)

    assertThatExceptionOfType(RegionNotFoundException::class.java)
      .isThrownBy { facade.findOne(secondRegion.id!!) }
      .withMessageContaining("id " + secondRegion.id)
  }

  @Test
  fun `should delete`() {
    facade.save(firstRegion)
    facade.save(secondRegion)

    facade.delete(secondRegion.id!!)

    val remainingRegions = facade.findAll()
    assertThat(remainingRegions).hasSize(1)
      .doesNotContain(secondRegion)
  }

  @Test
  fun `should update`() {
    facade.save(firstRegion)
    facade.save(secondRegion)
    val regionWithUpdatedData = RegionPayload(id = firstRegion.id, name = "UpdatedName")

    val updatedRegion = facade.save(regionWithUpdatedData)

    assertThat(updatedRegion).isEqualTo(regionWithUpdatedData)
    val allRegions = facade.findAll()
    assertThat(allRegions).hasSize(2)
      .containsOnlyOnce(regionWithUpdatedData)
  }
}
