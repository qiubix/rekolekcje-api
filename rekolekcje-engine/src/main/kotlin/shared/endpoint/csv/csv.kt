package pl.oaza.waw.rekolekcje.api.shared.endpoint.csv

data class CsvConversionError(
  val row: String,
  val problematicFields: List<String>
)

