package pl.oaza.waw.rekolekcje.api.members.domain

import javax.persistence.Embeddable

@Embeddable
data class Address(
  val street: String? = null,
  val streetNumber: String? = null,
  val flatNumber: String? = null,
  val postalCode: String? = null,
  val city: String? = null
)
