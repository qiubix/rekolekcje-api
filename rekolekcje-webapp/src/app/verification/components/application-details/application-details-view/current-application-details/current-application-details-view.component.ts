import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { AnimatorAssignment, RetreatApplication } from '@verification/model';
import { MatListOption, MatSelectionList } from '@angular/material/list';
import { SelectionModel } from '@angular/cdk/collections';

@Component({
  selector: 'reko-current-application-details',
  templateUrl: './current-application-details-view.component.html',
  styleUrls: ['./current-application-details-view.component.scss']
})
export class CurrentApplicationDetailsViewComponent implements OnInit {
  @Input() application: RetreatApplication;

  @Output() assign = new EventEmitter<AnimatorAssignment>();

  @ViewChild(MatSelectionList)
  private selectionList: MatSelectionList;

  private _selectedRole: string = null;
  private _selectedRetreat: number = null;

  constructor() {}

  ngOnInit() {
    this.selectionList.selectedOptions = new SelectionModel<MatListOption>(false);
    this._selectedRole = this.application.selectedRole;
  }

  assignApplication() {
    const info: AnimatorAssignment = {
      selectedRetreat: this._selectedRetreat,
      selectedRole: this._selectedRole
    };
    console.log('Assigning application to retreat turn: ', info);
    this.assign.emit(info);
  }

  isReadyToBeAssigned(): boolean {
    return this._selectedRole != null && this._selectedRetreat != null;
  }

  isRoleSelected(role: string): boolean {
    return this._selectedRole === role;
  }

  onSelectRetreat() {
    this._selectedRetreat = this.selectionList.selectedOptions.selected[0].value;
  }

  toggleRoleSelection(role: string) {
    if (this.isRoleSelected(role)) {
      this._selectedRole = null;
    } else {
      this._selectedRole = role;
    }
  }
}
