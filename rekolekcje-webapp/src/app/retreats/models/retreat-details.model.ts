import { Stage } from 'app/shared/models/stage.model';
import { Turns } from 'app/shared/models/turn.model';

export interface RetreatDetails {
  id?: number;
  turn?: Turns;
  stage?: Stage;
  localization?: string;
  startDate?: string;
  endDate?: string;
  priestName?: string;
  priestPhoneNumber?: string;
  moderatorName?: string;
  moderatorPhoneNumber?: string;
  status?: string;
  statistics?: RetreatStatistics;
  participants?: RetreatMember[];
  animators?: RetreatMember[];
}

export interface RetreatMember {
  memberId?: number;
  firstName?: string;
  lastName?: string;
  sex?: string;
  age?: number;
  paymentStatus?: string;
  animator?: boolean;
}

export interface RetreatStatistics {
  participantsAmount?: number;
  malesParticipants?: number;
  femalesParticipants?: number;
  averageAge?: number;
  underageParticipants?: number;
}
