package pl.oaza.waw.rekolekcje.api.opinions.infrastructure.persistence

import org.springframework.data.repository.Repository
import pl.oaza.waw.rekolekcje.api.opinions.domain.Opinion
import pl.oaza.waw.rekolekcje.api.opinions.domain.OpinionsReadRepository
import pl.oaza.waw.rekolekcje.api.opinions.domain.OpinionsWriteRepository

internal interface JpaOpinionsWriteRepository : OpinionsWriteRepository, Repository<Opinion, Long>
internal interface JpaOpinionsReadRepository : OpinionsReadRepository, Repository<Opinion, Long>