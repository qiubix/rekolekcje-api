import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Members } from '../../../state/members-reducer';
import { MembersSharedActions } from '../../../state/members-actions';
import { AppSelectors } from '@core/store/app-selectors';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { MemberDetails } from '@shared/models';
import { Observable } from 'rxjs/internal/Observable';

@Component({
  selector: 'member-add-edit',
  templateUrl: './member-add-edit.component.html',
  styleUrls: ['./member-add-edit.component.scss']
})
export class MemberAddEditComponent implements OnInit {
  editing: boolean;
  memberToEdit$: Observable<MemberDetails>;

  constructor(
    private store: Store<Members.State>,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private translate: TranslateService
  ) {
    this.editing = this.activatedRoute.snapshot.data['editing'];
  }

  ngOnInit(): void {
    this.memberToEdit$ = this.store.pipe(select(AppSelectors.getSelectedMember));
  }

  onFormSubmit(member: MemberDetails): void {
    if (member) {
      if (member.id) {
        this.store.dispatch(new MembersSharedActions.UpdateMember(member));
      } else {
        this.store.dispatch(new MembersSharedActions.CreateMember(member));
      }
    }
    this.router.navigateByUrl(`/members`);
  }

  get title() {
    return this.editing
      ? this.translate.get('members.form.edit')
      : this.translate.get('members.form.add');
  }
}
