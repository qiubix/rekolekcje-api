import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from '@shared/components/page-not-found/page-not-found.component';
import { AppComponent } from './app.component';
import { AuthGuard } from '@auth/guards/auth-guard.service';
import { MainComponent } from '@core/main/main.component';
import { HasCorrectRoleGuard } from '@auth/guards/has-correct-role.guard';

const routes: Routes = [
  {
    path: '',
    component: AppComponent,
    children: [
      {
        path: '',
        component: MainComponent,
        canActivateChild: [AuthGuard, HasCorrectRoleGuard],
        children: [
          {
            path: 'home',
            loadChildren: () => import('./home/home.module').then((x) => x.HomeModule),
          },
          {
            path: 'registration',
            loadChildren: () =>
              import('./registration/registration.module').then((x) => x.RegistrationModule),
          },
          {
            path: 'verification',
            loadChildren: () =>
              import('./verification/verification.module').then((x) => x.VerificationModule),
          },
          {
            path: 'moderation',
            loadChildren: () =>
              import('./moderation/moderation.module').then((x) => x.ModerationModule),
          },
          {
            path: 'members',
            loadChildren: () => import('./members/members.module').then((x) => x.MembersModule),
          },
          {
            path: 'community',
            loadChildren: () =>
              import('./diocese/community/community.module').then((x) => x.CommunityModule),
          },
          {
            path: 'parish',
            loadChildren: () =>
              import('./diocese/parish/parish.module').then((x) => x.ParishModule),
          },
          {
            path: 'retreats',
            loadChildren: () => import('./retreats/retreats.module').then((x) => x.RetreatsModule),
          },
          {
            path: '',
            redirectTo: 'home',
            pathMatch: 'full',
          },
        ],
      },
      {
        path: '',
        redirectTo: '',
        pathMatch: 'full',
      },
      {
        path: 'auth',
        loadChildren: () => import('./auth/auth.module').then((x) => x.AuthModule),
      },
      {
        path: 'opinions',
        component: MainComponent,
        loadChildren: () => import('./opinions/opinions.module').then((x) => x.OpinionsModule),
      },
      {
        path: 'users',
        component: MainComponent,
        loadChildren: () => import('./users/users.module').then((x) => x.UsersModule),
      },
      {
        path: '**',
        component: PageNotFoundComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
