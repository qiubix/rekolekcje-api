#!/bin/bash

green=`tput setaf 2`
reset=`tput sgr0`

SCRIPT_DIR=$(dirname `realpath $0`)

if [ "$1" = "start" ] ; then

  echo "Building Postgres container...."
  docker-compose -f ${SCRIPT_DIR}/docker/docker-compose-database.yml up -d
  echo "DB can be accesed via localhost:5430"

elif [ "$1" = "stop" ] ; then

  echo "Stopping database container..."
  docker-compose -f ${SCRIPT_DIR}/docker/docker-compose-database.yml stop

elif [ "$1" = "down" ] ; then

  echo "Database container is going down..."
  docker-compose -f ${SCRIPT_DIR}/docker/docker-compose-database.yml down

elif [ "$1" = "wipe" ] ; then

  echo "Wiping database container and postres image..."
  docker-compose -f ${SCRIPT_DIR}/docker/docker-compose-database.yml down -rmi all

else
    echo "Valid actions are:"
    echo "  - ${green}start${reset} - start database container"
    echo "  - ${green}stop${reset}  - stop database container"
    echo "  - ${green}down${reset}  - stop and remove database container without removing image"
    echo "  - ${green}wipe${reset}  - stop and remove database container and image"
  exit 0

fi
