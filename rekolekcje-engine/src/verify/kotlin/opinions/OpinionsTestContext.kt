package pl.oaza.waw.rekolekcje.api.opinions

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.jdbc.core.JdbcTemplate
import pl.oaza.waw.rekolekcje.api.members.api.MembersApiBehaviour
import pl.oaza.waw.rekolekcje.api.members.query.OpinionLinkQueryFacade
import pl.oaza.waw.rekolekcje.api.opinions.api.OpinionApiBehaviour
import pl.oaza.waw.rekolekcje.api.opinions.storage.OpinionsDatabase
import pl.oaza.waw.rekolekcje.api.shared.RestApi

@Configuration
@Profile("test")
class OpinionsTestContext {

  @Bean
  fun opinionsDatabase(jdbcTemplate: JdbcTemplate) = OpinionsDatabase(jdbcTemplate)

  @Bean
  fun opinionsApiBehaviour(restApi: RestApi) = OpinionApiBehaviour(restApi)
}
