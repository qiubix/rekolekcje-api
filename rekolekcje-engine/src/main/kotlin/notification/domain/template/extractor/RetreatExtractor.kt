package pl.oaza.waw.rekolekcje.api.notification.domain.template.extractor

import pl.oaza.waw.rekolekcje.api.application.domain.RetreatApplication
import pl.oaza.waw.rekolekcje.api.retreats.query.RetreatQueryFacade

internal class RetreatExtractor(
	private val application: RetreatApplication,
	private val retreatQueryFacade: RetreatQueryFacade
) : Extractor(ExtractorType.RETREAT) {
	override fun extractValue(): String {
		val retreat = retreatQueryFacade.findOneById(application.retreatId ?: return "Turnus nie wybrany")
		return if (retreat.stage != null) {
			retreat.stage.description + ", turnus " + retreat.turn.toString() +
					": " + retreat.startDate.toString() + " - " + retreat.endDate.toString()
		}
		else {
			"Turnus nie wybrany"
		}
	}
}
