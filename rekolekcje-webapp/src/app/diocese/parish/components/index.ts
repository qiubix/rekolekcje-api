import { ParishComponent } from './_root/parish.component';
import { ParishListComponent } from './list/parish-list.component';
import { ParishAddEditDialogComponent } from './parish-dialog/parish-dialog.component';
import { ParishFormComponent } from './parish-form/parish-form.component';

export const components: any[] = [
  ParishComponent,
  ParishListComponent,
  ParishAddEditDialogComponent,
  ParishFormComponent
];

export * from './_root/parish.component';
export * from './parish-dialog/parish-dialog.component';
