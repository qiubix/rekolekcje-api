package pl.oaza.waw.rekolekcje.api.shared

import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.web.context.WebApplicationContext

@ExtendWith(SpringExtension::class)
@SpringBootTest(
  webEnvironment = WebEnvironment.RANDOM_PORT,
  classes = [BaseTestContext::class],
  properties = ["spring.datasource.url=jdbc:tc:postgresql:10.6-alpine://rekolekcjedb-test",
    "spring.datasource.username=" + "postgres",
    "spring.datasource.password=" + "postgres",
    "spring.datasource.driver-class-name=org.testcontainers.jdbc.ContainerDatabaseDriver"
  ]
)
@ActiveProfiles("test", "test-local")
@AutoConfigureJsonTesters
@Tag("integration")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
abstract class BaseIntegrationTest {

  @Autowired
  protected lateinit var jdbcTemplate: JdbcTemplate

  @Autowired
  protected lateinit var webApplicationContext: WebApplicationContext

  @Autowired
  protected lateinit var mockMvc: MockMvc

  @Autowired
  protected lateinit var jsonMapper: ObjectMapper

  @Autowired
  protected lateinit var restApi: RestApi
}
