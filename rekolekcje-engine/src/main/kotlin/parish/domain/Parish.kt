package pl.oaza.waw.rekolekcje.api.parish.domain

import pl.oaza.waw.rekolekcje.api.core.ddd.AggregateRoot
import pl.oaza.waw.rekolekcje.api.parish.dto.ParishPayload

@AggregateRoot
interface Parish {

  fun dto(): ParishPayload

}
