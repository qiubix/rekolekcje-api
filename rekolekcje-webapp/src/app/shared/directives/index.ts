import { CurrentStageDirective } from '@shared/directives/current-stage.directive';

export const directives: any[] = [CurrentStageDirective];

export * from '@shared/directives/current-stage.directive';
