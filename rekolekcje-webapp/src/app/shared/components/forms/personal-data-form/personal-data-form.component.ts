import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Address, ContactInfo, PersonalData, SchoolType, Stage } from '@shared/models';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RekoValidators } from '@shared/validators/reko-validators';

@Component({
  selector: 'reko-personal-data-form',
  templateUrl: './personal-data-form.component.html',
  styleUrls: ['./personal-data-form.component.scss']
})
export class PersonalDataFormComponent implements OnInit {
  @Input() personalData: PersonalData;
  @Input() isAdult: Boolean;

  @Output() formReady = new EventEmitter<FormGroup>();

  form: FormGroup;

  stages = Stage;
  schoolType = SchoolType;
  schoolTypeKeys = Object.keys(this.schoolType);

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      firstName: [this.personalData ? this.personalData.firstName : '', Validators.required],
      lastName: [this.personalData ? this.personalData.lastName : '', Validators.required],
      pesel: [
        this.personalData ? this.personalData.pesel : '',
        [Validators.required, Validators.pattern(/^\d{11}$/)]
      ],
      phoneNumber: [
        this.personalData ? this.personalData.phoneNumber : null,
        RekoValidators.isPhoneNumberInvalid
      ],
      email: [this.personalData ? this.personalData.email : null, RekoValidators.isEmailInvalid],
      christeningDate: this.personalData ? this.personalData.christeningDate : null,
      placeOfBirth: this.personalData ? this.personalData.placeOfBirth : null,
      nameDay: this.personalData ? this.personalData.nameDay : null,
      schoolYear: this.personalData ? this.personalData.schoolYear : null,
      schoolType: this.personalData ? this.personalData.schoolType : null,
      parishId: this.personalData ? this.personalData.parishId : null,
      communityId: this.personalData ? this.personalData.communityId : null
    });

    this.formReady.emit(this.form);
  }

  onSubFormInitialized(name: string, subForm: FormGroup) {
    this.form.addControl(name, subForm);
  }

  getPeselErrorMessage(peselFormControl: AbstractControl | null): string {
    if (!peselFormControl) return '';

    if (peselFormControl.hasError('required')) {
      return 'members.form.validation.pesel-required';
    } else if (peselFormControl.hasError('pattern')) {
      return 'members.form.validation.pesel-incorrect';
    } else {
      return '';
    }
  }

  get address(): Address {
    return this.personalData ? this.personalData.address : {};
  }

  get father(): ContactInfo {
    return this.personalData ? this.personalData.father : {};
  }

  get mother(): ContactInfo {
    return this.personalData ? this.personalData.mother : {};
  }

  get emergencyContact(): ContactInfo {
    return this.personalData ? this.personalData.emergencyContact : {};
  }
}
