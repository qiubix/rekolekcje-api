import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Parishes } from '../../store/parish-reducer';
import { select, Store } from '@ngrx/store';
import { ParishSharedActions } from '../../store/parish-actions';
import { AppSelectors } from '../../../../core/store/app-selectors';
import { Parish } from '../../models/parish.model';

@Component({
  selector: 'parish-root',
  templateUrl: './parish.component.html',
  styleUrls: ['./parish.component.css']
})
export class ParishComponent implements OnInit, OnDestroy {
  parishes: Observable<Parish[]>;

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(private store: Store<Parishes.State>) {}

  ngOnInit() {
    this.store.dispatch(new ParishSharedActions.LoadParishList());
    this.parishes = this.store.pipe(select(AppSelectors.getParishList));
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  onAddParish(parish: Parish): void {
    this.store.dispatch(new ParishSharedActions.CreateParish(parish));
  }

  onDeleteParish(id: number): void {
    this.store.dispatch(new ParishSharedActions.DeleteParish(id));
  }

  onEditParish(parish: Parish): void {
    this.store.dispatch(new ParishSharedActions.UpdateParish(parish));
  }
}
