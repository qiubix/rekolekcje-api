package pl.oaza.waw.rekolekcje.api.parish

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.test.context.support.WithMockUser
import pl.oaza.waw.rekolekcje.api.shared.BaseIntegrationTest
import pl.oaza.waw.rekolekcje.api.parish.api.RegionApiBehaviours
import pl.oaza.waw.rekolekcje.api.parish.api.RegionApiExpectations
import pl.oaza.waw.rekolekcje.api.parish.domain.RegionsFacade
import pl.oaza.waw.rekolekcje.api.parish.dto.RegionPayload
import pl.oaza.waw.rekolekcje.api.parish.storage.RegionDatabase

internal class RegionAcceptanceTest : BaseIntegrationTest() {

  private val region1 = RegionPayload(1L, "Kryta")
  private val region2 = RegionPayload(2L, "Maguuma Jungle")
  private val allRegions = listOf(region1, region2)

  @Autowired
  private lateinit var facade: RegionsFacade

  @Autowired
  private lateinit var whenInApi: RegionApiBehaviours
  @Autowired
  private lateinit var thenInApi: RegionApiExpectations
  @Autowired
  private lateinit var database: RegionDatabase

  @BeforeEach
  fun setUp() {
    database.clearAll()
  }

  @Test
  @WithMockUser
  @Throws(Exception::class)
  fun `should get all regions`() {
    // given system has some regions
    saveAll(allRegions)

    // when requesting all regions
    val response = whenInApi.allAreRequested()

    // all are returned
    thenInApi.responseHasCorrectDataIgnoringId(response, allRegions)
  }

  @Test
  @WithMockUser
  @Throws(Exception::class)
  fun `should get a single region`() {
    // given system has some regions
    saveAll(allRegions)
    val existingRegion = whenInApi.existsElementWithTheSameData(region1)
    val idToFind = existingRegion.id

    // when requesting single region
    val response = whenInApi.singleIsRequested(idToFind)

    // it's details are returned
    thenInApi.responseHasExactPayload(response, region1.copy(id = idToFind))
  }

  @Test
  @WithMockUser
  @Throws(Exception::class)
  fun `should create a new region`() {
    // given new region
    val newRegion = RegionPayload(null, "Domain of Vabbi")

    // when posting new region
    val response = whenInApi.singleIsAdded(newRegion)

    // new region is created
    thenInApi.responseStatusIsCreated(response)
    thenInApi.responseHasCorrectDataIgnoringId(response, newRegion)
  }

  @Test
  @WithMockUser
  @Throws(Exception::class)
  fun `should assign id to region on creation`() {
    // given region data without id
    val newRegion = RegionPayload(null, "Domain of Ishtan")

    // when posting this data to endpoint
    val response = whenInApi.singleIsAdded(newRegion)

    // new region is created with generated id
    thenInApi.responseHasDataWithId(response)
  }

  @Test
  @WithMockUser
  @Throws(Exception::class)
  fun `should delete a single region`() {
    // given system has some regions
    saveAll(allRegions)
    val idToDelete = whenInApi.existsElementWithTheSameData(region2).id!!

    // when posting delete request with id
    val response = whenInApi.singleIsDeleted(idToDelete)

    // then single region is deleted
    thenInApi.responseStatusIsOk(response)
    thenInApi.doesNotExist(region2.copy(id = idToDelete))
    thenInApi.thereIsAnExactNumberOfEntities(allRegions.size - 1)
  }

  @Test
  @WithMockUser
  @Throws(Exception::class)
  fun `should update region with new data`() {
    // given system has some regions
    saveAll(allRegions)
    val idToUpdate = whenInApi.existsElementWithTheSameData(region2).id!!
    val updatedRegion = RegionPayload(idToUpdate, "Brand new region name")

    // when posting update request with new data
    val response = whenInApi.singleIsUpdated(updatedRegion)

    // then single region is updated
    thenInApi.responseStatusIsOk(response)
    thenInApi.thereIsAnExactNumberOfEntities(allRegions.size)
    thenInApi.responseHasExactPayload(response, updatedRegion)
    thenInApi.exists(updatedRegion)
  }

  private fun saveAll(regions: List<RegionPayload>) {
    regions.forEach { r -> facade.save(r) }
  }
}
