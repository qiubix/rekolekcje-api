package pl.oaza.waw.rekolekcje.api.tools.validation

internal data class ApiErrorView(
  val fieldErrors: List<ApiFieldError>? = null,
  val globalErrors: List<ApiGlobalError>? = null
)
