import { Action } from '@ngrx/store';
import { UserSummary } from '../model/user-summary.model';
import { SaveUserRequest } from '../model/save-user-request.model';
import { ChangePasswordRequest } from '../model/change-password-request.model';

export namespace UsersSharedActions {
  export const types = {
    LoadUsersList: '[Users] Load Users List',
    LoadUsersListFail: '[Users] Load Users List Fail',
    LoadUsersListSuccess: '[Users] Load Users List Success',
    CreateUser: '[Users] Create User',
    CreateUserSuccess: '[Users] Create User Success',
    CreateUserFail: '[Users] Create User Fail',
    UpdateUser: '[Users] Update user',
    UpdateUserSuccess: '[Users] Update user success',
    UpdateUserFail: '[Users] Update user fail',
    DeleteUser: '[Users] Delete User',
    DeleteUserSuccess: '[Users] Delete User Success',
    DeleteUserFail: '[Users] Delete User Fail',
    ChangePassword: '[Users] Change Password',
    ChangePasswordSuccess: '[Users] Change Password Success',
    ChangePasswordFail: '[Users] Change Password Fail',
    SelectUser: '[Users] Select User',
    SelectUserSuccess: '[Users] Select User Success',
    SelectUserFail: '[Users] Select User Fail'
  };

  /**
   * Load users list actions
   */
  export class LoadUsersList implements Action {
    type = types.LoadUsersList;

    constructor(public payload?: any) {}
  }

  export class LoadUsersListFail implements Action {
    type = types.LoadUsersListFail;

    constructor(public payload: any) {}
  }

  export class LoadUsersListSuccess implements Action {
    type = types.LoadUsersListSuccess;

    constructor(public payload: UserSummary[]) {}
  }

  /**
   * Users creation list actions
   */
  export class CreateUser implements Action {
    type = types.CreateUser;

    constructor(public payload: SaveUserRequest) {}
  }

  export class CreateUserSuccess implements Action {
    type = types.CreateUserSuccess;

    constructor(public payload: UserSummary) {}
  }

  export class CreateUserFail implements Action {
    type = types.CreateUserFail;

    constructor(public payload: any) {}
  }

  export class UpdateUser implements Action {
    type = types.UpdateUser;

    constructor(public payload: SaveUserRequest) {}
  }

  export class UpdateUserSuccess implements Action {
    type = types.UpdateUserSuccess;

    constructor(public payload: UserSummary) {}
  }

  export class UpdateUserFail implements Action {
    type = types.UpdateUserFail;

    constructor(public payload: any) {}
  }

  /**
   * Users delete actions
   */
  export class DeleteUser implements Action {
    type = types.DeleteUser;

    constructor(public payload: string) {}
  }

  export class DeleteUserSuccess implements Action {
    type = types.DeleteUserSuccess;

    constructor(public payload: string) {}
  }
  export class DeleteUserFail implements Action {
    type = types.DeleteUserFail;

    constructor(public payload: any) {}
  }

  export class ChangePassword implements Action {
    type = types.ChangePassword;
    constructor(public payload: ChangePasswordRequest) {}
  }

  export class ChangePasswordSuccess implements Action {
    type = types.ChangePasswordSuccess;
    constructor(public payload: string) {}
  }

  export class ChangePasswordFail implements Action {
    type = types.ChangePasswordFail;
    constructor(public payload: string) {}
  }

  export class SelectUser implements Action {
    type = types.SelectUser;
    constructor(public username: string) {}
  }

  export class SelectUserSuccess implements Action {
    type = types.SelectUserSuccess;
    constructor(public payload: UserSummary) {}
  }

  export class SelectUserFail implements Action {
    type = types.SelectUserFail;
    constructor(public payload: any) {}
  }
}
