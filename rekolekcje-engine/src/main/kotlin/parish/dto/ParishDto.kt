package pl.oaza.waw.rekolekcje.api.parish.dto

import pl.oaza.waw.rekolekcje.api.core.ddd.ValueObject

@ValueObject
data class ParishPayload(
  val id: Long? = null,
  val name: String? = null,
  val address: String? = null,
  val region: Long? = null
)

@ValueObject
data class RegionPayload(
  val id: Long? = null,
  val name: String? = null
)
