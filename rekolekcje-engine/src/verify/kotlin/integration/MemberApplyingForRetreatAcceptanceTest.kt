package pl.oaza.waw.rekolekcje.api.integration

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Import
import org.springframework.security.test.context.support.WithMockUser
import pl.oaza.waw.rekolekcje.api.application.api.ApplicationApiBehaviours
import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationStatus
import pl.oaza.waw.rekolekcje.api.application.endpoint.ApplicationSubmissionDto
import pl.oaza.waw.rekolekcje.api.application.endpoint.ChangeApplicationStatusRequest
import pl.oaza.waw.rekolekcje.api.application.endpoint.RetreatApplicationDto
import pl.oaza.waw.rekolekcje.api.application.fixture.ApplicationTestContext
import pl.oaza.waw.rekolekcje.api.application.fixture.animatorApplicationSubmissionDto
import pl.oaza.waw.rekolekcje.api.application.fixture.participantApplicationSubmissionDto
import pl.oaza.waw.rekolekcje.api.members.api.MembersApiBehaviour
import pl.oaza.waw.rekolekcje.api.members.api.MembersApiExpectations
import pl.oaza.waw.rekolekcje.api.members.storage.MembersDatabase
import pl.oaza.waw.rekolekcje.api.members.utils.memberWithSampleData
import pl.oaza.waw.rekolekcje.api.retreats.api.RetreatsApiBehaviour
import pl.oaza.waw.rekolekcje.api.retreats.api.RetreatsApiExpectations
import pl.oaza.waw.rekolekcje.api.retreats.endpoint.RetreatStatisticsDto
import pl.oaza.waw.rekolekcje.api.retreats.retreatDetailsWithSampleData1
import pl.oaza.waw.rekolekcje.api.retreats.retreatDetailsWithSampleData2
import pl.oaza.waw.rekolekcje.api.retreats.retreatSummary
import pl.oaza.waw.rekolekcje.api.retreats.sampleRetreatMember
import pl.oaza.waw.rekolekcje.api.retreats.storage.RetreatsDatabase
import pl.oaza.waw.rekolekcje.api.retreats.withAverageAge
import pl.oaza.waw.rekolekcje.api.retreats.withId
import pl.oaza.waw.rekolekcje.api.retreats.withNumberOfFemaleParticipants
import pl.oaza.waw.rekolekcje.api.retreats.withNumberOfParticipants
import pl.oaza.waw.rekolekcje.api.retreats.withParticipants
import pl.oaza.waw.rekolekcje.api.retreats.withStatistics
import pl.oaza.waw.rekolekcje.api.shared.BaseIntegrationTest

@Import(ApplicationTestContext::class)
internal class MemberApplyingForRetreatAcceptanceTest : BaseIntegrationTest() {

  @Autowired
  private lateinit var whenInMembersApi: MembersApiBehaviour
  @Autowired
  private lateinit var thenInMembersApi: MembersApiExpectations
  @Autowired
  private lateinit var whenInApplicationApi: ApplicationApiBehaviours
  @Autowired
  private lateinit var whenInRetreatsApi: RetreatsApiBehaviour
  @Autowired
  private lateinit var thenInRetreatsApi: RetreatsApiExpectations
  @Autowired
  private lateinit var retreatsDatabase: RetreatsDatabase
  @Autowired
  private lateinit var membersDatabase: MembersDatabase

  private val retreatOne = retreatDetailsWithSampleData1()
  private val retreatTwo = retreatDetailsWithSampleData2()

  @BeforeEach
  internal fun setUp() {
    retreatsDatabase.clear()
    membersDatabase.clear()
  }

  @Test
  @WithMockUser
  fun `should register participant to retreat`() {
    // given
    val existingRetreatId = whenInRetreatsApi.addAndGetId(retreatTwo)

    // when
    val application = participantApplicationIsSubmittedAndVerified(existingRetreatId)

    // then
    val memberToRegister = sampleRetreatMember(application)
    val expectedDetails = retreatTwo
      .withId(existingRetreatId)
      .withParticipants(setOf(memberToRegister))
      .withStatistics(
        RetreatStatisticsDto(
          participantsAmount = 1,
          malesParticipants = 0,
          femalesParticipants = 1,
          averageAge = memberToRegister.age.toDouble(),
          underageParticipants = 0
        )
      )
    thenInRetreatsApi.exists(expectedDetails)
  }

  @Test
  @WithMockUser
  fun `should add another participant to retreat already containing some participants`() {
    // given
    val existingRetreatId = whenInRetreatsApi.addAndGetId(retreatTwo)
    val firstApplication = participantApplicationIsSubmittedAndVerified(existingRetreatId)

    // when
    participantApplicationIsSubmittedAndVerified(existingRetreatId, "Another", "One")

    // then
    val expectedSummary = retreatSummary(retreatTwo.withId(existingRetreatId))
      .withNumberOfParticipants(2)
      .withNumberOfFemaleParticipants(2)
      .withAverageAge(firstApplication.member.personalData.age?.toDouble() ?: throw IllegalStateException())
    thenInRetreatsApi.exists(expectedSummary)
  }

  private fun participantApplicationIsSubmittedAndVerified(
    retreatId: Long,
    firstName: String = "Jack",
    lastName: String = "Ryan"
  ): RetreatApplicationDto {
    val application = participantApplicationSubmissionDto(
      retreatId = retreatId, member = memberWithSampleData(firstName = firstName, lastName = lastName)
    )
    return applicationIsSubmittedAndVerified(application)
  }

  private fun animatorApplicationIsSubmittedAndVerified(
    retreatId: Long,
    firstName: String = "Bruce",
    lastName: String = "Lee"
  ): RetreatApplicationDto {
    val application = animatorApplicationSubmissionDto(
      retreatId = retreatId, member = memberWithSampleData(firstName = firstName, lastName = lastName)
    )
    return applicationIsSubmittedAndVerified(application)
  }

  private fun applicationIsSubmittedAndVerified(application: ApplicationSubmissionDto): RetreatApplicationDto {
    val applicationId = whenInApplicationApi.alreadyExistsSomeApplication(application)
    val verifyRequest = ChangeApplicationStatusRequest(applicationId, ApplicationStatus.VERIFIED)
    whenInApplicationApi.applicationStatusIsChanged(verifyRequest)
    return whenInApplicationApi.singleApplicationIsRequested(applicationId).getContent()
  }

  /*

  @Test
  @WithMockUser
  fun `should add member to proper retreat depending on his application`() {
    // given
    val retreatDetails = retreatDetailsWithSampleData()
    whenInRetreatsApi.singleIsAdded(retreatDetails)
    val retreatId = whenInRetreatsApi.existsRetreatWithTheSameData(retreatDetails)
    val memberDetails = memberWithSampleData().applyingForRetreat(retreatId)

    // when
    whenInMembersApi.singleIsAdded(memberDetails)

    // then
    val memberId = whenInMembersApi.findIdOfElementWithTheSameData(memberDetails)
    val expectedRetreatDetails = retreatDetails.withId(retreatId).withParticipants(setOf(memberId))
    thenInRetreatsApi.exists(expectedRetreatDetails)
  }

  @Test
  @WithMockUser
  fun `should add member with status 'WAITING' by default`() {
    // given
    val memberDetails = memberWithMinimalData()

    // when
    val response = whenInMembersApi.singleIsAdded(memberDetails)

    // then
    val memberId = response.getContent<MemberSummary>().id
    val expectedMember = memberWithMinimalData()
      .withId(memberId)
      .copy(
        currentApplication = CurrentApplicationValue(memberStatus = MemberStatus.WAITING)
      )
    thenInMembersApi.exists(expectedMember)
  }

  @Test
  @WithMockUser
  fun `should change member status in retreat when he is approved`() {
    // given
    val memberDetails = memberWithSampleData(status = MemberStatus.WAITING)
    whenInMembersApi.singleIsAdded(memberDetails)
    val memberId = whenInMembersApi.findIdOfElementWithTheSameData(memberDetails)

    // when
    val response = whenInMembersApi.memberStatusIsChanged(memberId, MemberStatus.VERIFIED.name)

    // then
    response.hasStatusOk()
    val expectedMember = memberDetails.withId(memberId).withStatus(MemberStatus.VERIFIED)
    thenInMembersApi.exists(expectedMember)
  }
   */
}
