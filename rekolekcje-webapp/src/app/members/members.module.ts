import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { MembersRoutingModule } from './members-routing.module';
import { NavigationModule } from '@navigation/navigation.module';
import { AuthModule } from 'app/auth/auth.module';
import * as fromComponents from './components';
import { MembersComponent } from './components';

@NgModule({
  imports: [AuthModule, SharedModule, MembersRoutingModule, NavigationModule],
  declarations: [fromComponents.components],
  exports: [MembersComponent],
  entryComponents: []
})
export class MembersModule {}
