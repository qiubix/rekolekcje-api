package pl.oaza.waw.rekolekcje.api.application.domain

import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import pl.oaza.waw.rekolekcje.api.application.ApplicationTestFixture
import pl.oaza.waw.rekolekcje.api.application.participantApplication

internal class ApplicationStatusTest : ApplicationTestFixture() {

  @Test
  fun `should have status 'WAITING' by default`() {
    // given
    val application = RetreatApplication(
      applicationType = ApplicationType.PARTICIPANT,
      memberId = 123L,
      firstName = "John",
      lastName = "Doe"
    )

    // then
    assertThat(application.applicationStatus()).isEqualTo(ApplicationStatus.WAITING)
  }

  data class PositiveTestCase(
    val name: String,
    val persistanceFunction: ((ApplicationStatus) -> Long),
    val initialStatus: ApplicationStatus,
    val expectedStatus: ApplicationStatus
  )

  private fun shouldChangeStatusDataSource() = listOf(
    PositiveTestCase(
      "participant: approve when waiting",
      { saveApplicationWithStatus(it) },
      ApplicationStatus.WAITING,
      ApplicationStatus.VERIFIED
    ),
    PositiveTestCase(
      "participant: report a problem",
      { saveApplicationWithStatus(it) },
      ApplicationStatus.WAITING,
      ApplicationStatus.PROBLEM
    ),
    PositiveTestCase(
      "participant: approve in problem",
      { saveApplicationWithStatus(it) },
      ApplicationStatus.PROBLEM,
      ApplicationStatus.VERIFIED
    ),
    PositiveTestCase(
      "participant: register when verified",
      { saveApplicationWithStatus(it) },
      ApplicationStatus.VERIFIED,
      ApplicationStatus.REGISTERED
    ),
    PositiveTestCase(
      "participant: put on waiting list when verified",
      { saveApplicationWithStatus(it) },
      ApplicationStatus.VERIFIED,
      ApplicationStatus.WAITING_LIST
    ),
    PositiveTestCase(
      "participant: resign when registered",
      { saveApplicationWithStatus(it) },
      ApplicationStatus.REGISTERED,
      ApplicationStatus.RESIGNED
    )
  )

  @TestFactory
  fun `should change application status`() = shouldChangeStatusDataSource().map { testCase ->
    dynamicTest(testCase.name) {
      // given
      val id = testCase.persistanceFunction(testCase.initialStatus)

      // when
      commandFacade.changeApplicationStatus(changeStatus(id, testCase.expectedStatus))

      // then
      val application = findApplication(id)
      assertThat(application.applicationStatus()).isEqualTo(testCase.expectedStatus)
    }
  }

  @Test
  fun `should reject application`() {
    // given
    val reason = "Eats to much pizza."
    val id = saveApplicationWithStatus(ApplicationStatus.WAITING)

    // when
    commandFacade.changeApplicationStatus(changeStatus(id, ApplicationStatus.REJECTED, reason))

    // then
    val application = findApplication(id)
    assertThat(application.applicationStatus()).isEqualTo(ApplicationStatus.REJECTED)
    assertThat(application.statusAdditionalDetails()).isEqualTo(reason)
  }

  @Test
  fun `should not reject application without reason`() {
    // given
    val id = saveApplicationWithStatus(ApplicationStatus.WAITING)

    // when & then
    assertThatThrownBy {
      commandFacade.changeApplicationStatus(changeStatus(id, ApplicationStatus.REJECTED))
    }.isInstanceOf(IllegalArgumentException::class.java)
  }

  data class ErrorTestCase(
    val name: String,
    val initialStatus: ApplicationStatus,
    val targetStatus: ApplicationStatus
  )

  private fun shouldNotChangeApplicationStatusDataSource() = listOf(
    ErrorTestCase(
      "participant: should not put on waiting list when there is a problem",
      ApplicationStatus.PROBLEM,
      ApplicationStatus.WAITING_LIST
    ),
    ErrorTestCase(
      "participant: should not register when there is a problem",
      ApplicationStatus.PROBLEM,
      ApplicationStatus.REGISTERED
    ),
    ErrorTestCase(
      "participant: should not register when waiting for verification",
      ApplicationStatus.WAITING,
      ApplicationStatus.REGISTERED
    ),
    ErrorTestCase(
      "participant: should not register rejected application",
      ApplicationStatus.REJECTED,
      ApplicationStatus.REGISTERED
    )
  )

  @TestFactory
  fun `should throw when trying to change status`() = shouldNotChangeApplicationStatusDataSource().map { testCase ->
    dynamicTest(testCase.name) {
      // given
      val id = saveApplicationWithStatus(testCase.initialStatus)

      // when & then
      assertThatExceptionOfType(IllegalStateException::class.java)
        .isThrownBy { commandFacade.changeApplicationStatus(changeStatus(id, testCase.targetStatus)) }
    }
  }

  @TestFactory
  fun `should throw exception when trying to change status of non-existing application`() = listOf(
    ApplicationStatus.WAITING to null,
    ApplicationStatus.VERIFIED to null,
    ApplicationStatus.PROBLEM to null,
    ApplicationStatus.REGISTERED to null,
    ApplicationStatus.REJECTED to "rejection reason"
  ).map { (status, reason) ->
    dynamicTest("initial status: $status") {
      // given
      val idOfNonExistingApplication = 999L

      // when & then
      assertThatExceptionOfType(IllegalStateException::class.java)
        .isThrownBy {
          commandFacade.changeApplicationStatus(changeStatus(idOfNonExistingApplication, status, reason))
        }
    }
  }

  private fun allStatusesDataSource() = listOf(
    ApplicationStatus.WAITING,
    ApplicationStatus.VERIFIED,
    ApplicationStatus.PROBLEM,
    ApplicationStatus.REGISTERED,
    ApplicationStatus.REJECTED
  )

  @TestFactory
  fun `should allow to reset application's status to 'WAITING'`() = allStatusesDataSource().map { status ->
    dynamicTest("application with $status status") {
      // given
      val id = saveApplicationWithStatus(status)

      // when
      commandFacade.changeApplicationStatus(changeStatus(id, ApplicationStatus.WAITING))

      // then
      val application = findApplication(id)
      assertThat(application.applicationStatus()).isEqualTo(ApplicationStatus.WAITING)
    }
  }

  @Test
  fun `should return correct status`() {
    // given
    val id = saveApplicationWithStatus(ApplicationStatus.WAITING)

    // when
    val status = commandFacade.changeApplicationStatus(changeStatus(id, ApplicationStatus.VERIFIED))

    // then
    assertThat(status).isEqualTo(ApplicationStatus.VERIFIED)
  }

  @Test
  fun `should send notification when application was verified`() {
    // given
    val id = saveApplicationWithStatus(ApplicationStatus.WAITING)

    // when
    commandFacade.changeApplicationStatus(changeStatus(id, ApplicationStatus.VERIFIED))

    // then
    verify { applicationNotifierMock.applicationVerified(id) }
  }

  @Test
  fun `should send notification when application was rejected`() {
    // given
    val id = saveApplicationWithStatus(ApplicationStatus.WAITING)
    val reason = "You're not good enough."

    // when
    commandFacade.changeApplicationStatus(changeStatus(id, ApplicationStatus.REJECTED, reason))

    // then
    verify { applicationNotifierMock.applicationRejected(id, reason) }
  }

  private fun saveApplicationWithStatus(status: ApplicationStatus): Long {
    val application = participantApplication(status = status)
    val submittedApplication = commandFacade.submitNewApplication(application)
    return submittedApplication.getId() ?: throw IllegalStateException()
  }

  private fun changeStatus(memberId: Long, status: ApplicationStatus, reason: String? = null): ChangeApplicationStatus =
    ChangeApplicationStatus(
      memberId, status, reason
    )

  private fun findApplication(id: Long) = queryFacade.findApplicationById(id) ?: throw IllegalStateException()
}