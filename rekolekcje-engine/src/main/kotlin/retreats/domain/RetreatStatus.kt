package pl.oaza.waw.rekolekcje.api.retreats.domain

enum class RetreatStatus {
  ACTIVE,
  HISTORICAL
}