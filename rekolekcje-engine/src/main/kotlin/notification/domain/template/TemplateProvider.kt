package pl.oaza.waw.rekolekcje.api.notification.domain.template

import pl.oaza.waw.rekolekcje.api.notification.domain.template.extractor.ExtractorType
import pl.oaza.waw.rekolekcje.api.notification.dto.NotificationReason

interface TemplateProvider {
	fun provideTemplate(reason: NotificationReason): Template
}

data class Template(
	val subject: String,
	val htmlTemplate: String,
	val extractors: Set<ExtractorType>
)
