package pl.oaza.waw.rekolekcje.api.application.domain

enum class ApplicationStatus {
  WAITING,
  VERIFIED,
  REGISTERED,
  PROBLEM,
  REJECTED,
  RESIGNED,
  WAITING_LIST
}