package pl.oaza.waw.rekolekcje.api.members.storage

import pl.oaza.waw.rekolekcje.api.members.domain.KwcStatus
import java.time.LocalDate

internal data class PartialMemberDao (
    val id: Long? = null,
    val firstName: String? = null,
    val lastName: String? = null,
    val pesel: String? = null,
    val parishId: Long? = null,

  // personal data
    val christeningDate: LocalDate? = null,

  // address
    val postalCode: String? = null,

  // health report
    val currentTreatment: String? = null,

  // experience
    val kwcStatus: KwcStatus? = null,
    val numberOfCommunionDays: Int? = null
)
