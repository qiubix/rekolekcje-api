import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Kwc, Talents } from '@shared/models';

const resolvedPromise = Promise.resolve(null);

@Component({
  selector: 'reko-kwc-form',
  templateUrl: './kwc-form.component.html',
  styleUrls: ['./kwc-form.component.scss']
})
export class KwcFormComponent implements OnInit {
  form: FormGroup;

  @Input() data: Kwc;
  @Output() formReady = new EventEmitter<FormGroup>();

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.form = this.fb.group({
      status: this.data ? this.data.status : '',
      year: this.data ? this.data.year : ''
    });
    resolvedPromise.then(() => {
      this.formReady.emit(this.form);
    });
  }

  get yearControl(): FormControl {
    return this.form.get('year') as FormControl;
  }
}
