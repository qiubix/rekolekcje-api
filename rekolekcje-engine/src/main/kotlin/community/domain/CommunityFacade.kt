package pl.oaza.waw.rekolekcje.api.community.domain

import pl.oaza.waw.rekolekcje.api.community.dto.CommunityNotFoundException
import pl.oaza.waw.rekolekcje.api.community.dto.CommunityPayload
import pl.oaza.waw.rekolekcje.api.core.ddd.ApplicationService
import java.util.Objects.requireNonNull

/**
 * Manages domain objects of `community` subdomain.
 */
@ApplicationService
class CommunityFacade internal constructor(private val communityRepository: CommunityRepository) {

  /**
   * Retrieves all communities.
   *
   * @return list of community data as [CommunityPayload]
   */
  fun findAll(): List<CommunityPayload> {
    return communityRepository.findAll().map { it.dto() }
  }

  /**
   * Retrieves one community.
   *
   * @param id ID of the community to find
   * @return Community data as [CommunityPayload]
   */
  fun findOne(id: Long): CommunityPayload {
    requireNonNull<Long>(id)
    val foundCommunity = communityRepository.findById(id)
    when {
      foundCommunity != null -> return foundCommunity.dto()
      else -> throw CommunityNotFoundException(id)
    }
  }

  /**
   * Save community to system. This method either creates a new community, if no ID was provided
   * in payload, or updates an existing one with a matching ID.
   *
   * @param payload New community data
   * @return Saved community data as [CommunityPayload]
   */
  fun save(payload: CommunityPayload): CommunityPayload {
    requireNonNull(payload)
    val communityToSave = from(payload)
    return communityRepository
      .save(communityToSave)
      .dto()
  }

  /**
   * Deletes communities. Accepts multiple communities at once.
   *
   * @param ids Ids of communities to delete
   */
  fun delete(vararg ids: Long) {
    requireNonNull(ids)
    ids.forEach { communityRepository.deleteById(it) }
  }

  private fun from(payload: CommunityPayload): Community {
    return Community(
      payload.id,
      payload.name,
      payload.address
    )
  }
}
