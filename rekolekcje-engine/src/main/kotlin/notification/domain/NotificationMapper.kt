package pl.oaza.waw.rekolekcje.api.notification.domain

import pl.oaza.waw.rekolekcje.api.notification.CreateNotificationRequest
import pl.oaza.waw.rekolekcje.api.notification.dto.NotificationStatus
import java.time.LocalDateTime

internal class NotificationMapper {

  fun toNotification(request: CreateNotificationRequest): Notification {
    val now = LocalDateTime.now()
    return Notification(
        applicationId = request.applicationId,
        reason = request.reason,
        targetEmail = request.targetEmail,
        status = NotificationStatus.SCHEDULED,
        createdDate = now,
        lastModifiedDate = now,
        additionalInfo = request.additionalInfo
    )
  }

  fun copyWithId(notification: Notification, id: Long): Notification {
    return notification.copy(id = id)
  }
}