package pl.oaza.waw.rekolekcje.api.shared.value

enum class Sex {
  FEMALE,
  MALE
}