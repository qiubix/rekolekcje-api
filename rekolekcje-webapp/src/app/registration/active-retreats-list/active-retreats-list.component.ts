import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { RetreatStatus, RetreatSummary } from '../../retreats/models/retreat-summary.model';
import { select, Store } from '@ngrx/store';
import { Retreats } from '../../retreats/store/retreat-reducer';
import { AppSelectors } from '../../core/store/app-selectors';
import { map } from 'rxjs/operators';

@Component({
  selector: 'reko-active-retreats-list',
  templateUrl: './active-retreats-list.component.html',
  styleUrls: ['./active-retreats-list.component.scss']
})
export class ActiveRetreatsListComponent implements OnInit {
  availableRetreats$: Observable<RetreatSummary[]>;
  displayedColumns = ['stage', 'turn', 'dates', 'localization', 'options'];

  constructor(private store: Store<Retreats.State>) {}

  ngOnInit() {
    this.availableRetreats$ = this.store.pipe(
      select(AppSelectors.getRetreatsList),
      map(it => it.filter(retreat => retreat.status === RetreatStatus.ACTIVE))
    );
  }

  onRegister(retreatId: number) {}
}
