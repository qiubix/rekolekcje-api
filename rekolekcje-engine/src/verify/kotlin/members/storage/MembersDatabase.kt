package pl.oaza.waw.rekolekcje.api.members.storage

import org.springframework.jdbc.core.JdbcTemplate
import pl.oaza.waw.rekolekcje.api.members.domain.KwcStatus
import pl.oaza.waw.rekolekcje.api.members.endpoint.ContactInfo
import pl.oaza.waw.rekolekcje.api.members.endpoint.MemberDetails
import pl.oaza.waw.rekolekcje.api.shared.DaoTools
import java.sql.ResultSet
import java.util.concurrent.atomic.AtomicInteger

class MembersDatabase(private val jdbcTemplate: JdbcTemplate) {

  private val counter: AtomicInteger = AtomicInteger(0)

  internal val allMemberData: List<PartialMemberDao>
    get() = jdbcTemplate.query("SELECT * FROM members m", memberSampleDataRowMapper())

  fun clear() {
    jdbcTemplate.execute("TRUNCATE retreat_turns")
    jdbcTemplate.execute("TRUNCATE members CASCADE")
    jdbcTemplate.execute("TRUNCATE contacts CASCADE")
  }

  internal fun findIdOfMemberWithTheSameData(dto: MemberDetails): Long {
    val foundIds = jdbcTemplate.query(
      """
        SELECT * FROM members as m
        WHERE m.first_name = ? AND m.last_name = ? AND m.pesel = ?
      """.trimIndent(),
      arrayOf<Any>(
        dto.personalData.firstName,
        dto.personalData.lastName,
        dto.personalData.pesel
      )
    ) { rs, _ -> rs.getLong("id") }
    return foundIds.stream()
      .findAny()
      .orElseThrow { IllegalStateException("Cannot find $dto") }
  }

  internal fun getPersistedData(dto: MemberDetails): PartialMemberDao {
    val foundMembers = jdbcTemplate.query(
      """
        SELECT * FROM members as m
        WHERE m.first_name = ? AND m.last_name = ? AND m.pesel = ?
      """.trimIndent(),
      arrayOf<Any>(
        dto.personalData.firstName,
        dto.personalData.lastName,
        dto.personalData.pesel
      ),
      memberSampleDataRowMapper()
    )
    return foundMembers.stream()
      .findAny()
      .orElseThrow { IllegalStateException("Cannot find $dto") }
  }

  private fun memberSampleDataRowMapper(): (ResultSet, Int) -> PartialMemberDao {
    return { rs, _ ->
      PartialMemberDao(
        id = DaoTools.getLong(rs, "id"),
        firstName = rs.getString("first_name"),
        lastName = rs.getString("last_name"),
        pesel = rs.getString("pesel"),
        parishId = DaoTools.getLong(rs, "parish_id"),
        christeningDate = DaoTools.getLocalDate(rs, "christening_date"),
        postalCode = rs.getString("postal_code"),
        currentTreatment = rs.getString("current_treatment"),
        kwcStatus = mapKwcStatus(rs.getString("kwc_status")),
        numberOfCommunionDays = DaoTools.getInt(rs, "number_of_communion_days")
      )
    }
  }

  private fun mapKwcStatus(value: String?): KwcStatus? = if (value != null) KwcStatus.valueOf(value) else null

  internal fun getContacts(): List<ContactInfo> {
    return jdbcTemplate.query("select * from contacts", contactDataRowMapper())
  }

  private fun contactDataRowMapper(): (ResultSet, Int) -> ContactInfo {
    return { rs, _ ->
      ContactInfo(
        id = rs.getLong("id"),
        fullName = rs.getString("full_name"),
        phoneNumber = rs.getString("phone_number"),
        email = rs.getString("email")
      )
    }
  }

  internal fun persistPartialMemberData(dto: MemberDetails) {
    jdbcTemplate.update(
      """
        INSERT INTO members (id, first_name, last_name, email, pesel, parish_id, christening_date, postal_code, current_treatment, kwc_status)
        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?::ParticipantKwcStatus)
      """.trimIndent(),
      dto.id,
      dto.personalData.firstName,
      dto.personalData.lastName,
      dto.personalData.email,
      dto.personalData.pesel,
      dto.personalData.parishId,
      dto.personalData.christeningDate,
      dto.personalData.address?.postalCode,
      dto.healthReport?.currentTreatment,
      dto.experience.kwc?.status?.name
    )
  }
}
