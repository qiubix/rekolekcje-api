package pl.oaza.waw.rekolekcje.api.application.endpoint.events

import org.springframework.context.ApplicationEvent
import pl.oaza.waw.rekolekcje.api.application.endpoint.ApplicationSummary

data class ApplicationSubmittedEvent(
  val applicationId: Long
) : ApplicationEvent("")

data class ApplicationVerifiedEvent(
  val application: ApplicationSummary,
  val memberId: Long
) : ApplicationEvent("")

data class ApplicationRejectedEvent(
  val applicationId: Long,
  val reason: String
) : ApplicationEvent("")

data class ApplicationDeletedEvent(
  val applicationId: Long
) : ApplicationEvent("")