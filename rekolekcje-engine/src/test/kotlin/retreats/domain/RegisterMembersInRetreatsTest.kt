package pl.oaza.waw.rekolekcje.api.retreats.domain

import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.catchThrowable
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import pl.oaza.waw.rekolekcje.api.retreats.RetreatTest
import pl.oaza.waw.rekolekcje.api.retreats.sampleRetreat
import pl.oaza.waw.rekolekcje.api.retreats.sampleRetreatMember


internal class RegisterMembersInRetreatsTest : RetreatTest() {

  private val member = sampleRetreatMember(memberId = 123L)
  private val participant = sampleRetreatMember(memberId = 124L, animator = false)
  private val animator = sampleRetreatMember(memberId = 125L, animator = true)
  private val retreat = sampleRetreat()

  @Test
  fun `should register member in retreat`() {
    // given
    val retreatId = saveAndGetId(retreat)

    // when
    commandFacade.registerMember(retreatId, member)

    // then
    val storedRetreat = queryFacade.findOneById(retreatId)
    assertThat(storedRetreat.participants()).isEqualTo(setOf(member))
  }

  @Test
  fun `should return updated retreat after member registration`() {
    // given
    val retreatId = saveAndGetId(retreat)

    // when
    val updatedRetreat = commandFacade.registerMember(retreatId, member)

    // then
    assertThat(updatedRetreat.participants()).isEqualTo(setOf(member))
  }

  @Test
  fun `should register animator in retreat`() {
    // given
    val retreatId = saveAndGetId(retreat)

    // when
    val updatedRetreat = commandFacade.registerMember(retreatId, animator)

    // then
    assertThat(updatedRetreat.animators()).isEqualTo(setOf(animator))
  }

  @Test
  fun `should register participant when already some are registered`() {
    // given
    val retreatId = saveAndGetId(retreat)
    commandFacade.registerMember(retreatId, member)

    // when
    val updatedRetreat = commandFacade.registerMember(retreatId, participant)

    // then
    assertThat(updatedRetreat.participants()).isEqualTo(setOf(member, participant))
  }

  @Test
  fun `should publish event when member registered`() {
    // given
    val retreatId = saveAndGetId(retreat)

    // when
    commandFacade.registerMember(retreatId, animator)

    // then
    verify(exactly = 1) { retreatEventPublisherMock.memberRegistered(retreatId, animator.memberId) }
  }

  @Test
  fun `should not register member if limit reached`() {
    // given
    val retreatWithLimit = retreat.copy(participantsLimit = 20)

    val retreatId = saveAndGetId(retreatWithLimit)

    hasAlreadyRegisteredParticipants(retreatId = retreatId, amount = 20)

    // when
    val anotherParticipant = sampleRetreatMember(280L, animator = false)
    val ex = catchThrowable { commandFacade.registerMember(retreatId, anotherParticipant) }

    // then
    then(ex).isInstanceOf(RetreatParticipantLimitExceededException::class.java)

    then(ex.message).contains("20", "$retreatId")
  }

  @Test
  fun `should register animator when limit of participants reached`() {
    // given
    val retreatWithLimit = retreat.copy(participantsLimit = 25)

    val retreatId = saveAndGetId(retreatWithLimit)

    hasAlreadyRegisteredParticipants(retreatId = retreatId, amount = 25)

    // when
    val newAnimator = sampleRetreatMember(280L, animator = true)
    val updatedRetreat = commandFacade.registerMember(retreatId, newAnimator)

    // then
    then(updatedRetreat.numberOfParticipants()).isEqualTo(25)
  }

  @Test
  fun `should turn animators minimum requirement flag on when enough registered`() {
    // given
    val retreatWithAnimatorsThreshold = retreat.copy(minimumAnimatorsRequired = 3)

    val retreatId = saveAndGetId(retreatWithAnimatorsThreshold)

    val animatorOne = sampleRetreatMember(memberId = 1L, animator = true)
    commandFacade.registerMember(retreatId, animatorOne)

    val animatorTwo = sampleRetreatMember(memberId = 2L, animator = true)
    commandFacade.registerMember(retreatId, animatorTwo)

    val animatorThree = sampleRetreatMember(memberId = 3L, animator = true)

    // when
    val updatedRetreat = commandFacade.registerMember(retreatId, animatorThree)

    // then
    then(updatedRetreat.hasEnoughAnimators()).isTrue()
  }

  private fun hasAlreadyRegisteredParticipants(retreatId: Long, amount: Int) {
    for (i in 1..amount) {
      val sampleParticipant = sampleRetreatMember(memberId = i.toLong(), animator = false)
      commandFacade.registerMember(retreatId, sampleParticipant)
    }
  }
}
