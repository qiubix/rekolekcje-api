package pl.oaza.waw.rekolekcje.api.shared

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import org.mockito.Mockito.mock
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext


@TestConfiguration
class BaseTestContext {

  @Bean
  internal fun mockMvc(webApplicationContext: WebApplicationContext): MockMvc {
    return MockMvcBuilders
      .webAppContextSetup(webApplicationContext)
      .addFilter<DefaultMockMvcBuilder>({ request, response, chain ->
        response.characterEncoding = "UTF-8"
        chain.doFilter(request, response)
      }, "/*")
      .apply<DefaultMockMvcBuilder>(springSecurity())
      .build()
  }

  @Bean
  internal fun restApi(mockMvc: MockMvc, jsonMapper: ObjectMapper): RestApi {
    return RestApi(mockMvc, jsonMapper.registerKotlinModule())
  }

  @Bean
  internal fun javaMailSender(): JavaMailSender {
    return mock(JavaMailSender::class.java)
  }
}
