import { Component, Input, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'reko-date-details',
  templateUrl: './date-details.component.html',
  styleUrls: ['./date-details.component.scss'],
})
export class DateDetailsComponent implements OnInit {
  @Input() dateValue: string;
  @Input() label: string;

  formattedDate: string;

  ngOnInit() {
    this.formattedDate = this.dateValue ? moment(this.dateValue).locale('pl').format('L') : '';
  }
}
