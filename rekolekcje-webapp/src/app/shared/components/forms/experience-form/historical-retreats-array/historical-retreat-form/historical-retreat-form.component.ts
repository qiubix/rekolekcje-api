import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Config } from '@config';
import { Stage } from '../../../../../models/stage.model';
import { RetreatTurn } from '@shared/models/member-details.model';
import { Turns } from '@shared/models/turn.model';

@Component({
  selector: 'reko-historical-retreat-form',
  templateUrl: './historical-retreat-form.component.html',
  styleUrls: ['./historical-retreat-form.component.scss']
})
export class HistoricalRetreatFormComponent implements OnInit {
  @Input() historicalRetreatForm: FormGroup;
  @Input() index: number;
  @Input() retreatData: RetreatTurn;

  @Output() removed = new EventEmitter<number>();
  // @Output() formReady = new EventEmitter<FormGroup>();

  stages = Stage;
  stagesKeys = Object.keys(this.stages);
  turns = Turns;
  turnKeys = Object.keys(this.turns);

  static buildFormConfig(retreatTurn: RetreatTurn) {
    const yearValidators = [
      Validators.min(Config.minYear),
      Validators.max(new Date().getFullYear())
    ];

    return new FormGroup({
      stage: new FormControl(retreatTurn ? retreatTurn.stage : ''),
      year: new FormControl(retreatTurn ? retreatTurn.year : '', yearValidators)
    });
  }

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    // const yearValidators = [
    //   Validators.min(Config.minYear),
    //   Validators.max(new Date().getFullYear())
    // ];
    // this.historicalRetreatForm = this.fb.group({
    //   stage: this.retreatData.stage,
    //   location: this.retreatData.location,
    //   year: [this.retreatData.year, yearValidators]
    // });
    // this.formReady.emit(this.historicalRetreatForm);
  }

  get yearControl() {
    return this.historicalRetreatForm.get('year');
  }

  getYearValidationErrorMessage(): string {
    if (this.historicalRetreatForm.get('year').hasError('min')) {
      return 'Value cannot be lower than ' + Config.minYear + '.';
    } else if (this.historicalRetreatForm.get('year').hasError('max')) {
      return 'Value cannot be above ' + new Date().getFullYear() + '.';
    }
  }
}
