package pl.oaza.waw.rekolekcje.api.opinions.domain

interface OpinionsWriteRepository {
  fun save(opinion: Opinion): Opinion
}

interface OpinionsReadRepository {
  fun findById(id: Long): Opinion?

  fun findAll(): List<Opinion>
}
