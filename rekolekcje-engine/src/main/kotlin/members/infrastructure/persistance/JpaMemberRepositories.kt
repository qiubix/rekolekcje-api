package pl.oaza.waw.rekolekcje.api.members.infrastructure.persistance

import org.springframework.data.repository.Repository
import pl.oaza.waw.rekolekcje.api.members.domain.Member
import pl.oaza.waw.rekolekcje.api.members.domain.MemberReadRepository
import pl.oaza.waw.rekolekcje.api.members.domain.MemberWriteRepository

interface JpaMemberReadRepository : MemberReadRepository, Repository<Member, Long>

interface JpaMemberWriteRepository : MemberWriteRepository, Repository<Member, Long>