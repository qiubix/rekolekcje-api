package pl.oaza.waw.rekolekcje.api.notification.domain

import org.slf4j.LoggerFactory
import org.springframework.mail.MailException
import org.springframework.transaction.annotation.Transactional
import pl.oaza.waw.rekolekcje.api.email.domain.EmailService
import pl.oaza.waw.rekolekcje.api.notification.domain.template.NotificationTemplateService
import pl.oaza.waw.rekolekcje.api.notification.dto.NotificationStatus
import pl.oaza.waw.rekolekcje.api.notification.dto.NotificationStatus.FAILURE
import pl.oaza.waw.rekolekcje.api.notification.dto.NotificationStatus.SCHEDULED
import pl.oaza.waw.rekolekcje.api.notification.dto.NotificationStatus.SUCCESS
import java.time.LocalDateTime

internal open class NotificationSender(
        private val notificationRepository: NotificationRepository,
        private val emailService: EmailService,
        private val notificationTemplateService: NotificationTemplateService
) {

    private val log = LoggerFactory.getLogger(this.javaClass)

    @Transactional
    open fun sendNotification(notification: Notification) {
        try {
            val template = notificationTemplateService.fillInTemplate(notification)
            log.info("Sending mail to ${notification.targetEmail} with subject ${template.subject}")
            emailService.sendMessageWithTemplate(
                notification.targetEmail,
                template.subject,
                template.template,
                template.templateModel
            )
            updateNotification(notification, SUCCESS)
        } catch (e: MailException) {
            log.error("Cannot send notification to ${notification.targetEmail}, will retry ", e)
            updateNotification(notification, SCHEDULED)
        } catch (e: Exception) {
            // TODO: jakie wyjatki chcemy tu handlować?
            log.error("Cannot find email template for $notification", e)
            updateNotification(notification, FAILURE)
        }
    }

    @Transactional(readOnly = true)
    open fun findNotificationsToSend(): List<Notification> {
        return notificationRepository.findByStatus(SCHEDULED)
    }

    private fun updateNotification(notification: Notification, status: NotificationStatus) {
        notificationRepository.save(notification.copy(status = status, lastModifiedDate = LocalDateTime.now()))
    }
}