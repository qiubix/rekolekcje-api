import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetreatSummaryComponent } from './retreat-summary.component';

describe('RetreatSummaryComponent', () => {
  let component: RetreatSummaryComponent;
  let fixture: ComponentFixture<RetreatSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RetreatSummaryComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetreatSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
