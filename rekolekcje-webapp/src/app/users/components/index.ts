import { UsersComponent } from './_root/users.component';
import { ChangePasswordDialogComponent } from './dialogs/change-password-dialog/change-password-dialog.component';
import { UsersAddEditDialogComponent } from './dialogs/users-add-dialog/users-add-dialog.component';
import { ChangePasswordFormComponent } from './forms/change-password-form/change-password-form.component';
import { UserFormComponent } from './forms/user-form/user-form.component';
import { UsersListComponent } from './list/users-list.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { UserProfileComponent } from './user-profile/user-profile.component';

export const components: any[] = [
  UsersComponent,
  ChangePasswordDialogComponent,
  UsersAddEditDialogComponent,
  ChangePasswordFormComponent,
  UserFormComponent,
  UsersListComponent,
  SignUpComponent,
  UserProfileComponent
];

export * from './dialogs/change-password-dialog/change-password-dialog.component';
export * from './dialogs/users-add-dialog/users-add-dialog.component';
