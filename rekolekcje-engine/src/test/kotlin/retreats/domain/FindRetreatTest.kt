package pl.oaza.waw.rekolekcje.api.retreats.domain

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.Test
import pl.oaza.waw.rekolekcje.api.retreats.RetreatTest
import pl.oaza.waw.rekolekcje.api.retreats.sampleRetreat
import pl.oaza.waw.rekolekcje.api.shared.value.Stage

internal class FindRetreatTest : RetreatTest() {

  private val first = sampleRetreat(Stage.ODB, Turn.I)
  private val second = sampleRetreat(Stage.OND, Turn.II)

  @Test
  fun `should find all retreats`() {
    // given
    saveAll(setOf(first, second))

    // when
    val foundRetreat = queryFacade.findAll()

    // then
    assertThat(foundRetreat)
      .usingElementComparatorIgnoringFields("id")
      .containsExactly(first, second)
  }

  @Test
  fun `should find one retreats`() {
    // given
    commandFacade.save(first)
    val idToFind = saveAndGetId(second)

    // when
    val foundRetreat = queryFacade.findOneById(idToFind)

    // then
    assertThat(foundRetreat).isEqualTo(second.copy(id = idToFind))
  }

  @Test
  fun `should throw exception when no retreat found`() {
    // given
    val idOfNotExistingRetreat = 0L

    // when & then
    assertThatExceptionOfType(RetreatNotFoundException::class.java)
      .isThrownBy { queryFacade.findOneById(idOfNotExistingRetreat) }
  }
}
