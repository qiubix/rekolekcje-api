
//bootRun {
//  systemProperties System.properties
//  systemProperties.add(System.getProperties())
//}

val stage = task("stage") {
  dependsOn("build")
}

gradle.taskGraph.whenReady {
  if (this.hasTask(stage)) {
    tasks["test"].enabled = false
    tasks["verify"].enabled = false
    tasks["jacocoMerge"].enabled = false
    tasks["mergedReport"].enabled = false
  }
}
