package pl.oaza.waw.rekolekcje.api.members

import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.security.test.context.support.WithMockUser
import pl.oaza.waw.rekolekcje.api.members.endpoint.MemberDetails
import pl.oaza.waw.rekolekcje.api.members.endpoint.MemberSummary
import pl.oaza.waw.rekolekcje.api.members.endpoint.PersonalDataValue
import pl.oaza.waw.rekolekcje.api.members.utils.getAgeFromBirthDate
import pl.oaza.waw.rekolekcje.api.members.utils.memberWithSampleData
import pl.oaza.waw.rekolekcje.api.members.utils.withId
import pl.oaza.waw.rekolekcje.api.members.utils.withInvalidParishState
import pl.oaza.waw.rekolekcje.api.members.utils.withNonExistingParish
import pl.oaza.waw.rekolekcje.api.parish.dto.ParishPayload
import pl.oaza.waw.rekolekcje.api.shared.value.Sex

class AddMembersAcceptanceTest : BaseMembersAcceptanceTest() {

  @Test
  @WithMockUser
  fun `should return correct data when a new member is added`() {
    // given
    whenInApi.allAreAdded(members)

    // when
    val response = whenInApi.singleIsAdded(memberWithFullData)

    // then
    val memberId = whenInApi.findIdOfElementWithTheSameData(memberWithFullData)
    val expectedMember = memberWithFullData.withId(memberId)
    response.hasStatusCreated()
    thenInApi.responseHasSummaryDataMatchingDetails(response, expectedMember)
    thenInApi.exists(expectedMember)
  }

  @Test
  @WithMockUser
  fun `should set member id when a new one is added`() {
    // given
    whenInApi.allAreAdded(members)

    // when
    val response = whenInApi.singleIsAdded(memberWithFullData)

    // then
    response.hasDataWithId()
  }

  @Test
  @WithMockUser
  fun `should extract information from PESEL when adding new member`() {
    // given
    val member = MemberDetails(
      personalData = PersonalDataValue(
        firstName = "New",
        lastName = "Fish",
        pesel = "93020573638"
      )
    )

    // when
    val response = whenInApi.singleIsAdded(member)

    // then
    val id = whenInApi.findIdOfElementWithTheSameData(member)
    val expectedMember = MemberDetails(
      id = id,
      personalData = PersonalDataValue(
        firstName = "New",
        lastName = "Fish",
        pesel = "93020573638",
        birthDate = "05.02.1993",
        sex = Sex.MALE,
        age = getAgeFromBirthDate("1993-02-05")
      )
    )
    thenInApi.responseHasSummaryDataMatchingDetails(response, expectedMember)
    thenInApi.exists(expectedMember)
  }

  @Test
  @WithMockUser
  fun `should return correct payload with ids when multiple members are added`() {
    // given some members are already in database
    whenInApi.allAreAdded(members)
    val newMembers = listOf(
      memberWithSampleData("Random", "Joe", "81010154327"),
      memberWithFullData
    )

    // when multiple members are added at once
    val response = whenInApi.multipleMembersAreAdded(newMembers)

    // then all their data is returned with ids
    response.hasStatusCreated()
    thenInApi.responseHasPayloadWithIds(response)
  }

  @Test
  @WithMockUser
  fun `should persist all members when multiple are added at once`() {
    // given some members are already in database
    whenInApi.allAreAdded(members)
    val newMembers = listOf(
      memberWithSampleData("Random", "Joe", "81010154327"),
      memberWithFullData
    )

    // when multiple members are added
    val response = whenInApi.multipleMembersAreAdded(newMembers)

    // then their data is persisted
    val expectedMembers = thenInApi.responseHasMembersExistingInSystem(response)
    thenInApi.responseHasSummaryDataMatchingDetails(response, expectedMembers)
  }

  @ParameterizedTest(name = "{index}: invalid {1}")
  @MethodSource("invalidMembers")
  @WithMockUser
  fun `should not add member with invalid data`(
      invalidPayload: MemberDetails,
      expectedErrorCode: String
  ) {
    // when
    val response = whenInApi.singleIsAdded(invalidPayload)

    // then
    thenInApi.responseIsUnprocessableEntityAndHasCode(response, expectedErrorCode)
  }

  @Test
  @WithMockUser
  fun `should add parish to parish list when member with new parish is added`() {
    // given
    val member = memberWithSampleData().withNonExistingParish()
    val newParish = member.personalData.newParish ?: throw IllegalStateException()

    // when
    val response = whenInApi.singleIsAdded(member)

    // then
    val newParishId = response
      .getContent<MemberSummary>()
      .parishId
    response.hasStatusCreated()
    val expectedParish = ParishPayload(
      id = newParishId,
      name = newParish.name,
      address = newParish.address,
      region = newParish.region
    )
    thenInParishApi.exists(expectedParish)
  }

  @Test
  @WithMockUser
  fun `should not add parish if parishId in personal data is provided`() {
    // given
    val member = memberWithSampleData()
    val numberOfParishes = whenInParishApi.resourceAmountIsRequested()

    // when
    val response = whenInApi.singleIsAdded(member)

    // then
    response.hasStatusCreated()
    thenInParishApi.thereIsAnExactNumberOfEntities(numberOfParishes)
  }

  @Test
  @WithMockUser
  fun `should return BAD_REQUEST status if both parishId and newParish object are provided`() {
    // given
    val member = memberWithSampleData().withInvalidParishState()
    val numberOfParishes = whenInParishApi.resourceAmountIsRequested()

    // when
    val response = whenInApi.singleIsAdded(member)

    // then
    response.hasStatusIsBadRequest()
    thenInParishApi.thereIsAnExactNumberOfEntities(numberOfParishes)
  }

  @Test
  @WithMockUser
  fun `should import file`() {
    // given
    val memberDetails = MemberDetails(
      personalData = PersonalDataValue(
        firstName = "Louis",
        lastName = "Lane",
        pesel = "07270908683",
        age = getAgeFromBirthDate("2007-07-09"),
        sex = Sex.FEMALE
      )
    )
    val fileContent = """
      Nazwisko,Imię (imiona),PESEL,Wybór turnusu,Stopień rekolekcji
      ${memberDetails.personalData.lastName} ,${memberDetails.personalData.firstName} ,${memberDetails.personalData.pesel},as,dsgf
    """.trimIndent()

    // when
    val fileUploadResponse = whenInApi.fileIsUploaded(fileContent)

    // then
    fileUploadResponse.hasStatusOk()
    thenInApi.responseHasSummaryOfAllMemberDetailsIgnoringId(fileUploadResponse, listOf(memberDetails))
  }
}
