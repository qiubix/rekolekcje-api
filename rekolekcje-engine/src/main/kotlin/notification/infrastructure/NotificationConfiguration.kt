package pl.oaza.waw.rekolekcje.api.notification.infrastructure

import notification.endpoint.events.ApplicationSubmittedNotificationEventListener
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import pl.oaza.waw.rekolekcje.api.application.query.ApplicationQueryFacade
import pl.oaza.waw.rekolekcje.api.email.domain.EmailService
import pl.oaza.waw.rekolekcje.api.members.query.MembersQueryFacade
import pl.oaza.waw.rekolekcje.api.members.query.OpinionLinkQueryFacade
import pl.oaza.waw.rekolekcje.api.notification.domain.*
import pl.oaza.waw.rekolekcje.api.notification.domain.template.NotificationTemplateService
import pl.oaza.waw.rekolekcje.api.notification.domain.template.TemplateProvider
import pl.oaza.waw.rekolekcje.api.notification.infrastructure.config.TemplateConfig
import pl.oaza.waw.rekolekcje.api.retreats.query.RetreatQueryFacade

@Configuration
@EnableConfigurationProperties(TemplateConfig::class)
internal class NotificationConfiguration {

  @Bean
  fun notificationCommandFacade(
    notificationRepository: NotificationRepository
  ): NotificationCommandFacade {
    return NotificationCommandFacadeImpl(notificationRepository)
  }

  @Bean
  fun notificationScheduleExecutor(
    notificationSender: NotificationSender
  ): NotificationScheduleExecutor = NotificationScheduleExecutor(notificationSender)

  @Bean
  fun notificationSender(
    notificationRepository: NotificationRepository,
    emailService: EmailService,
    notificationTemplateService: NotificationTemplateService
  ): NotificationSender =
    NotificationSender(notificationRepository, emailService, notificationTemplateService)

  @Bean
  fun notificationTemplateService(
    applicationQueryFacade: ApplicationQueryFacade,
    retreatQueryFacade: RetreatQueryFacade,
    opinionLinkQueryFacade: OpinionLinkQueryFacade,
    membersQueryFacade: MembersQueryFacade,
    templateProvider: TemplateProvider
  ): NotificationTemplateService =
    NotificationTemplateService(applicationQueryFacade, membersQueryFacade, retreatQueryFacade, opinionLinkQueryFacade, templateProvider)

  @Bean
  fun applicationSubmittedNotificationEventListener(
    notificationCommandFacade: NotificationCommandFacade,
    applicationQueryFacade: ApplicationQueryFacade,
    membersQueryFacade: MembersQueryFacade
  ): ApplicationSubmittedNotificationEventListener = ApplicationSubmittedNotificationEventListener(
    notificationCommandFacade,
    applicationQueryFacade,
    membersQueryFacade
  )
}
