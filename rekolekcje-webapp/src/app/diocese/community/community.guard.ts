import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Communities } from './store/community-reducer';
import { Store } from '@ngrx/store';
import { CommunityActions } from './store/community-actions';
import { Injectable } from '@angular/core';

@Injectable()
export class CommunityGuard implements CanActivate {
  constructor(private store: Store<Communities.State>) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    this.store.dispatch(new CommunityActions.LoadCommunitiesList());
    return true;
  }
}
