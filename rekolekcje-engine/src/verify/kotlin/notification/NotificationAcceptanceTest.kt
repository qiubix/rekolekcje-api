package pl.oaza.waw.rekolekcje.api.notification

import com.icegreen.greenmail.configuration.GreenMailConfiguration
import com.icegreen.greenmail.junit5.GreenMailExtension
import com.icegreen.greenmail.util.ServerSetup
import javax.mail.internet.InternetAddress
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.RegisterExtension
import org.junit.jupiter.api.fail
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Import
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringExtension
import pl.oaza.waw.rekolekcje.api.application.api.ApplicationApiBehaviours
import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationStatus
import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationType
import pl.oaza.waw.rekolekcje.api.application.endpoint.ApplicationSubmissionDto
import pl.oaza.waw.rekolekcje.api.application.endpoint.RetreatApplicationDto
import pl.oaza.waw.rekolekcje.api.application.endpoint.events.ApplicationDeletedEvent
import pl.oaza.waw.rekolekcje.api.application.endpoint.events.ApplicationRejectedEvent
import pl.oaza.waw.rekolekcje.api.application.fixture.ApplicationTestContext
import pl.oaza.waw.rekolekcje.api.application.fixture.sampleMember
import pl.oaza.waw.rekolekcje.api.application.storage.ApplicationDatabase
import pl.oaza.waw.rekolekcje.api.email.EmailTestConfiguration
import pl.oaza.waw.rekolekcje.api.members.api.MembersApiBehaviour
import pl.oaza.waw.rekolekcje.api.members.storage.MembersDatabase
import pl.oaza.waw.rekolekcje.api.members.utils.withEmail
import pl.oaza.waw.rekolekcje.api.notification.api.NotificationDatabase
import pl.oaza.waw.rekolekcje.api.notification.api.NotificationsEventsBehaviour
import pl.oaza.waw.rekolekcje.api.notification.storage.NotificationStorageExpectations
import pl.oaza.waw.rekolekcje.api.retreats.api.RetreatsApiBehaviour
import pl.oaza.waw.rekolekcje.api.retreats.endpoint.events.MemberStatusChangedEvent
import pl.oaza.waw.rekolekcje.api.retreats.retreatDetailsWithSampleData1
import pl.oaza.waw.rekolekcje.api.retreats.storage.RetreatsDatabase
import pl.oaza.waw.rekolekcje.api.shared.BaseIntegrationTest
import pl.oaza.waw.rekolekcje.api.shared.endpoint.ContactDto

@ExtendWith(SpringExtension::class)
@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
    classes = [EmailTestConfiguration::class],
    properties = ["spring.datasource.url=jdbc:tc:postgresql:10.6-alpine://rekolekcjedb-test",
      "spring.datasource.username=" + "postgres",
      "spring.datasource.password=" + "postgres",
      "spring.datasource.driver-class-name=org.testcontainers.jdbc.ContainerDatabaseDriver"
    ]
)
@ActiveProfiles("test", "test-local", "email")
@AutoConfigureJsonTesters
@Tag("integration")
@Import(ApplicationTestContext::class)
internal class NotificationAcceptanceTest : BaseIntegrationTest() {

  companion object {

    private val serverSetup = ServerSetup(3025, "localhost", ServerSetup.PROTOCOL_SMTP)

    @JvmField
    @RegisterExtension
    val smtpServer: GreenMailExtension = GreenMailExtension(serverSetup)
        .withConfiguration(GreenMailConfiguration.aConfig().withDisabledAuthentication())

  }

  @Autowired
  private lateinit var notificationDatabase: NotificationDatabase

  @Autowired
  private lateinit var retreatsDatabase: RetreatsDatabase

  @Autowired
  private lateinit var membersDatabase: MembersDatabase

  @Autowired
  private lateinit var applicationDatabase: ApplicationDatabase

  @Autowired
  private lateinit var whenInRetreatsApi: RetreatsApiBehaviour

  @Autowired
  private lateinit var whenInMembersApi: MembersApiBehaviour

  @Autowired
  private lateinit var whenInApplicationApi: ApplicationApiBehaviours

  @Autowired
  private lateinit var whenInEventBus: NotificationsEventsBehaviour

  @Autowired
  private lateinit var thenInStorage: NotificationStorageExpectations

  private val participantEmail = "participant@email.com"
  private val suretyEmail = "surety@mail.com"

  @BeforeEach
  fun setup() {
    notificationDatabase.clear()
    retreatsDatabase.clear()
    membersDatabase.clear()
    applicationDatabase.clear()
  }

  @AfterAll
  fun tearDown() {
    retreatsDatabase.clear()
  }

  private val retreatOne = retreatDetailsWithSampleData1()

  @Test
  @WithMockUser
  fun `should send submission confirmation notification`() {
    // given
    whenInRetreatsApi.singleIsAdded(retreatOne)
    val existingRetreatId = whenInRetreatsApi.existsRetreatWithTheSameData(retreatOne)

    // when
    existsApplication(existingRetreatId)

    // then
    smtpServer.waitForIncomingEmail(2)
    val receivedMessages = smtpServer.receivedMessages
    assertThat(receivedMessages).hasSize(2)

    val email = receivedMessages[0]
    assertThat(email.from).hasSize(1)
    assertThat(email.from[0]).isEqualTo(InternetAddress("rekolekcje@oaza.waw.pl"))

    assertThat(email.allRecipients).hasSize(1)
    assertThat(email.allRecipients[0]).isEqualTo(InternetAddress(participantEmail))
    assertThat(email.subject).isEqualTo("Potwierdzenie zgłoszenia")
  }

  @Test
  @Disabled
  @WithMockUser
  fun `should send rejection notification`() {
    // given
    whenInRetreatsApi.singleIsAdded(retreatOne)
    val existingRetreatId = whenInRetreatsApi.existsRetreatWithTheSameData(retreatOne)

    // and
    val application = existsApplication(existingRetreatId)
    smtpServer.waitForIncomingEmail(2)
    smtpServer.purgeEmailFromAllMailboxes()

    // when
    val applicationRejectedEvent = ApplicationRejectedEvent(
      applicationId = application.id,
      reason = "We're sorry but not today."
    )
    whenInEventBus.eventIsPublished(applicationRejectedEvent)

    // then
    val emailsReceived = smtpServer.waitForIncomingEmail(1)
    if (emailsReceived) {
      val receivedMessages = smtpServer.receivedMessages
      assertThat(receivedMessages).hasSize(1)

      val email = smtpServer.receivedMessages[0]
      assertThat(email.from).hasSize(1)
      assertThat(email.from[0]).isEqualTo(InternetAddress("rekolekcje@oaza.waw.pl"))

      assertThat(email.allRecipients).hasSize(1)
      assertThat(email.allRecipients[0]).isEqualTo(InternetAddress(participantEmail))
      assertThat(email.subject).isEqualTo("Zgłoszenie odrzucone")
    } else {
      fail { "Email not received" }
    }
  }

  @Test
  @Disabled
  @WithMockUser
  fun `should send notification about successful enrollment`() {
    // given
    whenInRetreatsApi.singleIsAdded(retreatOne)
    val existingRetreatId = whenInRetreatsApi.existsRetreatWithTheSameData(retreatOne)

    // and
    val application = existsApplication(existingRetreatId, ApplicationStatus.VERIFIED)
    smtpServer.waitForIncomingEmail(2)
    smtpServer.purgeEmailFromAllMailboxes()

    // when
    val applicationRegisteredEvent = MemberStatusChangedEvent(
      retreatId = existingRetreatId,
      memberId = application.member.id ?: throw IllegalStateException(),
      applicationStatus = ApplicationStatus.REGISTERED
    )
    whenInEventBus.eventIsPublished(applicationRegisteredEvent)

    // then
    smtpServer.waitForIncomingEmail(1)
    val receivedMessages = smtpServer.receivedMessages
    assertThat(receivedMessages).hasSize(1)

    val email = receivedMessages[0]
    assertThat(email.from).hasSize(1)
    assertThat(email.from[0]).isEqualTo(InternetAddress("rekolekcje@oaza.waw.pl"))

    assertThat(email.allRecipients).hasSize(1)
    assertThat(email.allRecipients[0]).isEqualTo(InternetAddress(participantEmail))
    assertThat(email.subject).isEqualTo("Potwierdzenie rejestracji")
  }

  @Test
  @Disabled
  @WithMockUser
  fun `should send notification about putting member on waiting list`() {
    // given
    whenInRetreatsApi.singleIsAdded(retreatOne)
    val existingRetreatId = whenInRetreatsApi.existsRetreatWithTheSameData(retreatOne)

    // and
    val application = existsApplication(existingRetreatId, ApplicationStatus.VERIFIED)
    smtpServer.waitForIncomingEmail(2)
    smtpServer.purgeEmailFromAllMailboxes()

    // when
    val applicationRegisteredEvent = MemberStatusChangedEvent(
      retreatId = existingRetreatId,
      memberId = application.member.id ?: throw IllegalStateException(),
      applicationStatus = ApplicationStatus.WAITING_LIST
    )
    whenInEventBus.eventIsPublished(applicationRegisteredEvent)

    // then
    smtpServer.waitForIncomingEmail(1)
    val receivedMessages = smtpServer.receivedMessages
    assertThat(receivedMessages).hasSize(1)

    val email = receivedMessages[0]
    assertThat(email.from).hasSize(1)
    assertThat(email.from[0]).isEqualTo(InternetAddress("rekolekcje@oaza.waw.pl"))

    assertThat(email.allRecipients).hasSize(1)
    assertThat(email.allRecipients[0]).isEqualTo(InternetAddress(participantEmail))
    assertThat(email.subject).isEqualTo("Zgłoszenie na liście rezerwowej")
  }

  @Test
  @WithMockUser
  fun `should remove notifications`() {
    // given
    whenInRetreatsApi.singleIsAdded(retreatOne)
    val existingRetreatId = whenInRetreatsApi.existsRetreatWithTheSameData(retreatOne)

    // and
    val applicationId = existsApplication(existingRetreatId).id

    // expect
    smtpServer.waitForIncomingEmail(2)
    smtpServer.purgeEmailFromAllMailboxes()
    thenInStorage.applicationHasNotificationsCount(applicationId, 2)

    // when
    val applicationDeletedEvent = ApplicationDeletedEvent(
        applicationId = applicationId
    )
    whenInEventBus.eventIsPublished(applicationDeletedEvent)

    // then
    // TODO: tutaj by się przydało jakieś awaitility, bo event się niekoniecznie przetworzy
    thenInStorage.applicationHasNotificationsCount(applicationId, 0)
  }

  private fun existsApplication(retreatId: Long, status: ApplicationStatus? = null): RetreatApplicationDto {
    val application = ApplicationSubmissionDto(
      applicationType = ApplicationType.PARTICIPANT,
      retreatId = retreatId,
      memberDetails = sampleMember().withEmail(participantEmail),
      surety = ContactDto(
        fullName = "Surety",
        email = suretyEmail
      )
    )
    val response = whenInApplicationApi.newApplicationIsSubmitted(application)
    status?.let { applicationDatabase.setApplicationStatus(response.getContent<RetreatApplicationDto>().id, it) }
    return response.getContent()
  }
}
