package pl.oaza.waw.rekolekcje.api.opinions.fixture

import pl.oaza.waw.rekolekcje.api.members.infrastructure.encryption.MembersEncryptionService
import pl.oaza.waw.rekolekcje.api.opinions.endpoint.SubmitOpinionRequest
import pl.oaza.waw.rekolekcje.api.opinions.endpoint.SubmitterApiModel

object OpinionsTestData {
  private const val CONTENT_DEFAULT = "Not even close to get approval"

  fun createSubmitRequest(
      encryptedMemberId: String,
      content: String = CONTENT_DEFAULT,
      submitter: SubmitterApiModel = SubmitterTestData.createSubmitter()
  ): SubmitOpinionRequest {
    return SubmitOpinionRequest(
        encryptedMemberId = encryptedMemberId,
        content = content,
        submitter = submitter
    )
  }
}
