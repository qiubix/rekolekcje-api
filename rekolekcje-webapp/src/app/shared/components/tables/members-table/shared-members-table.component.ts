import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { MemberSummary } from '@members/models';
import {
  MembersFilter,
  SharedMembersTableFilterUtil,
} from './members-table-filtering/shared-members-table-filter.util';
import { Parish } from '@diocese/parish/models/parish.model';
import { Community } from '@diocese/community/models/community.model';
import { RetreatSummary } from '@retreats/models/retreat-summary.model';
import { Stage } from '@shared/models';
import { environment } from '@env';
import { Config } from '@config';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'reko-shared-members-table',
  templateUrl: './shared-members-table.component.html',
  styleUrls: ['./shared-members-table.component.scss'],
})
export class SharedMembersTableComponent implements OnInit {
  private _members: MemberSummary[] = [];

  readonly exportMembersUrl = environment.apiUrl + '/members/export';
  readonly defaultFileName = Config.defaultExportFileName;

  @Input() title: string;
  @Input() parishes: Parish[];
  @Input() communities: Community[];
  @Input() retreats: RetreatSummary[];
  @Input() editable: boolean;
  @Input() canAddNew: boolean;
  @Input() hasDetails: boolean;

  @Input()
  set members(members: MemberSummary[]) {
    this.dataSource.data = members;
    this._members = members;
    this.cd.detectChanges();
  }

  @Input() membersLoading: boolean;
  @Input() displayedColumns: string[];
  @Input() filters: string[];

  @Output() addMember: EventEmitter<void> = new EventEmitter<void>();
  @Output() deleteMember: EventEmitter<number> = new EventEmitter<number>();
  @Output() editMember: EventEmitter<MemberSummary> = new EventEmitter<MemberSummary>();
  @Output() selectMember: EventEmitter<MemberSummary> = new EventEmitter<MemberSummary>();
  @Output() importMembers: EventEmitter<void> = new EventEmitter<void>();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  get members(): MemberSummary[] {
    return this._members;
  }

  dataSource: MatTableDataSource<MemberSummary> = new MatTableDataSource<MemberSummary>();
  stages = Stage;

  constructor(private cd: ChangeDetectorRef) {}

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
    // this.dataSource.filterPredicate = this.filterMember.bind(this);
    this.dataSource.sort = this.sort;
    // this.dataSource.sortingDataAccessor = this.sortMembers.bind(this);
  }

  onFilterChange(filter: MembersFilter): void {
    this.dataSource.data = SharedMembersTableFilterUtil.filterMembers(
      this.members,
      this.retreats,
      this.communities,
      filter
    );
  }

  private sortMembers(data: MemberSummary, sortHeaderId: string): string | number {
    switch (sortHeaderId) {
      case 'firstName':
      case 'lastName':
      case 'age':
      case 'applyingForRetreatTurn':
      case 'community':
        return '';
    }
  }

  onAddNewMember(): void {
    this.addMember.emit();
  }

  onEditMember(member: MemberSummary): void {
    this.editMember.emit(member);
  }

  onDeleteMember(member: MemberSummary): void {
    this.deleteMember.emit(member.id);
  }

  onSelectMember(member: MemberSummary): void {
    if (this.hasDetails) {
      this.selectMember.emit(member);
    }
  }

  onImportMembers(): void {
    this.importMembers.emit();
  }
}
