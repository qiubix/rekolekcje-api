import { OnDestroy, Pipe, PipeTransform } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Retreats } from '../../../retreats/store/retreat-reducer';
import { RetreatSummary } from '../../../retreats/models/retreat-summary.model';
import { AppSelectors } from '../../../core/store/app-selectors';
import { filter, take, takeUntil, tap } from 'rxjs/operators';
import { RetreatsSharedActions } from '../../../retreats/store/retreat-actions';
import { Stage } from '../../models/stage.model';
import { Subject } from 'rxjs';

@Pipe({
  name: 'retreatTitle'
})
export class RetreatTitlePipe implements PipeTransform, OnDestroy {
  private ngUnsubscribe: Subject<void> = new Subject<void>();
  private _retreats: RetreatSummary[] = [];
  private _stages = Stage;

  constructor(private _store: Store<Retreats.State>) {
    this._store
      .pipe(
        select(AppSelectors.getRetreatsList),
        takeUntil(this.ngUnsubscribe),
        tap(retreats => {
          if (!retreats || retreats.length === 0) {
            this._store.dispatch(new RetreatsSharedActions.LoadRetreatsList());
          }
        }),
        filter(retreats => retreats.length > 0),
        take(1)
      )
      .subscribe(retreats => (this._retreats = retreats));
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  transform(retreatId: number, args?: any): string {
    if (this._retreats === []) {
      return 'Brak turnusów';
    } else if (!retreatId) {
      return 'Turnus nie wybrany';
    } else {
      const retreat = this._retreats.find(it => it.id === retreatId);
      return (
        this._stages[retreat.stage] + ': turnus ' + retreat.turn + ' (' + retreat.startDate + ')'
      );
    }
  }
}
