package pl.oaza.waw.rekolekcje.api.members.endpoint

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Test
import pl.oaza.waw.rekolekcje.api.members.infrastructure.encryption.MembersEncryptionService
import java.lang.IllegalArgumentException
import java.security.GeneralSecurityException
import java.util.*
import java.util.stream.Collectors.*
import java.util.stream.IntStream

internal class MembersEncryptionServiceTest {

  private val tested: MembersEncryptionService =
    MembersEncryptionService("1234567890ABCDEF1234567890ABCDEF", "password")

  @Test
  fun `should decrypt encrypted memberId`() {
    // given
    val memberId = 123456L
    // when
    val encrypted = tested.encryptMemberId(memberId)
    val decrypted = tested.decryptMemberId(encrypted)
    // then
    assertThat(decrypted).isEqualTo(memberId)
  }

  @Test
  fun `should throw when input is garbage`() {
    // given
    val invalidBytes: ByteArray = IntStream.range(0, 255)
        .mapToObj { i -> i.toByte() }
        .collect(toList())
        .toByteArray()
    val encrypted = Base64.getEncoder().encodeToString(invalidBytes)
    //then
    assertThatThrownBy { tested.decryptMemberId(encrypted) }.isInstanceOf(IllegalArgumentException::class.java)
  }
}
