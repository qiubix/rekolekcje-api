import {
  AfterViewChecked,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { RetreatSummary } from '@retreats/models/retreat-summary.model';
import { ApplicationSubmission, ApplicationType, RetreatApplication } from '@verification/model';
import { ContactInfo, Experience, HealthReport, MemberDetails, PersonalData } from '@shared/models';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';

@Component({
  selector: 'reko-add-edit-application-wizard',
  templateUrl: './add-edit-application-wizard.component.html',
  styleUrls: ['./add-edit-application-wizard.component.scss'],
  providers: [
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: { showError: true }
    }
  ]
})
export class AddEditApplicationWizardComponent implements OnInit, AfterViewChecked {
  @Input() retreats: RetreatSummary[];
  @Input() application: RetreatApplication;
  @Input() isAnimator: Boolean;

  @Output() submit = new EventEmitter<ApplicationSubmission>();

  fullForm: FormGroup;

  constructor(private _fb: FormBuilder, private _cd: ChangeDetectorRef) {}

  ngOnInit() {
    this.fullForm = this._fb.group({});
  }

  ngAfterViewChecked(): void {
    this._cd.detectChanges();
  }

  onSubFormInitialized(name: string, subForm: FormGroup) {
    this.fullForm.addControl(name, subForm);
  }

  get retreatApplicationForm(): FormGroup {
    return this.fullForm.get('retreatApplication') as FormGroup;
  }

  get personalDataForm(): FormGroup {
    return this.fullForm.get('personalData') as FormGroup;
  }

  get personalData(): PersonalData {
    return this.application ? this.application.member.personalData : {};
  }

  get healthReportForm(): FormGroup {
    return this.fullForm.get('healthReport') as FormGroup;
  }

  get healthReport(): HealthReport {
    return this.application ? this.application.member.healthReport : {};
  }

  get experienceForm(): FormGroup {
    return this.fullForm.get('experience') as FormGroup;
  }

  get experience(): Experience {
    return this.application ? this.application.member.experience : {};
  }

  onSubmit() {
    console.log('Previous application value: ', this.application);
    const formValue = this.fullForm.value;
    const retreatId = formValue.retreatApplication.retreatId
      ? formValue.retreatApplication.retreatId
      : this.application
      ? this.application.retreatId
      : null;

    const memberDetails: MemberDetails = Object.assign(
      {},
      this.application ? this.application.member : {},
      {
        personalData: formValue.personalData,
        experience: formValue.experience,
        healthReport: formValue.healthReport
      }
    );

    const surety: ContactInfo = Object.assign(
      {},
      this.application ? this.application.surety : {},
      formValue.retreatApplication.surety
    );
    const application: ApplicationSubmission = {
      id: this.application ? this.application.id : null,
      applicationType: this.isAnimator ? ApplicationType.ANIMATOR : ApplicationType.PARTICIPANT,
      animatorRoles: formValue.retreatApplication.animatorRoles,
      availability: formValue.retreatApplication.availability,
      surety: surety,
      retreatId: retreatId,
      memberDetails: memberDetails
    };
    console.log('Trying to submit application: ', application);
    this.submit.emit(application);
  }
}
