package pl.oaza.waw.rekolekcje.api.members.domain

interface MemberReadRepository {

  fun findById(id: Long): Member?

  fun findAll(): List<Member>

  fun existsById(id: Long): Boolean

}

interface MemberWriteRepository {

  fun save(entity: Member): Member

  fun deleteById(id: Long)

}