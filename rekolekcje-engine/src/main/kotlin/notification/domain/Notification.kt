package pl.oaza.waw.rekolekcje.api.notification.domain

import pl.oaza.waw.rekolekcje.api.core.ddd.AggregateRoot
import pl.oaza.waw.rekolekcje.api.notification.dto.NotificationReason
import pl.oaza.waw.rekolekcje.api.notification.dto.NotificationStatus
import pl.oaza.waw.rekolekcje.api.notification.dto.NotificationSummary
import java.time.LocalDateTime
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table


@AggregateRoot
@Entity
@Table(name = "NOTIFICATIONS")
internal data class Notification(
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  val id: Long? = null,
  val applicationId: Long,
  @Enumerated(EnumType.STRING)
  val reason: NotificationReason,
  val targetEmail: String,
  @Enumerated(EnumType.STRING)
  val status: NotificationStatus,
  val createdDate: LocalDateTime,
  val lastModifiedDate: LocalDateTime,
  val additionalInfo: String? = null
) {

  fun summary(): NotificationSummary {
    return NotificationSummary(
      id = id ?: throw IllegalStateException("Notification should have an ID."),
      applicationId = applicationId,
      reason = reason,
      status = status
    )
  }
}