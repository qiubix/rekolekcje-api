package pl.oaza.waw.rekolekcje.api.tools.validation

import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.validation.BindingResult
import org.springframework.validation.FieldError
import org.springframework.validation.ObjectError
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.context.request.WebRequest
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler

@ControllerAdvice
internal class ApiValidationExceptionHandler : ResponseEntityExceptionHandler() {

  override fun handleMethodArgumentNotValid(
    ex: MethodArgumentNotValidException,
    headers: HttpHeaders, status: HttpStatus, request: WebRequest
  ): ResponseEntity<Any> {
    val bindingResult = ex.bindingResult
    val apiErrorView = collectApiErrors(bindingResult)
    return ResponseEntity(apiErrorView, HttpStatus.UNPROCESSABLE_ENTITY)
  }

  private fun collectApiErrors(bindingResult: BindingResult): ApiErrorView {
    val fieldErrors = buildFieldErrorsList(bindingResult.fieldErrors)
    val globalErrors = buildGlobalErrorsList(bindingResult.globalErrors)
    return ApiErrorView(fieldErrors = fieldErrors, globalErrors = globalErrors)
  }

  private fun buildFieldErrorsList(fieldErrors: List<FieldError>): List<ApiFieldError> {
    return fieldErrors.map {
      ApiFieldError(
        field = it.field,
        code = it.code,
        message = it.defaultMessage,
        rejectedObject = it.rejectedValue
      )
      }
  }

  private fun buildGlobalErrorsList(globalErrors: List<ObjectError>): List<ApiGlobalError> {
    return globalErrors.map { ApiGlobalError(it.code) }
  }
}
