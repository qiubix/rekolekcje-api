package pl.oaza.waw.rekolekcje.api.retreats.domain

enum class Turn {
  I,
  II,
  III,
  IV
}
