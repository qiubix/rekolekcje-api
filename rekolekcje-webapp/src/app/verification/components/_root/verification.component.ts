import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { AppSelectors } from '@core/store/app-selectors';
import { Router } from '@angular/router';
import { VerificationStatistics } from '@verification/model';
import { VerificationApiService } from '../../state/verification-api.service';
import { RetreatSummary } from '@retreats/models/retreat-summary.model';
import { Verification } from '@verification/state/verification-reducer';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'reko-verification',
  templateUrl: './verification.component.html',
  styleUrls: ['./verification.component.scss'],
})
export class VerificationComponent implements OnInit {
  stats$: Observable<VerificationStatistics>;
  activeRetreats$: Observable<RetreatSummary[]>;

  constructor(
    private router: Router,
    private store: Store<Verification.State>,
    public dialog: MatDialog,
    public verificationService: VerificationApiService
  ) {}

  ngOnInit(): void {
    this.stats$ = this.store.pipe(select(AppSelectors.getVerificationStatistics));
    this.activeRetreats$ = this.store.pipe(select(AppSelectors.getRetreatsList));
  }
}
