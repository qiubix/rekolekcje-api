package pl.oaza.waw.rekolekcje.api.members.storage

import pl.oaza.waw.rekolekcje.api.members.endpoint.MemberDetails

class MembersStorageBehaviour(private val database: MembersDatabase) {

  fun existMembersWithSampleData(members: List<MemberDetails>) {
    members.forEach { dto -> database.persistPartialMemberData(dto) }
  }

  fun existsSingleMemberWithSampleData(dto: MemberDetails) {
    database.persistPartialMemberData(dto)
  }

  fun memberWithTheSameDataIsFound(member: MemberDetails): Long {
    return database.findIdOfMemberWithTheSameData(member)
  }

//  fun historicalRetreatsForMemberAreFound(memberId: Long): Set<Long> {
//    return database.getAllRetreatTurnDataOfMember(memberId)
//        .mapNotNull { it.id }
//        .toSet()
//  }
}
