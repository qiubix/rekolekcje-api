package pl.oaza.waw.rekolekcje.api.retreats.infrastructure.persistance

import org.springframework.data.repository.Repository
import pl.oaza.waw.rekolekcje.api.retreats.domain.Retreat
import pl.oaza.waw.rekolekcje.api.retreats.domain.RetreatReadRepository
import pl.oaza.waw.rekolekcje.api.retreats.domain.RetreatWriteRepository

interface JpaRetreatReadRepository: RetreatReadRepository, Repository<Retreat, Long>

interface JpaRetreatWriteRepository: RetreatWriteRepository, Repository<Retreat, Long>