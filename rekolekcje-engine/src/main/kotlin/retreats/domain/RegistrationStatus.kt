package pl.oaza.waw.rekolekcje.api.retreats.domain

enum class RegistrationStatus {
  REGISTERED,
  WAITING_LIST
}