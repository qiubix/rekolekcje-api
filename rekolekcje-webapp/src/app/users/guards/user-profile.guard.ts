import { Injectable } from '@angular/core';
import {
  CanActivate,
  CanActivateChild,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { Users } from '../store/users-reduces';
import { AppSelectors } from '../../core/store/app-selectors';
import { catchError, filter, switchMap, take, tap } from 'rxjs/operators';
import { UserSummary } from '../model/user-summary.model';
import { UsersSharedActions } from '../store/users-actions';
import { AuthService } from '../../auth/auth.service';

@Injectable({ providedIn: 'root' })
export class UserProfileGuard implements CanActivate, CanActivateChild {
  constructor(private store: Store<Users.State>, private authService: AuthService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    const username = this.authService.getCurrentUsername();
    return this.getFromStoreOrAPI(username).pipe(
      switchMap(() => of(true)),
      catchError(() => of(false))
    );
  }

  private getFromStoreOrAPI(username: string): Observable<any> {
    return this.store.pipe(
      select(AppSelectors.getCurrentUser),
      tap((user: UserSummary) => {
        if (!user || user.username !== username) {
          this.store.dispatch(new UsersSharedActions.SelectUser(username));
        }
      }),
      filter((data: UserSummary) => data && data.username !== null && data.username === username),
      take(1)
    );
  }

  canActivateChild(
    childRoute: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> {
    return this.canActivate(childRoute, state);
  }
}
