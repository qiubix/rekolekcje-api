import { RetreatsComponent } from './_root/retreats.component';
import { RetreatAddEditDialog } from './add-edit/retreat-dialog/add-edit-dialog.component';
import { RetreatFormComponent } from './add-edit/retreat-form/retreat-form.component';
import { RetreatsListComponent } from './list/retreats-list.component';
import { RetreatDetailsComponent } from './details/retreat-details.component';
import { RetreatSummaryComponent } from './details/retreat-details-view/retreat-summary/retreat-summary.component';
import { MembersSimpleListComponent } from './details/retreat-details-view/members-simple-list/members-simple-list.component';
import { RetreatDetailsViewComponent } from './details/retreat-details-view/retreat-details-view.component';

export const components: any[] = [
  RetreatsComponent,
  RetreatAddEditDialog,
  RetreatFormComponent,
  RetreatsListComponent,
  RetreatDetailsComponent,
  RetreatDetailsViewComponent,
  RetreatSummaryComponent,
  MembersSimpleListComponent
];
