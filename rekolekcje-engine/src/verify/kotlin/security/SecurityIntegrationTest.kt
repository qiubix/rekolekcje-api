package pl.oaza.waw.rekolekcje.api.security

import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.Matchers
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import pl.oaza.waw.rekolekcje.api.shared.BaseIntegrationTest
import pl.oaza.waw.rekolekcje.api.members.endpoint.MembersEndpoint.Companion.MEMBERS_URI
import pl.oaza.waw.rekolekcje.api.user.domain.UserNotFoundException
import pl.oaza.waw.rekolekcje.api.user.domain.UserUpdateService
import pl.oaza.waw.rekolekcje.api.user.endpoint.AuthenticationRequest

internal class SecurityIntegrationTest : BaseIntegrationTest() {

  private val authenticationUri = AUTH_URI + SIGN_IN_URI
  private val refreshJwtUri = AUTH_URI + REFRESH_TOKEN_URI
  private val securedResourceUri = MEMBERS_URI

  private val username = "testuser"
  private val password = "testpassword"

  @Autowired
  private lateinit var userUpdateService: UserUpdateService

  @Autowired
  private lateinit var authApi: AuthApi

  @BeforeEach
  fun setup() {
    deleteTestUser()
  }

  @Test
  fun `as an registered user I want to obtain JWT on login`() {
    // given authorized user
    authApi.registerNewUserAsAdmin(username, password)
    val loginPayload = AuthenticationRequest(username, password)

    // when trying to sign in
    val response = restApi.performPost(authenticationUri, loginPayload)

    // then token is returned in response
    response.andExpect(status().isOk)
      .andExpect(jsonPath("$.token", Matchers.notNullValue()))
  }

  @Test
  fun `as an registered user I want to use my token to access secured resource`() {
    // given authorized user with a token
    authApi.registerNewUserAsAdmin(username, password)
    val token = authApi.getTokenForCredentials(username, password)

    // when trying to access protected resource using token
    val httpHeaders = createAuthorizationHeaders(token)
    val response = restApi.performGet(securedResourceUri, httpHeaders)

    // then resource is returned
    response.andExpect(status().isOk)
  }

  @Test
  fun `as an anonymous user without token I want to have denied access to secured resource`() {
    mockMvc
      .perform(get(securedResourceUri))
      .andExpect(status().isUnauthorized)
  }

  @Test
  fun `as an user with invalid token I want to have denied access to secured resource`() {
    // given registered user with invalid token
    authApi.registerNewUserAsAdmin(username, password)
    val invalidToken = "invalid token"

    // when trying to access protected resource with token
    val httpHeaders = createAuthorizationHeaders(invalidToken)
    val response = restApi.performGet(securedResourceUri, httpHeaders)

    // then request is denied
    response.andExpect(status().isUnauthorized)
  }

  @Test
  fun `as an registered user I cannot login with incorrect password`() {
    // given
    authApi.registerNewUserAsAdmin(username, password)

    // when
    val authenticationResult = authApi.requestAuthenticationToken(username, "incorrect-password")

    // then
    authenticationResult.andExpect(status().isUnauthorized)
  }

  @Test
  fun `as an authenticated user I can refresh my token`() {
    // given authenticated user
    authApi.registerNewUserAsAdmin(username, password)
    val oldToken = authApi.getTokenForCredentials(username, password)
    // wait before refreshing token, so that creation time is not the same
    Thread.sleep(1000)

    // when requesting to refresh token
    val response = restApi.performGet(refreshJwtUri, createAuthorizationHeaders(oldToken))

    // then new token is returned
    val refreshedToken = authApi.getTokenFromResponse(response)
    assertThat(refreshedToken)
      .isNotBlank()
      .isNotEqualTo(oldToken)
  }

  private fun createAuthorizationHeaders(token: String): HttpHeaders {
    val httpHeaders = HttpHeaders()
    httpHeaders.set(HttpHeaders.AUTHORIZATION, "Bearer $token")
    return httpHeaders
  }

  private fun deleteTestUser() {
    try {
      userUpdateService.deleteByUsername(username)
    } catch (ex: UserNotFoundException) {
      // ignore
    }
  }
}
