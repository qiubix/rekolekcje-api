package pl.oaza.waw.rekolekcje.api.retreats.infrastructure

import pl.oaza.waw.rekolekcje.api.retreats.domain.Retreat
import pl.oaza.waw.rekolekcje.api.retreats.domain.RetreatReadRepository
import pl.oaza.waw.rekolekcje.api.retreats.domain.RetreatWriteRepository
import pl.oaza.waw.rekolekcje.api.tools.repository.InMemoryReadRepository
import pl.oaza.waw.rekolekcje.api.tools.repository.InMemoryWriteRepository

class InMemoryRetreatReadRepository(storage: Map<Long, Retreat>):
  InMemoryReadRepository<Retreat>(storage), RetreatReadRepository

class InMemoryRetreatWriteRepository(storage: HashMap<Long, Retreat>) :
  InMemoryWriteRepository<Retreat>(storage), RetreatWriteRepository
