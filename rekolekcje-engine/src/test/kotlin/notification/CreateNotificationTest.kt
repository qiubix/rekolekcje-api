package pl.oaza.waw.rekolekcje.api.notification

import notification.domain.NotificationTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import pl.oaza.waw.rekolekcje.api.notification.dto.NotificationReason
import pl.oaza.waw.rekolekcje.api.notification.dto.NotificationStatus.SCHEDULED
import pl.oaza.waw.rekolekcje.api.notification.dto.NotificationSummary

internal class CreateNotificationTest : NotificationTest() {

  @Test
  fun `should persist notification with scheduled status when notification is created`() {
    // given
    val notification = sampleNotification()

    // when
    val notificationSummary = commandFacade.create(notification)

    // then
    assertSummaryMatchesDetails(
        notificationSummary,
        notification
    )
  }

  private fun sampleNotification(): CreateNotificationRequest {
    return CreateNotificationRequest(applicationId = 1L, reason = NotificationReason.APPLICATION_SUBMITTED, targetEmail = "a@test.com")
  }

  private fun assertSummaryMatchesDetails(summary: NotificationSummary, request: CreateNotificationRequest) {
    assertThat(summary.applicationId).isEqualTo(request.applicationId)
    assertThat(summary.reason).isEqualTo(request.reason)
    assertThat(summary.status).isEqualTo(SCHEDULED)
  }
}
