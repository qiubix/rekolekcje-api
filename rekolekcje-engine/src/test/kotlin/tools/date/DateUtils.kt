package pl.oaza.waw.rekolekcje.api.tools.date

import java.time.LocalDate
import java.time.Period
import java.time.format.DateTimeFormatter

class DateCreator {

  private val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")

  fun localDate(date: String): LocalDate {
    return LocalDate.parse(date, formatter)
  }
}

fun getAgeFromBirthDate(isoBirthDate: String) =
  Period.between(LocalDate.parse(isoBirthDate), LocalDate.now()).years
