import { NgModule } from '@angular/core';
import { MainMenuBarComponent } from './main-menu-bar/main-menu-bar.component';
import { AuthModule } from '@auth/auth.module';
import { UserMenuComponent } from './user-menu/user-menu.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '@shared/material';

@NgModule({
  imports: [CommonModule, RouterModule, MaterialModule, TranslateModule, AuthModule],
  declarations: [MainMenuBarComponent, UserMenuComponent],
  exports: [MainMenuBarComponent, UserMenuComponent],
})
export class NavigationModule {}
