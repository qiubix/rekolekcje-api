import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Config } from '../../../config/config';
import { Parish } from './models/parish.model';
import { Observable } from 'rxjs/internal/Observable';

@Injectable()
export class ParishService {
  constructor(private http: HttpClient) {}

  getAll(): Observable<Parish[]> {
    return this.http.get<Parish[]>(Config.endpoints.parishModule);
  }

  getOne(id: number): Observable<Parish> {
    return this.http.get<Parish>(Config.endpoints.parishModule + '/' + id);
  }

  update(details: Parish): Observable<Parish> {
    return this.http.put<Parish>(Config.endpoints.parishModule, details);
  }

  addNew(details: Parish): Observable<Parish> {
    return this.http.post<Parish>(Config.endpoints.parishModule, details);
  }

  deleteOne(id: number): Observable<any> {
    return this.http.delete<number>(Config.endpoints.parishModule + '/' + id);
  }
}
