package pl.oaza.waw.rekolekcje.api.application.domain

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import pl.oaza.waw.rekolekcje.api.application.ApplicationTestFixture
import pl.oaza.waw.rekolekcje.api.application.animatorApplication
import pl.oaza.waw.rekolekcje.api.application.participantApplication

internal class AddApplicationTest : ApplicationTestFixture() {

  private fun applicationsDataSource() = listOf(
    "animator application" to animatorApplication(),
    "participant application" to participantApplication()
  )

  @TestFactory
  fun `should add new application`() = applicationsDataSource().map { (testCase, application) ->
    dynamicTest(testCase) {
      commandFacade.submitNewApplication(application)

      val foundApplications = queryFacade.findAllApplications()
      assertThat(foundApplications)
        .usingElementComparatorIgnoringFields("id")
        .contains(application)
    }
  }

  @TestFactory
  fun `should return correct application after creation`() = applicationsDataSource().map { (testCase, application) ->
    dynamicTest(testCase) {
      val submittedApplication = commandFacade.submitNewApplication(application)

      val foundApplications = queryFacade.findAllApplications()
      assertThat(foundApplications).contains(submittedApplication)
      assertThat(submittedApplication.getId()).isNotNull()
    }
  }

  @Test
  fun `new animator application should not have specific retreat set yet`() {
    val application = animatorApplication().copy(retreatId = 123L)

    val submittedApplication = commandFacade.submitNewApplication(application)

    val expectedApplication = application.copy(id = submittedApplication.getId(), retreatId = null)
    assertThat(submittedApplication).isEqualTo(expectedApplication)
  }
}