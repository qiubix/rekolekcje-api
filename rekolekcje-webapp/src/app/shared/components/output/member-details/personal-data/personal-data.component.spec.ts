import { PersonalDataComponent } from './personal-data.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { membersTestingModule } from '@members/members-testing.module';

describe('PersonalDataComponent', () => {
  let component: PersonalDataComponent;
  let fixture: ComponentFixture<PersonalDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule(membersTestingModule).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonalDataComponent);
    component = fixture.componentInstance;
    component.personalData = {};
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
