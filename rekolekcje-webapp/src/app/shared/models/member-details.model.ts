import { Parish } from '@diocese/parish/models/parish.model';
import { Sex, Kwc, MemberStatus, PaymentStatus, ContactInfo, OpinionDetails } from './';

export interface MemberDetails {
  id?: number;
  personalData?: PersonalData;
  experience?: Experience;
  healthReport?: HealthReport;
  opinions?: OpinionDetails[];
}

export interface PersonalData {
  firstName?: string;
  lastName?: string;
  pesel?: string;
  phoneNumber?: number;
  email?: string;
  address?: Address;
  parishId?: number;
  newParish?: Parish;
  christeningDate?: string;
  father?: ContactInfo;
  mother?: ContactInfo;
  emergencyContact?: ContactInfo;
  schoolYear?: string;
  schoolType?: string;
  nameDay?: string;
  communityId?: number;
  sex?: Sex;
  age?: number;
  birthDate?: string;
  placeOfBirth?: string;
}

export interface Address {
  streetName?: string;
  streetNumber?: number;
  flatNumber?: number;
  postalCode?: string;
  city?: string;
}

export interface CurrentApplication {
  retreatId?: number;
  memberStatus?: MemberStatus;
  paymentStatus?: PaymentStatus;
  animator?: boolean;
  animatorRole?: string;
}

export interface AnimatorApplication {
  animatorRoles?: string[];
  availability?: string[];
  surety?: string;
  suretyEmail?: string;
}

export interface Experience {
  animator?: ContactInfo;
  kwc?: Kwc;
  formationInCommunity?: FormationInCommunity;
  talents?: Talents;
  deuterocatechumenate?: Deuterocatechumenate;
  historicalRetreats?: RetreatTurn[];
  experienceAsAnimator?: ExperienceAsAnimator;
}

export interface ExperienceAsAnimator {
  otherSkills?: string[];
  numberOfAnimatorDays?: number;
  diakonia?: string;
  retreatsAsAnimator?: string[];
  otherRetreats?: string[];
  leadingGroupToFormationStage?: string;
  communityFunctions?: string;
}

export enum Diakonia {
  MUSIC = 'Muzyczna',
  WORD = 'Słowa',
  FORMATION = 'Formacji diakonii',
  COMMUNICATION = 'Komunikowania społecznego',
  RETREAT = 'Oaz Rekolekcyjnych',
  EVANGELIZATION = 'Ewangelizacji',
  PRAYER = 'Modlitwy',
  LITURGIC = 'Liturgiczna'
}

export enum OtherSkills {
  DRIVERS_LICENCE = 'DRIVERS_LICENCE',
  SANEPID_BOOK = 'SANEPID_BOOK',
  PLAYS_INSTRUMENT = 'PLAYS_INSTRUMENT',
  SINGS = 'SINGS'
}

export enum OtherRetreats {
  KODA = 'KODA',
  ANIMATORS_SCHOOL = 'ANIMATORS_SCHOOL',
  ANIMATORS_SCHOOL_IN_PROGRESS = 'ANIMATORS_SCHOOL_IN_PROGRESS',
  ORAE = 'ORAE',
  ORD = 'ORD',
  KAMuzO = 'KAMuzO',
  MUSIC_ANIMATOR_SCHOOL = 'MUSIC_ANIMATOR_SCHOOL'
}

export interface FormationInCommunity {
  numberOfCommunionDays?: number;
  numberOfPrayerRetreats?: number;
  formationMeetingsInMonth?: number;
  formationInSmallGroup?: boolean;
}

export interface Talents {
  choir?: boolean;
  altarBoy?: boolean;
  flowerGirl?: boolean;
  musicalTalents?: string;
}

export interface Deuterocatechumenate {
  year?: number;
  stepsTaken?: number;
  stepsPlannedThisYear?: number;
  celebrationsTaken?: number;
  celebrationsPlannedThisYear?: number;
  missionCelebrationYear?: number;
  callByNameCelebrationYear?: number;
  holyTriduumYear?: number;
}

export interface RetreatTurn {
  stage?: string;
  year?: number;
}

export interface HealthReport {
  currentTreatment?: string;
  mentalDisorders?: string;
  medications?: string;
  allergies?: string;
  medicalDiet?: string;
  cannotHike?: boolean;
  hikingContraindications?: string;
  hasMotionSickness?: boolean;
  other?: string;
}
