package pl.oaza.waw.rekolekcje.api.application.domain

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import pl.oaza.waw.rekolekcje.api.application.endpoint.ApplicationNotFoundException
import pl.oaza.waw.rekolekcje.api.shared.value.PaymentStatus

class ApplicationCommandFacade(
  private val applicationReadRepository: ApplicationReadRepository,
  private val applicationWriteRepository: ApplicationWriteRepository,
  private val applicationNotifier: ApplicationNotifier
) {

  private val logger: Logger = LoggerFactory.getLogger(this.javaClass.name)
  private val changeApplicationStatusService: ChangeApplicationStatusService =
    ChangeApplicationStatusService(
      applicationReadRepository,
      applicationWriteRepository,
      applicationNotifier
    )

  fun submitNewApplication(application: RetreatApplication): RetreatApplication {
    logger.info("New application was submitted: $application.")
    val applicationToSave = application.clearRetreatIfAnimatorsApplication()

    val savedApplication = applicationWriteRepository.save(applicationToSave)
    applicationNotifier.newApplicationSubmitted(savedApplication.getId()!!)
    return savedApplication
  }

  fun updateApplication(application: RetreatApplication): RetreatApplication {
    val applicationId = application.getId() ?: throw IllegalStateException("Application to update must have id")
    logger.info("Updating application [$applicationId]")
    val existingApplication = applicationReadRepository.findById(applicationId)
      ?: throw ApplicationNotFoundException("Application [$applicationId] does not exist")

    val applicationToSave = if (application.isAnimator()) {
      application.copy(
        retreatId = existingApplication.retreatId,
        status = existingApplication.status
      )
    } else {
      application.copy(
        status = existingApplication.status
      )
    }
    return applicationWriteRepository.save(applicationToSave)
  }

  private fun RetreatApplication.clearRetreatIfAnimatorsApplication() =
    if (isAnimator()) withNoRetreat() else this


  fun changePaymentStatus(id: Long, paymentStatus: PaymentStatus): PaymentStatus {
    val application = applicationReadRepository.findById(id) ?: throw IllegalStateException()
    logger.info("Application $id: Changing payment status to $paymentStatus.")
    val applicationWithChangedStatus = application.changePaymentStatus(paymentStatus)
    applicationWriteRepository.save(applicationWithChangedStatus as RetreatApplication)
    return applicationWithChangedStatus.paymentStatus()
  }

  fun changeApplicationStatus(request: ChangeApplicationStatus): ApplicationStatus {
    logger.info("Application ${request.id}: Changing application status to ${request.status}")
    return when (request.status) {
      ApplicationStatus.VERIFIED -> changeApplicationStatusService.approve(request.id)
      ApplicationStatus.WAITING -> changeApplicationStatusService.resetStatus(request.id)
      ApplicationStatus.REGISTERED -> changeApplicationStatusService.enroll(request.id)
      ApplicationStatus.PROBLEM -> changeApplicationStatusService.reportProblem(request.id)
      ApplicationStatus.REJECTED -> changeApplicationStatusService.reject(
        request.id,
        requireNotNull(request.reason)
      )
      ApplicationStatus.WAITING_LIST -> changeApplicationStatusService.putOnWaitingList(request.id)
      ApplicationStatus.RESIGNED -> changeApplicationStatusService.resign(request.id)
    }.applicationStatus()
  }

  fun removeApplication(id: Long) {
    logger.info("Removing application [$id]")

    if (!applicationReadRepository.existsById(id)) {
      throw ApplicationNotFoundException("Cannot remove application [$id], because it does not exist")
    }

    applicationWriteRepository.deleteById(id)
  }

  fun assignAnimator(request: AssignAnimator) {
    val id = request.id
    val application = applicationReadRepository.findById(id) ?: throw IllegalArgumentException()
    if (application.applicationType == ApplicationType.PARTICIPANT) {
      throw IllegalArgumentException()
    }
    val assignedApplication = application.assignApplication(request.retreatId, request.role)
    applicationWriteRepository.save(assignedApplication)
  }

}
