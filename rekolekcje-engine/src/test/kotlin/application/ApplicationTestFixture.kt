package pl.oaza.waw.rekolekcje.api.application

import io.mockk.mockk
import org.junit.jupiter.api.AfterEach
import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationNotifier
import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationReadRepository
import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationWriteRepository
import pl.oaza.waw.rekolekcje.api.application.domain.RetreatApplication
import pl.oaza.waw.rekolekcje.api.application.infrastructure.ApplicationConfiguration
import pl.oaza.waw.rekolekcje.api.application.infrastructure.InMemoryApplicationReadRepository
import pl.oaza.waw.rekolekcje.api.application.infrastructure.InMemoryApplicationWriteRepository

abstract class ApplicationTestFixture {

  private val applicationConfiguration = ApplicationConfiguration()
  private val applicationStorage = HashMap<Long, RetreatApplication>()
  private val applicationReadRepository = InMemoryApplicationReadRepository(applicationStorage)
  private val applicationWriteRepository = InMemoryApplicationWriteRepository(applicationStorage)

  protected val applicationNotifierMock = mockk<ApplicationNotifier>(relaxUnitFun = true)
  protected val commandFacade = applicationConfiguration.applicationCommandFacade(
    applicationReadRepository = applicationReadRepository,
    applicationWriteRepository = applicationWriteRepository,
    applicationNotifier = applicationNotifierMock
  )
  protected val queryFacade = applicationConfiguration.applicationQueryFacade(applicationReadRepository)

  private val applicationReadRepositoryMock = mockk<ApplicationReadRepository>()
  private val applicationWriteRepositoryMock = mockk<ApplicationWriteRepository>()
  protected val commandFacadeWithMocks = applicationConfiguration.applicationCommandFacade(
    applicationReadRepositoryMock,
    applicationWriteRepositoryMock,
    applicationNotifierMock
  )

  @AfterEach
  internal fun tearDown() {
    applicationStorage.clear()
  }

  protected fun persistAllApplications(applications: List<RetreatApplication>) {
    applications.forEach { applicationWriteRepository.save(it) }
  }

  protected fun persistApplication(application: RetreatApplication): Long =
    applicationWriteRepository.save(application).getId() ?: throw IllegalStateException()

  protected fun findPersistedApplication(id: Long): RetreatApplication =
    applicationReadRepository.findById(id) ?: throw IllegalStateException()
}