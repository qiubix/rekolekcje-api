package pl.oaza.waw.rekolekcje.api.members.query

import pl.oaza.waw.rekolekcje.api.members.domain.Member
import pl.oaza.waw.rekolekcje.api.members.domain.MemberReadRepository
import pl.oaza.waw.rekolekcje.api.shared.exceptions.MemberNotFoundException

internal open class MembersQueryFacadeImpl internal constructor(
  private val memberReadRepository: MemberReadRepository
) : MembersQueryFacade {

  override fun findAllMembers(): List<Member> = memberReadRepository.findAll()

  override fun find(id: Long): Member = memberReadRepository.findById(id) ?: throw MemberNotFoundException(id)

  override fun findMultipleMembers(ids: Set<Long>): Set<Member> =
    ids
      .mapNotNull { memberReadRepository.findById(it) }
      .toSet()

  override fun existsById(memberId: Long) = memberReadRepository.existsById(memberId)

  override fun getEmail(memberId: Long): String? =
    memberReadRepository.findById(memberId)?.personalData?.contactInfo?.email

  override fun isAdult(memberId: Long): Boolean {
    val age = memberReadRepository.findById(memberId)?.personalData?.age() ?: 0
    return age >= 18
  }

}
