package pl.oaza.waw.rekolekcje.api.notification.domain.template.extractor

import pl.oaza.waw.rekolekcje.api.notification.domain.Notification

internal class RejectionReasonExtractor(
  private val notification: Notification
): Extractor(ExtractorType.REJECTION_REASON) {
  override fun extractValue(): String {
    return notification.additionalInfo ?: throw IllegalStateException("Could not find rejection reason")
  }
}