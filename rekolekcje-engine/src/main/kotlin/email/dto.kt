package pl.oaza.waw.rekolekcje.api.email

import pl.oaza.waw.rekolekcje.api.core.ddd.ValueObject

@ValueObject
data class SendEmailRequest(
  val to: String,
  val subject: String,
  val body: String
)

@ValueObject
data class SendEmailWithTemplateRequest(
  val to: String,
  val subject: String,
  val template: String,
  val templateModel: Map<String, Any>
)
