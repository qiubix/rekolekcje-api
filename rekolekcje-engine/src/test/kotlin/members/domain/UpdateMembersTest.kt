package pl.oaza.waw.rekolekcje.api.members.domain

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import pl.oaza.waw.rekolekcje.api.members.MemberTestFixture

internal class UpdateMembersTest : MemberTestFixture() {

  private val sampleMember1 = sampleMember(
    firstName = "Paul",
    lastName = "George",
    pesel = "90010112349"
  )
  private val sampleMember2 = sampleMember(
    firstName = "Roy",
    lastName = "Hibbert",
    pesel = "92010112343"
  )
  private val sampleMember3 = sampleMember(
    firstName = "George",
    lastName = "Hill",
    pesel = "93010112340"
  )

  @Test
  fun `should have the same number of members after updating one`() {
    // given
    commandFacade.saveAll(listOf(sampleMember1, sampleMember2, sampleMember3))
    val id = getIdOfCorrespondingMemberFromSystem(sampleMember1)
    val memberWithUpdatedData = fullMember().withId(id)

    // when
    commandFacade.save(memberWithUpdatedData)

    // then
    assertThat(queryFacade.findAllMembers()).hasSize(3)
  }

  @Test
  fun `should update existing member`() {
    // given
    commandFacade.saveAll(listOf(sampleMember1, sampleMember2, sampleMember3))
    val id = getIdOfCorrespondingMemberFromSystem(sampleMember2)
    val memberWithUpdatedData = fullMember().withId(id)

    // when
    commandFacade.save(memberWithUpdatedData)

    // then
    val storedMemberId = getIdOfCorrespondingMemberFromSystem(memberWithUpdatedData)
    val storedMember = queryFacade.find(storedMemberId)
    assertThat(storedMember).isEqualTo(memberWithUpdatedData)
  }

  @Test
  fun `should return correct member after update`() {
    val sampleMember = sampleMember()
    val savedMember = commandFacade.save(sampleMember)
    val updatedMember = fullMember().withId(savedMember.getId() ?: throw IllegalStateException())

    val returnedMember = commandFacade.save(updatedMember)

    assertThat(returnedMember).isEqualTo(updatedMember)
  }

}
