import { Component, Input } from '@angular/core';
import { FormationInCommunity } from '@shared/models';

@Component({
  selector: 'reko-formation-in-community-details',
  templateUrl: './formation-in-community-details.component.html',
  styleUrls: ['./formation-in-community-details.component.scss']
})
export class FormationInCommunityDetailsComponent {
  @Input() data: FormationInCommunity;
}
