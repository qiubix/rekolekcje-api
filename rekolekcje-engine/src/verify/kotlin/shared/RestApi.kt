package pl.oaza.waw.rekolekcje.api.shared

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.mock.web.MockMultipartFile
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import org.springframework.test.web.servlet.request.RequestPostProcessor

class RestApi(private val mockMvc: MockMvc, val jsonMapper: ObjectMapper) {

  @Throws(Exception::class)
  fun performPost(uri: String, body: Any): Response {
    val request = post(uri)
      .contentType(MediaType.APPLICATION_JSON)
      .content(jsonMapper.writeValueAsString(body))
    val resultActions = mockMvc.perform(request)
    return Response(resultActions)
  }

  @Throws(Exception::class)
  fun performPost(uri: String, body: Any, headers: HttpHeaders): Response {
    val request = post(uri)
      .contentType(MediaType.APPLICATION_JSON)
      .headers(headers)
      .content(jsonMapper.writeValueAsString(body))
    val resultActions = mockMvc.perform(request)
    return Response(resultActions)
  }

  @Throws(Exception::class)
  fun performPost(uri: String, body: Any, postProcessor: RequestPostProcessor): Response {
    val request = post(uri)
      .with(postProcessor)
      .contentType(MediaType.APPLICATION_JSON)
      .content(jsonMapper.writeValueAsString(body))
    val resultActions = mockMvc.perform(request)
    return Response(resultActions)
  }

  @Throws(Exception::class)
  fun performGet(uri: String): Response {
    val request = get(uri)
    val resultActions = mockMvc.perform(request)
    return Response(resultActions)
  }

  @Throws(Exception::class)
  fun performGet(uri: String, postProcessor: RequestPostProcessor): Response {
    val request = get(uri)
      .with(postProcessor)
    val resultActions = mockMvc.perform(request)
    return Response(resultActions)
  }

  @Throws(Exception::class)
  fun performGet(uri: String, headers: HttpHeaders): Response {
    val request = get(uri).headers(headers)
    val resultActions = mockMvc.perform(request)
    return Response(resultActions)
  }

  fun performPut(uri: String, body: String): Response {
    val request = put(uri)
      .contentType(MediaType.TEXT_PLAIN)
      .content(body)
    val resultActions = mockMvc.perform(request)
    return Response(resultActions)
  }

  fun performPut(uri: String, body: Any, postProcessor: RequestPostProcessor): Response {
    val request = put(uri)
      .with(postProcessor)
      .contentType(MediaType.APPLICATION_JSON)
      .content(jsonMapper.writeValueAsString(body))
    val resultActions = mockMvc.perform(request)
    return Response(resultActions)
  }

  @Throws(Exception::class)
  fun performPut(uri: String, body: Any): Response {
    val request = put(uri)
      .contentType(MediaType.APPLICATION_JSON)
      .content(jsonMapper.writeValueAsString(body))
    val resultActions = mockMvc.perform(request)
    return Response(resultActions)
  }

  @Throws(Exception::class)
  fun performDelete(uri: String): Response {
    val request = delete(uri)
    val resultActions = mockMvc.perform(request)
    return Response(resultActions)
  }

  fun performFileUpload(uri: String, fileContent: String): Response {
    val file = MockMultipartFile("file", fileContent.toByteArray())
    val request = multipart(uri).file(file)
    val resultActions = mockMvc.perform(request)
    return Response(resultActions)
  }
}
