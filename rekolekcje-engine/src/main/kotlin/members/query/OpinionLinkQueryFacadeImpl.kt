package pl.oaza.waw.rekolekcje.api.members.query

import org.springframework.beans.factory.annotation.Value
import pl.oaza.waw.rekolekcje.api.members.infrastructure.encryption.MembersEncryptionService

class OpinionLinkQueryFacadeImpl internal constructor(
  private val membersEncryptionService: MembersEncryptionService
) : OpinionLinkQueryFacade {

  @Value("\${application.opinion.link}")
  private lateinit var opinionLinkPrefix: String

  override fun generateLink(memberId: Long): String {
    return opinionLinkPrefix + membersEncryptionService.encryptMemberId(memberId)
  }

  override fun retrieveMemberId(encryptedMemberId: String): Long {
    return membersEncryptionService.decryptMemberId(encryptedMemberId)
  }
}
