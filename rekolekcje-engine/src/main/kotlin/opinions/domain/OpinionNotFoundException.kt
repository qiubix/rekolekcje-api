package pl.oaza.waw.rekolekcje.api.opinions.domain

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.NOT_FOUND)
class OpinionNotFoundException(id: Long) : RuntimeException(String.format(MESSAGE, id)) {

  companion object {
    private const val MESSAGE = "No opinion found with id: %d."
  }
}