package pl.oaza.waw.rekolekcje.api.parish.api

import pl.oaza.waw.rekolekcje.api.shared.BaseApiBehaviour
import pl.oaza.waw.rekolekcje.api.shared.RestApi
import pl.oaza.waw.rekolekcje.api.parish.REGIONS_API_URI
import pl.oaza.waw.rekolekcje.api.parish.dto.RegionNotFoundException
import pl.oaza.waw.rekolekcje.api.parish.dto.RegionPayload

class RegionApiBehaviours(restApi: RestApi) :
  BaseApiBehaviour<RegionPayload>(restApi, REGIONS_API_URI) {

  fun resourceAmountIsRequested(): Int {
    return restApi.performGet(baseUri)
      .getContentAsList<RegionPayload>()
      .size
  }

  fun existsElementWithTheSameData(payload: RegionPayload): RegionPayload {
    val response = restApi.performGet(baseUri)
    val allElements = response.getContentAsList<RegionPayload>()
    val foundElement = allElements.find { haveTheSameData(payload, it) }
    return foundElement ?: throw getNotFoundException(payload)
  }

  override fun haveTheSameData(first: RegionPayload, second: RegionPayload): Boolean {
    return first.name == second.name
  }

  override fun getNotFoundException(payload: RegionPayload): RuntimeException {
    return RegionNotFoundException(payload)
  }
}
