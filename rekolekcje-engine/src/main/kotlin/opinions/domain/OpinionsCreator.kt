package pl.oaza.waw.rekolekcje.api.opinions.domain

import pl.oaza.waw.rekolekcje.api.opinions.endpoint.SubmitOpinionRequest
import java.time.Clock
import java.time.LocalDate

internal class OpinionsCreator(
    private val clock: Clock
) {
  fun fromSubmitRequest(submitRequest: SubmitOpinionRequest, memberId: Long) =
      with(submitRequest) {
        Opinion(
            memberId = memberId,
            content = content,
            createdDate = LocalDate.now(clock),
            submitter = submitter.toDomain()
        )
      }
}
