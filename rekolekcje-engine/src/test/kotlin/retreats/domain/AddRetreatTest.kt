package pl.oaza.waw.rekolekcje.api.retreats.domain

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import pl.oaza.waw.rekolekcje.api.retreats.RetreatTest
import pl.oaza.waw.rekolekcje.api.retreats.sampleRetreat
import pl.oaza.waw.rekolekcje.api.retreats.sampleRetreatMember
import pl.oaza.waw.rekolekcje.api.retreats.withMembers
import pl.oaza.waw.rekolekcje.api.shared.value.Stage

internal class AddRetreatTest : RetreatTest() {

  @Test
  fun `should add single retreat`() {
    // given
    val retreat = sampleRetreat()

    // when
    val addedRetreat = commandFacade.save(retreat)

    // then
    assertThat(queryFacade.findAll()).contains(addedRetreat)
  }

  @Test
  fun `should assign id on addition`() {
    // given
    val retreat = sampleRetreat()

    // when
    val addedRetreat = commandFacade.save(retreat)

    // then
    assertThat(addedRetreat.getId()).isNotNull()
  }

  @Test
  fun `should add with members`() {
    // given
    val members = setOf(
      sampleRetreatMember(2L, "Aaa", "One"),
      sampleRetreatMember(4L, "Bbbb", "One"),
      sampleRetreatMember(21L, "Ccc", "One"),
      sampleRetreatMember(22L, "Ddd", "One"),
      sampleRetreatMember(23L, "Eee", "One", animator = true)
    )
    val retreat = Retreat(
      stage = Stage.OND,
      status = RetreatStatus.ACTIVE,
      turn = Turn.II
    ).withMembers(members)

    // when
    val savedRetreat = commandFacade.save(retreat)

    // then
    assertThat(savedRetreat).isNotNull
    assertThat(savedRetreat.animators() + savedRetreat.participants()).isEqualTo(members)
  }
}
