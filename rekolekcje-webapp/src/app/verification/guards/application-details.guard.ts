import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { Verification } from '../state/verification-reducer';
import { catchError, filter, switchMap, take, tap } from 'rxjs/operators';
import { AppSelectors } from '@core/store/app-selectors';
import { RetreatApplication } from '@verification/model';
import { VerificationActions } from '../state/verification-actions';

@Injectable({ providedIn: 'root' })
export class ApplicationDetailsGuard implements CanActivate {
  constructor(private store: Store<Verification.State>) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    const applicationId = +next.params['id'];
    const animator = JSON.parse(next.queryParams['animator']);
    return this.getFromStoreOrAPI(applicationId, animator).pipe(
      switchMap(() => of(true)),
      catchError(() => of(false))
    );
  }

  getFromStoreOrAPI(applicationId: number, animator: boolean): Observable<any> {
    return this.store.pipe(
      select(AppSelectors.getSelectedApplication),
      tap((application: RetreatApplication) => {
        if (!application || application.id != applicationId) {
          this.store.dispatch(
            new VerificationActions.GetSelectedApplication({ id: applicationId })
          );
        }
      }),
      filter((data: RetreatApplication) => data && data.id === applicationId),
      take(1)
    );
  }
}
