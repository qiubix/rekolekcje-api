package pl.oaza.waw.rekolekcje.api.application.storage

import org.springframework.jdbc.core.JdbcTemplate
import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationStatus

class ApplicationDatabase(private val jdbcTemplate: JdbcTemplate) {

  fun clear() {
    jdbcTemplate.execute("DELETE FROM retreat_applications")
  }

  fun setApplicationStatus(id: Long, status: ApplicationStatus) {
    jdbcTemplate.update("UPDATE retreat_applications SET status = ? WHERE id = ?", status.toString(), id)
  }

  fun setRetreatId(id: Long, retreatId: Long) {
    jdbcTemplate.update("UPDATE retreat_applications SET retreat_id = ? WHERE id = ?", retreatId, id)
  }

}