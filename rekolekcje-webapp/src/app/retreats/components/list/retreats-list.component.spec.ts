import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetreatsListComponent } from './retreats-list.component';

describe('RetreatsListComponent', () => {
  let component: RetreatsListComponent;
  let fixture: ComponentFixture<RetreatsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RetreatsListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetreatsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
