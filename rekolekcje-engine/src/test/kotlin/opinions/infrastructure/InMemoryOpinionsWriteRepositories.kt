package pl.oaza.waw.rekolekcje.api.opinions.infrastructure

import pl.oaza.waw.rekolekcje.api.opinions.domain.Opinion
import pl.oaza.waw.rekolekcje.api.opinions.domain.OpinionsReadRepository
import pl.oaza.waw.rekolekcje.api.opinions.domain.OpinionsWriteRepository
import pl.oaza.waw.rekolekcje.api.tools.repository.InMemoryReadRepository
import pl.oaza.waw.rekolekcje.api.tools.repository.InMemoryWriteRepository

class InMemoryOpinionsWriteRepository(storage: HashMap<Long, Opinion>) :
    InMemoryWriteRepository<Opinion>(storage),
    OpinionsWriteRepository

class InMemoryOpinionsReadRepository(storage: HashMap<Long, Opinion>) :
    InMemoryReadRepository<Opinion>(storage),
    OpinionsReadRepository

