package pl.oaza.waw.rekolekcje.api.security

data class AuthenticationResponse(
  val token: String
)
