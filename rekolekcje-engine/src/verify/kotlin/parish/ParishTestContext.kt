package pl.oaza.waw.rekolekcje.api.parish

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.jdbc.core.JdbcTemplate
import pl.oaza.waw.rekolekcje.api.shared.RestApi
import pl.oaza.waw.rekolekcje.api.parish.api.ParishApiBehaviours
import pl.oaza.waw.rekolekcje.api.parish.api.ParishApiExpectations
import pl.oaza.waw.rekolekcje.api.parish.storage.ParishDatabase

@Configuration
@Profile("test")
internal class ParishTestContext {

  @Bean
  fun parishDatabase(jdbcTemplate: JdbcTemplate): ParishDatabase {
    return ParishDatabase(jdbcTemplate)
  }

  @Bean
  fun parishApiBehaviours(restApi: RestApi): ParishApiBehaviours {
    return ParishApiBehaviours(restApi)
  }

  @Bean
  fun parishApiExpectations(restApi: RestApi): ParishApiExpectations {
    return ParishApiExpectations(restApi)
  }
}
