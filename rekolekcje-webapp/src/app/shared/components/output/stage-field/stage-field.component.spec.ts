import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StageFieldComponent } from './stage-field.component';

describe('StageFieldComponent', () => {
  let component: StageFieldComponent;
  let fixture: ComponentFixture<StageFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [StageFieldComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StageFieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
