import { Pipe, PipeTransform } from '@angular/core';
import { MemberStatus } from '@shared/models';
import { TranslateService } from '@ngx-translate/core';

@Pipe({
  name: 'memberStatusTranslate'
})
export class MemberStatusTranslatePipe implements PipeTransform {
  constructor(private translate: TranslateService) {}

  transform(memberStatus: MemberStatus, args?: any): string {
    let value = '';
    const key = 'members.currentApplication.memberStatus.';
    this.translate.get(key + memberStatus).subscribe(it => (value = it));
    return value;
  }
}
