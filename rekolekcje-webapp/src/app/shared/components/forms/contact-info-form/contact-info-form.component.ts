import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ContactInfo } from '@shared/models';
import { FormBuilder, FormGroup } from '@angular/forms';

const resolvedPromise = Promise.resolve(null);

@Component({
  selector: 'reko-contact-info-form',
  templateUrl: './contact-info-form.component.html',
  styleUrls: ['./contact-info-form.component.scss']
})
export class ContactInfoFormComponent implements OnInit {
  @Input() title: string;
  @Input() contactInfo: ContactInfo;
  @Output() formReady = new EventEmitter<FormGroup>();
  public form: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.form = this.fb.group({
      id: this.contactInfo ? this.contactInfo.id : null,
      fullName: this.contactInfo ? this.contactInfo.fullName : null,
      phoneNumber: this.contactInfo ? this.contactInfo.phoneNumber : null,
      email: this.contactInfo ? this.contactInfo.email : null
    });
    resolvedPromise.then(() => {
      this.formReady.emit(this.form);
    });
  }
}
