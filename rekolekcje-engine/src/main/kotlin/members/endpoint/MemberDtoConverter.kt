package pl.oaza.waw.rekolekcje.api.members.endpoint

import pl.oaza.waw.rekolekcje.api.members.domain.Address
import pl.oaza.waw.rekolekcje.api.members.domain.Deuterocatechumenate
import pl.oaza.waw.rekolekcje.api.members.domain.Experience
import pl.oaza.waw.rekolekcje.api.members.domain.HealthReport
import pl.oaza.waw.rekolekcje.api.members.domain.HistoricalRetreat
import pl.oaza.waw.rekolekcje.api.members.domain.Kwc
import pl.oaza.waw.rekolekcje.api.members.domain.Member
import pl.oaza.waw.rekolekcje.api.members.domain.MemberContact
import pl.oaza.waw.rekolekcje.api.members.domain.OtherRetreats
import pl.oaza.waw.rekolekcje.api.members.domain.OtherSkills
import pl.oaza.waw.rekolekcje.api.members.domain.PersonalData
import pl.oaza.waw.rekolekcje.api.members.domain.Pesel
import pl.oaza.waw.rekolekcje.api.opinions.endpoint.OpinionDetailsResponse
import pl.oaza.waw.rekolekcje.api.shared.domain.Contact
import pl.oaza.waw.rekolekcje.api.shared.value.Stage
import java.time.LocalDate

fun toDetailsDto(member: Member, opinions: List<OpinionDetailsResponse> = emptyList()): MemberDetails {
  return MemberDetails(
    id = member.getId(),
    personalData = PersonalDataValue(
      firstName = member.personalData.contactInfo.firstName ?: throw IllegalStateException(),
      lastName = member.personalData.contactInfo.lastName ?: throw IllegalStateException(),
      pesel = member.personalData.pesel.value,
      parishId = member.personalData.parishId,
      communityId = member.personalData.communityId,
      email = member.personalData.contactInfo.email,
      phoneNumber = member.personalData.contactInfo.phoneNumber,
      address = member.personalData.address?.toDto(),
      mother = member.personalData.mother?.toDto(),
      father = member.personalData.father?.toDto(),
      birthDate = member.personalData.birthDate(),
      sex = member.personalData.sex(),
      age = member.personalData.age(),
      placeOfBirth = member.personalData.placeOfBirth,
      christeningDate = member.personalData.christeningDate,
      emergencyContact = member.personalData.emergencyContact?.toDto(),
      schoolYear = member.personalData.schoolYear,
      schoolType = member.personalData.schoolType,
      nameDay = member.personalData.nameDay
    ),
    experience = member.experience?.toDto() ?: ExperienceValue(),
    healthReport = member.healthReport?.toDto(),
    opinions = opinions
  )
}

private fun Address.toDto() = AddressValue(
  streetName = street,
  streetNumber = streetNumber,
  flatNumber = flatNumber,
  postalCode = postalCode,
  city = city
)

private fun Contact.toDto() = ContactInfo(
  id = id,
  fullName = fullName,
  phoneNumber = phoneNumber,
  email = email
)

private fun Experience.toDto() = ExperienceValue(
  kwc = kwc?.toDto(),
  animator = animator?.toDto(),
  formationInCommunity = FormationInCommunityValue(
    numberOfCommunionDays = numberOfCommunionDays,
    numberOfPrayerRetreats = numberOfPrayerRetreats,
    formationInSmallGroup = formationInSmallGroup,
    formationMeetingsInMonth = formationMeetingsInMonth
  ).takeUnless { it == FormationInCommunityValue() },
  historicalRetreats = historicalRetreats?.map { it.toDto() }?.toSet() ?: emptySet(),
  deuterocatechumenate = deuterocatechumenate?.toDto(),
  talents = TalentsValue(
    choir = choir,
    musicalTalents = musicalTalents,
    altarBoy = altarBoy,
    flowerGirl = flowerGirl
  ).takeUnless { it == TalentsValue() },
  experienceAsAnimator = ExperienceAsAnimatorValue(
    leadingGroupToFormationStage = leadingGroupToFormationStage,
    numberOfAnimatorDays = numberOfAnimatorDays,
    diakonia = diakonia,
    otherRetreats = getOtherRetreats(),
    otherSkills = getOtherSkills(),
    retreatsAsAnimator = convertRetreatsAsAnimator(getRetreatsAsAnimator()),
    communityFunctions = communityFunctions
  ).takeUnless { it == ExperienceAsAnimatorValue() }
)

private fun Kwc.toDto() = KwcDto(
  status = kwcStatus,
  year = kwcSince?.year
)

private fun Deuterocatechumenate.toDto() = DeuterocatechumenateValue(
  year = year,
  stepsTaken = stepsTaken,
  stepsPlannedThisYear = stepsPlannedThisYear,
  celebrationsTaken = celebrationsTaken,
  celebrationsPlannedThisYear = celebrationsPlannedThisYear,
  missionCelebrationYear = missionCelebrationYear,
  callByNameCelebrationYear = callByNameCelebrationYear,
  holyTriduumYear = holyTriduumYear
)

private fun convertRetreatsAsAnimator(rawValue: Set<String>?): Set<RetreatAsAnimatorDto> =
  rawValue?.map {
    RetreatAsAnimatorDto(
      stage = Stage.valueOf(it.split(":")[0]),
      quantity = it.split(":")[1].toInt()
    )
  }?.toSet() ?: emptySet()

private fun HistoricalRetreat.toDto(): RetreatTurnDto = RetreatTurnDto(
  stage = this.stage,
  year = Integer.valueOf(this.year)
)

private fun HealthReport.toDto() = HealthReportValue(
  currentTreatment = currentTreatment,
  medications = medications,
  allergies = allergies,
  cannotHike = cannotHike,
  hasMotionSickness = hasMotionSickness,
  hikingContraindications = illnessHistory,
  mentalDisorders = mentalDisorders,
  other = other
)

fun fromDto(dto: MemberDetails): Member {
  return Member(
    id = dto.id,
    personalData = PersonalData(
      contactInfo = MemberContact(
        firstName = dto.personalData.firstName,
        lastName = dto.personalData.lastName,
        phoneNumber = dto.personalData.phoneNumber,
        email = dto.personalData.email
      ),
      pesel = Pesel(dto.personalData.pesel),
      address = dto.personalData.address?.fromDto(),
      parishId = dto.personalData.parishId,
      communityId = dto.personalData.communityId,
      mother = dto.personalData.mother?.fromDto(),
      father = dto.personalData.father?.fromDto(),
      placeOfBirth = dto.personalData.placeOfBirth,
      christeningDate = dto.personalData.christeningDate,
      emergencyContact = dto.personalData.emergencyContact?.fromDto(),
      schoolYear = dto.personalData.schoolYear,
      schoolType = dto.personalData.schoolType,
      nameDay = dto.personalData.nameDay
    ),
    healthReport = dto.healthReport?.fromDto(),
    experience = Experience(
      kwc = dto.experience.kwc?.fromDto(),
      numberOfCommunionDays = dto.experience.formationInCommunity?.numberOfCommunionDays,
      numberOfPrayerRetreats = dto.experience.formationInCommunity?.numberOfPrayerRetreats,
      formationInSmallGroup = dto.experience.formationInCommunity?.formationInSmallGroup,
      formationMeetingsInMonth = dto.experience.formationInCommunity?.formationMeetingsInMonth,
      leadingGroupToFormationStage = dto.experience.experienceAsAnimator?.leadingGroupToFormationStage,
      deuterocatechumenate = dto.experience.deuterocatechumenate?.fromDto(),
      choir = dto.experience.talents?.choir,
      musicalTalents = dto.experience.talents?.musicalTalents,
      altarBoy = dto.experience.talents?.altarBoy,
      flowerGirl = dto.experience.talents?.flowerGirl,
      numberOfAnimatorDays = dto.experience.experienceAsAnimator?.numberOfAnimatorDays,
      diakonia = dto.experience.experienceAsAnimator?.diakonia,
      animator = dto.experience.animator?.fromDto(),
      historicalRetreats = dto.experience.historicalRetreats.map { it.fromDto() },
      animatorsExperience = fromAnimatorsExperienceDto(dto.experience.experienceAsAnimator?.retreatsAsAnimator ?: emptySet()),
      otherRetreats = fromOtherRetreatsDto(dto.experience.experienceAsAnimator?.otherRetreats ?: emptySet()),
      otherSkills = fromOtherSkillsDto(dto.experience.experienceAsAnimator?.otherSkills ?: emptySet()),
      communityFunctions = dto.experience.experienceAsAnimator?.communityFunctions
    )
  )
}

private fun DeuterocatechumenateValue.fromDto() = Deuterocatechumenate(
  year = year,
  stepsTaken = stepsTaken,
  stepsPlannedThisYear = stepsPlannedThisYear,
  celebrationsTaken = celebrationsTaken,
  celebrationsPlannedThisYear = celebrationsPlannedThisYear,
  missionCelebrationYear = missionCelebrationYear,
  holyTriduumYear = holyTriduumYear,
  callByNameCelebrationYear = callByNameCelebrationYear
)

private fun KwcDto.fromDto() = Kwc(
  kwcStatus = status,
  kwcSince = if (year != null) LocalDate.of(year, 1, 1) else null
)

private fun ContactInfo.fromDto() = Contact(
  id = id,
  fullName = fullName,
  phoneNumber = phoneNumber,
  email = email
)

private fun AddressValue.fromDto() = Address(
  street = streetName,
  streetNumber = streetNumber,
  flatNumber = flatNumber,
  postalCode = postalCode,
  city = city
)

private fun HealthReportValue.fromDto() = HealthReport(
  currentTreatment = currentTreatment,
  medications = medications,
  allergies = allergies,
  cannotHike = cannotHike,
  hasMotionSickness = hasMotionSickness,
  illnessHistory = hikingContraindications,
  mentalDisorders = mentalDisorders,
  other = other
)

private fun RetreatTurnDto.fromDto(): HistoricalRetreat = HistoricalRetreat(
  stage = stage,
  year = year.toString()
)

private fun fromAnimatorsExperienceDto(dto: Set<RetreatAsAnimatorDto>): String? =
  if (dto.isNotEmpty()) dto.joinToString(",") { "${it.stage.name}:${it.quantity}" } else null

private fun fromOtherSkillsDto(dto: Set<OtherSkills>): String? =
  if (dto.isNotEmpty()) dto.joinToString(",") { it.name } else null

private fun fromOtherRetreatsDto(dto: Set<OtherRetreats>): String? =
  if (dto.isNotEmpty()) dto.joinToString(",") { it.name } else null
