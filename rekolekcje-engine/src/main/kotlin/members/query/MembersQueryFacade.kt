package pl.oaza.waw.rekolekcje.api.members.query

import pl.oaza.waw.rekolekcje.api.members.domain.Member


interface MembersQueryFacade {

  /**
   * Returns all summaries of members.
   */
  fun findAllMembers(): List<Member>

  /**
   * Finds details of a single member by ID.
   */
  fun find(id: Long): Member

  /**
   * Finds many members at once.
   */
  fun findMultipleMembers(ids: Set<Long>): Set<Member>

  /**
   * Checks if member exist.
   */
  fun existsById(memberId: Long): Boolean

  fun getEmail(memberId: Long): String?

  fun isAdult(memberId: Long): Boolean

}