package pl.oaza.waw.rekolekcje.api.notification

import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.jdbc.core.JdbcTemplate
import pl.oaza.waw.rekolekcje.api.notification.api.NotificationDatabase
import pl.oaza.waw.rekolekcje.api.notification.api.NotificationsEventsBehaviour
import pl.oaza.waw.rekolekcje.api.notification.storage.NotificationStorageExpectations

@Configuration
@Profile("test")
internal class NotificationTestContext {

  @Bean
  fun notificationDatabase(jdbcTemplate: JdbcTemplate): NotificationDatabase {
    return NotificationDatabase(jdbcTemplate)
  }

  @Bean
  fun notificationsEventsBehaviour(eventPublisher: ApplicationEventPublisher): NotificationsEventsBehaviour =
      NotificationsEventsBehaviour(eventPublisher)

  @Bean
  fun notificationStorageExpectations(database: NotificationDatabase): NotificationStorageExpectations {
    return NotificationStorageExpectations(database)
  }
}
