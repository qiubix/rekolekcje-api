package pl.oaza.waw.rekolekcje.api.retreats.storage

import org.springframework.jdbc.core.JdbcTemplate

class RetreatsDatabase(private val jdbcTemplate: JdbcTemplate) {

  fun clear() {
    jdbcTemplate.execute("DELETE FROM retreats")
  }
}
