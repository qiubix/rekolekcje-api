import { Pipe, PipeTransform } from '@angular/core';
import { Parish } from '../../../diocese/parish/models/parish.model';

@Pipe({
  name: 'formatParish'
})
export class FormatParishPipe implements PipeTransform {
  transform(parish: Parish, args?: any): string {
    return parish ? parish.name : '';
  }
}
