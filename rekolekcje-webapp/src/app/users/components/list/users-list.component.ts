import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { UserSummary } from '../../model/user-summary.model';
import { SaveUserRequest } from '../../model/save-user-request.model';
import { ChangePasswordDialogComponent, UsersAddEditDialogComponent } from '@users/components';
import { DeleteConfirmAlertDialog } from '@shared/components';
import { UserRole } from '../../model/role.model';
import { ChangePasswordRequest } from '../../model/change-password-request.model';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss'],
})
export class UsersListComponent implements OnInit {
  @Input()
  set users(users: UserSummary[]) {
    this.dataSource.data = users;
    this._users = users;
  }

  @Output() addUser: EventEmitter<SaveUserRequest> = new EventEmitter();
  @Output() editUser: EventEmitter<SaveUserRequest> = new EventEmitter();
  @Output() deleteUser: EventEmitter<string> = new EventEmitter();
  @Output() changePassword: EventEmitter<ChangePasswordRequest> = new EventEmitter();

  @ViewChild(MatPaginator)
  public paginator: MatPaginator;

  @ViewChild(MatSort)
  public sort: MatSort;

  get users(): UserSummary[] {
    return this._users;
  }

  public _users: UserSummary[] = [];

  public dataSource: MatTableDataSource<UserSummary> = new MatTableDataSource<UserSummary>();

  public displayedColumns: string[] = ['username', 'email', 'roles', 'enabled', 'options'];

  userRoles = UserRole;
  userRoleKeys = Object.keys(this.userRoles);

  constructor(private readonly cd: ChangeDetectorRef, private readonly dialog: MatDialog) {}

  public ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  openSignUpUserDialog() {
    this.dialog
      .open(UsersAddEditDialogComponent, {
        disableClose: true,
      })
      .afterClosed()
      .subscribe((signUpRequest: SaveUserRequest) => {
        if (signUpRequest) {
          this.addUser.emit(signUpRequest);
        }
      });
  }

  public openConfirmDeleteAlert(username: string): void {
    const dialogRef = this.dialog.open(DeleteConfirmAlertDialog, { disableClose: false });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.deleteUser.emit(username);
      }
    });
  }

  openEditUserDialog(user: UserSummary) {
    this.dialog
      .open(UsersAddEditDialogComponent, {
        disableClose: true,
        data: {
          showPasswordForm: false,
          user: user,
        },
      })
      .afterClosed()
      .subscribe((request: SaveUserRequest) => {
        if (request) {
          this.editUser.emit(request);
        }
      });
  }

  openChangePasswordDialog(username: string) {
    this.dialog
      .open(ChangePasswordDialogComponent, {
        disableClose: true,
        data: { username: username },
      })
      .afterClosed()
      .subscribe((request: ChangePasswordRequest) => {
        if (request) {
          this.changePassword.emit(request);
        }
      });
  }

  toggleActiveUser(user: UserSummary) {
    const saveUserRequest: SaveUserRequest = {
      username: user.username,
      password: '',
      email: user.email,
      enabled: !user.enabled,
      roles: user.roles,
    };
    this.editUser.emit(saveUserRequest);
  }

  displayRoles(roles: UserRole[]) {
    return roles.map((it) => UserRole[it]).join(', ');
  }
}
