import { Component, Input, OnInit } from '@angular/core';
import { Transportation } from '@verification/model';

@Component({
  selector: 'reko-transportation-details',
  templateUrl: './transportation-details.component.html',
  styleUrls: ['./transportation-details.component.scss']
})
export class TransportationDetailsComponent implements OnInit {
  @Input() data: Transportation;
  @Input() title: string;

  constructor() {}

  ngOnInit() {}
}
