package pl.oaza.waw.rekolekcje.api.shared.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.NOT_FOUND)
class MemberNotFoundException : RuntimeException {

  companion object {
    private const val MESSAGE_FULL_DATA = "No member found with this data: %s."
    private const val MESSAGE_ID = "No member found with this id: %d."
  }

  constructor(id: Long) : super(String.format(MESSAGE_ID, id))
}