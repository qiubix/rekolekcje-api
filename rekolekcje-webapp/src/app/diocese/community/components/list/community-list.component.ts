import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Community } from '../../models/community.model';
import { CommunityDialogComponent } from '@diocese/community/components';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { DeleteConfirmAlertDialog } from '@shared/components';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'reko-community-list',
  templateUrl: './community-list.component.html',
  styleUrls: ['./community-list.component.scss'],
})
export class CommunityListComponent {
  @Output() addNew: EventEmitter<Community> = new EventEmitter<Community>();
  @Output() delete: EventEmitter<number> = new EventEmitter<number>();
  @Output() edit: EventEmitter<Community> = new EventEmitter<Community>();

  dataSource: MatTableDataSource<Community> = new MatTableDataSource<Community>();
  displayedColumns = ['name', 'address', 'options'];

  addDialogTitle$: Observable<string>;
  editDialogTitle$: Observable<string>;

  constructor(private dialog: MatDialog, private translate: TranslateService) {
    this.addDialogTitle$ = this.translate.get('community.dialog.add');
    this.editDialogTitle$ = this.translate.get('community.dialog.edit');
  }

  @Input()
  set communities(communities: Community[]) {
    this.dataSource.data = communities;
  }

  openAddEditDialog(community: Community = {}): void {
    this.dialog
      .open(CommunityDialogComponent, {
        data: {
          dialogTitle: community.id ? this.editDialogTitle$ : this.addDialogTitle$,
          community: community,
        },
        disableClose: true,
      })
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          if (result.id) {
            this.edit.emit(result);
          } else {
            this.addNew.emit(result);
          }
        }
      });
  }

  openConfirmDeleteAlert(id: number): void {
    this.dialog
      .open(DeleteConfirmAlertDialog)
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.delete.emit(id);
        }
      });
  }
}
