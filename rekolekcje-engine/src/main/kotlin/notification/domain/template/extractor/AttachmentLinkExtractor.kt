package pl.oaza.waw.rekolekcje.api.notification.domain.template.extractor

import pl.oaza.waw.rekolekcje.api.application.domain.RetreatApplication
import pl.oaza.waw.rekolekcje.api.members.query.MembersQueryFacade

internal class AttachmentLinkExtractor(
  private val application: RetreatApplication,
  private val membersQueryFacade: MembersQueryFacade
) : Extractor(ExtractorType.ATTACHMENT_LINK) {

  companion object {
    const val ANIMATOR_ADULT = "https://drive.google.com/file/d/1HSFnUnFQNdGb0V8G1-WWsV7Xac6IhiAH/view?usp=sharing"
    const val ANIMATOR_CHILD = "https://drive.google.com/file/d/1HWpCHL10czg1I3-W5e95Qjsz96fKUWex/view?usp=sharing"
    const val PARTICIPANT_CHILD = "https://drive.google.com/file/d/1HPtoRhiVUfrQY1ia7z_fKe_uFekyzVFV/view?usp=sharing"
    const val PARTICIPANT_ADULT = "https://drive.google.com/file/d/1HBvPJDrBnHDm1h0Sj-rPb2dN_n4c0Bq2/view?usp=sharing"
  }

  override fun extractValue(): String {
    val isAdult = membersQueryFacade.isAdult(application.memberId)
    return when {
      application.isAnimator() && isAdult -> ANIMATOR_ADULT
      application.isAnimator() && !isAdult -> ANIMATOR_CHILD
      !application.isAnimator() && isAdult -> PARTICIPANT_ADULT
      !application.isAnimator() && !isAdult -> PARTICIPANT_CHILD
      else -> PARTICIPANT_CHILD
    }
  }

}