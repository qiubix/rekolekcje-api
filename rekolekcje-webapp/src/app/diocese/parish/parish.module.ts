import { NgModule } from '@angular/core';
import { ParishRoutingModule } from './parish-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { ParishService } from './parish.service';
import * as fromComponents from './components';
import { ParishAddEditDialogComponent, ParishComponent } from './components';

@NgModule({
  imports: [SharedModule, ParishRoutingModule],
  declarations: [fromComponents.components],
  providers: [ParishAddEditDialogComponent, ParishService],
  exports: [ParishComponent]
})
export class ParishModule {}
