import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { MemberSummary } from 'app/members/models/member-summary.model';
import { RetreatMember } from '@retreats/models/retreat-details.model';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'reko-members-simple-list',
  templateUrl: './members-simple-list.component.html',
  styleUrls: ['./members-simple-list.component.scss'],
})
export class MembersSimpleListComponent implements OnInit, OnChanges {
  displayedColumns = ['firstName', 'lastName', 'status', 'options'];

  @ViewChild('memberInput') memberInput: ElementRef;

  availableMembers: MemberSummary[] = [];

  private _members: RetreatMember[];

  @Input()
  set members(members: RetreatMember[]) {
    this._members = members;
    this.membersDataSource.data = members;
  }

  get members(): RetreatMember[] {
    return this._members;
  }

  membersDataSource: MatTableDataSource<RetreatMember> = new MatTableDataSource<RetreatMember>();

  @Output() goToDetails: EventEmitter<Number> = new EventEmitter<Number>();
  @Output() delete: EventEmitter<Number> = new EventEmitter<Number>();

  ngOnInit(): void {}

  ngOnChanges(): void {
    // if (this.allMembers !== null) {
    //   const registeredMembers = this.allMembers.filter(it => this.retreatMemberIds.includes(it.id));
    //   this.membersDataSource = new MatTableDataSource<MemberSummary>(registeredMembers);
    //   this.availableMembers = this.allMembers.filter(it => !this.isMemberSelected(it));
    // }
  }

  private isMemberSelected(member: MemberSummary): boolean {
    return !!this.membersDataSource.data.find((it) => member.id === it.memberId);
  }

  // onAddMember(event: MatAutocompleteSelectedEvent) {
  //   this.memberInput.nativeElement.value = '';
  //   const memberToAdd = event.option.value;
  //   const data = this.membersDataSource.data;
  //   data.push(memberToAdd);
  //   this.membersDataSource.data = data;
  //   this.addMember.emit(memberToAdd);
  // }

  removeMember(member: RetreatMember) {
    const id = member.memberId;
    this.membersDataSource.data = this.membersDataSource.data.filter((it) => it.memberId !== id);
    this.delete.emit(id);
  }

  memberDetails(member: RetreatMember) {
    this.goToDetails.emit(member.memberId);
  }

  // getParishName(parishId: number): string {
  //   const parish = this.parishes.find(p => p.id === parishId);
  //   return parish ? parish.name : '';
  // }
}
