package pl.oaza.waw.rekolekcje.api.members

import org.junit.jupiter.api.Test
import org.springframework.security.test.context.support.WithMockUser
import pl.oaza.waw.rekolekcje.api.members.utils.memberWithSampleData
import pl.oaza.waw.rekolekcje.api.members.utils.withId

class DeleteMembersAcceptanceTest : BaseMembersAcceptanceTest() {

  @Test
  @WithMockUser
  fun `should delete a single member`() {
    // given
    val member = memberWithSampleData()
    whenInApi.singleIsAdded(member)
    val existingMember = whenInApi.existsElementWithTheSameData(member)
    val id = existingMember.id ?: throw IllegalStateException()

    // when
    val response = whenInApi.singleIsDeleted(id)

    // then
    response.hasStatusOk()
    thenInApi.doesNotExist(existingMember.withId(id))
  }

}
