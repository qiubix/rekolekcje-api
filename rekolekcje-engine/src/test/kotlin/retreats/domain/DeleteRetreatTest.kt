package pl.oaza.waw.rekolekcje.api.retreats.domain

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import pl.oaza.waw.rekolekcje.api.retreats.RetreatTest
import pl.oaza.waw.rekolekcje.api.retreats.sampleRetreat
import pl.oaza.waw.rekolekcje.api.shared.value.Stage

internal class DeleteRetreatTest : RetreatTest() {

  @Test
  fun `should delete one retreat turn`() {
    // given
    val first = sampleRetreat(Stage.ODB, Turn.I)
    val second = sampleRetreat(Stage.OND, Turn.II)
    val firstWithId = commandFacade.save(first)
    val secondWithId = commandFacade.save(second)
    val idToDelete = secondWithId.getId() ?: throw IllegalStateException()

    // when
    commandFacade.delete(idToDelete)

    // then
    val remainingRetreats = queryFacade.findAll()
    assertThat(remainingRetreats)
      .doesNotContain(secondWithId)
      .hasSize(1)
      .contains(firstWithId)
  }
}
