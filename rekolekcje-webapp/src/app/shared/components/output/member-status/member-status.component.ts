import { Component, Input, OnInit } from '@angular/core';
import { MemberStatus } from '../../../models/member-status.model';

@Component({
  selector: 'reko-member-status',
  templateUrl: './member-status.component.html',
  styleUrls: ['./member-status.component.scss']
})
export class MemberStatusComponent implements OnInit {
  @Input() memberStatus: MemberStatus;

  ngOnInit() {}
}
