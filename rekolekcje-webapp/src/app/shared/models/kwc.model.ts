export interface Kwc {
  status: KwcStatus;
  year?: number;
}

export enum KwcStatus {
  MEMBER = 'Członek',
  CANDIDATE = 'Kandydat',
  NOT_ENGAGED = 'Nie należy'
}
