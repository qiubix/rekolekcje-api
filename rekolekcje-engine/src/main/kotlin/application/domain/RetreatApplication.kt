package pl.oaza.waw.rekolekcje.api.application.domain

import pl.oaza.waw.rekolekcje.api.shared.domain.Contact
import pl.oaza.waw.rekolekcje.api.shared.domain.EntityWithId
import pl.oaza.waw.rekolekcje.api.shared.value.PaymentStatus

data class RetreatApplication(
  private val id: Long? = null,
  val applicationType: ApplicationType,
  val firstName: String,
  val lastName: String,
  val memberId: Long,
  val status: ApplicationStatus = ApplicationStatus.WAITING,
  val statusAdditionalDetails: String? = null,
  val paymentStatus: PaymentStatus = PaymentStatus.NOTHING,
  val surety: Contact? = null,
  val retreatId: Long? = null,
  val animatorRoles: Set<AnimatorRole>? = null,
  val availability: Set<Long>? = null,
  val selectedRole: AnimatorRole? = null
) : EntityWithId, VerifiableApplication {

  override fun getId(): Long? = id

  override fun withId(id: Long): EntityWithId = copy(id = id)

  override fun applicationStatus(): ApplicationStatus = status

  override fun statusAdditionalDetails(): String? = statusAdditionalDetails

  override fun paymentStatus(): PaymentStatus = paymentStatus

  override fun changeApplicationStatus(status: ApplicationStatus, additionalDetails: String?): VerifiableApplication =
    copy(status = status, statusAdditionalDetails = additionalDetails)

  override fun changePaymentStatus(newStatus: PaymentStatus): VerifiableApplication = copy(paymentStatus = newStatus)

  fun getType(): ApplicationType = applicationType

  fun isPending(): Boolean = status == ApplicationStatus.WAITING || status == ApplicationStatus.PROBLEM

  fun isAnimator() = applicationType == ApplicationType.ANIMATOR

  fun withNoRetreat() = copy(retreatId = null)

  fun applicationAssigned(): Boolean =
      applicationType == ApplicationType.PARTICIPANT
          || (applicationType == ApplicationType.ANIMATOR && retreatId != null && selectedRole != null)

  fun assignApplication(retreatId: Long, role: AnimatorRole): RetreatApplication =
      copy(retreatId = retreatId, selectedRole = role)
}
