import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { RetreatSummary } from '@retreats/models/retreat-summary.model';
import { RetreatApplication } from '@verification/model';
import { ContactInfo } from '@shared/models';

@Component({
  selector: 'reko-animator-application-form',
  templateUrl: './animator-application-form.component.html',
  styleUrls: ['./animator-application-form.component.scss']
})
export class AnimatorApplicationFormComponent implements OnInit {
  @Input() retreats: RetreatSummary[];
  @Input() data: RetreatApplication;
  @Output() formReady = new EventEmitter<FormGroup>();

  form: FormGroup;

  constructor(private _fb: FormBuilder) {}

  ngOnInit(): void {
    console.log('Init animator application form');
    this.form = this._fb.group({
      availability: [this.data ? this.data.availability : []],
      animatorRoles: [this.data ? this.data.animatorRoles : []],
      otherInfo: []
    });

    this.formReady.emit(this.form);
  }

  onSubFormInitialized(name: string, subForm: FormGroup) {
    this.form.addControl(name, subForm);
  }

  get surety(): ContactInfo {
    return this.data ? this.data.surety : null;
  }
}
