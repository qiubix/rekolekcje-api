import { Component, Input } from '@angular/core';
import { Parish } from '@diocese/parish/models/parish.model';

@Component({
  selector: 'parish-details',
  templateUrl: './parish-details.component.html',
  styleUrls: ['./parish-details.component.scss']
})
export class ParishDetailsComponent {
  @Input() parish: Parish;
}
