
import Versions.archUnitVersion
import Versions.assertJVersion
import Versions.embeddedPostgresVersion
import Versions.exposedVersion
import Versions.gagVersion
import Versions.greenMailVersion
import Versions.jjwtVersion
import Versions.jsonVersion
import Versions.junitVersion
import Versions.kotlinCsvVersion
import Versions.mockkVersion
import Versions.openapiUiVersion
import Versions.testContainersVersion

object Versions {
  const val swaggerVersion = "3.0.0"
  const val openapiUiVersion = "1.6.6"
  const val gagVersion = "1.0.1"
  const val jjwtVersion = "0.7.0"
  const val jsonVersion = "20160810"
  const val kotlinCsvVersion = "0.7.3"
  const val exposedVersion = "0.20.1"

  const val junitVersion = "5.5.2"
  const val assertJVersion = "3.19.0"
  const val embeddedPostgresVersion = "2.10"
  const val testContainersVersion = "1.15.1"
  const val mockkVersion = "1.9.3"
  const val greenMailVersion = "1.6.0"
  const val archUnitVersion = "0.15.0"
}

object Libs {
  object Spring {
    const val actuator = "org.springframework.boot:spring-boot-starter-actuator"
    const val dataJpa = "org.springframework.boot:spring-boot-starter-data-jpa"
    const val dataRest = "org.springframework.boot:spring-boot-starter-data-rest"
    const val security = "org.springframework.boot:spring-boot-starter-security"
    const val jdbc = "org.springframework.boot:spring-boot-starter-jdbc"
    const val web = "org.springframework.boot:spring-boot-starter-web"
    const val mail = "org.springframework.boot:spring-boot-starter-mail"
    const val thymeleaf = "org.springframework.boot:spring-boot-starter-thymeleaf"
    const val validation = "org.springframework.boot:spring-boot-starter-validation"
  }

  object Kotlin {
    const val sdtLib = "org.jetbrains.kotlin:kotlin-stdlib-jdk8"
    const val reflect = "org.jetbrains.kotlin:kotlin-reflect"
    const val jacksonModule = "com.fasterxml.jackson.module:jackson-module-kotlin"
    const val csv = "com.github.doyaaaaaken:kotlin-csv-jvm:$kotlinCsvVersion"
  }

  const val openApiUi = "org.springdoc:springdoc-openapi-ui:$openapiUiVersion"

  const val liquibase = "org.liquibase:liquibase-core"
  const val json = "org.json:json:$jsonVersion"
  const val jjwt = "io.jsonwebtoken:jjwt:$jjwtVersion"
  const val gag = "com.google.gag:gag:$gagVersion"
  const val hibernateJava8 = "org.hibernate:hibernate-java8"
  const val jacksonJsr310 = "com.fasterxml.jackson.datatype:jackson-datatype-jsr310"
  const val exposed = "org.jetbrains.exposed:exposed-spring-boot-starter:$exposedVersion"
}

object RuntimeLibs {
  object Spring {
    const val devTools = "org.springframework.boot:spring-boot-devtools"
  }

  const val postgresql = "org.postgresql:postgresql"
}

object TestLibs {

  object Spring {
    const val test = "org.springframework.boot:spring-boot-starter-test"
    const val security = "org.springframework.security:spring-security-test"
  }

  const val junit = "org.junit.jupiter:junit-jupiter:$junitVersion"
  const val assertJ = "org.assertj:assertj-core:$assertJVersion"
  const val postgresqlEmbedded = "ru.yandex.qatools.embed:postgresql-embedded:$embeddedPostgresVersion"
  const val testContainersPostgresql = "org.testcontainers:postgresql:$testContainersVersion"
  const val testContainersJUnit = "org.testcontainers:junit-jupiter:$testContainersVersion"
  const val mockk = "io.mockk:mockk:$mockkVersion"
  const val greenMail = "com.icegreen:greenmail-junit5:$greenMailVersion"
  const val greenMailSpring = "com.icegreen:greenmail-spring:$greenMailVersion"
  const val archUnit = "com.tngtech.archunit:archunit-junit5:$archUnitVersion"
  const val awaitility = "org.awaitility:awaitility-kotlin:4.0.3"
}
