import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Talents } from '@shared/models';

const resolvedPromise = Promise.resolve(null);

@Component({
  selector: 'reko-talent-form',
  templateUrl: './talent-form.component.html',
  styleUrls: ['./talent-form.component.scss']
})
export class TalentFormComponent implements OnInit {
  form: FormGroup;

  @Input() data: Talents;
  @Output() formReady = new EventEmitter<FormGroup>();

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.form = this.fb.group({
      choir: this.data ? this.data.choir : null,
      altarBoy: this.data ? this.data.altarBoy : null,
      flowerGirl: this.data ? this.data.flowerGirl : null,
      musicalTalents: this.data ? this.data.musicalTalents : ''
    });
    resolvedPromise.then(() => {
      this.formReady.emit(this.form);
    });
  }
}
