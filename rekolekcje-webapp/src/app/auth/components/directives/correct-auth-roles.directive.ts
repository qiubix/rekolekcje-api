import { Directive, Input, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthService } from '../../auth.service';

@Directive({
  selector: '[rekoAuthRoles]',
  providers: [AuthService]
})
export class CorrectAuthRolesDirective implements OnInit {
  @Input() public rekoAuthRoles: string[] = [];

  constructor(
    private readonly templateRef: TemplateRef<any>,
    private readonly viewContainer: ViewContainerRef,
    private readonly authService: AuthService
  ) {}

  public ngOnInit(): void {
    this.validate();

    this.checkRoleAndCreateView();
  }

  private validate(): void {
    const errorMessage = 'No roles were provided to directive';

    if (!this.rekoAuthRoles && this.rekoAuthRoles.length === 0) {
      throw new Error(errorMessage);
    }
  }

  private checkRoleAndCreateView(): void {
    const currentUserRoles: string[] = this.authService.getCurrentUserRoles();
    if (currentUserRoles.length === 0) {
      this.viewContainer.clear();
      return;
    }

    const hasRequiredRole = this.validateHasRequiredRoles(currentUserRoles);
    if (hasRequiredRole) {
      this.viewContainer.createEmbeddedView(this.templateRef);
    } else {
      this.viewContainer.clear();
    }
  }

  private validateHasRequiredRoles(currentRoles: string[]): boolean {
    const hasRequiredRoleTest = currentRole => this.rekoAuthRoles.includes(currentRole);

    return currentRoles.some(hasRequiredRoleTest);
  }
}
