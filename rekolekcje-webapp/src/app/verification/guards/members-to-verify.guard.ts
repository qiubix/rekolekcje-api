import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { Members } from '../../members/state/members-reducer';
import { catchError, filter, switchMap, take, tap } from 'rxjs/operators';
import { AppSelectors } from '../../core/store/app-selectors';
import { MembersSharedActions } from '../../members/state/members-actions';
import { CommunityActions } from '../../diocese/community/store/community-actions';
import { RetreatsSharedActions } from '../../retreats/store/retreat-actions';
import { VerificationActions } from '../state/verification-actions';

@Injectable({ providedIn: 'root' })
export class MembersToVerifyGuard implements CanActivate {
  constructor(private store: Store<Members.State>) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.getFromStoreOrAPI().pipe(
      switchMap(() => of(true)),
      catchError(() => of(false))
    );
  }

  getFromStoreOrAPI(): Observable<any> {
    return this.store.pipe(
      select(AppSelectors.getVerificationStatistics),
      tap(stats => {
        if (!stats) {
          this.store.dispatch(new VerificationActions.GetStatistics());
        }
        // this.store.dispatch(new MembersSharedActions.LoadMembersToVerify());
        this.store.dispatch(new RetreatsSharedActions.LoadRetreatsList());
        this.store.dispatch(new CommunityActions.LoadCommunitiesList());
      }),
      filter(stats => stats != null),
      take(1)
    );
  }
}
