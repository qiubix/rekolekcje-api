package pl.oaza.waw.rekolekcje.api.application.domain

data class ChangeApplicationStatus(
  val id: Long,
  val status: ApplicationStatus,
  val reason: String? = null
)

data class AssignAnimator(
  val id: Long,
  val retreatId: Long,
  val role: AnimatorRole
)