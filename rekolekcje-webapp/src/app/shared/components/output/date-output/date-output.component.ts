import { Component, Input, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'reko-date-output',
  templateUrl: './date-output.component.html',
  styleUrls: ['./date-output.component.scss'],
})
export class DateOutputComponent implements OnInit {
  @Input() dateValue: string;

  formattedDate: string;

  ngOnInit() {
    if (this.dateValue) {
      this.formattedDate = moment(this.dateValue).locale('pl').format('L');
    }
  }
}
