import { Pipe, PipeTransform } from '@angular/core';
import { PaymentStatus } from '../../models/member-status.model';

@Pipe({
  name: 'colorForPaymentStatus'
})
export class ColorForPaymentStatusPipe implements PipeTransform {
  transform(paymentStatus: PaymentStatus, args?: any): String {
    switch (paymentStatus) {
      case PaymentStatus.DOWN_PAYMENT:
        return '#ffd740';
      case PaymentStatus.FULL:
        return 'green';
      default:
        return 'red';
    }
  }
}
