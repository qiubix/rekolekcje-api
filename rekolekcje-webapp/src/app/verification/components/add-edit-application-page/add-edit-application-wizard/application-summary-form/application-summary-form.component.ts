import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ContactInfo } from '@shared/models';
import { RetreatApplication, Transportation } from '@verification/model';

@Component({
  selector: 'reko-application-summary-form',
  templateUrl: './application-summary-form.component.html',
  styleUrls: ['./application-summary-form.component.scss']
})
export class ApplicationSummaryFormComponent implements OnInit {
  form: FormGroup;

  @Input() data: RetreatApplication;

  @Output() formReady = new EventEmitter<FormGroup>();

  constructor(private _fb: FormBuilder) {}

  ngOnInit() {
    this.form = this._fb.group({
      otherInfo: [this.data ? this.data.other : ''],
      certificationNeeded: [this.data ? this.data.certificationNeeded : false]
    });
    this.formReady.emit(this.form);
  }

  onSubFormInitialized(name: string, subForm: FormGroup) {
    this.form.addControl(name, subForm);
  }

  get transportation(): Transportation {
    return this.data ? this.data.transportation : null;
  }

  get certificationContact(): ContactInfo {
    return this.data ? this.data.certificationContact : null;
  }

  get otherInfoControl() {
    return this.form.get('otherInfo');
  }
}
