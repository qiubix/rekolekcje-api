import { Component, Inject, OnInit } from '@angular/core';
import { AuthService } from '@auth/auth.service';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env';
import { TranslateService } from '@ngx-translate/core';
import { Config } from '@config';
import { Store } from '@ngrx/store';
import { Members } from '@members/state/members-reducer';
import { VerificationActions } from '@verification/state/verification-actions';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'reko-alternative-import-dialog',
  templateUrl: './alternative-import-dialog.component.html',
  styleUrls: ['./alternative-import-dialog.component.scss'],
})
export class AlternativeImportDialogComponent implements OnInit {
  fileName = '';
  fileInvalid = true;
  importUrl: string;
  file: File;
  isAnimator: boolean = false;

  constructor(
    private http: HttpClient,
    private dialogRef: MatDialogRef<AlternativeImportDialogComponent>,
    private snackBar: MatSnackBar,
    private translate: TranslateService,
    private store: Store<Members.State>,
    private authService: AuthService,
    @Inject(MAT_DIALOG_DATA) private data?: any
  ) {
    this.importUrl = data ? data.importUrl : '/members/import';
  }

  ngOnInit() {}

  onFileSelected(event: any) {
    const file: File = event.target.files[0];
    console.log('File selected: ', file);
    if (file) {
      this.fileName = file.name;
      this.fileInvalid = false;
      this.file = file;
    }
  }

  submit() {
    console.log('Submitting file: ', this.fileName);
    const multipartData = new FormData();
    multipartData.append('file', this.file);
    const importUrl = this.isAnimator ? this.importUrl : '/application/participants/import';
    this.http
      .post(environment.apiUrl + importUrl, multipartData, {
        headers: { Authorization: 'Bearer ' + this.authService.getToken() },
      })
      .subscribe((response) => {
        this.snackBar.open(
          this.translate.instant('members.list.import.success'),
          this.translate.instant('shared.ok'),
          { duration: Config.notificationDuration }
        );
        this.store.dispatch(new VerificationActions.GetPendingApplications());
        this.dialogRef.close();
      });
  }
}
