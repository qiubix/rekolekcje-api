package pl.oaza.waw.rekolekcje.api.application.domain

data class VerificationStatistics(
  val numberOfActiveApplications: Int,
  val alreadyVerified: Int,
  val remainingToVerify: Int,
  val animatorsRemaining: Int,
  val participantsRemaining: Int
)
