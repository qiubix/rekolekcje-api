package pl.oaza.waw.rekolekcje.api.members

import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.springframework.security.test.context.support.WithMockUser
import pl.oaza.waw.rekolekcje.api.members.endpoint.MemberPublicData
import pl.oaza.waw.rekolekcje.api.members.endpoint.MemberSummary
import pl.oaza.waw.rekolekcje.api.members.utils.memberWithMinimalData
import pl.oaza.waw.rekolekcje.api.opinions.domain.Submitter
import pl.oaza.waw.rekolekcje.api.opinions.endpoint.SubmitOpinionRequest
import pl.oaza.waw.rekolekcje.api.opinions.endpoint.SubmitterApiModel

class FindMembersAcceptanceTest : BaseMembersAcceptanceTest() {

  @Test
  @WithMockUser
  fun `should get summary of all members when they are already saved`() {
    // given
    whenInApi.allAreAdded(members)

    // when
    val response = whenInApi.allAreRequested()

    // then
    thenInApi.responseHasSummaryOfAllMemberDetailsIgnoringId(response, members)
  }

  @Test
  @WithMockUser
  fun `should get one member when he is already saved`() {
    // given
    whenInApi.allAreAdded(members)
    val id = whenInApi.findIdOfElementWithTheSameData(sampleMember)

    // when
    val response = whenInApi.singleIsRequested(id)

    // then
    response.hasStatusOk()
    thenInApi.responseHasCorrectDataIgnoringId(response, sampleMember)
  }

  @Test
  @WithMockUser
  fun `should return full data of a single member`() {
    // given
    whenInApi.singleIsAdded(memberWithFullData)
    val expectedMember = whenInApi.existsElementWithTheSameData(memberWithFullData)

    // when
    val response = whenInApi.singleIsRequested(expectedMember.id)

    // then
    response.hasStatusOk()
    thenInApi.responseHasCorrectDataIgnoringId(response, expectedMember)
  }

  @Test
  @WithMockUser
  fun `should export data in file`() {
    // given
    whenInApi.singleIsAdded(memberWithFullData)
//    val existingMember = whenInApi.existsElementWithTheSameData(memberWithFullData)

    // when
    val exportResponse = whenInApi.allAreExported()

    // then
    exportResponse.hasStatusOk()
//    thenInApi.exportedFileContainsMembers(exportResponse, listOf(existingMember))
  }

  @Test
  @WithMockUser
  fun `should attach all member's opinions to member's details`() {
    // given
    val memberId = whenInApi.singleIsAdded(memberWithFullData).getContent<MemberSummary>().id
    val encryptedMemberId = whenInApi.encryptedMemberIdIsSentByEmail(memberId)
    val otherMemberId = whenInApi.singleIsAdded(memberWithMinimalData()).getContent<MemberSummary>().id
    val otherEncryptedMemberId = whenInApi.encryptedMemberIdIsSentByEmail(otherMemberId)
    val sampleOpinions = listOf(
      SubmitOpinionRequest(
        encryptedMemberId = encryptedMemberId,
        content = "$memberId: First opinion",
        submitter = SubmitterApiModel("Animator", "ani@mail.com", "123", Submitter.Role.ANIMATOR)
      ),
      SubmitOpinionRequest(
        encryptedMemberId = encryptedMemberId,
        content = "$memberId: Second opinion",
        submitter = SubmitterApiModel("Mod", "mod@mail.com", "342", Submitter.Role.MODERATOR)
      ),
      SubmitOpinionRequest(
        encryptedMemberId = otherEncryptedMemberId,
        content = "$otherMemberId: Only one opinion",
        submitter = SubmitterApiModel("Moderator", "mod@mail.com", "342423", Submitter.Role.MODERATOR)
      )
    )
    val existingOpinions = whenInOpinionsApi.alreadyExistSomeOpinions(sampleOpinions)

    // when
    val response = whenInApi.singleIsRequested(memberId)

    // then
    response.hasStatusOk()
    val expectedOpinions = existingOpinions.filter { it.memberId == memberId }
    thenInApi.responseHasMemberOpinions(response, expectedOpinions)
  }

  @Test
  fun `should allow unauthorized user to fetch member's full name`() {
    // given
    val memberId = whenInApi.singleIsAddedByAuthorizedUser(memberWithFullData)
    val encryptedMemberId = whenInApi.encryptedMemberIdIsSentByEmail(memberId)

    // when
    val response = whenInApi.fullNameIsRequestedAsAnonymous(encryptedMemberId)

    // then
    val expectedPayload = MemberPublicData(
      "${memberWithFullData.personalData.firstName} ${memberWithFullData.personalData.lastName}"
    )
    thenInApi.responseHasCorrectPayload(response, expectedPayload)
  }
}
