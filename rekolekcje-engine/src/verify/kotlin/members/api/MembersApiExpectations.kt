package pl.oaza.waw.rekolekcje.api.members.api

import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.Matchers.contains
import org.hamcrest.Matchers.equalTo
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import pl.oaza.waw.rekolekcje.api.members.endpoint.MemberDetails
import pl.oaza.waw.rekolekcje.api.members.endpoint.MemberPublicData
import pl.oaza.waw.rekolekcje.api.members.endpoint.MemberSummary
import pl.oaza.waw.rekolekcje.api.members.endpoint.MembersEndpoint.Companion.MEMBERS_URI
import pl.oaza.waw.rekolekcje.api.members.utils.assertSummariesMatchesDetails
import pl.oaza.waw.rekolekcje.api.members.utils.assertSummaryMatchesDetails
import pl.oaza.waw.rekolekcje.api.opinions.endpoint.OpinionDetailsResponse
import pl.oaza.waw.rekolekcje.api.shared.Response
import pl.oaza.waw.rekolekcje.api.shared.RestApi

class MembersApiExpectations(private val restApi: RestApi) {

  private val baseUri = MEMBERS_URI

  private val all: List<MemberDetails>
    get() = restApi.performGet(baseUri)
      .getContentAsList<MemberSummary>()
      .map { findDetailsCorrespondingToSummary(it) }

  fun responseHasSummaryDataMatchingDetails(response: Response, details: MemberDetails) {
    response.andExpect(status().is2xxSuccessful)
    val responseContent = response.getContent<MemberSummary>()
    assertSummaryMatchesDetails(responseContent, details)
  }

  fun responseHasSummaryDataMatchingDetails(
    response: Response,
    details: List<MemberDetails>
  ) {
    response.andExpect(status().is2xxSuccessful)
    val summaries = response.getContentAsList<MemberSummary>()
    val detailsWithIds = assignIdsToDetailsFromMatchingSummaries(details, summaries)
    assertSummariesMatchesDetails(summaries, detailsWithIds)
  }

  fun responseHasCorrectDataIgnoringId(response: Response, expectedPayload: MemberDetails) {
    response.andExpect(status().is2xxSuccessful)
    val responseContent = response.getContent<MemberDetails>()
    assertThat(responseContent)
      .isEqualToIgnoringGivenFields(expectedPayload, "id")
  }

  fun responseHasCorrectDataIgnoringId(
    response: Response,
    expectedPayload: List<MemberDetails>
  ) {
    response.andExpect(status().is2xxSuccessful)
    val returnedPayloadAsList = response.getContentAsList<MemberDetails>()
    assertThat(returnedPayloadAsList)
      .usingRecursiveFieldByFieldElementComparator()
      .usingElementComparatorIgnoringFields("id")
      .containsAll(expectedPayload)
  }

  fun exists(payload: MemberDetails) {
    val matchingDetails = all.find { it.id == payload.id } ?: throw IllegalStateException()
    assertThat(matchingDetails)
      .usingRecursiveComparison()
      .ignoringFields(
        "personalData.mother.id",
        "personalData.father.id",
        "personalData.emergencyContact.id",
        "experience.animator.id"
      )
      .isEqualTo(payload)
  }

  fun firstIsEqualTo(payload: MemberDetails) {
    assertThat(all[0]).isEqualTo(payload)
  }

  fun doesNotExist(payload: MemberDetails) {
    assertThat(all).doesNotContain(payload)
  }

  fun thereIsAnExactNumberOfEntities(size: Int) {
    assertThat(all.size).isEqualTo(size)
  }

  fun responseHasPayloadWithIds(response: Response) {
    val members = response.getContentAsList<MemberSummary>()
    assertThat(members)
      .extracting("id")
      .doesNotContainNull()
  }

  fun responseIsUnprocessableEntityAndHasCode(response: Response, expectedCode: String) {
    response
      .andExpect(status().isUnprocessableEntity)
      .andExpect(jsonPath("$.fieldErrors[*].code", contains(equalTo(expectedCode))))
  }

  fun responseHasMembersExistingInSystem(response: Response): List<MemberDetails> {
    val existingSummaries = restApi.performGet(baseUri).getContentAsList<MemberSummary>()
    val summariesInResponse = response.getContentAsList<MemberSummary>()
    assertThat(existingSummaries).containsAll(summariesInResponse)
    val detailsFromResponse = summariesInResponse.map { findDetailsCorrespondingToSummary(it) }
    assertThat(all).containsAll(detailsFromResponse)
    return detailsFromResponse
  }

  fun responseHasSummaryOfAllMemberDetailsIgnoringId(
    response: Response,
    details: List<MemberDetails>
  ) {
    val returnedSummaries = response.getContentAsList<MemberSummary>()
    val detailsWithIds = assignIdsToDetailsFromMatchingSummaries(details, returnedSummaries)
    assertSummariesMatchesDetails(returnedSummaries, detailsWithIds)
  }

  private fun findDetailsCorrespondingToSummary(it: MemberSummary) =
    restApi.performGet("$baseUri/${it.id}").getContent<MemberDetails>()

  fun responseHasMemberOpinions(response: Response, existingOpinions: List<OpinionDetailsResponse>) {
    val opinionsInResponse = response.getContent<MemberDetails>().opinions
    assertThat(opinionsInResponse).isEqualTo(existingOpinions)
  }

  fun existsMemberWithMatchingData(memberDetails: MemberDetails): MemberDetails {
    val matchingMember = all.find {
      it.personalData.firstName == memberDetails.personalData.firstName
          && it.personalData.lastName == memberDetails.personalData.lastName
          && it.personalData.pesel == memberDetails.personalData.pesel
    }
    assertThat(matchingMember).isNotNull
    return matchingMember ?: throw IllegalStateException()
  }

  fun responseHasCorrectPayload(response: Response, expectedPayload: MemberPublicData) {
    response.hasStatusOk()
    val actualPayload = response.getContent<MemberPublicData>()
    assertThat(actualPayload).isEqualTo(expectedPayload)
  }
}
