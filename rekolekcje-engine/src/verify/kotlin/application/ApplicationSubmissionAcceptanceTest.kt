package pl.oaza.waw.rekolekcje.api.application

import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.springframework.security.test.context.support.WithMockUser
import pl.oaza.waw.rekolekcje.api.application.endpoint.RetreatApplicationDto
import pl.oaza.waw.rekolekcje.api.application.fixture.BaseApplicationAcceptanceTest
import pl.oaza.waw.rekolekcje.api.application.fixture.animatorApplicationSubmissionDto
import pl.oaza.waw.rekolekcje.api.application.fixture.fullApplicationFromSubmission
import pl.oaza.waw.rekolekcje.api.application.fixture.participantApplicationSubmissionDto
import pl.oaza.waw.rekolekcje.api.application.fixture.sampleMember
import pl.oaza.waw.rekolekcje.api.members.utils.withEmail
import pl.oaza.waw.rekolekcje.api.members.utils.withId

internal class ApplicationSubmissionAcceptanceTest : BaseApplicationAcceptanceTest() {

  @Test
  @WithMockUser
  fun `should create new animator application`() {
    // given
    val member = sampleMember()
    val animatorApplication = animatorApplicationSubmissionDto(member = member)

    // when
    val response = whenInApi.newApplicationIsSubmitted(animatorApplication)

    // then
    response.hasStatusCreated()
    response.hasDataWithId()
    val existingMember = thenInMembersApi.existsMemberWithMatchingData(member)
    val expectedId = response.getContent<RetreatApplicationDto>().id
    val expectedSuretyId = response.getContent<RetreatApplicationDto>().surety?.id
    val expectedApplication = fullApplicationFromSubmission(
      submissionDto = animatorApplication,
      applicationId = expectedId,
      suretyId = expectedSuretyId,
      member = existingMember
    )
    thenInApi.exists(expectedApplication)
  }

  @Test
  @WithMockUser
  @Disabled
  fun `should not allow submitting application for non-existing member`() {
    TODO("Not yet implemented")
  }

  @Test
  @WithMockUser
  fun `should create new participant application`() {
    // given
    val member = sampleMember()
    val retreatId = 55L
    val participantApplication = participantApplicationSubmissionDto(member = sampleMember(), retreatId = retreatId)

    // when
    val response = whenInApi.newApplicationIsSubmitted(participantApplication)

    // then
    response.hasStatusCreated()
    response.hasDataWithId()
    val expectedId = response.getContent<RetreatApplicationDto>().id
    val existingMember = thenInMembersApi.existsMemberWithMatchingData(member)
    val expectedApplication = fullApplicationFromSubmission(
      submissionDto = participantApplication,
      applicationId = expectedId,
      member = existingMember
    )
    thenInApi.exists(expectedApplication)
  }

  @Test
  @WithMockUser
  fun `should update participant application`() {
    // given
    val member = sampleMember()
    val participantApplication = participantApplicationSubmissionDto(member, 1001L)
    whenInApi.alreadyExistSomeApplications(listOf(participantApplication))

    val existingMember = whenInMembersApi.existsElementWithTheSameData(member)

    val existingApplications = whenInApi.allParticipantApplicationsAreRequested()
      .getContentAsList<RetreatApplicationDto>()
    val existingApplicationId = existingApplications.first().id

    // when
    val updatedMember = existingMember.withEmail("updated@gmail.com")
    val updatedApplication = participantApplicationSubmissionDto(updatedMember, 1001L)
    whenInApi.updatedApplicationIsSubmitted(existingApplicationId, updatedApplication)

    // then
    thenInMembersApi.exists(updatedApplication.memberDetails)
  }

  @Test
  @WithMockUser
  fun `should remove application`() {
    // given
    val member = sampleMember()
    val participantApplication = participantApplicationSubmissionDto(member, 1001L)
    whenInApi.alreadyExistSomeApplications(listOf(participantApplication))

    val existingMember = whenInMembersApi.existsElementWithTheSameData(member)

    val existingApplicationId = whenInApi
      .allParticipantApplicationsAreRequested()
      .getContentAsList<RetreatApplicationDto>()
      .first().id

    // when
    whenInApi.singleApplicationIsDeleted(existingApplicationId)

    // then
    thenInApi.thereIsSpecificNumberOfApplications(0)
    thenInMembersApi.exists(existingMember)
  }
}