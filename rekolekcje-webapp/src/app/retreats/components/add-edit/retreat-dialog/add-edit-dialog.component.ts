import { Component, Inject, OnInit } from '@angular/core';
import { RetreatDetails } from '../../../models/retreat-details.model';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'add-edit-dialog',
  templateUrl: './add-edit-dialog.component.html',
  styleUrls: ['./add-edit-dialog.component.scss'],
})
export class RetreatAddEditDialog implements OnInit {
  dialogTitle: string;
  retreatData: RetreatDetails;

  constructor(
    public dialogRef: MatDialogRef<RetreatAddEditDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.dialogTitle = data.dialogTitle;
    this.retreatData = data.retreatData ? data.retreatData : {};
  }

  ngOnInit() {}

  submit(retreat: RetreatDetails): void {
    if (retreat) {
      this.dialogRef.close(retreat);
    } else {
      this.dialogRef.close();
    }
  }
}
