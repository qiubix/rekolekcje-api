package pl.oaza.waw.rekolekcje.api.retreats.query

import pl.oaza.waw.rekolekcje.api.retreats.domain.Retreat
import pl.oaza.waw.rekolekcje.api.retreats.domain.RetreatNotFoundException

/**
 * Provides methods for accessing retreats' data.
 */
interface RetreatQueryFacade {

  fun findAll(): List<Retreat>

  /**
   * Finds a single retreat.
   *
   * @param id id of retreat to be searched
   * @return RetreatDetails object if retreat was found
   * @throws RetreatNotFoundException if retreat was not found
   */
  fun findOneById(id: Long): Retreat

}
