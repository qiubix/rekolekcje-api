package pl.oaza.waw.rekolekcje.api.parish.domain

import java.util.Objects.requireNonNull
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentMap
import java.util.concurrent.atomic.AtomicLong

internal class InMemoryRegionsRepository : RegionsRepository {

  var nextId = AtomicLong(1)
  var data: ConcurrentMap<Long, Region> = ConcurrentHashMap()

  override fun findAll(): List<Region> {
    return ArrayList(data.values)
  }

  override fun findById(id: Long): Region? {
    return data[id]
  }

  override fun save(region: Region): Region {
    requireNonNull(region)
    var regionToSave = region
    if (regionToSave.id == null) {
      regionToSave = region.copy(id = nextId.getAndIncrement())
    }
    data[regionToSave.id] = regionToSave
    return regionToSave
  }

  override fun deleteById(id: Long) {
    data.remove(id)
  }
}
