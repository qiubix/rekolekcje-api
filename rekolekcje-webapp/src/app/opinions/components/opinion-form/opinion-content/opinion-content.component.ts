import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'reko-opinion-content',
  templateUrl: './opinion-content.component.html',
  styleUrls: ['./opinion-content.component.scss']
})
export class OpinionContentComponent implements OnInit {
  @Input() inputControl: FormControl;

  ngOnInit() {}
}
