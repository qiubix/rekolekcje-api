import { Pipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';
import { MembersApiService } from '@members/state/members-api.service';

@Pipe({
  name: 'findMemberFullNameFromEncrypted'
})
export class FindMemberFullNameFromEncryptedPipe implements PipeTransform {
  constructor(private _membersApiService: MembersApiService) {}

  transform(encryptedMemberId: string, args?: any): Observable<string> {
    return this._membersApiService
      .getMemberFullNameFromEncrypted(encryptedMemberId)
      .pipe(map(it => it.fullName));
  }
}
