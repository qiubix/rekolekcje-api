package pl.oaza.waw.rekolekcje.api.members.endpoint

import org.springframework.core.io.Resource
import org.springframework.core.io.UrlResource
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import pl.oaza.waw.rekolekcje.api.members.query.MembersQueryFacade
import pl.oaza.waw.rekolekcje.api.opinions.domain.OpinionsQueryFacade
import pl.oaza.waw.rekolekcje.api.opinions.endpoint.OpinionDetailsResponse
import java.nio.file.Files
import java.nio.file.Paths

@Service
internal class MembersFetchService(
  private val membersQueryFacade: MembersQueryFacade,
  private val opinionsQueryFacade: OpinionsQueryFacade
) {

  fun getAllMemberSummaries(): List<MemberSummary> {
    val foundMembers = membersQueryFacade.findAllMembers()
    return foundMembers.map { MemberSummary(it) }
  }

  fun getSingleMemberDetails(id: Long): MemberDetails {
    val opinions = opinionsQueryFacade.getAllOpinionsForMember(id).map { OpinionDetailsResponse.of(it) }
    return toDetailsDto(membersQueryFacade.find(id), opinions)
  }

  fun exportAllToCsv(): ResponseEntity<Resource> {
    val filePath = Paths.get("exportedData.txt")
    val contents = "Some file contents"
    Files.write(filePath, contents.toByteArray())
    val resourceToExport = UrlResource(filePath.toUri())
    return ResponseEntity.ok()
      .contentLength(filePath.toFile().length())
      .contentType(MediaType.APPLICATION_OCTET_STREAM)
      .header(
        HttpHeaders.CONTENT_DISPOSITION,
        "attachment; filename=\"${resourceToExport.filename}\""
      )
      .body(resourceToExport)
  }

  fun getMemberFullName(id: Long): MemberPublicData {
    val foundMember = membersQueryFacade.find(id)
    val firstName = foundMember.personalData.contactInfo.firstName ?: throw IllegalStateException()
    val lastName = foundMember.personalData.contactInfo.lastName ?: throw IllegalStateException()
    return MemberPublicData("$firstName $lastName")
  }
}