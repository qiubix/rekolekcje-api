rootProject.name="rekolekcje-api"

pluginManagement {
  includeBuild("build-logic")
}

include("rekolekcje-engine", "rekolekcje-webapp")
