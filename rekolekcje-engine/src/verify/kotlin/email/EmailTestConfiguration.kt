package pl.oaza.waw.rekolekcje.api.email

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.icegreen.greenmail.util.ServerSetup
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Primary
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.JavaMailSenderImpl
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import pl.oaza.waw.rekolekcje.api.shared.RestApi
import java.util.Properties

@TestConfiguration
class EmailTestConfiguration {
  @Bean
  internal fun mockMvc(webApplicationContext: WebApplicationContext): MockMvc {
    return MockMvcBuilders
      .webAppContextSetup(webApplicationContext)
      .apply<DefaultMockMvcBuilder>(SecurityMockMvcConfigurers.springSecurity())
      .build()
  }

  @Bean
  internal fun restApi(mockMvc: MockMvc, jsonMapper: ObjectMapper): RestApi {
    return RestApi(mockMvc, jsonMapper.registerKotlinModule())
  }

  @Bean
  @Primary
  fun javaMailSender(): JavaMailSender {
    val mailSender = JavaMailSenderImpl()
    val mailProperties = Properties()
    mailProperties["mail.smtp.auth"] = true
    mailProperties["mail.smtp.starttls.enable"] = true
    mailSender.javaMailProperties = mailProperties
    mailSender.host = "localhost"
    mailSender.port = 3025
    mailSender.protocol = ServerSetup.PROTOCOL_SMTP
    mailSender.username = "no-reply@oaza.waw.pl"
    mailSender.password = "password"
    return mailSender
  }
}
