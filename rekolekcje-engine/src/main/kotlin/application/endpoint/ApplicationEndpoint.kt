package pl.oaza.waw.rekolekcje.api.application.endpoint

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import pl.oaza.waw.rekolekcje.api.API_URI
import pl.oaza.waw.rekolekcje.api.application.endpoint.ApplicationEndpoint.Companion.APPLICATION_URI

@RestController
@RequestMapping(APPLICATION_URI)
class ApplicationEndpoint internal constructor(
  private val applicationSubmissionService: ApplicationSubmissionService,
  private val applicationModificationService: ApplicationModificationService,
  private val applicationFetchService: ApplicationFetchService,
  private val applicationImportService: ApplicationImportService
) {

  companion object {
    const val APPLICATION_URI = "$API_URI/application"
    const val ANIMATORS_URI = "/animators"
    const val PENDING_URI = "/pending"
    const val VERIFIED_URI = "/verified"
    const val PARTICIPANTS_URI = "/participants"
    const val PAYMENT_URI = "/payment"
    const val STATUS_URI = "/status"
    const val ASSIGN_URI = "/assign"
    const val STATS_URI = "/stats"
  }

  @GetMapping
  fun getAllApplications(): List<RetreatApplicationDto> = applicationFetchService.getAllApplications()

  @GetMapping("/{id}")
  fun getApplicationDetails(@PathVariable id: Long): RetreatApplicationDto =
    applicationFetchService.getSingleApplicationDetails(id)

  @PostMapping()
  @ResponseStatus(HttpStatus.CREATED)
  fun submitApplication(@RequestBody submissionDto: ApplicationSubmissionDto): RetreatApplicationDto =
    applicationSubmissionService.submitNewApplication(submissionDto)

  @PutMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  fun updateApplication(
    @PathVariable id: Long,
    @RequestBody submissionDto: ApplicationSubmissionDto
  ): RetreatApplicationDto = applicationModificationService.updateSubmission(id, submissionDto)

  @DeleteMapping("/{id}")
  fun removeApplication(@PathVariable id: Long): Long = applicationSubmissionService.removeApplication(id)

  @GetMapping(ANIMATORS_URI)
  fun getAllAnimatorApplications(): List<RetreatApplicationDto> = applicationFetchService.getAllAnimatorApplications()

  @GetMapping(PARTICIPANTS_URI)
  fun getAllParticipantApplications(): List<RetreatApplicationDto> =
    applicationFetchService.getAllParticipantApplications()

  // TODO
  @GetMapping(PENDING_URI)
  fun getAllPendingApplications(): List<ApplicationSummary> = applicationFetchService.getAllPendingApplications()

  // TODO
  @GetMapping(VERIFIED_URI)
  fun getAllVerifiedApplications(): List<ApplicationSummary> = applicationFetchService.getAllReadyApplications()

  @PostMapping("$ANIMATORS_URI/import")
  fun importAnimatorApplications(@RequestParam file: MultipartFile): List<RetreatApplicationDto> {
    val fileContents = String(file.bytes)
    return applicationImportService.importAnimatorsFromCsv(fileContents)
  }

  @PostMapping("$PARTICIPANTS_URI/import")
  fun importParticipantApplications(@RequestParam file: MultipartFile): List<RetreatApplicationDto> {
    val fileContents = String(file.bytes)
    return applicationImportService.importParticipantsFromCsv(fileContents)
  }

  @PostMapping(STATUS_URI)
  fun changeApplicationStatus(@RequestBody request: ChangeApplicationStatusRequest): ChangeApplicationStatusResponse =
    applicationModificationService.changeApplicationStatus(request)

  @PostMapping(PAYMENT_URI)
  fun changePaymentStatus(@RequestBody request: ChangePaymentStatusRequest): ChangePaymentStatusResponse =
    applicationModificationService.changeApplicationPaymentStatus(request)

  @PostMapping("$ANIMATORS_URI$ASSIGN_URI")
  fun assignAnimator(@RequestBody request: AssignAnimatorRequest): AssignAnimatorResponse =
      applicationModificationService.assignAnimator(request)

  @GetMapping(STATS_URI)
  fun getVerificationStatistics() = applicationFetchService.getVerificationStatistics()


  // TODO: uspójnić exception handling
  @ExceptionHandler(IllegalArgumentException::class)
  fun returnBadRequestForIllegalArgumentException(): ResponseEntity<*> {
    return ResponseEntity.badRequest().build<Any>()
  }
}