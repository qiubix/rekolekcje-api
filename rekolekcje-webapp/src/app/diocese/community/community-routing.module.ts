import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommunityComponent } from './components/_root/community.component';
import { CommunityGuard } from './community.guard';

const communityRoutes: Routes = [
  {
    path: '',
    component: CommunityComponent,
    canActivate: [CommunityGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(communityRoutes)],
  exports: [RouterModule]
})
export class CommunityRoutingModule {}
