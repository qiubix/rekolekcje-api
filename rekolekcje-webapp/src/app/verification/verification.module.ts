import { NgModule } from '@angular/core';

import { VerificationRoutingModule } from './verification-routing.module';
import { SharedModule } from '@shared/shared.module';
import { AuthModule } from '@auth/auth.module';

import * as fromComponents from './components';
import { AnimatorVsParticipantDialogComponent, RejectionReasonDialogComponent } from './components';

@NgModule({
  declarations: [fromComponents.components],
  imports: [AuthModule, SharedModule, VerificationRoutingModule],
  entryComponents: [AnimatorVsParticipantDialogComponent, RejectionReasonDialogComponent]
})
export class VerificationModule {}
