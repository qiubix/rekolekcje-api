import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RetreatAddEditDialog } from './add-edit-dialog.component';

describe('RetreatAddEditDialog', () => {
  let component: RetreatAddEditDialog;
  let fixture: ComponentFixture<RetreatAddEditDialog>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RetreatAddEditDialog]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetreatAddEditDialog);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
