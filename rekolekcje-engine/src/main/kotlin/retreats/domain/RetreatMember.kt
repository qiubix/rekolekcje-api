package pl.oaza.waw.rekolekcje.api.retreats.domain

import pl.oaza.waw.rekolekcje.api.shared.value.PaymentStatus
import pl.oaza.waw.rekolekcje.api.shared.value.Sex
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "RETREAT_MEMBERS")
data class RetreatMember(
  // TODO: this should be actually application id, as retreat member is always connected to a particular application
  @Id
  val memberId: Long,
  val firstName: String,
  val lastName: String,
  @Enumerated(EnumType.STRING)
  val sex: Sex,
  val age: Int,
  @Enumerated(EnumType.STRING)
  val paymentStatus: PaymentStatus,
  val animator: Boolean,
  @Enumerated(EnumType.STRING)
  val registrationStatus: RegistrationStatus
)