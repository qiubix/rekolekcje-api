package pl.oaza.waw.rekolekcje.api.community.dto

class CommunityNotFoundException : RuntimeException {

  constructor(id: Long) : super("No community with id " + id + "found.")

  constructor(payload: CommunityPayload) : super("No community found with this payload: $payload")
}

