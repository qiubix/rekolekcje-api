package pl.oaza.waw.rekolekcje.api.application.fixture

import pl.oaza.waw.rekolekcje.api.application.domain.AnimatorRole
import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationStatus
import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationType
import pl.oaza.waw.rekolekcje.api.application.endpoint.ApplicationSubmissionDto
import pl.oaza.waw.rekolekcje.api.application.endpoint.ApplicationSummary
import pl.oaza.waw.rekolekcje.api.application.endpoint.RetreatApplicationDto
import pl.oaza.waw.rekolekcje.api.members.endpoint.MemberDetails
import pl.oaza.waw.rekolekcje.api.members.endpoint.PersonalDataValue
import pl.oaza.waw.rekolekcje.api.shared.endpoint.ContactDto
import pl.oaza.waw.rekolekcje.api.shared.value.PaymentStatus


fun animatorApplicationSubmissionDto(
  member: MemberDetails = sampleMember(),
  animatorRoles: Set<AnimatorRole> = hashSetOf(AnimatorRole.GROUP, AnimatorRole.QUARTERMASTER),
  availability: Set<Long> = hashSetOf(12L, 14L),
  retreatId: Long? = null
) = ApplicationSubmissionDto(
  applicationType = ApplicationType.ANIMATOR,
  memberDetails = member,
  animatorRoles = animatorRoles,
  availability = availability,
  retreatId = retreatId,
  surety = ContactDto(
    fullName = "Kal-El",
    email = "kalel@mail.com"
  )
)

fun sampleMember(
  firstName: String = "Bruce",
  lastName: String = "Wayne",
  pesel: String = "77021071814"
) = MemberDetails(
  personalData = PersonalDataValue(
    firstName = firstName,
    lastName = lastName,
    pesel = pesel
  )
)

fun fullApplicationFromSubmission(
  submissionDto: ApplicationSubmissionDto,
  applicationId: Long,
  member: MemberDetails = submissionDto.memberDetails,
  suretyId: Long? = null
) = RetreatApplicationDto(
  id = applicationId,
  type = submissionDto.applicationType,
  member = member,
  animatorRoles = submissionDto.animatorRoles,
  availability = submissionDto.availability,
  retreatId = submissionDto.retreatId,
  surety = submissionDto.surety?.copy(id = suretyId)
)

fun participantApplicationSubmissionDto(
  member: MemberDetails = sampleMember(),
  retreatId: Long = 45L
) = ApplicationSubmissionDto(
  applicationType = ApplicationType.PARTICIPANT,
  memberDetails = member,
  retreatId = retreatId
)

fun applicationSummary(
  member: MemberDetails,
  status: ApplicationStatus,
  isAnimator: Boolean
) = ApplicationSummary(
  id = 0,
  firstName = member.personalData.firstName,
  lastName = member.personalData.lastName,
  status = status,
  paymentStatus = PaymentStatus.NOTHING,
  isAnimator = isAnimator
)

data class SampleApplicationData(
  val member: MemberDetails,
  val isAnimator: Boolean,
  val status: ApplicationStatus = ApplicationStatus.WAITING
)