import { Component, Input, OnInit } from '@angular/core';
import { RetreatApplication } from '@verification/model';

@Component({
  selector: 'reko-retreat-application-badges',
  templateUrl: './retreat-application-badges.component.html',
  styleUrls: ['./retreat-application-badges.component.scss']
})
export class RetreatApplicationBadgesComponent implements OnInit {
  @Input() retreatApplication: RetreatApplication;
  @Input() isAnimator: boolean;

  constructor() {}

  ngOnInit() {}
}
