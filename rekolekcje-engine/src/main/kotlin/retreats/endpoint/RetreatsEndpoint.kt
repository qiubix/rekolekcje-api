package pl.oaza.waw.rekolekcje.api.retreats.endpoint

import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import pl.oaza.waw.rekolekcje.api.API_URI
import pl.oaza.waw.rekolekcje.api.retreats.domain.RetreatCommandFacade
import pl.oaza.waw.rekolekcje.api.retreats.domain.RetreatNotFoundException
import pl.oaza.waw.rekolekcje.api.retreats.query.RetreatQueryFacade
import pl.oaza.waw.rekolekcje.api.retreats.endpoint.RetreatsEndpoint.Companion.RETREATS_API_URI


/**
 * This is the controller which provides operations to manage retreats. This class allows to perform
 * CRUD operations related with retreats in system.
 */
@RestController
@RequestMapping(RETREATS_API_URI)
class RetreatsEndpoint(
  private val retreatQueryFacade: RetreatQueryFacade,
  private val retreatCommandFacade: RetreatCommandFacade
) {

  companion object {
    const val RETREATS_API_URI = "$API_URI/retreats"
  }

  private val logger = LoggerFactory.getLogger(this.javaClass.name)

  /**
   * Returns summaries of all retreats in the system.
   *
   * @return list of [RetreatSummary] objects
   * @see RetreatSummary
   */
  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  @Transactional
  fun getAllRetreats(): List<RetreatSummary> = retreatQueryFacade.findAll().map { RetreatSummary(it) }

  /**
   * Returns retreat details as [RetreatDetails].
   *
   * @param id id of retreat to be found
   * @return RetreatDetails object if retreat was found or 404 code status, if was not
   */
  @GetMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  @Transactional
  fun getDetailsById(@PathVariable(name = "id") id: Long): RetreatDetails {
    return RetreatDetails(retreatQueryFacade.findOneById(id))
  }

  /**
   * Creates new retreat.
   *
   * @param payload new retreat data
   * @return saved retreat data as [RetreatSummary] and 201 status code
   */
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @Transactional
  fun createRetreat(@RequestBody payload: RetreatDetails): RetreatSummary {
    val savedRetreat = retreatCommandFacade.save(fromDto(payload))
    return RetreatSummary(savedRetreat)
  }

  /**
   * Updates single retreat.
   *
   * @param payload updated retreat data
   * @return updated retreat data saved in repository as [RetreatSummary]
   */
  @PutMapping
  @ResponseStatus(HttpStatus.OK)
  @Transactional
  fun updateRetreatData(@RequestBody payload: RetreatDetails): RetreatSummary {
    val savedRetreat = retreatCommandFacade.save(fromDto(payload))
    return RetreatSummary(savedRetreat)
  }

  /**
   * This method allows to delete retreat from repository. The 404 status is returned when there is
   * no retreat with this id.
   *
   * @param id id of retreat to be deleted
   */
  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  @Transactional
  fun deleteRetreat(@PathVariable(name = "id") id: Long) {
    retreatCommandFacade.delete(id)
  }

  @ExceptionHandler(RetreatNotFoundException::class)
  fun returnNotFoundStatus(): ResponseEntity<*> {
    return ResponseEntity.notFound().build<Any>()
  }
}
