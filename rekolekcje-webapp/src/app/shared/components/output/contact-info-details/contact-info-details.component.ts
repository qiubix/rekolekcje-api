import { Component, Input } from '@angular/core';
import { ContactInfo } from '@shared/models';

@Component({
  selector: 'reko-contact-info-details',
  templateUrl: './contact-info-details.component.html',
  styleUrls: ['./contact-info-details.component.scss']
})
export class ContactInfoDetailsComponent {
  @Input() contact: ContactInfo;
  @Input() title: String;
}
