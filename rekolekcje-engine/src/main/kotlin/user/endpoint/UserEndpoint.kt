package pl.oaza.waw.rekolekcje.api.user.endpoint

import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException
import pl.oaza.waw.rekolekcje.api.API_URI
import pl.oaza.waw.rekolekcje.api.user.domain.IncorrectPasswordException
import pl.oaza.waw.rekolekcje.api.user.domain.UserAlreadyExistsException
import pl.oaza.waw.rekolekcje.api.user.domain.UserNotFoundException
import pl.oaza.waw.rekolekcje.api.user.domain.UserRegisterService
import pl.oaza.waw.rekolekcje.api.user.domain.UserUpdateService
import pl.oaza.waw.rekolekcje.api.user.query.UserQueryService

@RestController
@RequestMapping(UserEndpoint.USERS_URI)
class UserEndpoint(
  private val userQueryService: UserQueryService,
  private val userRegisterService: UserRegisterService,
  private val userUpdateService: UserUpdateService
) {

  private val log = LoggerFactory.getLogger(UserEndpoint::class.java)

  companion object {
    const val USERS_URI = "$API_URI/users"
    const val REGISTER_URI = "/sign-up"
    const val ENABLE_URI = "/enable"
    const val DISABLE_URI = "/disable"
    const val PASSWORD_URI = "/password"
  }

  @GetMapping
  @PreAuthorize("hasRole('ADMIN')")
  fun fetchAll() = userQueryService.findAll()

  @GetMapping("/{username}")
  @PreAuthorize("#username == authentication.principal.username")
  fun getOne(@PathVariable(name = "username") username: String): UserSummary? {
    // TODO: allow to get profile only if somebody is admin or user is logged in and is trying to get own profile
    val userSummary = userQueryService.findByUsername(username)
    return userSummary
  }

  /**
   * Endpoint for registering new users. Only users with role 'ADMIN' are authorized to register new
   * users.
   *
   * @param signUpRequest [SignUpRequest] Data of the user we want to register.
   * @return [UserSummary] summary of user data after successful sign-up or BAD_REQUEST status.
   */
  @PostMapping(REGISTER_URI)
  fun signUp(@RequestBody signUpRequest: SignUpRequest): UserSummary {
    val userSummary =
      try {
        userRegisterService.registerUser(signUpRequest)
      } catch (ex: UserAlreadyExistsException) {
        throw ResponseStatusException(
          HttpStatus.BAD_REQUEST,
          "User with username ${signUpRequest.username} already exists",
          ex
        )
      }
    log.info("Sign up successful for {}", userSummary.username)
    return userSummary
  }


  @DeleteMapping("/{username}")
  @PreAuthorize("hasRole('ADMIN')")
  fun deleteOneUser(@PathVariable(name = "username") username: String): ResponseEntity<String> {
    try {
      userUpdateService.deleteByUsername(username)
    } catch (ex: UserNotFoundException) {
      throw ResponseStatusException(
        HttpStatus.NOT_FOUND,
        "No user with username $username found",
        ex
      )
    }
    log.info("Delete user {} succeded", username)
    return ResponseEntity.ok(username)
  }


  @PutMapping
  @PreAuthorize("hasRole('ADMIN')")
  fun updateUser(@RequestBody updateUserRequest: UpdateUserRequest): UserSummary {
    val updatedUser =
      try {
        userUpdateService.update(updateUserRequest)
      } catch (ex: UserNotFoundException) {
        throw ResponseStatusException(
          HttpStatus.NOT_FOUND,
          "No user with username ${updateUserRequest.username} found",
          ex
        )
      }
    log.info("User's {} info updated", updateUserRequest.username)
    return updatedUser
  }

  @PutMapping(PASSWORD_URI)
  @PreAuthorize("hasRole('ADMIN')")
  fun changePassword(@RequestBody changePasswordRequest: ChangePasswordRequest): UserSummary {
    val updatedUser =
      try {
        userUpdateService.changePassword(changePasswordRequest)
      } catch (ex: UserNotFoundException) {
        throw ResponseStatusException(
          HttpStatus.NOT_FOUND,
          "No user with username ${changePasswordRequest.username} found",
          ex
        )
      } catch (ex: IncorrectPasswordException) {
        throw ResponseStatusException(
          HttpStatus.UNAUTHORIZED,
          "Incorrect old password",
          ex
        )
      }
    log.info("Password changed for user {}", changePasswordRequest.username)
    return updatedUser

  }


  @PutMapping(DISABLE_URI)
  @PreAuthorize("hasRole('ADMIN')")
  fun disableUser(@RequestBody username: String): ResponseEntity<String> {
    try {
      userUpdateService.disableUser(username)
    } catch (ex: UserNotFoundException) {
      throw ResponseStatusException(
        HttpStatus.NOT_FOUND,
        "No user with username $username found",
        ex
      )
    }
    return ResponseEntity.ok(username)
  }


  @PutMapping(ENABLE_URI)
  @PreAuthorize("hasRole('ADMIN')")
  fun enableUser(@RequestBody username: String): ResponseEntity<String> {
    try {
      userUpdateService.enableUser(username)
    } catch (ex: UserNotFoundException) {
      throw ResponseStatusException(
        HttpStatus.NOT_FOUND,
        "No user with username $username found",
        ex
      )
    }
    return ResponseEntity.ok(username)
  }

}
