package pl.oaza.waw.rekolekcje.api.email.domain

import pl.oaza.waw.rekolekcje.api.email.SendEmailRequest
import pl.oaza.waw.rekolekcje.api.email.SendEmailWithTemplateRequest

interface EmailFacade {

  fun sendEmail(email: SendEmailRequest)

  fun sendEmailWithTemplate(email: SendEmailWithTemplateRequest)

}
