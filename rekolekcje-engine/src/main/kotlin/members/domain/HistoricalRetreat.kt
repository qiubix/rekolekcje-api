package pl.oaza.waw.rekolekcje.api.members.domain

import pl.oaza.waw.rekolekcje.api.shared.value.Stage
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.Table
import javax.validation.constraints.Pattern

@Entity
@Table(name = "HISTORICAL_RETREATS")
data class HistoricalRetreat(
  @Id
  @GeneratedValue
  val id: Long? = null,
//  @Column(name = "MEMBER_ID")
//  val member: Long? = null,
  @Enumerated(EnumType.STRING)
  val stage: Stage,
  @Pattern(regexp = "dddd")
  val year: String
)
