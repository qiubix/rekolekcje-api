package pl.oaza.waw.rekolekcje.api.email.domain

internal interface EmailService {

  fun sendSimpleMessage(to: String, subject: String, content: String)

  fun sendHtmlMessage(to: String, subject: String, htmlContent: String)

  fun sendMessageWithTemplate(to: String, subject: String, template: String, templateModel: Map<String, Any>)

}
