import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Members } from '../state/members-reducer';
import { MembersSharedActions } from '../state/members-actions';
import { ParishSharedActions } from '../../diocese/parish/store/parish-actions';
import { CommunityActions } from '../../diocese/community/store/community-actions';
import { Observable, of } from 'rxjs';
import { AppSelectors } from '../../core/store/app-selectors';
import {
  catchError,
  first,
  switchMap,
  tap,
  withLatestFrom,
  combineLatest,
  filter
} from 'rxjs/operators';
import { RetreatsSharedActions } from 'app/retreats/store/retreat-actions';
import { RetreatSummary } from 'app/retreats/models/retreat-summary.model';

@Injectable({ providedIn: 'root' })
export class MembersGuard implements CanActivate {
  constructor(private store: Store<Members.State>) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.getFromStoreOrAPI().pipe(
      switchMap(() => of(true)),
      catchError(() => of(false))
    );
  }

  getFromStoreOrAPI(): Observable<any> {
    return this.store.pipe(
      select(AppSelectors.getMembersList),
      withLatestFrom(this.store.pipe(select(AppSelectors.getMembersLoading))),
      first(),
      tap(([members, loading]) => {
        if (!loading) {
          // if (!loading && (!members || !members.length)) {
          // console.log('Fetching members');
          this.store.dispatch(new MembersSharedActions.LoadMembersList());
          this.store.dispatch(new ParishSharedActions.LoadParishList());
          this.store.dispatch(new CommunityActions.LoadCommunitiesList());
          this.store.dispatch(new RetreatsSharedActions.LoadRetreatsList());
        }
      })
      // combineLatest(this.store.pipe(select(AppSelectors.getRetreatsList))),
      // filter(([[members, loading], retreats]) => this.retreatsReady(retreats))
    );
  }

  // private retreatsReady(retreats: RetreatSummary[]): boolean {
  //   return retreats && retreats.length > 0;
  // }
}
