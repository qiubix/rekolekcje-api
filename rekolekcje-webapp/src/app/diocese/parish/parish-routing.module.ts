import { NgModule } from '@angular/core';
import { ParishComponent } from './components/_root/parish.component';
import { RouterModule, Routes } from '@angular/router';

const parishRoutes: Routes = [
  {
    path: '',
    component: ParishComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(parishRoutes)],
  exports: [RouterModule]
})
export class ParishRoutingModule {}
