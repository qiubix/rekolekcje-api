package pl.oaza.waw.rekolekcje.api.parish.domain

import pl.oaza.waw.rekolekcje.api.parish.dto.ParishNotFoundException
import pl.oaza.waw.rekolekcje.api.parish.dto.ParishPayload
import pl.oaza.waw.rekolekcje.api.parish.infrastructure.ParishFactory
import java.util.Objects.requireNonNull

/**
 * This class provides methods to manage [Parish] in repository.
 */
open class ParishFacade internal constructor(private val parishRepository: ParishRepository) {

  private val parishFactory = ParishFactory()

  /**
   * This method provides saving parishes to repository.
   * If parish with the same id already exists, data of existing parish will be updated.
   *
   * @param parishPayload parish data to be saved to repository as [ParishPayload] object
   * @return parish data saved to repository as ParishPayload object
   * @throws NullPointerException if `parishPayload` is `null`
   */
  fun save(parishPayload: ParishPayload): ParishPayload {
    requireNonNull(parishPayload)
    var parish = parishFactory.from(parishPayload)
    parish = parishRepository.save(parish)
    return parish.dto()
  }

  /**
   * Returns all parishes in repository as list of [ParishPayload] objects.
   *
   * @return list of ParishPayload objects
   */
  fun findAll(): List<ParishPayload> {
    return parishRepository.findAll().map { it.dto() }
  }

  /**
   * Deletes parishes from repository by id.
   *
   * @param ids ids of parishes to be removed from repository
   * @throws NullPointerException if `ids` is `null`
   */
  fun delete(vararg ids: Long) {
    requireNonNull(ids)
    ids.forEach { parishRepository.deleteById(it) }
  }

  /**
   * This method finds parish in repository by id. Method returns parish as [ParishPayload] object
   * or throws [ParishNotFoundException]
   *
   * @param id id of parish to be searched
   * @return ParishPayload object if parish was found
   * @throws ParishNotFoundException if parish was not found
   * @throws NullPointerException if `id` is `null`
   */
  fun findOne(id: Long): ParishPayload {
    requireNonNull<Long>(id)
    val parish = parishRepository.findById(id)
    when {
      parish != null -> return parish.dto()
      else -> throw ParishNotFoundException(id)
    }
  }
}
