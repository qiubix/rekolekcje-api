package pl.oaza.waw.rekolekcje.api.members.domain

import pl.oaza.waw.rekolekcje.api.core.ddd.AggregateRoot
import pl.oaza.waw.rekolekcje.api.shared.domain.EntityWithId
import javax.persistence.Embedded
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@AggregateRoot
@Entity
@Table(name = "MEMBERS")
data class Member(
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  private val id: Long? = null,
  @Embedded
  val personalData: PersonalData,
  @Embedded
  val healthReport: HealthReport? = null,
  @Embedded
  val experience: Experience? = Experience()

)  : EntityWithId {

  override fun withId(id: Long): Member = copy(id = id)

  override fun getId(): Long? = id
}
