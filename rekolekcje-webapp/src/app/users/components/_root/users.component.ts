import { Component, OnInit } from '@angular/core';
import { UsersApiService } from '../../store/users-api.service';
import { UserSummary } from '../../model/user-summary.model';
import { Observable } from 'rxjs/internal/Observable';
import { select, Store } from '@ngrx/store';
import { Users } from '../../store/users-reduces';
import { AppSelectors } from '@core/store/app-selectors';
import { UsersSharedActions } from '../../store/users-actions';
import { SaveUserRequest } from '../../model/save-user-request.model';
import { ChangePasswordRequest } from '../../model/change-password-request.model';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'reko-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
  public users$: Observable<UserSummary[]>;
  public usersLoading$: Observable<boolean>;

  constructor(
    private readonly usersService: UsersApiService,
    private readonly store: Store<Users.State>,
    private notificationBar: MatSnackBar
  ) {}

  public ngOnInit(): void {
    this.store.dispatch(new UsersSharedActions.LoadUsersList());
    this.users$ = this.store.pipe(select(AppSelectors.getUsersList));
    this.usersLoading$ = this.store.pipe(select(AppSelectors.getUsersLoading));
    this.store.pipe(select(AppSelectors.getUsersNotification)).subscribe((notification: string) => {
      if (notification) {
        this.notificationBar.open(notification, null, { duration: 2000 });
      }
    });
  }

  public onAddUser(signUpRequest: SaveUserRequest) {
    this.store.dispatch(new UsersSharedActions.CreateUser(signUpRequest));
  }

  public onEditUser(saveUserRequest: SaveUserRequest) {
    this.store.dispatch(new UsersSharedActions.UpdateUser(saveUserRequest));
  }

  public onDeleteUser(username: string) {
    this.store.dispatch(new UsersSharedActions.DeleteUser(username));
  }

  public onChangeUserPassword(request: ChangePasswordRequest) {
    this.store.dispatch(new UsersSharedActions.ChangePassword(request));
  }
}
