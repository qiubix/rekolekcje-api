package pl.oaza.waw.rekolekcje.api.notification.storage

import org.assertj.core.api.Assertions.assertThat
import pl.oaza.waw.rekolekcje.api.notification.api.NotificationDatabase

class NotificationStorageExpectations(private val database: NotificationDatabase) {

  fun applicationHasNotificationsCount(applicationId: Long, expectedCount: Int) {
    assertThat(database.getNotifications(applicationId).size).isEqualTo(expectedCount)
  }
}
