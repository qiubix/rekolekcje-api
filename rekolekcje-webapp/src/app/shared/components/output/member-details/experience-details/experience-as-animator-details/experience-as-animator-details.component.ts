import { Component, Input } from '@angular/core';
import { ExperienceAsAnimator } from '@shared/models';

@Component({
  selector: 'reko-experience-as-animator-details',
  templateUrl: './experience-as-animator-details.component.html',
  styleUrls: ['./experience-as-animator-details.component.scss']
})
export class ExperienceAsAnimatorDetailsComponent {
  @Input() data: ExperienceAsAnimator;
}
// otherSkills?: string[];
// numberOfAnimatorDays?: number;
// diakonia?: string;
// retreatsAsAnimator?: string[];
// otherRetreats?: string[];
// leadingGroupToFormationStage?: string;
// communityFunctions?: string;
