package pl.oaza.waw.rekolekcje.api.retreats.endpoint.events

import org.springframework.context.ApplicationListener
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import pl.oaza.waw.rekolekcje.api.application.endpoint.events.ApplicationVerifiedEvent
import pl.oaza.waw.rekolekcje.api.members.endpoint.MembersFetchService
import pl.oaza.waw.rekolekcje.api.retreats.domain.RegistrationStatus
import pl.oaza.waw.rekolekcje.api.retreats.domain.RetreatCommandFacade
import pl.oaza.waw.rekolekcje.api.retreats.domain.RetreatMember

@Component
internal class ApplicationVerifiedEventListener(
  private val retreatCommandFacade: RetreatCommandFacade,
  private val membersFetchService: MembersFetchService
) : ApplicationListener<ApplicationVerifiedEvent> {

  @Transactional
  override fun onApplicationEvent(event: ApplicationVerifiedEvent) {
    // TODO: test behaviour for animator, when there is no retreatId yet
    val retreatId = event.application.retreatId ?: return
    val member = membersFetchService.getSingleMemberDetails(event.memberId)
    retreatCommandFacade.registerMember(
      retreatId = retreatId,
      member = RetreatMember(
        memberId = event.memberId,
        firstName = event.application.firstName,
        lastName = event.application.lastName,
        paymentStatus = event.application.paymentStatus,
        animator = event.application.isAnimator,
        registrationStatus = RegistrationStatus.REGISTERED,
        sex = member.personalData.sex ?: throw IllegalStateException(),
        age = member.personalData.age ?: throw IllegalStateException()
      )
    )
  }

}