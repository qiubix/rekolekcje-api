package pl.oaza.waw.rekolekcje.api.opinions.domain

interface OpinionsQueryFacade {

  fun getById(id: Long): Opinion

  fun getAllOpinionsForMember(memberId: Long): List<Opinion>

}