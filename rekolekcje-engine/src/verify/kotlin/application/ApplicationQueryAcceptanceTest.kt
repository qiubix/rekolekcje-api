package pl.oaza.waw.rekolekcje.api.application

import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.springframework.security.test.context.support.WithMockUser
import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationStatus
import pl.oaza.waw.rekolekcje.api.application.fixture.BaseApplicationAcceptanceTest
import pl.oaza.waw.rekolekcje.api.application.fixture.SampleApplicationData
import pl.oaza.waw.rekolekcje.api.application.fixture.animatorApplicationSubmissionDto
import pl.oaza.waw.rekolekcje.api.application.fixture.applicationSummary
import pl.oaza.waw.rekolekcje.api.application.fixture.fullApplicationFromSubmission
import pl.oaza.waw.rekolekcje.api.application.fixture.participantApplicationSubmissionDto
import pl.oaza.waw.rekolekcje.api.application.fixture.sampleMember

internal class ApplicationQueryAcceptanceTest : BaseApplicationAcceptanceTest() {

  private val firstMemberDetails = sampleMember()
  private val secondMemberDetails = sampleMember(
    firstName = "Clark",
    lastName = "Kent",
    pesel = "70070186731"
  )
  private val thirdMemberDetails = sampleMember(
    firstName = "Diana",
    lastName = "Prince",
    pesel = "46012459245"
  )
  private val fourthMemberDetals = sampleMember(
    firstName = "Louis",
    lastName = "Lane",
    pesel = "96041327765"
  )

  @Test
  @WithMockUser
  fun `should fetch all animator applications`() {
    // given
    val animator1 = animatorApplicationSubmissionDto(member = firstMemberDetails)
    val participant1 = participantApplicationSubmissionDto(member = firstMemberDetails)
    val animator2 = animatorApplicationSubmissionDto(member = secondMemberDetails)
    val participant2 = participantApplicationSubmissionDto(member = thirdMemberDetails)
    val applicationIds = whenInApi.alreadyExistSomeApplications(listOf(animator1, participant1, animator2, participant2))

    // when
    val response = whenInApi.allAnimatorApplicationsAreRequested()

    // then
    response.hasStatusOk()
    thenInApi.responseHasAllApplicationsWithMatchingMemberDetails(
      response, listOf(
        fullApplicationFromSubmission(animator1, applicationIds[0]),
        fullApplicationFromSubmission(animator2, applicationIds[2])
      )
    )
  }

  @Test
  @WithMockUser
  fun `should find all participant applications`() {
    // given
    val animator1 = animatorApplicationSubmissionDto(member = firstMemberDetails)
    val participant1 = participantApplicationSubmissionDto(member = firstMemberDetails)
    val animator2 = animatorApplicationSubmissionDto(member = secondMemberDetails)
    val participant2 = participantApplicationSubmissionDto(member = thirdMemberDetails)
    val applicationIds = whenInApi.alreadyExistSomeApplications(listOf(animator1, participant1, animator2, participant2))

    // when
    val response = whenInApi.allParticipantApplicationsAreRequested()

    // then
    response.hasStatusOk()
    thenInApi.responseHasAllApplicationsWithMatchingMemberDetails(
      response, listOf(
        fullApplicationFromSubmission(participant1, applicationIds[1]),
        fullApplicationFromSubmission(participant2, applicationIds[3])
      )
    )
  }

  @Test
  @WithMockUser
  fun `should find one application`() {
    // given
    val firstApplication = animatorApplicationSubmissionDto(member = firstMemberDetails)
    val secondApplication = animatorApplicationSubmissionDto(member = secondMemberDetails)
    val thirdApplication = participantApplicationSubmissionDto(member = thirdMemberDetails)
    val applicationIds = whenInApi.alreadyExistSomeApplications(
      listOf(firstApplication, secondApplication, thirdApplication)
    )

    // when
    val animatorResponse = whenInApi.singleApplicationIsRequested(applicationIds[1])

    // then
    animatorResponse.hasStatusOk()
    thenInApi.responseHasApplicationWithMatchingMemberDetails(
      animatorResponse,
      fullApplicationFromSubmission(secondApplication, applicationIds[1])
    )

    // when
    val participantResponse = whenInApi.singleApplicationIsRequested(applicationIds[2])

    // then
    participantResponse.hasStatusOk()
    thenInApi.responseHasApplicationWithMatchingMemberDetails(
      participantResponse,
      fullApplicationFromSubmission(thirdApplication, applicationIds[2])
    )
  }

  @Test
  @WithMockUser
  fun `should return NOT_FOUND code when there is no application with given id`() {
    // given
    val nonExistingId = 321L

    // when
    val response = whenInApi.singleApplicationIsRequested(nonExistingId)

    // then
    response.hasStatusNotFound()
  }

  @Test
  @WithMockUser
  @Disabled
  fun `should find all applications waiting for verification`() {
    // given
    whenInApi.alreadyExistSomeApplications(
      listOf(
        SampleApplicationData(member = firstMemberDetails, isAnimator = true),
        SampleApplicationData(member = secondMemberDetails, isAnimator = true, status = ApplicationStatus.VERIFIED),
        SampleApplicationData(member = secondMemberDetails, isAnimator = false),
        SampleApplicationData(member = thirdMemberDetails, isAnimator = false, status = ApplicationStatus.VERIFIED),
        SampleApplicationData(member = fourthMemberDetals, isAnimator = false)
      )
    )

    // when
    val response = whenInApi.allPendingApplicationsAreRequested()

    // then
    val expectedSummaries = listOf(
      applicationSummary(firstMemberDetails, ApplicationStatus.WAITING, true),
      applicationSummary(secondMemberDetails, ApplicationStatus.WAITING, false),
      applicationSummary(fourthMemberDetals, ApplicationStatus.WAITING, false)
    )
    thenInApi.responseHasAllApplicationSummaries(response, expectedSummaries)
  }

  @Test
  @WithMockUser
  @Disabled
  fun `should find all applications already verified`() {
    // given
    whenInApi.alreadyExistSomeApplications(
      listOf(
        SampleApplicationData(member = firstMemberDetails, isAnimator = true),
        SampleApplicationData(member = secondMemberDetails, isAnimator = true, status = ApplicationStatus.VERIFIED),
        SampleApplicationData(member = secondMemberDetails, isAnimator = false),
        SampleApplicationData(member = thirdMemberDetails, isAnimator = false, status = ApplicationStatus.VERIFIED),
        SampleApplicationData(member = fourthMemberDetals, isAnimator = false)
      )
    )

    // when
    val response = whenInApi.allReadyApplicationsAreRequested()

    // then
    val expectedSummaries = listOf(
      applicationSummary(secondMemberDetails, ApplicationStatus.VERIFIED, true),
      applicationSummary(thirdMemberDetails, ApplicationStatus.VERIFIED, false)
    )
    thenInApi.responseHasAllApplicationSummaries(response, expectedSummaries)
  }
}