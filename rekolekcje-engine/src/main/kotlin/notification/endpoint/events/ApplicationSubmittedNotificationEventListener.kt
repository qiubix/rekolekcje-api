package notification.endpoint.events

import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationListener
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import pl.oaza.waw.rekolekcje.api.application.domain.RetreatApplication
import pl.oaza.waw.rekolekcje.api.application.endpoint.events.ApplicationSubmittedEvent
import pl.oaza.waw.rekolekcje.api.application.query.ApplicationQueryFacade
import pl.oaza.waw.rekolekcje.api.members.query.MembersQueryFacade
import pl.oaza.waw.rekolekcje.api.notification.CreateNotificationRequest
import pl.oaza.waw.rekolekcje.api.notification.domain.NotificationCommandFacade
import pl.oaza.waw.rekolekcje.api.notification.dto.NotificationReason


@Component
internal class ApplicationSubmittedNotificationEventListener(
    private val notificationCommandFacade: NotificationCommandFacade,
    private val applicationQueryFacade: ApplicationQueryFacade,
    private val membersQueryFacade: MembersQueryFacade
) : ApplicationListener<ApplicationSubmittedEvent> {

  private val log = LoggerFactory.getLogger(this.javaClass)

  @Transactional
  override fun onApplicationEvent(event: ApplicationSubmittedEvent) {
    val applicationId = event.applicationId
    val application = applicationQueryFacade.findApplicationById(applicationId)

    if (application == null) {
      log.error("Cannot find application with id $applicationId. Unable to send any related notifications.")
      return
    }

    val memberEmail = membersQueryFacade.getEmail(application.memberId)

    memberEmail?.let {
      sendSubmissionConfirmation(applicationId, memberEmail)
    } ?: run {
      log.error("For application $applicationId cannot notify member without email address")
      return
    }

    askSuretyForOpinion(application, applicationId)

  }

  private fun sendSubmissionConfirmation(applicationId: Long, memberEmail: String) {
    notificationCommandFacade.create(CreateNotificationRequest(
      applicationId = applicationId,
      reason = NotificationReason.APPLICATION_SUBMITTED,
      targetEmail = memberEmail
    ))
  }

  private fun askSuretyForOpinion(application: RetreatApplication, applicationId: Long) {
    application.surety?.email?.let {
      notificationCommandFacade.create(CreateNotificationRequest(
          applicationId = applicationId,
          reason = NotificationReason.OPINION_NEEDED,
          targetEmail = it
      ))
    } ?: log.error("Cannot find surety email for application ${applicationId}, not sending email")
  }
}
