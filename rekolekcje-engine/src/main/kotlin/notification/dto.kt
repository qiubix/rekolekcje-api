package pl.oaza.waw.rekolekcje.api.notification

import pl.oaza.waw.rekolekcje.api.core.ddd.ValueObject
import pl.oaza.waw.rekolekcje.api.notification.dto.NotificationReason

@ValueObject
data class CreateNotificationRequest(
    val applicationId: Long,
    val reason: NotificationReason,
    val targetEmail: String,
    val additionalInfo: String? = null
)
