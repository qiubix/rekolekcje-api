import { Component, Input, OnInit } from '@angular/core';
import * as moment from 'moment';
import { Moment } from 'moment';
import { FormControl } from '@angular/forms';
import { MatDatepickerInputEvent } from '@angular/material/datepicker';

@Component({
  selector: 'reko-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss'],
})
export class DatepickerComponent implements OnInit {
  value: Moment;

  @Input() id: string = 'default-picker-id';
  @Input() initialValue: string;
  @Input() labelKey: string;
  @Input() inputControl: FormControl;

  ngOnInit() {
    let stringValue = this.inputControl.value;
    this.value = moment(stringValue).local(false);
  }

  onDateChanged(event: MatDatepickerInputEvent<Moment>) {
    let date: Moment = event.value;
    let formattedDate = date.local(false).format('YYYY-MM-DD');
    this.inputControl.setValue(formattedDate);
  }
}
