import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { UsersRoutingModule } from './users-routing.module';
import { NavigationModule } from '../navigation/navigation.module';
import { ChangePasswordDialogComponent, UsersAddEditDialogComponent } from './components';
import * as fromComponents from './components';

@NgModule({
  declarations: [fromComponents.components],
  imports: [SharedModule, UsersRoutingModule, NavigationModule],
  entryComponents: [UsersAddEditDialogComponent, ChangePasswordDialogComponent]
})
export class UsersModule {}
