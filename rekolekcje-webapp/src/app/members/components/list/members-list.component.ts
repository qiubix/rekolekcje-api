import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
// import { DeleteConfirmAlertDialog, ImportDialogComponent } from '@shared/components';
import { DeleteConfirmAlertDialog } from '@shared/components';
import { Community } from '@diocese/community/models/community.model';
import { Stage } from '@shared/models/stage.model';
import { Parish } from '@diocese/parish/models/parish.model';
import { MemberSummary } from '@members/models';
import { Config } from '@config';
import { environment } from '@env';
import { RetreatSummary } from '@retreats/models/retreat-summary.model';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'members-list',
  templateUrl: './members-list.component.html',
  styleUrls: ['./members-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MembersListComponent implements OnInit {
  @Input() parishes: Parish[];
  @Input() communities: Community[];
  @Input() retreats: RetreatSummary[];

  @Input()
  set members(members: MemberSummary[]) {
    this._members = members;
    this.cd.detectChanges();
  }

  @Input() membersLoading: boolean;

  @Output() addMember: EventEmitter<void> = new EventEmitter<void>();
  @Output() deleteMember: EventEmitter<number> = new EventEmitter<number>();
  @Output() editMember: EventEmitter<MemberSummary> = new EventEmitter<MemberSummary>();
  @Output() selectMember: EventEmitter<MemberSummary> = new EventEmitter<MemberSummary>();

  readonly defaultFileName = Config.defaultExportFileName;
  displayedColumns = [
    'firstName',
    'lastName',
    // 'applyingForRetreatTurn',
    // 'memberStatus',
    // 'paymentStatus',
    'community',
    'options',
  ];
  readonly exportMembersUrl = environment.apiUrl + '/members/export';
  stages = Stage;

  get members(): MemberSummary[] {
    return this._members;
  }

  private _members: MemberSummary[] = [];

  constructor(private cd: ChangeDetectorRef, private dialog: MatDialog) {}

  ngOnInit() {}

  goToDetails(member: MemberSummary): void {
    this.selectMember.emit(member);
  }

  emitAddNewEvent(): void {
    this.addMember.emit();
  }

  emitEditEvent(member: MemberSummary): void {
    this.editMember.emit(member);
  }

  openImportDialog(): void {
    // this.dialog.open(ImportDialogComponent);
  }

  openConfirmDeleteAlert(id: number) {
    this.dialog
      .open(DeleteConfirmAlertDialog)
      .afterClosed()
      .subscribe((result: boolean | undefined) => {
        if (result) {
          this.deleteMember.emit(id);
        }
      });
  }
}
