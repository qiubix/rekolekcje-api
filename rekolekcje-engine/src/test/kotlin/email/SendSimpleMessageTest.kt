package pl.oaza.waw.rekolekcje.api.email

import io.mockk.verify
import org.junit.jupiter.api.Test
import pl.oaza.waw.rekolekcje.api.email.domain.EmailTest


internal class SendSimpleMessageTest : EmailTest() {

  @Test
  fun `should send simple message with given params`() {
    // given
    val recipient = "some@mail.com"
    val subject = "This is a test subject"
    val body = "Some sample message body."
    val sampleSendEmailRequest = SendEmailRequest(
      to = recipient,
      subject = subject,
      body = body
    )

    // when
    emailCommandFacade.sendEmail(sampleSendEmailRequest)

    // then
    val simpleMailMessageSample = buildSimpleMailMessage(
      to = recipient,
      subject = subject,
      body = body
    )
    verify(exactly = 1) { javaMailSenderMock.send(simpleMailMessageSample) }
  }

}
