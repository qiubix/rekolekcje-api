package pl.oaza.waw.rekolekcje.api.tools.validation

internal data class ApiGlobalError(
  private val code: String? = null
)
