import { Action, Store } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { Retreats } from './retreat-reducer';
import { Injectable } from '@angular/core';
import { RetreatDetails } from 'app/retreats/models/retreat-details.model';
import { catchError, concatMapTo, map, switchMap } from 'rxjs/operators';
import { RetreatsSharedActions } from './retreat-actions';
import { RetreatSummary } from '../models/retreat-summary.model';
import { RetreatsService } from './retreats.service';
import CreateRetreatSuccess = RetreatsSharedActions.CreateRetreatSuccess;
import DeleteRetreatSuccess = RetreatsSharedActions.DeleteRetreatSuccess;
import UpdateRetreatSuccess = RetreatsSharedActions.UpdateRetreatSuccess;
import SelectRetreat = RetreatsSharedActions.SelectRetreat;
import SelectRetreatSuccess = RetreatsSharedActions.SelectRetreatSuccess;
import CreateRetreatFail = RetreatsSharedActions.CreateRetreatFail;
import DeleteRetreatFail = RetreatsSharedActions.DeleteRetreatFail;
import LoadRetreatsListSuccess = RetreatsSharedActions.LoadRetreatsListSuccess;
import LoadRetreatsListFail = RetreatsSharedActions.LoadRetreatsListFail;
import UpdateRetreatFail = RetreatsSharedActions.UpdateRetreatFail;
import SelectRetreatFail = RetreatsSharedActions.SelectRetreatFail;

@Injectable()
export class RetreatsEffects {
  @Effect()
  CreateRetreat: Observable<Action> = this.actions.pipe(
    ofType(RetreatsSharedActions.types.CreateRetreat),
    switchMap((action: RetreatsSharedActions.CreateRetreat) =>
      this.service.addNew(action.payload).pipe(
        map(data => new CreateRetreatSuccess(data)),
        catchError(error => of(new CreateRetreatFail(error)))
      )
    )
  );

  @Effect()
  DeleteRetreat: Observable<Action> = this.actions.pipe(
    ofType(RetreatsSharedActions.types.DeleteRetreat),
    switchMap((action: RetreatsSharedActions.DeleteRetreat) =>
      this.service.deleteOne(action.payload).pipe(
        map(data => new DeleteRetreatSuccess(action.payload)),
        catchError(error => of(new DeleteRetreatFail(error)))
      )
    )
  );

  @Effect()
  LoadRetreatsList: Observable<Action> = this.actions.pipe(
    ofType(RetreatsSharedActions.types.LoadRetreatsList),
    switchMap((action: RetreatsSharedActions.LoadRetreatsList) =>
      this.service.getAll().pipe(
        map((data: RetreatSummary[]) => new LoadRetreatsListSuccess(data)),
        catchError(error => of(new LoadRetreatsListFail(error)))
      )
    )
  );

  @Effect()
  SelectRetreat: Observable<Action> = this.actions.pipe(
    ofType(RetreatsSharedActions.types.SelectRetreat),
    switchMap((action: RetreatsSharedActions.SelectRetreat) =>
      this.service.getOne(action.payload).pipe(
        map((data: RetreatDetails) => new SelectRetreatSuccess(data)),
        catchError(error => of(new SelectRetreatFail(error)))
      )
    )
  );

  @Effect()
  UpdateRetreat: Observable<Action> = this.actions.pipe(
    ofType(RetreatsSharedActions.types.UpdateRetreat),
    switchMap((action: RetreatsSharedActions.UpdateRetreat) => {
      // console.log('Sending PUT request with updated retreat... Payload: ', action.payload);
      return this.service.update(action.payload);
    }),
    switchMap((res: RetreatSummary) => [new UpdateRetreatSuccess(res), new SelectRetreat(res.id)]),
    catchError(error => of(new UpdateRetreatFail(error)))
  );

  constructor(
    private actions: Actions,
    private store: Store<Retreats.State>,
    private service: RetreatsService
  ) {}
}
