package pl.oaza.waw.rekolekcje.api.shared.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.BAD_REQUEST)
class InvalidPayloadException : RuntimeException(MESSAGE) {

  companion object {
    private const val MESSAGE = "Invalid payload state exception"
  }

}
