import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { ApplicationSubmission, ApplicationType, RetreatApplication } from '@verification/model';
import { Observable } from 'rxjs/internal/Observable';
import { AppSelectors } from '@core/store/app-selectors';
import { RetreatSummary } from '@retreats/models/retreat-summary.model';
import { RetreatsSharedActions } from '@retreats/store/retreat-actions';
import { ActivatedRoute, Router } from '@angular/router';
import { Verification } from '@verification/state/verification-reducer';
import { VerificationActions } from '@verification/state/verification-actions';

@Component({
  selector: 'reko-add-edit-application-page',
  templateUrl: './add-edit-application-page.component.html',
  styleUrls: ['./add-edit-application-page.component.scss']
})
export class AddEditApplicationPageComponent implements OnInit {
  readonly editMode: boolean;
  animator: boolean;

  selectedApplication$: Observable<RetreatApplication>;
  retreats$: Observable<RetreatSummary[]>;

  constructor(
    private store: Store<Verification.State>,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.editMode = this.activatedRoute.snapshot.data['editing'];
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.animator = JSON.parse(params.animator);
    });
    this.store.dispatch(new RetreatsSharedActions.LoadRetreatsList());
    this.selectedApplication$ = this.store.pipe(select(AppSelectors.getSelectedApplication));
    this.retreats$ = this.store.pipe(select(AppSelectors.getRetreatsList));
  }

  get title(): string {
    return this.editMode ? 'Edytuj zgłoszenie' : 'Nowe zgłoszenie';
  }

  onSubmit(application: ApplicationSubmission) {
    const applicationWithType = Object.assign({}, application, {
      applicationType: this.animator ? ApplicationType.ANIMATOR : ApplicationType.PARTICIPANT
    });
    const payload = { application: applicationWithType, update: this.editMode };
    console.log('Trying to submit application ', payload);
    this.store.dispatch(new VerificationActions.SaveApplication(payload));
    this.store.dispatch(new VerificationActions.GetPendingApplications());
    this.router.navigate(['verification']);
  }
}
