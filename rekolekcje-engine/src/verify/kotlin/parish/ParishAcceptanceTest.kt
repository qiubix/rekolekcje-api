package pl.oaza.waw.rekolekcje.api.parish

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.test.context.support.WithMockUser
import pl.oaza.waw.rekolekcje.api.shared.BaseIntegrationTest
import pl.oaza.waw.rekolekcje.api.parish.api.ParishApiBehaviours
import pl.oaza.waw.rekolekcje.api.parish.api.ParishApiExpectations
import pl.oaza.waw.rekolekcje.api.parish.domain.ParishFacade
import pl.oaza.waw.rekolekcje.api.parish.dto.ParishPayload
import pl.oaza.waw.rekolekcje.api.parish.fixture.buildParish
import pl.oaza.waw.rekolekcje.api.parish.storage.ParishDatabase

internal class ParishAcceptanceTest : BaseIntegrationTest() {

  private val firstParish = buildParish("First", "Address no 1.").copy(id = 1L)
  private val secondParish = buildParish("Second", "Address no 2.").copy(id = 2L)
  private val parishes = listOf(firstParish, secondParish)

  @Autowired
  private lateinit var parishFacade: ParishFacade

  @Autowired
  private lateinit var whenInApi: ParishApiBehaviours

  @Autowired
  private lateinit var thenInApi: ParishApiExpectations

  @Autowired
  private lateinit var database: ParishDatabase

  @BeforeEach
  fun setUp() {
    database.clear()
  }

  @Test
  @WithMockUser
  @Throws(Exception::class)
  fun `should get all parishes`() {
    // given system has some parishes
    saveAllParishes(parishes)

    // when requesting all parishes
    val response = whenInApi.allAreRequested()

    // all are returned
    thenInApi.responseHasCorrectDataIgnoringId(response, parishes)
  }

  @Test
  @WithMockUser
  @Throws(Exception::class)
  fun `should get a single parish`() {
    // given system has some parishes
    saveAllParishes(parishes)
    val idToFind = whenInApi.existsElementWithTheSameData(secondParish).id

    // when requesting single parish
    val response = whenInApi.singleIsRequested(idToFind)

    // it's details are returned
    thenInApi.responseHasExactPayload(response, secondParish.copy(id = idToFind))
  }

  @Test
  @WithMockUser
  @Throws(Exception::class)
  fun `should create a new parish`() {
    // given new parish
    val parishToAdd = buildParish("Parish to add", "New parish address")

    // when posting new parish
    val response = whenInApi.singleIsAdded(parishToAdd)

    // new parish is created
    thenInApi.responseStatusIsCreated(response)
    thenInApi.responseHasCorrectDataIgnoringId(response, parishToAdd)
  }

  @Test
  @WithMockUser
  @Throws(Exception::class)
  fun `should assign id to parish on creation`() {
    // given parish data without id
    val parishToAdd = buildParish("Brand new parish", "Some address")

    // when posting this data to endpoint
    val response = whenInApi.singleIsAdded(parishToAdd)

    // new parish is created with generated id
    thenInApi.responseHasDataWithId(response)
  }

  @Test
  @WithMockUser
  @Throws(Exception::class)
  fun `should delete a single parish`() {
    // given system has some parishes
    saveAllParishes(parishes)
    val idToDelete = whenInApi.existsElementWithTheSameData(secondParish).id!!

    // when posting delete request with id
    whenInApi.singleIsDeleted(idToDelete)

    // then single parish is deleted
    thenInApi.thereIsAnExactNumberOfEntities(parishes.size - 1)
    thenInApi.doesNotExist(secondParish.copy(id = idToDelete))
  }

  @Test
  @WithMockUser
  @Throws(Exception::class)
  fun `should update parish with new data`() {
    // given system has some parishes
    saveAllParishes(parishes)
    val idToUpdate = whenInApi.existsElementWithTheSameData(secondParish).id!!
    val parishWithNewData = buildParish("Brand new name", "Completely new address")
      .copy(id = idToUpdate)

    // when posting update request with new data
    val response = whenInApi.singleIsUpdated(parishWithNewData)

    // then single parish is updated
    thenInApi.responseStatusIsOk(response)
    thenInApi.thereIsAnExactNumberOfEntities(parishes.size)
    thenInApi.responseHasExactPayload(response, parishWithNewData)
    thenInApi.exists(parishWithNewData)
  }

  private fun saveAllParishes(parishes: List<ParishPayload>) {
    parishes.forEach { parish -> parishFacade.save(parish) }
  }
}
