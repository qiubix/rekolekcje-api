import { createFeatureSelector, createSelector } from '@ngrx/store';
import { Members } from '@members/state/members-reducer';
import { Parishes } from '@diocese/parish/store/parish-reducer';
import { Retreats } from '@retreats/store/retreat-reducer';
import { Communities } from '@diocese/community/store/community-reducer';
import { Users } from '@users/store/users-reduces';
import { Verification } from '@verification/state/verification-reducer';

export namespace AppSelectors {
  export const getMembersModuleState = createFeatureSelector<Members.State>('membersModule');
  export const getVerificationModuleState = createFeatureSelector<Verification.State>(
    'verificationModule'
  );
  export const getParishModuleState = createFeatureSelector<Parishes.State>('parishModule');
  export const getCommunitiesModuleState = createFeatureSelector<Communities.State>(
    'communitiesModule'
  );
  export const getRetreatsModuleState = createFeatureSelector<Retreats.State>('retreatsModule');
  export const getUsersModuleState = createFeatureSelector<Users.State>('usersModule');

  /**
   * Members selectors
   */
  export const getMembersList = createSelector(
    getMembersModuleState,
    (state: Members.State) => state.membersList
  );

  export const getMembersToVerify = createSelector(
    getMembersModuleState,
    (state: Members.State) => state.membersToVerify
  );

  export const getMembersLoading = createSelector(
    getMembersModuleState,
    (state: Members.State) => state.membersLoading
  );

  export const getSelectedMember = createSelector(
    getMembersModuleState,
    (state: Members.State) => state.selectedMember
  );

  export const getRecentlyCreatedMemberId = createSelector(
    getMembersModuleState,
    (state: Members.State) => state.recentlyCreatedMemberId
  );

  export const getCurrentApplicationStage = createSelector(
    getMembersModuleState,
    (state: Members.State) => state.currentApplicationStage
  );

  export const getVerificationStatistics = createSelector(
    getVerificationModuleState,
    (state: Verification.State) => state.statistics
  );

  export const getSelectedApplication = createSelector(
    getVerificationModuleState,
    (state: Verification.State) => state.selectedApplication
  );

  export const getPendingApplications = createSelector(
    getVerificationModuleState,
    (state: Verification.State) => state.pendingApplications
  );

  export const getVerifiedApplications = createSelector(
    getVerificationModuleState,
    (state: Verification.State) => state.verifiedApplications
  );

  /**
   * Parish selectors
   */
  export const getParishList = createSelector(
    getParishModuleState,
    (state: Parishes.State) => state.parishList
  );

  export const getSelectedParish = createSelector(
    getParishModuleState,
    (state: Parishes.State) => state.selectedParish
  );

  /**
   * Community selectors
   */
  export const getCommunityList = createSelector(
    getCommunitiesModuleState,
    (state: Communities.State) => state.communitiesList
  );

  export const getCommunityListLoading = createSelector(
    getCommunitiesModuleState,
    (state: Communities.State) => state.communitiesListLoading
  );

  export const getSelectedCommunity = createSelector(
    getCommunitiesModuleState,
    (state: Communities.State) => state.selectedCommunity
  );

  /**
   * Retreats selectors
   */
  export const getRetreatsList = createSelector(
    getRetreatsModuleState,
    (state: Retreats.State) => state.retreatsList
  );

  export const getRetreatsLoading = createSelector(
    getRetreatsModuleState,
    (state: Retreats.State) => state.retreatsLoading
  );

  export const getSelectedRetreat = createSelector(
    getRetreatsModuleState,
    (state: Retreats.State) => state.selectedRetreat
  );

  export const getRetreatDetailsLoading = createSelector(
    getRetreatsModuleState,
    (state: Retreats.State) => state.retreatDetailsLoading
  );

  /**
   * Users selectors
   */
  export const getUsersList = createSelector(
    getUsersModuleState,
    (state: Users.State) => state.usersList
  );

  export const getUsersLoading = createSelector(
    getUsersModuleState,
    (state: Users.State) => state.usersLoading
  );

  export const getUsersNotification = createSelector(
    getUsersModuleState,
    (state: Users.State) => state.notification
  );

  export const getCurrentUser = createSelector(
    getUsersModuleState,
    (state: Users.State) => state.currentUser
  );
}
