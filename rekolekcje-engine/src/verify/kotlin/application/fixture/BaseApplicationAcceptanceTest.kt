package pl.oaza.waw.rekolekcje.api.application.fixture

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringExtension
import pl.oaza.waw.rekolekcje.api.application.api.ApplicationApiBehaviours
import pl.oaza.waw.rekolekcje.api.application.api.ApplicationApiExpectations
import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationStatus
import pl.oaza.waw.rekolekcje.api.application.storage.ApplicationDatabase
import pl.oaza.waw.rekolekcje.api.members.api.MembersApiBehaviour
import pl.oaza.waw.rekolekcje.api.members.api.MembersApiExpectations
import pl.oaza.waw.rekolekcje.api.members.storage.MembersDatabase
import pl.oaza.waw.rekolekcje.api.shared.BaseTestContext

@ExtendWith(SpringExtension::class)
@SpringBootTest(
  webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
  classes = [BaseTestContext::class, ApplicationTestContext::class],
  properties = ["spring.datasource.url=jdbc:tc:postgresql:10.6-alpine://rekolekcjedb-test",
    "spring.datasource.username=" + "postgres",
    "spring.datasource.password=" + "postgres",
    "spring.datasource.driver-class-name=org.testcontainers.jdbc.ContainerDatabaseDriver"
  ]
)
@ActiveProfiles("test", "test-local")
@AutoConfigureJsonTesters
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
abstract class BaseApplicationAcceptanceTest {

  @Autowired
  private lateinit var database: ApplicationDatabase

  @Autowired
  private lateinit var membersDatabase: MembersDatabase

  @Autowired
  protected lateinit var whenInApi: ApplicationApiBehaviours

  @Autowired
  protected lateinit var thenInApi: ApplicationApiExpectations

  @Autowired
  protected lateinit var whenInMembersApi: MembersApiBehaviour

  @Autowired
  protected lateinit var thenInMembersApi: MembersApiExpectations

  @BeforeEach
  internal fun setUp() {
    database.clear()
    membersDatabase.clear()
  }

  protected fun updateStatus(id: Long, status: ApplicationStatus) {
    database.setApplicationStatus(id, status)
  }
}