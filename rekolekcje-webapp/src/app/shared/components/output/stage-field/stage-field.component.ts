import { Component, Input, OnInit } from '@angular/core';
import { Stage } from '../../../models/stage.model';

@Component({
  selector: 'reko-stage-field',
  templateUrl: './stage-field.component.html',
  styleUrls: ['./stage-field.component.scss']
})
export class StageFieldComponent implements OnInit {
  @Input() label: String;
  @Input() stage: Stage;

  stageValue: String;
  stages = Stage;

  ngOnInit(): void {
    this.stageValue = this.stage ? this.stages[this.stage] : '';
  }
}
