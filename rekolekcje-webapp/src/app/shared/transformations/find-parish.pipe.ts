import { OnDestroy, Pipe, PipeTransform } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Parishes } from '../../diocese/parish/store/parish-reducer';
import { AppSelectors } from '../../core/store/app-selectors';
import { filter, map, take, takeUntil, tap } from 'rxjs/operators';
import { ParishSharedActions } from '../../diocese/parish/store/parish-actions';
import { Parish } from '../../diocese/parish/models/parish.model';
import { Observable } from 'rxjs/internal/Observable';
import { Subject } from 'rxjs';

@Pipe({
  name: 'findParish'
})
export class FindParishPipe implements PipeTransform, OnDestroy {
  private _parishes$: Observable<Parish[]>;
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(private _store: Store<Parishes.State>) {
    this._parishes$ = this._store.pipe(
      select(AppSelectors.getParishList),
      takeUntil(this.ngUnsubscribe),
      // tap(parishes => {
      //   if (!parishes || parishes.length === 0) {
      //     this._store.dispatch(new ParishSharedActions.LoadParishList());
      //   }
      // }),
      filter(parishes => parishes.length > 0),
      take(1)
    );
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  transform(parishId: number, args?: any): Observable<Parish> {
    return this._parishes$.pipe(map(parishes => parishes.find(it => it.id === parishId)));
  }
}
