package pl.oaza.waw.rekolekcje.api.members

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.test.context.support.WithMockUser
import pl.oaza.waw.rekolekcje.api.shared.BaseIntegrationTest
import pl.oaza.waw.rekolekcje.api.members.api.MembersApiBehaviour
import pl.oaza.waw.rekolekcje.api.members.api.MembersApiExpectations
import pl.oaza.waw.rekolekcje.api.members.storage.MembersDatabase
import pl.oaza.waw.rekolekcje.api.members.storage.MembersStorageBehaviour
import pl.oaza.waw.rekolekcje.api.members.storage.MembersStorageExpectations
import pl.oaza.waw.rekolekcje.api.members.storage.PartialMemberFactory
import pl.oaza.waw.rekolekcje.api.members.utils.memberWithSampleData
import pl.oaza.waw.rekolekcje.api.members.utils.memberWithUpdatedData

internal class MembersIntegrationTest : BaseIntegrationTest() {

  @Autowired
  private lateinit var whenInStorage: MembersStorageBehaviour
  @Autowired
  private lateinit var thenInStorage: MembersStorageExpectations

  @Autowired
  private lateinit var whenInMembersApi: MembersApiBehaviour
  @Autowired
  private lateinit var thenInMembersApi: MembersApiExpectations

  @Autowired
  private lateinit var database: MembersDatabase

  private val partialMemberFactory = PartialMemberFactory()

  private val memberWithMinimalData = partialMemberFactory.withMinimalData(991L)
  private val sampleMember = partialMemberFactory.withSampleData(992L)
  private val members = listOf(memberWithMinimalData, sampleMember)

  @BeforeEach
  fun setup() {
    database.clear()
  }

  @Test
  @WithMockUser
  fun `should get all members when they are already saved`() {
    whenInStorage.existMembersWithSampleData(members)

    val response = whenInMembersApi.allAreRequested()

    thenInMembersApi.responseHasSummaryDataMatchingDetails(response, members)
  }

  @Test
  @WithMockUser
  fun `should persist member when he is added`() {
    whenInStorage.existMembersWithSampleData(members)
    val sampleMember = memberWithSampleData("Han", "Solo", "81010154327")

    whenInMembersApi.singleIsAdded(sampleMember)

    thenInStorage.numberOfMembersIsEqualTo(members.size + 1)
    thenInStorage.correctDataIsPersisted(sampleMember)
  }

  @Test
  @WithMockUser
  fun `should delete a single member`() {
    whenInStorage.existsSingleMemberWithSampleData(sampleMember)
    val id = sampleMember.id

    val response = whenInMembersApi.singleIsDeleted(id!!)

    response.hasStatusOk()
    thenInStorage.memberNoLongerExists(id)
    val numberOfContacts = thenInStorage.numberOfContacts()
    assertThat(numberOfContacts).isZero()
  }

  @Test
  @WithMockUser
  fun `should update persisted data when single member is updated`() {
    whenInStorage.existMembersWithSampleData(members)
    val id = sampleMember.id
    val numberOfContacts = thenInStorage.numberOfContacts()

    val memberWithNewData = memberWithUpdatedData(id)
    whenInMembersApi.singleIsUpdated(memberWithNewData)

    thenInStorage.numberOfMembersIsEqualTo(members.size)
    thenInStorage.correctDataIsPersisted(memberWithNewData)
    val newNumberOfContacts = thenInStorage.numberOfContacts()
    assertThat(newNumberOfContacts).isEqualTo(numberOfContacts + 3)
  }

}
