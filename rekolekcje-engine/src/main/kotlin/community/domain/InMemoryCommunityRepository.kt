package pl.oaza.waw.rekolekcje.api.community.domain

import java.util.Objects.requireNonNull
import java.util.concurrent.atomic.AtomicLong

internal class InMemoryCommunityRepository : CommunityRepository {

  private val nextId = AtomicLong(1)
  private val data = HashMap<Long, Community>()

  override fun findAll(): List<Community> {
    return ArrayList(data.values)
  }

  override fun findById(id: Long): Community? {
    return data[id]
  }

  override fun save(community: Community): Community {
    requireNonNull(community)
    var entityToSave = community
    if (community.id == null) {
      entityToSave = community.copy(id = nextId.getAndIncrement())
    }
    data[entityToSave.id!!] = entityToSave
    return entityToSave
  }

  override fun deleteById(id: Long) {
    data.remove(id)
  }
}
