import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Stage } from '@shared/models';
import { debounceTime, distinctUntilChanged, takeUntil, withLatestFrom } from 'rxjs/operators';
import { Config } from '@config';
import { Subject } from 'rxjs';
import { ApplicationSummary } from '@verification/model';
import { ApplicationListViewFilterUtil } from '@verification/components/application-list/application-list-view/application-list-view-filter/application-list-view-filter.util';
import { Retreats } from '@retreats/store/retreat-reducer';
import { select, Store } from '@ngrx/store';
import { AppSelectors } from '@core/store/app-selectors';
import { RetreatsSharedActions } from '@retreats/store/retreat-actions';

export interface ApplicationTableFilter {
  stage: string;
}

@Component({
  selector: 'reko-application-list-view-filter',
  templateUrl: './application-list-view-filter.component.html',
  styleUrls: ['./application-list-view-filter.component.scss']
})
export class ApplicationListViewFilterComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  @Input() applications: ApplicationSummary[];

  @Output() filterChange = new EventEmitter<ApplicationSummary[]>();

  form: FormGroup;
  stagesKeys = Object.keys(Stage);

  constructor(private _fb: FormBuilder, private _store: Store<Retreats.State>) {}

  ngOnInit() {
    this.form = this._fb.group({
      stage: ''
    });
    this._store.dispatch(new RetreatsSharedActions.LoadRetreatsList());
    const retreats$ = this._store.pipe(select(AppSelectors.getRetreatsList));
    this.form.valueChanges
      .pipe(
        withLatestFrom(retreats$),
        debounceTime(Config.inputDebounceTime),
        distinctUntilChanged(),
        takeUntil(this.ngUnsubscribe)
      )
      .subscribe(([value, retreats]) => {
        const filteredList = ApplicationListViewFilterUtil.filterApplications(
          this.applications,
          retreats,
          value
        );
        this.filterChange.emit(filteredList);
      });
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
