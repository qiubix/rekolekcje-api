package pl.oaza.waw.rekolekcje.api.members.domain

/**
 * Provides methods for modifying state of members in the system.
 */
interface MembersCommandFacade {

  /**
   * Saves member created from given [Member]. This method can be used either
   * for creating new member or updating an existing one. After saving the member, the
   * result is transformed to [Member] again. When the provided details does not have
   * an id, new one will be generated and returned with the result.
   * <br></br>
   * It assumes that the payload is valid, as the validation is performed in the higher layer.
   */
  fun save(member: Member): Member

  /**
   * Deletes members from repository by id.
   *
   * @param id ID of member to be removed from repository
   */
  fun delete(id: Long)

  /**
   * Adds multiple members at once.
   *
   * @param members List of members to add
   * @return List of saved members, containing ids.
   */
  fun saveAll(members: List<Member>): List<Member>

}
