package pl.oaza.waw.rekolekcje.api.email.domain

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.mail.SimpleMailMessage
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.MimeMessageHelper
import org.thymeleaf.context.Context
import org.thymeleaf.spring5.SpringTemplateEngine

internal class EmailServiceImpl(private val emailSender: JavaMailSender,
                                private val templateEngine: SpringTemplateEngine) : EmailService {

  private val log = LoggerFactory.getLogger(EmailServiceImpl::class.java)

  @Value("\${spring.mail.from}")
  private lateinit var from: String

  override fun sendSimpleMessage(to: String, subject: String, content: String) {
    val message = SimpleMailMessage()
    message.setFrom(from)
    message.setTo(to)
    message.setSubject(subject)
    message.setText(content)
    log.info("Sending simple message $from to $to with subject: $subject")
    emailSender.send(message)
  }

  override fun sendHtmlMessage(to: String, subject: String, htmlContent: String) {
    val message = emailSender.createMimeMessage()
    val helper = MimeMessageHelper(message, true, "UTF-8")
    helper.setFrom(from)
    helper.setTo(to)
    helper.setSubject(subject)
    helper.setText(htmlContent, true)
    log.info("Sending HTML message $from to $to with subject: $subject")
    emailSender.send(message)
  }

  override fun sendMessageWithTemplate(to: String, subject: String, template: String, templateModel: Map<String, Any>) {
    val thymeleafContext = Context()
    thymeleafContext.setVariables(templateModel)
    val htmlBody = templateEngine.process(template, thymeleafContext)
    sendHtmlMessage(to, subject, htmlBody)
  }
}
