import { Pipe, PipeTransform } from '@angular/core';
import { PaymentStatus } from '../../models/member-status.model';
import { TranslateService } from '@ngx-translate/core';

@Pipe({
  name: 'paymentStatusTranslate'
})
export class PaymentStatusTranslatePipe implements PipeTransform {
  constructor(private translate: TranslateService) {}

  transform(paymentStatus: PaymentStatus, args?: any): string {
    let value = '';
    const key = 'members.currentApplication.paymentStatus.';
    this.translate.get(key + paymentStatus).subscribe(it => (value = it));
    return value;
  }
}
