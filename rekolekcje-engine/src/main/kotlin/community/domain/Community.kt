package pl.oaza.waw.rekolekcje.api.community.domain

import pl.oaza.waw.rekolekcje.api.community.dto.CommunityPayload
import pl.oaza.waw.rekolekcje.api.core.ddd.AggregateRoot
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@AggregateRoot
@Entity
@Table(name = "COMMUNITIES")
internal data class Community(
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  val id: Long? = null,
  val name: String,
  val address: String? = null
) {

  fun dto(): CommunityPayload {
    return CommunityPayload(id, name, address)
  }
}
