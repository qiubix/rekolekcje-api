package pl.oaza.waw.rekolekcje.api.application.endpoint.csv

import pl.oaza.waw.rekolekcje.api.application.endpoint.RetreatApplicationDto
import pl.oaza.waw.rekolekcje.api.application.endpoint.csv.ApplicationCsvRowConverter.Companion.suretyContactEmailKey
import pl.oaza.waw.rekolekcje.api.application.endpoint.csv.ApplicationCsvRowConverter.Companion.suretyContactKey


internal val applicationFieldToHeaders: Map<String, List<String>> = mapOf(
  suretyContactKey to listOf("Imię i nazwisko osoby poręczającej", "Imię i nazwisko animatora grupki"),
  suretyContactEmailKey to listOf("Adres e-mail osoby poręczającej", "Adres e-mail animatora grupki"),
  RetreatApplicationDto::animatorRoles.name to listOf("Funkcja:"),
  RetreatApplicationDto::availability.name to listOf("Animator deklaruje gotowość wyjazdu na:"),
  RetreatApplicationDto::retreatId.name to listOf("Wybór turnusu i rekolekcji")
)
