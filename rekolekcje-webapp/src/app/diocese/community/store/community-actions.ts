import { Action } from '@ngrx/store';
import { Community } from '../models/community.model';

export namespace CommunityActions {
  export const types = {
    CreateCommunity: '[Communities] Create Community',
    CreateCommunityFail: '[Communities] Create Community Fail',
    CreateCommunitySuccess: '[Communities] Create Community Success',
    DeleteCommunity: '[Communities] Delete Community',
    DeleteCommunityFail: '[Communities] Delete Community Fail',
    DeleteCommunitySuccess: '[Communities] Delete Community Success',
    LoadCommunitiesList: '[Communities] Load Communities List',
    LoadCommunitiesListFail: '[Communities] Load Communities List Fail',
    LoadCommunitiesListSuccess: '[Communities] Load Communities List Success',
    SelectCommunity: '[Communities] Select Community',
    SelectCommunityFail: '[Communities] Select Community Fail',
    SelectCommunitySuccess: '[Communities] Select Community Success',
    UpdateCommunity: '[Communities] Update Community',
    UpdateCommunityFail: '[Communities] Update Community Fail',
    UpdateCommunitySuccess: '[Communities] Update Community Success'
  };

  /**
   * Create Community actions
   */
  export class CreateCommunity implements Action {
    type = types.CreateCommunity;

    constructor(public payload: Community) {}
  }

  export class CreateCommunityFail implements Action {
    type = types.CreateCommunityFail;

    constructor(public payload: any) {}
  }

  export class CreateCommunitySuccess implements Action {
    type = types.CreateCommunitySuccess;

    constructor(public payload: Community) {}
  }

  /**
   * Delete Community actions
   */
  export class DeleteCommunity implements Action {
    type = types.DeleteCommunity;

    constructor(public payload: number) {}
  }

  export class DeleteCommunityFail implements Action {
    type = types.DeleteCommunityFail;

    constructor(public payload: any) {}
  }

  export class DeleteCommunitySuccess implements Action {
    type = types.DeleteCommunitySuccess;

    constructor(public payload: number) {}
  }

  /**
   * Load Community list actions
   */
  export class LoadCommunitiesList implements Action {
    type = types.LoadCommunitiesList;

    constructor(public payload?: any) {}
  }

  export class LoadCommunitiesListFail implements Action {
    type = types.LoadCommunitiesListFail;

    constructor(public payload: any) {}
  }

  export class LoadCommunitiesListSuccess implements Action {
    type = types.LoadCommunitiesListSuccess;

    constructor(public payload: Community[]) {}
  }

  /**
   * Select Community actions
   */
  export class SelectCommunity implements Action {
    type = types.SelectCommunity;

    constructor(public payload: Community) {}
  }

  export class SelectCommunityFail implements Action {
    type = types.SelectCommunityFail;

    constructor(public payload: any) {}
  }

  export class SelectCommunitySuccess implements Action {
    type = types.SelectCommunitySuccess;

    constructor(public payload: Community) {}
  }

  /**
   * Update Community actions
   */
  export class UpdateCommunity implements Action {
    type = types.UpdateCommunity;

    constructor(public payload: Community) {}
  }

  export class UpdateCommunityFail implements Action {
    type = types.UpdateCommunityFail;

    constructor(public payload: any) {}
  }

  export class UpdateCommunitySuccess implements Action {
    type = types.UpdateCommunitySuccess;

    constructor(public payload: Community) {}
  }
}
