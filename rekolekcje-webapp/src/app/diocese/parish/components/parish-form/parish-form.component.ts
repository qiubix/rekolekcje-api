import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Parish } from '../../models/parish.model';

@Component({
  selector: 'reko-parish-form',
  templateUrl: './parish-form.component.html',
  styleUrls: ['./parish-form.component.scss']
})
export class ParishFormComponent implements OnInit {
  @Input() formData: Parish;

  @Output() formOutput: EventEmitter<Parish> = new EventEmitter();

  form: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.form = this.fb.group({
      name: [this.formData ? this.formData.name : '', Validators.required],
      address: [this.formData ? this.formData.address : '', Validators.required]
    });
  }

  cancel(): void {
    this.formOutput.emit();
  }

  submit(): void {
    if (this.form.invalid) {
      this.form.markAsDirty();
      return;
    }
    this.formOutput.emit(this.form.value);
  }
}
