package pl.oaza.waw.rekolekcje.api.retreats.domain

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import pl.oaza.waw.rekolekcje.api.retreats.RetreatTest
import pl.oaza.waw.rekolekcje.api.retreats.sampleRetreat
import pl.oaza.waw.rekolekcje.api.retreats.sampleRetreatMember
import pl.oaza.waw.rekolekcje.api.shared.value.Stage


internal class RemoveMemberFromRetreatsTest : RetreatTest() {

  @Test
  fun `should remove member from all retreats`() {
    // given
    val memberId = 123L
    val member = sampleRetreatMember(memberId = memberId)
    val firstRetreatId = saveAndGetId(sampleRetreat(stage = Stage.OR_I))
    val secondRetreatId = saveAndGetId(sampleRetreat(stage = Stage.OR_II))
    commandFacade.registerMember(firstRetreatId, member)
    commandFacade.registerMember(secondRetreatId, member)

    // when
    commandFacade.removeMemberFromAllRetreats(memberId)

    // then
    queryFacade.findAll().forEach {
      assertThat(it.participants()).doesNotContain(member)
    }
  }
}
