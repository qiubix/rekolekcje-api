package pl.oaza.waw.rekolekcje.api.application.query

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import pl.oaza.waw.rekolekcje.api.application.ApplicationTestFixture
import pl.oaza.waw.rekolekcje.api.application.animatorApplication
import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationStatus
import pl.oaza.waw.rekolekcje.api.application.participantApplication

internal class ApplicationVerificationStatisticsTest: ApplicationTestFixture() {

  private val first = animatorApplication(11L, ApplicationStatus.WAITING)
  private val second = animatorApplication(12L, ApplicationStatus.PROBLEM)
  private val third = animatorApplication(13L, ApplicationStatus.VERIFIED)
  private val fourth = participantApplication(14L, ApplicationStatus.WAITING)
  private val fifth = participantApplication(15L, ApplicationStatus.VERIFIED)
  private val sixth = participantApplication(16L, ApplicationStatus.REGISTERED)
  private val seventh = participantApplication(17L, ApplicationStatus.REJECTED)


  @BeforeEach
  fun setUp() {
    persistAllApplications(listOf(first, second, third, fourth, fifth, sixth, seventh))
  }

  @Test
  fun `should return total number of active applications`() {
    // when
    val statistics = queryFacade.getVerificationStatistics()

    // then
    assertThat(statistics.numberOfActiveApplications).isEqualTo(7)
  }

  @Test
  fun `should return number of already verified applications`() {
    // when
    val statistics = queryFacade.getVerificationStatistics()

    // then
    assertThat(statistics.alreadyVerified).isEqualTo(4)
  }

  @Test
  fun `should return number of applications remaining to verify`() {
    // when
    val statistics = queryFacade.getVerificationStatistics()

    // then
    assertThat(statistics.remainingToVerify).isEqualTo(3)
  }

  @Test
  fun `should return number of remaining participant applications to verify`() {
    // when
    val statistics = queryFacade.getVerificationStatistics()

    // then
    assertThat(statistics.participantsRemaining).isEqualTo(1)
  }

  @Test
  fun `should return number of remaining animator applications to verify`() {
    // when
    val statistics = queryFacade.getVerificationStatistics()

    // then
    assertThat(statistics.animatorsRemaining).isEqualTo(2)
  }
}