package pl.oaza.waw.rekolekcje.api.members.domain

import javax.persistence.Embeddable

@Embeddable
data class HealthReport(
  val currentTreatment: String? = null,
  val mentalDisorders: String? = null,
  val medications: String? = null,
  val allergies: String? = null,
  val medicalDiet: String? = null,
  val cannotHike: Boolean? = null,
  val illnessHistory: String? = null,
  val hasMotionSickness: Boolean? = null,
  val other: String? = null
)
