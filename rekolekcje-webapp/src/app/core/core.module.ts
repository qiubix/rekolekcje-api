import { NgModule } from '@angular/core';
import { MainComponent } from './main/main.component';
import { NavigationModule } from '../navigation/navigation.module';
import { RouterModule } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  imports: [NavigationModule, BrowserModule, BrowserAnimationsModule, RouterModule],
  declarations: [MainComponent],
  exports: [MainComponent]
})
export class CoreModule {}
