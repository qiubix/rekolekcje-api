import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { MembersModule } from './members/members.module';
import { AppRoutingModule } from './app-routing.module';
import { NavigationModule } from './navigation/navigation.module';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { MembersEffects } from './members/state/members-effects.service';
import { MembersReducer } from './members/state/members-reducer';
import { AppReducer } from './core/store/app-store';
import { HomeModule } from './home/home.module';
import { ParishModule } from './diocese/parish/parish.module';
import { ParishReducer } from './diocese/parish/store/parish-reducer';
import { ParishEffects } from './diocese/parish/store/parish-effects';
import { RetreatsModule } from './retreats/retreats.module';
import { RetreatsReducer } from './retreats/store/retreat-reducer';
import { RetreatsEffects } from './retreats/store/retreat-effects';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { HttpBackend, HttpClient } from '@angular/common/http';
import { CommunitiesReducer } from './diocese/community/store/community-reducer';
import { CommunityEffects } from './diocese/community/store/community-effects';
import { CommunityModule } from './diocese/community/community.module';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { CoreModule } from './core/core.module';
import { UsersModule } from './users/users.module';
import { UsersEffects } from './users/store/users-effects';
import { UsersReducer } from './users/store/users-reduces';
import { MultiTranslateHttpLoader } from 'ngx-translate-multi-http-loader';
import { RegistrationModule } from './registration/registration.module';
import { VerificationModule } from './verification/verification.module';
import { ModerationModule } from './moderation/moderation.module';
import { OpinionsModule } from './opinions/opinions.module';
import { VerificationReducer } from './verification/state/verification-reducer';
import { VerificationEffects } from './verification/state/verification-effects';

export function HttpLoaderFactory(http: HttpBackend) {
  return new MultiTranslateHttpLoader(http, [
    { prefix: './assets/i18n/core/' },
    { prefix: './assets/i18n/shared/' },
    { prefix: './assets/i18n/community/' },
    { prefix: './assets/i18n/home/' },
    { prefix: './assets/i18n/members/' },
    { prefix: './assets/i18n/parish/' },
    { prefix: './assets/i18n/retreats/' },
    { prefix: './assets/i18n/moderation/' },
    { prefix: './assets/i18n/opinions/' },
    { prefix: './assets/i18n/registration/' },
    { prefix: './assets/i18n/verification/' },
    { prefix: './assets/i18n/users/' },
  ]);
}

@NgModule({
  declarations: [AppComponent],
  imports: [
    AppRoutingModule,
    CoreModule,
    NavigationModule,
    MembersModule,
    // HomeModule,
    ParishModule,
    RetreatsModule,
    CommunityModule,
    UsersModule,
    RegistrationModule,
    VerificationModule,
    ModerationModule,
    OpinionsModule,
    /**
     * Store and effects imports
     */
    StoreModule.forRoot(AppReducer.reducer),
    StoreModule.forFeature('membersModule', MembersReducer.reducer),
    StoreModule.forFeature('verificationModule', VerificationReducer.reducer),
    StoreModule.forFeature('parishModule', ParishReducer.reducer),
    StoreModule.forFeature('communitiesModule', CommunitiesReducer.reducer),
    StoreModule.forFeature('retreatsModule', RetreatsReducer.reducer),
    StoreModule.forFeature('usersModule', UsersReducer.reducer),
    EffectsModule.forRoot([
      MembersEffects,
      VerificationEffects,
      ParishEffects,
      CommunityEffects,
      RetreatsEffects,
      UsersEffects,
    ]),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpBackend],
      },
    }),
  ],
  providers: [{ provide: MAT_DATE_LOCALE, useValue: 'pl' }],
  bootstrap: [AppComponent],
})
export class AppModule {}
