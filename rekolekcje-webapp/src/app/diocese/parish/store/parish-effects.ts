import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { Action, Store } from '@ngrx/store';
import { Parishes } from './parish-reducer';
import { ParishSharedActions } from './parish-actions';
import { catchError, map, switchMap } from 'rxjs/operators';
import { Parish } from '../models/parish.model';
import { ParishService } from '../parish.service';
import CreateParishSuccess = ParishSharedActions.CreateParishSuccess;
import CreateParishFail = ParishSharedActions.CreateParishFail;
import LoadParishListSuccess = ParishSharedActions.LoadParishListSuccess;
import LoadParishListFail = ParishSharedActions.LoadParishListFail;
import DeleteParishSuccess = ParishSharedActions.DeleteParishSuccess;
import DeleteParishFail = ParishSharedActions.DeleteParishFail;
import UpdateParishSuccess = ParishSharedActions.UpdateParishSuccess;
import UpdateParishFail = ParishSharedActions.UpdateParishFail;
import SelectParishSuccess = ParishSharedActions.SelectParishSuccess;

@Injectable()
export class ParishEffects {
  constructor(
    private actions: Actions,
    private store: Store<Parishes.State>,
    private service: ParishService
  ) {}

  @Effect()
  CreateParish: Observable<Action> = this.actions.pipe(
    ofType(ParishSharedActions.types.CreateParish),
    switchMap((action: ParishSharedActions.CreateParish) =>
      this.service.addNew(action.payload).pipe(
        map(data => new CreateParishSuccess(data)),
        catchError(error => of(new CreateParishFail(error)))
      )
    )
  );

  @Effect()
  LoadParishList: Observable<Action> = this.actions.pipe(
    ofType(ParishSharedActions.types.LoadParishList),
    switchMap((action: ParishSharedActions.LoadParishList) =>
      this.service.getAll().pipe(
        map((data: Parish[]) => new LoadParishListSuccess(data)),
        catchError(error => of(new LoadParishListFail(error)))
      )
    )
  );

  @Effect()
  DeleteParish: Observable<Action> = this.actions.pipe(
    ofType(ParishSharedActions.types.DeleteParish),
    switchMap((action: ParishSharedActions.DeleteParish) =>
      this.service.deleteOne(action.payload).pipe(
        map(data => new DeleteParishSuccess(action.payload)),
        catchError(error => of(new DeleteParishFail(error)))
      )
    )
  );

  @Effect()
  UpdateParish: Observable<Action> = this.actions.pipe(
    ofType(ParishSharedActions.types.UpdateParish),
    switchMap((action: ParishSharedActions.UpdateParish) =>
      this.service.update(action.payload).pipe(
        map(data => new UpdateParishSuccess(data)),
        catchError(error => of(new UpdateParishFail(error)))
      )
    )
  );

  @Effect()
  SelectParish: Observable<Action> = this.actions.pipe(
    ofType(ParishSharedActions.types.SelectParish),
    switchMap((action: ParishSharedActions.SelectParish) =>
      of(new SelectParishSuccess(action.payload))
    )
  );
}
