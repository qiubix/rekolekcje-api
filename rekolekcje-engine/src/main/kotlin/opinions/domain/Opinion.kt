package pl.oaza.waw.rekolekcje.api.opinions.domain

import pl.oaza.waw.rekolekcje.api.core.ddd.AggregateRoot
import pl.oaza.waw.rekolekcje.api.shared.domain.EntityWithId
import java.time.LocalDate
import javax.persistence.*

@AggregateRoot
@Entity
@Table(name = "OPINIONS")
data class Opinion(
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private val id: Long? = null,
    val memberId: Long,
    val content: String,
    val createdDate: LocalDate,
    @Embedded
    val submitter: Submitter
) : EntityWithId {
  override fun withId(id: Long) = copy(id = id)

  override fun getId() = id
}