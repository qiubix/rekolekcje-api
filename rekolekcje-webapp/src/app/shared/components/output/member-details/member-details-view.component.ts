import { Component, Input } from '@angular/core';
import { MemberDetails } from '@shared/models/member-details.model';

@Component({
  selector: 'reko-member-details-view',
  templateUrl: './member-details-view.component.html',
  styleUrls: ['./member-details-view.component.scss']
})
export class MemberDetailsViewComponent {
  @Input() member: MemberDetails;
}
