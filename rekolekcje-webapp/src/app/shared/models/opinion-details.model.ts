import { ContactInfo } from './';

export interface OpinionDetails {
  id: number;
  content: string;
  created: string;
  submitter: ContactInfo;
}
