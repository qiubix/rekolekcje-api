import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Community } from '../../../models/community.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'reko-community-form',
  templateUrl: './community-form.component.html',
  styleUrls: ['./community-form.component.scss']
})
export class CommunityFormComponent implements OnInit {
  @Input() community: Community;

  @Output() formOutput: EventEmitter<Community> = new EventEmitter<Community>();

  form: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.form = this.fb.group({
      name: [this.community ? this.community.name : '', Validators.required],
      address: [this.community ? this.community.address : '']
    });
  }

  cancel(): void {
    this.formOutput.emit();
  }

  submit(): void {
    if (this.form.invalid) {
      this.form.markAsDirty();
      return;
    }
    this.formOutput.emit(this.form.value);
  }
}
