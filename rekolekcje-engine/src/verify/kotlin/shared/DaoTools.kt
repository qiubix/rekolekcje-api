package pl.oaza.waw.rekolekcje.api.shared

import java.sql.ResultSet
import java.sql.SQLException
import java.time.LocalDate

object DaoTools {

  @Throws(SQLException::class)
  fun getLong(resultSet: ResultSet, columnName: String): Long? {
    val value = resultSet.getLong(columnName)
    return if (resultSet.wasNull()) null else value
  }

  @Throws(SQLException::class)
  fun getInt(resultSet: ResultSet, columnName: String): Int? {
    val value = resultSet.getInt(columnName)
    return if (resultSet.wasNull()) null else value
  }

  @Throws(SQLException::class)
  fun getLocalDate(resultSet: ResultSet, columnName: String): LocalDate? {
    val date = resultSet.getDate(columnName)
    return date?.toLocalDate()
  }
}
