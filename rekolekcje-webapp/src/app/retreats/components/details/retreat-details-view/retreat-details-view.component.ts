import { Component, EventEmitter, Input, Output } from '@angular/core';
import { RetreatDetails, RetreatMember } from '../../../models/retreat-details.model';

@Component({
  selector: 'reko-retreat-details-view',
  templateUrl: './retreat-details-view.component.html',
  styleUrls: ['./retreat-details-view.component.scss']
})
export class RetreatDetailsViewComponent {
  @Input() retreatLoading: boolean = false;
  @Input() retreatData: RetreatDetails;
  @Input() allParticipants: RetreatMember[] = [];
  @Input() allAnimators: RetreatMember[] = [];

  @Output() goBack = new EventEmitter<void>();
  @Output() edit = new EventEmitter<RetreatDetails>();
  @Output() delete = new EventEmitter<void>();

  onGoBack() {
    this.goBack.emit();
  }

  onEdit() {
    this.edit.emit(this.retreatData);
  }

  onDelete() {
    this.delete.emit();
  }
}
