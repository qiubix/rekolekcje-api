package pl.oaza.waw.rekolekcje.api.members.domain

import java.time.LocalDate
import javax.persistence.Embeddable
import javax.persistence.EnumType
import javax.persistence.Enumerated

@Embeddable
data class Kwc(
  val kwcSince: LocalDate? = null,
  @Enumerated(EnumType.STRING)
  val kwcStatus: KwcStatus? = null
)

enum class KwcStatus {
  CANDIDATE,
  MEMBER,
  NOT_ENGAGED
}
