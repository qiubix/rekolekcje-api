package pl.oaza.waw.rekolekcje.api.members.domain

import pl.oaza.waw.rekolekcje.api.shared.domain.Contact
import javax.persistence.CascadeType
import javax.persistence.Column
import javax.persistence.Embeddable
import javax.persistence.Embedded
import javax.persistence.FetchType
import javax.persistence.JoinColumn
import javax.persistence.OneToMany
import javax.persistence.OneToOne

@Embeddable
data class Experience(
  @Embedded
  val kwc: Kwc? = null,
  val numberOfCommunionDays: Int? = null,
  val numberOfPrayerRetreats: Int? = null,
  val formationInSmallGroup: Boolean? = null,
  val formationMeetingsInMonth: Int? = null,
  val leadingGroupToFormationStage: String? = null,
  @Embedded
  val deuterocatechumenate: Deuterocatechumenate? = null,
  @Column(name = "NUMBER_OF_ODB")
  val numberOfODB: Int? = null,
  @Column(name = "NUMBER_OF_OND")
  val numberOfOND: Int? = null,
  val numberOfRetreatsWithParents: Int? = null,
  val choir: Boolean? = null,
  val musicalTalents: String? = null,
  val altarBoy: Boolean? = null,
  val flowerGirl: Boolean? = null,
  val wasAnimatorBefore: Boolean? = null,
  val otherSkills: String? = null,
  val animatorsSchool: Boolean? = null,
  val animatorsBlessing: Boolean? = null,
  val koda: Boolean? = null,
  val numberOfAnimatorDays: Int? = null,
  val diakonia: String? = null,
  @OneToOne(cascade = [CascadeType.ALL], orphanRemoval = true)
  val animator: Contact? = null,
  @OneToMany(cascade = [CascadeType.ALL], orphanRemoval = true, fetch = FetchType.LAZY)
  @JoinColumn(name = "member_id")
  val historicalRetreats: List<HistoricalRetreat>? = emptyList(),
  val animatorsExperience: String? = null,
  val otherRetreats: String? = null,
  val communityFunctions: String? = null
) {
  fun getRetreatsAsAnimator() = animatorsExperience?.split(",")?.toSet() ?: emptySet()
  fun getOtherRetreats() = otherRetreats?.split(",")?.map { OtherRetreats.valueOf(it) }?.toSet() ?: emptySet()
  fun getOtherSkills() = otherSkills?.split(",")?.map { OtherSkills.valueOf(it) }?.toSet() ?: emptySet()
}

enum class OtherSkills {
  DRIVERS_LICENCE,
  SANEPID_BOOK,
  PLAYS_INSTRUMENT,
  SINGS
}

enum class OtherRetreats {
  KODA,
  ANIMATORS_SCHOOL,
  ANIMATORS_SCHOOL_IN_PROGRESS,
  ORAE,
  ORD,
  KAMuzO,
  MUSIC_ANIMATOR_SCHOOL
}