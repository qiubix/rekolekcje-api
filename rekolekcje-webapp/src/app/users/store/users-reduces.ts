import { UserSummary } from '../model/user-summary.model';
import { UsersSharedActions } from './users-actions';

export namespace Users {
  export interface State {
    usersList: UserSummary[];
    usersLoading: boolean;
    currentUser: UserSummary;
    notification: string;
  }

  export const initialState: Users.State = {
    usersList: [],
    usersLoading: false,
    currentUser: null,
    notification: null
  };
}

export namespace UsersReducer {
  export function reducer(state: Users.State = Users.initialState, action): Users.State {
    switch (action.type) {
      case UsersSharedActions.types.LoadUsersList: {
        return {
          ...state,
          usersLoading: true
        };
      }

      case UsersSharedActions.types.LoadUsersListFail: {
        return {
          ...state,
          usersLoading: false
        };
      }

      case UsersSharedActions.types.LoadUsersListSuccess: {
        return {
          ...state,
          usersList: action.payload,
          usersLoading: false
        };
      }

      case UsersSharedActions.types.CreateUserSuccess: {
        const users: UserSummary[] = [...state.usersList];
        users.push(action.payload as UserSummary);
        return {
          ...state,
          usersList: users
        };
      }

      case UsersSharedActions.types.UpdateUserSuccess: {
        return {
          ...state,
          usersList: state.usersList.map((user: UserSummary) =>
            user.username === action.payload.username ? action.payload : user
          )
        };
      }

      case UsersSharedActions.types.DeleteUserSuccess: {
        return {
          ...state,
          usersList: state.usersList.filter((user: UserSummary) => user.username !== action.payload)
        };
      }

      case UsersSharedActions.types.ChangePasswordSuccess:
      case UsersSharedActions.types.ChangePasswordFail: {
        return {
          ...state,
          notification: action.payload
        };
      }

      case UsersSharedActions.types.SelectUserSuccess: {
        return {
          ...state,
          currentUser: action.payload
        };
      }

      case UsersSharedActions.types.SelectUserFail: {
        return {
          ...state,
          currentUser: null
        };
      }

      default:
        return state;
    }
  }
}
