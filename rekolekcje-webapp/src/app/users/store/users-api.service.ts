import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserSummary } from '../model/user-summary.model';
import { Config } from '../../../config/config';
import { SaveUserRequest } from '../model/save-user-request.model';
import { ChangePasswordRequest } from '../model/change-password-request.model';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class UsersApiService {
  constructor(private http: HttpClient) {}

  public getAll(): Observable<UserSummary[]> {
    return this.http.get<UserSummary[]>(Config.endpoints.usersModule);
  }

  public createOne(signUpRequest: SaveUserRequest): Observable<UserSummary> {
    return this.http.post<UserSummary>(`${Config.endpoints.usersModule}/sign-up`, signUpRequest);
  }

  public updateOne(saveUserRequest: SaveUserRequest): Observable<UserSummary> {
    return this.http.put<UserSummary>(`${Config.endpoints.usersModule}`, saveUserRequest);
  }

  public deleteOne(username: string): Observable<UserSummary> {
    return this.http.delete<UserSummary>(`${Config.endpoints.usersModule}/${username}`);
  }

  public changePassword(request: ChangePasswordRequest): Observable<string> {
    return this.http
      .put<UserSummary>(`${Config.endpoints.usersModule}/password`, request)
      .pipe(
        map(
          (userSummary: UserSummary) =>
            'Hasło zostało zmienione dla użytkownika ' + userSummary.username
        )
      );
  }

  public getOne(username: string): Observable<UserSummary> {
    return this.http.get<UserSummary>(`${Config.endpoints.usersModule}/${username}`);
  }
}
