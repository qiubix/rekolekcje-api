package pl.oaza.waw.rekolekcje.api.application.domain

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.Test
import pl.oaza.waw.rekolekcje.api.application.ApplicationTestFixture
import pl.oaza.waw.rekolekcje.api.application.animatorApplication
import pl.oaza.waw.rekolekcje.api.application.participantApplication

internal class AssignAnimatorTest : ApplicationTestFixture() {

  @Test
  fun `should assign animator to retreat`() {
    val applicationId = persistApplication(animatorApplication())
    val retreatId = 23L
    val selectedRole = AnimatorRole.MUSICIAN

    val request = AssignAnimator(applicationId, retreatId, selectedRole)
    commandFacade.assignAnimator(request)

    val persistedApplication = findPersistedApplication(applicationId)
    assertThat(persistedApplication.retreatId).isEqualTo(retreatId)
    assertThat(persistedApplication.selectedRole).isEqualTo(AnimatorRole.MUSICIAN)
  }

  @Test
  fun `should not allow participant assignment`() {
    val applicationId = persistApplication(participantApplication())

    assertThatExceptionOfType(IllegalArgumentException::class.java)
        .isThrownBy { commandFacade.assignAnimator(AssignAnimator(applicationId, 12L, AnimatorRole.LITURGIST)) }
  }

  @Test
  fun `should throw exception when trying to assign non-existing application`() {
    val nonExistingApplicationId = 321L

    assertThatExceptionOfType(IllegalArgumentException::class.java)
        .isThrownBy { commandFacade.assignAnimator(AssignAnimator(nonExistingApplicationId, 12L, AnimatorRole.LITURGIST)) }
  }
}