export interface JwtUser {
  username: string;
  token: string;
}
