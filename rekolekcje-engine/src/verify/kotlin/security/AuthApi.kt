package pl.oaza.waw.rekolekcje.api.security

import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import pl.oaza.waw.rekolekcje.api.shared.Response
import pl.oaza.waw.rekolekcje.api.shared.RestApi
import pl.oaza.waw.rekolekcje.api.user.endpoint.AuthenticationRequest
import pl.oaza.waw.rekolekcje.api.user.endpoint.SignUpRequest
import pl.oaza.waw.rekolekcje.api.user.endpoint.UserEndpoint.Companion.REGISTER_URI
import pl.oaza.waw.rekolekcje.api.user.endpoint.UserEndpoint.Companion.USERS_URI

internal class AuthApi(private val restApi: RestApi) {

  private val signUpUri = USERS_URI + REGISTER_URI
  private val loginUri = AUTH_URI + SIGN_IN_URI

  fun registerNewUserAsAdmin(username: String, password: String): Response {
    val signUpPayload = SignUpRequest(username, password, "sample@mail.com")
    val response = restApi.performPost(signUpUri, signUpPayload, user("admin").roles("ADMIN"))
    response.andExpect(status().isOk)
    return response
  }

  fun getTokenForCredentials(username: String, password: String): String {
    val authenticationResponse = requestAuthenticationToken(username, password)
    return getTokenFromResponse(authenticationResponse)
  }

  fun requestAuthenticationToken(username: String, password: String): Response {
    val loginPayload = AuthenticationRequest(username, password)
    return restApi.performPost(loginUri, loginPayload)
  }

  fun getTokenFromResponse(response: Response): String {
    val content = response.getContent<AuthenticationResponse>()
    return content.token
  }
}
