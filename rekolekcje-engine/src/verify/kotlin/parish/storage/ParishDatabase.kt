package pl.oaza.waw.rekolekcje.api.parish.storage

import org.springframework.jdbc.core.JdbcTemplate

class ParishDatabase(private val jdbcTemplate: JdbcTemplate) {

  fun clear() {
    jdbcTemplate.execute("DELETE FROM parishes")
  }
}
