package pl.oaza.waw.rekolekcje.api.parish.dto

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.NOT_FOUND)
class ParishNotFoundException : RuntimeException {

  constructor(id: Long) : super("No parish with id $id found!")

  constructor(data: ParishPayload) : super("No parish found with this data: $data")
}

@ResponseStatus(HttpStatus.NOT_FOUND)
class RegionNotFoundException : RuntimeException {

  constructor(id: Long) : super("No region with id $id found!")

  constructor(data: RegionPayload) : super("No region found with this data: $data")
}
