import { Stage } from 'app/shared/models/stage.model';
import { Turns } from 'app/shared/models/turn.model';

export interface RetreatSummary {
  id?: number;
  status?: RetreatStatus;
  turn?: Turns;
  stage?: Stage;
  localization?: string;
  startDate?: string;
  endDate?: string;
  participantsAmount?: number;
  malesParticipants?: number;
  femalesParticipants?: number;
  averageAge?: number;
  underageParticipants?: number;
}

export enum RetreatStatus {
  ACTIVE = 'ACTIVE',
  FUTURE = 'FUTURE',
  HISTORICAL = 'HISTORICAL'
}
