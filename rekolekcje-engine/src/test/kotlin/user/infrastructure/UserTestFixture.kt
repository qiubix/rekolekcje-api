package pl.oaza.waw.rekolekcje.api.user.infrastructure

import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.BeforeEach
import org.springframework.security.crypto.password.PasswordEncoder
import pl.oaza.waw.rekolekcje.api.user.endpoint.SignUpRequest
import pl.oaza.waw.rekolekcje.api.user.endpoint.UserRole


abstract class UserTestFixture {

  private val userWriteRepository = InMemoryUserWriteRepository()
  private val userReadRepository = InMemoryUserReadRepository(userWriteRepository.storage)

  private val userConfiguration = UserConfiguration()

  protected val passwordEncoder = userConfiguration.passwordEncoder()
  protected val passwordEncoderMock = mockk<PasswordEncoder>()

  protected val userUpdateService =
    userConfiguration.userUpdateService(userReadRepository, userWriteRepository, passwordEncoderMock)
  protected val userRegisterService =
    userConfiguration.userRegisterService(userReadRepository, userWriteRepository, passwordEncoderMock)
  protected val userAuthenticationService =
    userConfiguration.userAuthenticationService(userReadRepository, passwordEncoderMock)

  protected val userQueryService = userConfiguration.userQueryService(userReadRepository)

  protected val username = "testUser"
  protected val password = "test-password"
  protected val email = "registration@email.com"
  protected val signUpRequest = SignUpRequest(
    username = username,
    password = password,
    email = email
  )
  protected val encodedPassword = "encoded-password"

  @BeforeEach
  fun setup() {
    every { passwordEncoderMock.encode(password) } returns encodedPassword
    every { passwordEncoderMock.matches(password, encodedPassword) } returns true
    clearUsers()
  }

  private fun clearUsers() {
    userWriteRepository.storage.clear()
  }

  protected fun findPasswordForUser(username: String): String {
    val user = userReadRepository.findByUsername(username) ?: throw IllegalStateException("User should be present")
    return user.password
  }

  fun SignUpRequest.withRoles(roles: Set<UserRole>) = this.copy(roles = roles)

}
