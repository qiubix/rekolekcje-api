package pl.oaza.waw.rekolekcje.api.opinions.infrastructure

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import pl.oaza.waw.rekolekcje.api.members.infrastructure.encryption.MembersEncryptionService
import pl.oaza.waw.rekolekcje.api.members.query.MembersQueryFacade
import pl.oaza.waw.rekolekcje.api.opinions.domain.*
import java.time.Clock

@Configuration
internal class OpinionsConfiguration {

  @Bean
  fun clock(): Clock = Clock.systemUTC()

  @Bean
  fun opinionsCommandFacade(
      opinionsWriteRepository: OpinionsWriteRepository,
      clock: Clock,
      membersQueryFacade: MembersQueryFacade,
      membersEncryptionService: MembersEncryptionService
  ): OpinionsCommandFacadeImpl {
    val opinionCreator = OpinionsCreator(clock)

    return OpinionsCommandFacadeImpl(
        writeRepository = opinionsWriteRepository,
        opinionsCreator = opinionCreator,
        membersQueryFacade = membersQueryFacade,
        membersEncryptionService = membersEncryptionService
    )
  }

  @Bean
  fun opinionsQueryFacade(
      readRepository: OpinionsReadRepository
  ) = OpinionsQueryFacadeImpl(readRepository)
}
