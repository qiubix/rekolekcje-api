import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { Subject } from 'rxjs';
import { RetreatSummary } from '@retreats/models/retreat-summary.model';
import { Stage } from '@shared/models';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'reko-shared-retreats-table',
  templateUrl: './shared-retreats-table.component.html',
  styleUrls: ['./shared-retreats-table.component.scss'],
})
export class SharedRetreatsTableComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy {
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  @Input() retreats: RetreatSummary[];
  @Input() displayedColumns: string[];
  @Input() details: boolean;
  @Input() editable: boolean;
  @Input() registration: boolean;

  @Output() deleteRetreat: EventEmitter<number> = new EventEmitter<number>();
  @Output() editRetreat: EventEmitter<number> = new EventEmitter<number>();
  @Output() selectRetreat: EventEmitter<RetreatSummary> = new EventEmitter<RetreatSummary>();
  @Output() archiveRetreat: EventEmitter<number> = new EventEmitter<number>();
  @Output() registerOnRetreat: EventEmitter<number> = new EventEmitter<number>();

  dataSource: MatTableDataSource<RetreatSummary>;
  stages = Stage;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor() {}

  ngOnInit() {}

  ngOnChanges(): void {
    this.dataSource = new MatTableDataSource<RetreatSummary>(this.retreats);
  }

  // @TODO refactor this to set input
  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  onDelete(id: number) {
    this.deleteRetreat.emit(id);
  }

  onEdit(id: number) {
    this.editRetreat.emit(id);
  }

  onSelect(retreat: RetreatSummary) {
    this.selectRetreat.emit(retreat);
  }

  onArchive(id: number) {
    this.archiveRetreat.emit(id);
  }

  onRegister(id: number) {
    this.registerOnRetreat.emit(id);
  }
}
