import { Pipe, PipeTransform } from '@angular/core';
import { MemberSummary } from '@members/models';

@Pipe({
  name: 'memberFullName'
})
export class MemberFullNamePipe implements PipeTransform {
  transform(member: MemberSummary, args?: any): string {
    return member ? member.firstName + ' ' + member.lastName : '';
  }
}
