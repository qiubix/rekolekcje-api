import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs/internal/Observable';
import { Community } from '@diocese/community/models/community.model';
import { select, Store } from '@ngrx/store';
import { Communities } from '@diocese/community/store/community-reducer';
import { AppSelectors } from '@core/store/app-selectors';
import { filter, take, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'reko-community-picker',
  templateUrl: './community-picker.component.html',
  styleUrls: ['./community-picker.component.scss']
})
export class CommunityPickerComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  @Input() value: number;
  @Input() inputControl: FormControl;

  communities$: Observable<Community[]>;

  constructor(private _store: Store<Communities.State>) {
    this.communities$ = this._store.pipe(
      select(AppSelectors.getCommunityList),
      takeUntil(this.ngUnsubscribe),
      // tap(list => {
      //   console.log('current communities: ', list);
      // }),
      filter(list => list.length > 0),
      take(1)
    );
  }

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }
}
