import { MembersComponent } from './_root/members.component';
import { MemberAddEditComponent } from './add-edit/add-edit-page/member-add-edit.component';
import { MemberFormComponent } from './add-edit/detailed-form/member-form.component';
import { MemberDetailsComponent } from './details/member-details.component';
import { MembersListComponent } from './list/members-list.component';

export const components: any[] = [
  MembersComponent,
  MemberAddEditComponent,
  MemberFormComponent,
  MemberDetailsComponent,
  MembersListComponent
];

export * from './_root/members.component';
