package pl.oaza.waw.rekolekcje.api.members.api

import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user
import pl.oaza.waw.rekolekcje.api.members.endpoint.MemberDetails
import pl.oaza.waw.rekolekcje.api.members.endpoint.MemberSummary
import pl.oaza.waw.rekolekcje.api.members.endpoint.MembersEndpoint.Companion.ADD_MANY_URI
import pl.oaza.waw.rekolekcje.api.members.endpoint.MembersEndpoint.Companion.FULL_NAME_ENCRYPTED_URI
import pl.oaza.waw.rekolekcje.api.members.endpoint.MembersEndpoint.Companion.MEMBERS_URI
import pl.oaza.waw.rekolekcje.api.members.query.OpinionLinkQueryFacade
import pl.oaza.waw.rekolekcje.api.shared.Response
import pl.oaza.waw.rekolekcje.api.shared.RestApi

class MembersApiBehaviour(
  private val restApi: RestApi,
  private val opinionLinkQueryFacade: OpinionLinkQueryFacade
) {

  private val baseUri = MEMBERS_URI

  fun allAreRequested(): Response {
    return restApi.performGet(baseUri)
  }

  fun singleIsRequested(id: Long?): Response {
    return restApi.performGet("$baseUri/$id")
  }

  fun singleIsAdded(payload: MemberDetails): Response {
    return restApi.performPost(baseUri, payload)
  }

  fun singleIsDeleted(id: Long): Response {
    return restApi.performDelete("$baseUri/$id")
  }

  fun singleIsUpdated(updatedPayload: MemberDetails): Response {
    return restApi.performPut(baseUri, updatedPayload)
  }

  fun allAreAdded(payloadAsList: List<MemberDetails>) {
    for (payload in payloadAsList) {
      restApi.performPost(baseUri, payload)
    }
  }

  fun memberExists(payload: MemberDetails): MemberDetails {
    val response = singleIsAdded(payload)
    val memberId = response.getContent<MemberSummary>().id
    return singleIsRequested(memberId).getContent()
  }

  fun existsElementWithTheSameData(payload: MemberDetails): MemberDetails {
    val allSummaries = getAllSummaries()
    val matchingSummary = allSummaries
      .find { isSummaryMatchingDetails(it, payload) }
      ?: throw getNotFoundException(payload)
    val getOneRequest = singleIsRequested(matchingSummary.id)
    return getOneRequest.getContent()
  }

  fun multipleMembersAreAdded(newMembers: List<MemberDetails>): Response {
    return restApi.performPost(MEMBERS_URI + ADD_MANY_URI, newMembers)
  }

  fun findIdOfElementWithTheSameData(details: MemberDetails): Long {
    val existingMember = existsElementWithTheSameData(details)
    return existingMember.id ?: throw IllegalStateException("Cannot find element with data: $details")
  }

  private fun isSummaryMatchingDetails(summary: MemberSummary, details: MemberDetails): Boolean {
    return summary.firstName == details.personalData.firstName
        && summary.lastName == details.personalData.lastName
  }

  private fun getNotFoundException(payload: MemberDetails): RuntimeException {
    return IllegalStateException("Cannot find element with data: $payload")
  }

  private fun getAllSummaries(): List<MemberSummary> {
    val response = restApi.performGet(baseUri)
    return response.getContentAsList()
  }

  fun fileIsUploaded(fileContent: String): Response {
    return restApi.performFileUpload("$baseUri/import", fileContent)
  }

  fun allAreExported(): Response {
    return restApi.performGet("$baseUri/export")
  }

  fun singleIsAddedByAuthorizedUser(member: MemberDetails): Long {
    val response = restApi.performPost(baseUri, member, user("admin").roles("ADMIN"))
    response.hasStatusCreated()
    return response.getContent<MemberSummary>().id
  }

  fun fullNameIsRequestedAsAnonymous(encryptedMemberId: String): Response {
    return restApi.performGet("$baseUri$FULL_NAME_ENCRYPTED_URI/$encryptedMemberId")
  }

  fun encryptedMemberIdIsSentByEmail(memberId: Long): String {
    return opinionLinkQueryFacade.generateLink(memberId).split("/").last()
  }

  fun alreadyExistsMember(member: MemberDetails): Long {
    return singleIsAdded(member).getContent<MemberDetails>().id ?: throw IllegalStateException()
  }
}
