package pl.oaza.waw.rekolekcje.api.retreats.domain

import pl.oaza.waw.rekolekcje.api.core.ddd.DomainRepository

@DomainRepository
interface RetreatReadRepository {

  fun findById(id: Long): Retreat?

  fun findAll(): List<Retreat>

}

@DomainRepository
interface RetreatWriteRepository {

  fun deleteById(id: Long)

  fun save(entity: Retreat): Retreat

}