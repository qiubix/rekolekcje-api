import { Component, Input } from '@angular/core';
import { Sex } from '@shared/models/sex.model';
import { Address, PersonalData } from '@shared/models/member-details.model';

@Component({
  selector: 'personal-data',
  templateUrl: './personal-data.component.html',
  styleUrls: ['../member-details-view.component.scss', './personal-data.component.scss']
})
export class PersonalDataComponent {
  @Input() personalData: PersonalData;

  private _sex = Sex;

  constructor() {}

  // TODO: move to pipe
  formatSex(sexValue: Sex): string {
    if (!sexValue) return '';
    return this._sex[sexValue] === Sex.MALE ? 'Male' : 'Female';
  }

  get address(): Address {
    return this.personalData.address ? this.personalData.address : {};
  }
}
