import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Retreats } from '../../store/retreat-reducer';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { RetreatsSharedActions } from '../../store/retreat-actions';
import { AppSelectors } from 'app/core/store/app-selectors';
import { catchError, switchMap, tap, filter, combineLatest } from 'rxjs/operators';
import { MembersSharedActions } from '../../../members/state/members-actions';
import { ParishSharedActions } from '../../../diocese/parish/store/parish-actions';
import { RetreatDetails } from '../../models/retreat-details.model';
import { RetreatSummary } from '../../models/retreat-summary.model';

@Injectable()
export class RetreatDetailsGuard implements CanActivate {
  constructor(private store: Store<Retreats.State>) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    const retreatId = +route.params['id'];
    return this.getFromStoreOrAPI(retreatId).pipe(
      switchMap(() => of(true)),
      catchError(() => of(false))
    );
  }

  getFromStoreOrAPI(id: number): Observable<any> {
    return this.store.pipe(
      select(AppSelectors.getSelectedRetreat),
      tap((retreat: RetreatDetails) => {
        if (!retreat || retreat.id !== id) {
          this.store.dispatch(new RetreatsSharedActions.SelectRetreat(id));
        }
        this.store.dispatch(new RetreatsSharedActions.LoadRetreatsList());
      }),
      combineLatest(this.store.select(AppSelectors.getRetreatsList)),
      filter(
        ([details, retreats]) => this.detailsReady(details, id) && this.retreatsReady(retreats, id)
      )
    );
  }

  private detailsReady(details: RetreatDetails, id: number): boolean {
    return details && details.id !== null && details.id === id;
  }

  private retreatsReady(retreats: RetreatSummary[], id: number): boolean {
    return retreats && retreats.length > 0 && retreats.map(it => it.id).includes(id);
  }
}
