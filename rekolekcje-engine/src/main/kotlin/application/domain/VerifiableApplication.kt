package pl.oaza.waw.rekolekcje.api.application.domain

import pl.oaza.waw.rekolekcje.api.shared.value.PaymentStatus

interface VerifiableApplication {

  fun applicationStatus(): ApplicationStatus

  fun statusAdditionalDetails(): String?

  fun paymentStatus(): PaymentStatus

  fun approve(): VerifiableApplication = changeApplicationStatus(ApplicationStatus.VERIFIED)

  fun reject(reason: String): VerifiableApplication {
    require(reason.isNotBlank())
    return changeApplicationStatus(ApplicationStatus.REJECTED, reason)
  }

  fun register(): VerifiableApplication =
    if (isVerified()) {
      changeApplicationStatus(ApplicationStatus.REGISTERED)
    } else {
      throw IllegalStateException("Cannot enroll member that has not been verified.")
    }

  fun resetToWaiting(): VerifiableApplication = changeApplicationStatus(ApplicationStatus.WAITING)

  fun putOnWaitingList(): VerifiableApplication =
    if (isVerified()) {
      changeApplicationStatus(ApplicationStatus.WAITING_LIST)
    } else {
      throw IllegalStateException("Cannot put on waiting list member that has not been verified.")
    }

  fun resign(): VerifiableApplication = changeApplicationStatus(ApplicationStatus.RESIGNED)

  fun reportProblem(): VerifiableApplication = changeApplicationStatus(ApplicationStatus.PROBLEM)

  fun isVerified(): Boolean = applicationStatus() == ApplicationStatus.VERIFIED

  fun changeApplicationStatus(status: ApplicationStatus, additionalDetails: String? = null): VerifiableApplication

  fun changePaymentStatus(newStatus: PaymentStatus): VerifiableApplication
}