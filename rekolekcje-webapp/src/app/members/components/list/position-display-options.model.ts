export enum PositionDisplayOption {
  ALL_MEMBERS = 'ALL_MEMBERS',
  ONLY_PARTICIPANTS = 'ONLY_PARTICIPANTS',
  ONLY_ANIMATORS = 'ONLY_ANIMATORS'
}
