import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Opinion, OpinionSubmittedResponse } from '@opinions/model';
import { Observable } from 'rxjs/internal/Observable';
import { Config } from '@config';

@Injectable({ providedIn: 'root' })
export class OpinionApiService {
  constructor(private http: HttpClient) {}

  submitOne(opinion: Opinion): Observable<OpinionSubmittedResponse> {
    return this.http.post<OpinionSubmittedResponse>(
      Config.endpoints.opinionsModule + '/submit',
      opinion
    );
  }
}
