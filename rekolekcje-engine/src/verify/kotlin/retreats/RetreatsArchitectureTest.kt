package pl.oaza.waw.rekolekcje.api.retreats

import com.tngtech.archunit.core.domain.JavaClasses
import com.tngtech.archunit.junit.AnalyzeClasses
import com.tngtech.archunit.junit.ArchTest
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition

@AnalyzeClasses(packages = ["pl.oaza.waw.rekolekcje.api.retreats"])
internal class RetreatsArchitectureTest {

  @ArchTest
  fun `domain package should not depend on infrastructure`(importedClasses: JavaClasses) {
    val rule = ArchRuleDefinition
      .noClasses().that().resideInAPackage("..domain..")
      .should().dependOnClassesThat()
      .resideInAPackage("..infrastructure..")
    rule.check(importedClasses)
  }

  @ArchTest
  fun `domain should not depend on adapters`(importedClasses: JavaClasses) {
    val rule = ArchRuleDefinition
      .noClasses().that().resideInAPackage("..domain..")
      .should().dependOnClassesThat()
      .resideInAPackage("..endpoint..")
    rule.check(importedClasses)
  }

  @ArchTest
  fun `domain should not depend on Spring framework`(importedClasses: JavaClasses) {
    val rule = ArchRuleDefinition
      .noClasses().that().resideInAPackage("..domain..")
      .should().dependOnClassesThat()
      .resideInAPackage("org.springframework..")
    rule.check(importedClasses)
  }

}