import { TestBed, async, inject } from '@angular/core/testing';

import { HasCorrectRoleGuard } from './has-correct-role.guard';

describe('HasCorrectRoleGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HasCorrectRoleGuard]
    });
  });

  it('should ...', inject([HasCorrectRoleGuard], (guard: HasCorrectRoleGuard) => {
    expect(guard).toBeTruthy();
  }));
});
