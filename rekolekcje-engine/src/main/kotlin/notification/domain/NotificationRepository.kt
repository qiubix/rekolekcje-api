package pl.oaza.waw.rekolekcje.api.notification.domain

import org.springframework.data.repository.Repository
import pl.oaza.waw.rekolekcje.api.core.ddd.DomainRepository
import pl.oaza.waw.rekolekcje.api.notification.dto.NotificationStatus

@DomainRepository
internal interface NotificationRepository : Repository<Notification, Long> {

  fun findById(id: Long): Notification?

  fun findAll(): List<Notification>

  fun findByStatus(status: NotificationStatus): List<Notification>

  fun save(notification: Notification): Notification

  fun deleteById(id: Long)

  fun deleteByApplicationId(applicationId: Long)
}