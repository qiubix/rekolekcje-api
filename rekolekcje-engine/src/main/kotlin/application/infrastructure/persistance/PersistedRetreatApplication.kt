package pl.oaza.waw.rekolekcje.api.application.infrastructure.persistance

import pl.oaza.waw.rekolekcje.api.application.domain.AnimatorRole
import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationStatus
import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationType
import pl.oaza.waw.rekolekcje.api.application.domain.RetreatApplication
import pl.oaza.waw.rekolekcje.api.shared.domain.Contact
import pl.oaza.waw.rekolekcje.api.shared.value.PaymentStatus
import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name = "RETREAT_APPLICATIONS")
data class PersistedRetreatApplication(
  @Id
  @GeneratedValue
  val id: Long? = null,

  @Enumerated(EnumType.STRING)
  val applicationType: ApplicationType,

  val memberId: Long,
  val firstName: String,
  val lastName: String,

  @Enumerated(EnumType.STRING)
  val status: ApplicationStatus? = null,

  @Enumerated(EnumType.STRING)
  val paymentStatus: PaymentStatus? = null,

  val retreatId: Long? = null,
  @Enumerated(EnumType.STRING)
  val selectedRole: AnimatorRole? = null,

  val animatorRoles: String? = null,
  val availability: String? = null,

  @OneToOne(cascade = [CascadeType.ALL], fetch = FetchType.LAZY, orphanRemoval = true)
  @JoinColumn(name = "surety_id")
  val surety: Contact? = null
) {
  constructor(domainEntity: RetreatApplication) : this(
    id = domainEntity.getId(),
    applicationType = domainEntity.applicationType,
    memberId = domainEntity.memberId,
    firstName = domainEntity.firstName,
    lastName = domainEntity.lastName,
    status = domainEntity.status,
    paymentStatus = domainEntity.paymentStatus,
    retreatId = domainEntity.retreatId,
    selectedRole = domainEntity.selectedRole,
    surety = domainEntity.surety,
    animatorRoles = domainEntity.animatorRoles?.joinToString(","),
    availability = domainEntity.availability?.joinToString(",")
  )

  fun toDomainEntity(): RetreatApplication = RetreatApplication(
    id = id,
    applicationType = applicationType,
    memberId = memberId,
    firstName = firstName,
    lastName = lastName,
    status = status ?: ApplicationStatus.WAITING,
    paymentStatus = paymentStatus ?: PaymentStatus.NOTHING,
    retreatId = retreatId,
    selectedRole = selectedRole,
    animatorRoles = extractAnimatorRoles(),
    availability = extractAvailability(),
    surety = surety
  )

  private fun extractAnimatorRoles(): Set<AnimatorRole>? =
    if (animatorRoles.isNullOrBlank()) null
    else animatorRoles.split(",").map { AnimatorRole.valueOf(it) }.toHashSet()

  private fun extractAvailability(): Set<Long>? =
    if (availability.isNullOrBlank()) null else availability.split(",").map { it.toLong() }.toSet()
}