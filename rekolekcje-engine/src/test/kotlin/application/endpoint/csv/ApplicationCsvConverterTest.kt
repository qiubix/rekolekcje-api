package pl.oaza.waw.rekolekcje.api.application.endpoint.csv

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import pl.oaza.waw.rekolekcje.api.application.domain.AnimatorRole
import pl.oaza.waw.rekolekcje.api.members.endpoint.csv.convertMembersFromCsv
import pl.oaza.waw.rekolekcje.api.shared.endpoint.ContactDto
import java.nio.file.Files
import java.nio.file.Paths

internal class ApplicationCsvConverterTest {

  data class TestCase(
    val name: String,
    val fileName: String,
    val expectedApplication: ConvertedApplicationData
  )

  private fun dataSource() = listOf(
    TestCase("Animator", "Animatorzy",
      ConvertedApplicationData(
        availability = setOf(
          "Turnus I, KODA",
          "Turnus II, OND",
          "Turnus II, ONZ_II",
          "Turnus III, ONZ_I"
        ),
        animatorRoles = setOf(
          AnimatorRole.GROUP,
          AnimatorRole.QUARTERMASTER,
          AnimatorRole.LITURGIST
        ),
        surety = ContactDto(
          fullName = "Maria Nowak",
          email = "123asd312@gmail.com"
        )
      )
    ),
    TestCase("Participant", "ONZ-pełnoletni",
      ConvertedApplicationData(
        selectedRetreat = "Turnus II, ONZ_II",
        surety = ContactDto(
          fullName = "Gandalf Szary",
          email = "michal.poweski@gmail.com"
        )
      )
    )
  )

  @TestFactory
  fun `should import application data`() = dataSource().map { testCase ->
    dynamicTest(testCase.name) {
      // given
      val path = Paths.get("src/test/resources/${testCase.fileName}.csv")
      val content = String(Files.readAllBytes(path), Charsets.UTF_8)

      // when
      val conversionResult = convertApplicationFromCsv(content)

      // then
      val convertedApplication = conversionResult.convertedApplications[0]
      assertThat(convertedApplication)
        .usingRecursiveComparison()
        .ignoringFields("member")
        .isEqualTo(testCase.expectedApplication)
    }
  }

  @Test
  fun `should import animator's application with member`() {
    // given
    val path = Paths.get("src/test/resources/Animatorzy.csv")
    val content = String(Files.readAllBytes(path), Charsets.UTF_8)

    // when
    val conversionResult = convertApplicationFromCsv(content)

    // then
    val expectedConvertedMember = convertMembersFromCsv(content).convertedMembers[0]
    val expectedConvertedApplication = ConvertedApplicationData(
      availability = setOf(
        "Turnus I, KODA",
        "Turnus II, OND",
        "Turnus II, ONZ_II",
        "Turnus III, ONZ_I"
      ),
      animatorRoles = setOf(
        AnimatorRole.GROUP,
        AnimatorRole.QUARTERMASTER,
        AnimatorRole.LITURGIST
      ),
      surety = ContactDto(
        fullName = "Maria Nowak",
        email = "123asd312@gmail.com"
      ),
      member = expectedConvertedMember
    )
    val convertedApplication = conversionResult.convertedApplications[0]
    assertThat(convertedApplication).isEqualTo(expectedConvertedApplication)
  }
}