package pl.oaza.waw.rekolekcje.api.notification.api

import org.springframework.context.ApplicationEventPublisher
import pl.oaza.waw.rekolekcje.api.application.endpoint.events.ApplicationDeletedEvent
import pl.oaza.waw.rekolekcje.api.application.endpoint.events.ApplicationRejectedEvent
import pl.oaza.waw.rekolekcje.api.application.endpoint.events.ApplicationSubmittedEvent
import pl.oaza.waw.rekolekcje.api.application.endpoint.events.ApplicationVerifiedEvent
import pl.oaza.waw.rekolekcje.api.retreats.endpoint.events.MemberStatusChangedEvent

class NotificationsEventsBehaviour(private val eventPublisher: ApplicationEventPublisher) {

  fun eventIsPublished(event: ApplicationSubmittedEvent) {
    eventPublisher.publishEvent(event)
  }

  fun eventIsPublished(event: ApplicationVerifiedEvent) {
    eventPublisher.publishEvent(event)
  }

  fun eventIsPublished(event: ApplicationRejectedEvent) {
    eventPublisher.publishEvent(event)
  }

  fun eventIsPublished(event: ApplicationDeletedEvent) {
    eventPublisher.publishEvent(event)
  }

  fun eventIsPublished(event: MemberStatusChangedEvent) {
    eventPublisher.publishEvent(event)
  }
}
