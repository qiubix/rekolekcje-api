import { NgModule } from '@angular/core';

import { OpinionsRoutingModule } from './opinions-routing.module';
import { SharedModule } from '@shared/shared.module';
import * as fromComponents from './components';

@NgModule({
  declarations: [fromComponents.components],
  imports: [SharedModule, OpinionsRoutingModule]
})
export class OpinionsModule {}
