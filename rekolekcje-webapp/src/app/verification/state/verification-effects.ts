import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { Action, Store } from '@ngrx/store';
import { catchError, map, switchMap } from 'rxjs/operators';
import { VerificationApiService } from './verification-api.service';
import { VerificationActions } from './verification-actions';
import VerificationActionFail = VerificationActions.VerificationActionFail;
import GetStatisticsSuccess = VerificationActions.GetStatisticsSuccess;
import { Verification } from './verification-reducer';
import GetAnimatorApplicationsSuccess = VerificationActions.GetPendingApplicationsSuccess;
import GetSelectedApplicationSuccess = VerificationActions.GetSelectedApplicationSuccess;
import GetVerifiedApplicationsSuccess = VerificationActions.GetVerifiedApplicationsSuccess;
import SaveApplicationSuccess = VerificationActions.SaveApplicationSuccess;
import ChangeApplicationStatusSuccess = VerificationActions.ChangeApplicationStatusSuccess;
import AssignAnimatorSuccess = VerificationActions.AssignAnimatorSuccess;
import DeleteApplicationSuccess = VerificationActions.DeleteApplicationSuccess;

@Injectable()
export class VerificationEffects {
  @Effect()
  GetStatistics: Observable<Action> = this.actions.pipe(
    ofType(VerificationActions.types.GetStatistics),
    switchMap((action: VerificationActions.GetStatistics) =>
      this.service.getStatistics().pipe(
        map(data => new GetStatisticsSuccess(data)),
        catchError(error => of(new VerificationActionFail(error)))
      )
    )
  );

  @Effect()
  GetPendingApplications: Observable<Action> = this.actions.pipe(
    ofType(VerificationActions.types.GetPendingApplications),
    switchMap((action: VerificationActions.GetPendingApplications) =>
      this.service.getPendingApplications().pipe(
        map(data => new GetAnimatorApplicationsSuccess(data)),
        catchError(error => of(new VerificationActionFail(error)))
      )
    )
  );

  @Effect()
  GetVerifiedApplications: Observable<Action> = this.actions.pipe(
    ofType(VerificationActions.types.GetVerifiedApplications),
    switchMap((action: VerificationActions.GetVerifiedApplications) =>
      this.service.getVerifiedApplications().pipe(
        map(data => new GetVerifiedApplicationsSuccess(data)),
        catchError(error => of(new VerificationActionFail(error)))
      )
    )
  );

  @Effect()
  GetSelectedAnimatorApplications: Observable<Action> = this.actions.pipe(
    ofType(VerificationActions.types.GetSelectedApplication),
    switchMap((action: VerificationActions.GetSelectedApplication) =>
      this.service.getSelectedApplication(action.payload.id).pipe(
        map(data => new GetSelectedApplicationSuccess(data)),
        catchError(error => of(new VerificationActionFail(error)))
      )
    )
  );

  @Effect()
  SaveApplication: Observable<Action> = this.actions.pipe(
    ofType(VerificationActions.types.SaveApplication),
    switchMap((action: VerificationActions.SaveApplication) =>
      this.service.saveApplication(action.payload.application, action.payload.update).pipe(
        map(data => new SaveApplicationSuccess(data)),
        catchError(error => of(new VerificationActionFail(error)))
      )
    )
  );

  @Effect()
  DeleteApplication: Observable<Action> = this.actions.pipe(
    ofType(VerificationActions.types.DeleteApplication),
    switchMap((action: VerificationActions.DeleteApplication) =>
      this.service.deleteApplication(action.payload).pipe(
        map(data => new DeleteApplicationSuccess(data)),
        catchError(error => of(new VerificationActionFail(error)))
      )
    )
  );

  @Effect()
  ChangeApplicationStatus: Observable<Action> = this.actions.pipe(
    ofType(VerificationActions.types.ChangeApplicationStatus),
    switchMap((action: VerificationActions.ChangeApplicationStatus) =>
      this.service.changeApplicationStatus(action.payload).pipe(
        map(data => new ChangeApplicationStatusSuccess(data)),
        catchError(error => of(new VerificationActionFail(error)))
      )
    )
  );

  @Effect()
  AssignAnimator: Observable<Action> = this.actions.pipe(
    ofType(VerificationActions.types.AssignAnimator),
    switchMap((action: VerificationActions.AssignAnimator) =>
      this.service
        .assignAnimatorToRetreat(
          action.payload.applicationId,
          action.payload.selectedRetreat,
          action.payload.selectedRole
        )
        .pipe(
          map(data => new AssignAnimatorSuccess(data)),
          catchError(error => of(new VerificationActionFail(error)))
        )
    )
  );

  constructor(
    private actions: Actions,
    private store: Store<Verification.State>,
    private service: VerificationApiService
  ) {}
}
