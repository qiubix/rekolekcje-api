export interface SaveUserRequest {
  username: string;
  password: string;
  email: string;
  enabled: boolean;
  roles: string[];
}
