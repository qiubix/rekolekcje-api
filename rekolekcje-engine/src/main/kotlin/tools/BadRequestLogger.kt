package pl.oaza.waw.rekolekcje.api.tools

import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus

@ControllerAdvice
internal class BadRequestLogger {

  private val log = LoggerFactory.getLogger(this.javaClass.name)

  @ExceptionHandler
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  fun handle(ex: HttpMessageNotReadableException) {
    log.error("Returning HTTP 400 - Bad Request", ex)
    throw ex
  }

}
