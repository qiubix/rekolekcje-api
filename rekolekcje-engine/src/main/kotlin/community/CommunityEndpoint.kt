package pl.oaza.waw.rekolekcje.api.community

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import pl.oaza.waw.rekolekcje.api.API_URI
import pl.oaza.waw.rekolekcje.api.community.domain.CommunityFacade
import pl.oaza.waw.rekolekcje.api.community.dto.CommunityNotFoundException
import pl.oaza.waw.rekolekcje.api.community.dto.CommunityPayload

const val COMMUNITY_API_URI = "$API_URI/communities"

@RestController
@RequestMapping(COMMUNITY_API_URI)
internal class CommunityEndpoint(private val communityFacade: CommunityFacade) {

  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  fun getAll() : List<CommunityPayload> = communityFacade.findAll()

  @GetMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  fun getOneById(@PathVariable(name = "id") id: Long): CommunityPayload {
    return communityFacade.findOne(id)
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  fun createRetreat(@RequestBody payload: CommunityPayload): CommunityPayload {
    return communityFacade.save(payload)
  }

  @PutMapping
  @ResponseStatus(HttpStatus.OK)
  fun updateRetreatData(@RequestBody payload: CommunityPayload): CommunityPayload {
    return communityFacade.save(payload)
  }

  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  fun deleteRetreat(@PathVariable(name = "id") id: Long) {
    communityFacade.delete(id)
  }

  @ExceptionHandler(CommunityNotFoundException::class)
  fun returnNotFoundStatus(): ResponseEntity<*> {
    return ResponseEntity.notFound().build<Any>()
  }
}
