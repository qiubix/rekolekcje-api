package pl.oaza.waw.rekolekcje.api.shared.endpoint

import pl.oaza.waw.rekolekcje.api.shared.domain.Contact

data class ContactDto(
  val id: Long? = null,
  val fullName: String? = null,
  val phoneNumber: String? = null,
  val email: String? = null
) {
  fun toDomainEntity() = Contact(
    id = id,
    fullName = fullName,
    phoneNumber = phoneNumber,
    email = email
  )
}

fun Contact.toDto() = ContactDto(
  id = id,
  fullName = fullName,
  phoneNumber = phoneNumber,
  email = email
)
