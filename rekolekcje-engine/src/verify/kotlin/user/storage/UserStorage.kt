package pl.oaza.waw.rekolekcje.api.user.storage

import org.springframework.jdbc.core.JdbcTemplate
import pl.oaza.waw.rekolekcje.api.shared.DaoTools
import pl.oaza.waw.rekolekcje.api.user.domain.Role
import pl.oaza.waw.rekolekcje.api.user.domain.User
import java.sql.ResultSet
import java.sql.Types

class UserStorage(private val jdbcTemplate: JdbcTemplate) {

  fun deleteUser(username: String) {
    findUser(username)?.run {
      jdbcTemplate.update("DELETE FROM user_role WHERE user_id = ?", this.id)
      jdbcTemplate.update("DELETE FROM users WHERE username = ?", this.username)
    }
  }

  fun findAllUsers(): List<User> = jdbcTemplate.query("SELECT * FROM users", userRowMapper())

  fun findUser(username: String): User? =
    jdbcTemplate.query("SELECT * FROM users WHERE username = ?", arrayOf(username), userRowMapper()).firstOrNull()

  fun findUserRoles(userId: Long): Set<Role> {
    return jdbcTemplate.query("SELECT role FROM user_role WHERE user_id = ?", arrayOf(userId), roleMapper()).toSet()
  }

  private fun userRowMapper(): (ResultSet, Int) -> User {
    return { rs, _ ->
      User(
        id = DaoTools.getLong(rs, "id"),
        username = rs.getString("username"),
        password = rs.getString("password"),
        email = rs.getString("email"),
        enabled = rs.getBoolean("enabled")
      )
    }
  }

  private fun roleMapper(): (ResultSet, Int) -> Role {
    return { rs, _ -> Role.valueOf(rs.getString("role")) }
  }
}
