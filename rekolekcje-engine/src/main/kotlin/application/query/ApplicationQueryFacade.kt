package pl.oaza.waw.rekolekcje.api.application.query

import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationReadRepository
import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationType
import pl.oaza.waw.rekolekcje.api.application.domain.RetreatApplication
import pl.oaza.waw.rekolekcje.api.application.domain.VerificationStatistics

class ApplicationQueryFacade(private val applicationReadRepository: ApplicationReadRepository) {

  fun findAllApplications(): List<RetreatApplication> = applicationReadRepository.findAll()

  fun findAnimatorApplications(): List<RetreatApplication> =
    applicationReadRepository.findAll().filter { it.applicationType == ApplicationType.ANIMATOR }

  fun findParticipantApplications(): List<RetreatApplication> =
    applicationReadRepository.findAll().filter { it.applicationType == ApplicationType.PARTICIPANT }

  fun getVerificationStatistics(): VerificationStatistics {
    val allApplications = applicationReadRepository.findAll()
    return VerificationStatistics(
      numberOfActiveApplications = allApplications.size,
      alreadyVerified = allApplications.count { !it.isPending() },
      remainingToVerify = allApplications.count { it.isPending() },
      animatorsRemaining = allApplications
        .filter { it.applicationType == ApplicationType.ANIMATOR }
        .count { it.isPending() },
      participantsRemaining = allApplications
        .filter { it.applicationType == ApplicationType.PARTICIPANT }
        .count { it.isPending() }
    )
  }

  fun findPendingApplications(): List<RetreatApplication> = applicationReadRepository.findAll().filter { it.isPending() }

  fun findReadyApplications(): List<RetreatApplication> = applicationReadRepository.findAll().filter { !it.isPending() }

  fun findApplicationById(id: Long): RetreatApplication? = applicationReadRepository.findById(id)

  fun findByRetreatAndMember(retreatId: Long, memberId: Long): RetreatApplication? = findAllApplications()
    .find { it.memberId == memberId && it.retreatId == retreatId }

}