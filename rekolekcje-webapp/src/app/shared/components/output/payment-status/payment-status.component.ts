import { Component, Input, OnInit } from '@angular/core';
import { PaymentStatus } from '../../../models/member-status.model';

@Component({
  selector: 'reko-payment-status',
  templateUrl: './payment-status.component.html',
  styleUrls: ['./payment-status.component.scss']
})
export class PaymentStatusComponent implements OnInit {
  @Input() paymentStatus: PaymentStatus;

  ngOnInit() {}
}
