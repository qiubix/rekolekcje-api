import { Component, Input } from '@angular/core';
import { KwcStatus } from '@shared/models/kwc.model';
import { Experience } from '@shared/models/member-details.model';

@Component({
  selector: 'experience-details',
  templateUrl: './experience-details.component.html',
  styleUrls: ['../member-details-view.component.scss', './experience-details.component.scss']
})
export class ExperienceDetailsComponent {
  @Input() experienceData: Experience;
}
