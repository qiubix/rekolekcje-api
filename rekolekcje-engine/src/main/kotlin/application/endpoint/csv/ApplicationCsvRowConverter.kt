package pl.oaza.waw.rekolekcje.api.application.endpoint.csv

import pl.oaza.waw.rekolekcje.api.application.domain.AnimatorRole
import pl.oaza.waw.rekolekcje.api.application.endpoint.RetreatApplicationDto
import pl.oaza.waw.rekolekcje.api.members.endpoint.MemberDetails
import pl.oaza.waw.rekolekcje.api.shared.endpoint.ContactDto

internal class ApplicationCsvRowConverter(private val fieldToHeaders: Map<String, List<String>>) {

  companion object {
    const val suretyContactKey = "suretyContactKey"
    const val suretyContactEmailKey = "suretyContactEmailKey"
    const val odbParticipantKey = "odbParticipantKey"
    const val ondParticipantKey = "ondParticipantKey"
    const val onzIParticipantKey = "onzIParticipantKey"
    const val onzIIParticipantKey = "onzIIParticipantKey"
    const val onzIIIParticipantKey = "onzIIIParticipantKey"
    const val odbAnimatorKey = "odbAnimatorKey"
    const val ondAnimatorKey = "ondAnimatorKey"
    const val onzIAnimatorKey = "onzIAnimatorKey"
    const val onzIIAnimatorKey = "onzIIAnimatorKey"
    const val onzIIIAnimatorKey = "onzIIIAnimatorKey"
  }

  fun read(row: Map<String, String>, memberDetails: MemberDetails): ConvertedApplicationData {
    return ConvertedApplicationData(
      animatorRoles = extractAnimatorRoles(row),
      availability = extractAvailability(row),
      surety = ContactDto(
        fullName = extractString(row, suretyContactKey),
        email = extractString(row, suretyContactEmailKey)
      ),
      selectedRetreat = extractString(row, RetreatApplicationDto::retreatId.name),
      member = memberDetails
    )
  }

  private fun extractString(row: Map<String, String>, fieldName: String): String? {
    val availableHeaders = fieldToHeaders[fieldName] ?: return null
    return availableHeaders
      .mapNotNull { row[it] }
      .filter { it.isNotBlank() }
      .map { it.trim() }
      .firstOrNull()
  }

  private fun extractAvailability(row: Map<String, String>): Set<String> {
    val rawAvailability = extractString(row, RetreatApplicationDto::availability.name)
    return rawAvailability
      ?.split(", ")
      ?.zipWithNext { a, b -> "$a, $b" }
      ?.filter { it.startsWith("Turnus") }
      ?.toSet() ?: emptySet()
  }

  private fun extractAnimatorRoles(row: Map<String, String>): Set<AnimatorRole> {
    val rawAnimatorRoles = extractString(row, RetreatApplicationDto::animatorRoles.name)
    return rawAnimatorRoles?.split(", ")?.mapNotNull { animatorRolesMap[it] }?.toSet() ?: emptySet()
  }

  private val animatorRolesMap: Map<String, AnimatorRole> = mapOf(
    "Animator grupy" to AnimatorRole.GROUP,
    "Animator gospodarczy" to AnimatorRole.QUARTERMASTER,
    "Animator liturgiczny" to AnimatorRole.LITURGIST,
    "Animator muzyczny" to AnimatorRole.MUSICIAN
  )
}