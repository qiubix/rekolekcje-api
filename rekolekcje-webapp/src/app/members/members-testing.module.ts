import { StoreModule } from '@ngrx/store';
import { AppReducer } from '../core/store/app-store';
import { MembersReducer } from './state/members-reducer';
import { ParishReducer } from '../diocese/parish/store/parish-reducer';
import { RouterTestingModule } from '@angular/router/testing';
import { MembersModule } from './members.module';
import { SharedModule } from '../shared/shared.module';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

export class MatDialogRefMock {}

export let membersTestingModule = {
  imports: [
    StoreModule.forRoot(AppReducer.reducer),
    StoreModule.forFeature('membersModule', MembersReducer.reducer),
    StoreModule.forFeature('parishModule', ParishReducer.reducer),
    RouterTestingModule,
    MembersModule,
    SharedModule,
    NoopAnimationsModule,
  ],
  providers: [
    { provide: MAT_DIALOG_DATA, useValue: {} },
    { provide: MatDialogRef, useClass: MatDialogRefMock },
  ],
};
