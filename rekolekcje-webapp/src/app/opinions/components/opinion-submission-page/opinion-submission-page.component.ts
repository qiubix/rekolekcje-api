import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Opinion } from '@opinions/model';
import { OpinionApiService } from '@opinions/state/opinion-api.service';
import { AuthService } from '@auth/auth.service';

@Component({
  selector: 'reko-opinion-submission-page',
  templateUrl: './opinion-submission-page.component.html',
  styleUrls: ['./opinion-submission-page.component.scss']
})
export class OpinionSubmissionPageComponent implements OnInit {
  encryptedMemberId: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private opinionApiService: OpinionApiService,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.encryptedMemberId = this.route.snapshot.params['encryptedMemberId'];
  }

  onSubmit(opinion: Opinion) {
    this.opinionApiService.submitOne(opinion).subscribe(response => {
      console.log('Opinion submitted: ', response);
      if (this.authService.isAuthenticated()) {
        console.log('Back to list...');
        this.router.navigate(['opinions']);
      } else {
        console.log('Thank you page...');
        this.router.navigate(['opinions', 'thanks']);
      }
    });
  }
}
