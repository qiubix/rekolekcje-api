import { Component, Input, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { OpinionDetails } from '@shared/models';

@Component({
  selector: 'reko-opinions-details',
  templateUrl: './opinions-details.component.html',
  styleUrls: ['./opinions-details.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)'))
    ])
  ]
})
export class OpinionsDetailsComponent implements OnInit {
  @Input() opinions: OpinionDetails[];

  dataSource;
  displayedColumns = ['submitter', 'created'];
  expandedElement: OpinionDetails | null;

  ngOnInit(): void {
    this.dataSource = this.opinions ? this.opinions : [];
    console.log('ds', this.dataSource);
  }
}
