package pl.oaza.waw.rekolekcje.api.integration

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import pl.oaza.waw.rekolekcje.api.shared.BaseIntegrationTest

internal class StartupIntegrationTest : BaseIntegrationTest() {

  @Autowired
  private val context: ApplicationContext? = null

  @Test
  fun shouldStartWithApplicationContext() {
    assertThat(context).isNotNull()
  }
}
