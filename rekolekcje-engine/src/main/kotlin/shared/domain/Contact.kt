package pl.oaza.waw.rekolekcje.api.shared.domain

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table


@Entity
@Table(name = "CONTACTS")
data class Contact(
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  val id: Long? = null,
  val fullName: String? = null,
  val phoneNumber: String? = null,
  val email: String? = null
)

