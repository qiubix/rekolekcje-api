import { Component, Input } from '@angular/core';

@Component({
  selector: 'reko-single-stat',
  templateUrl: './single-stat.component.html',
  styleUrls: ['./single-stat.component.scss']
})
export class SingleStatComponent {
  @Input() icon: string;
  @Input() value: number;
  @Input() label: string;
}
