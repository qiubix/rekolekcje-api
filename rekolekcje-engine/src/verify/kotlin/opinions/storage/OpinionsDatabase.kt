package pl.oaza.waw.rekolekcje.api.opinions.storage

import org.springframework.jdbc.core.JdbcTemplate
import pl.oaza.waw.rekolekcje.api.members.domain.KwcStatus
import pl.oaza.waw.rekolekcje.api.shared.DaoTools
import pl.oaza.waw.rekolekcje.api.members.endpoint.MemberDetails
import pl.oaza.waw.rekolekcje.api.members.endpoint.ContactInfo
import java.sql.ResultSet
import java.util.concurrent.atomic.AtomicInteger

class OpinionsDatabase(private val jdbcTemplate: JdbcTemplate) {

  fun clear() {
    jdbcTemplate.execute("TRUNCATE opinions")
  }
}
