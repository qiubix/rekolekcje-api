package pl.oaza.waw.rekolekcje.api.parish.infrastructure

import pl.oaza.waw.rekolekcje.api.parish.domain.Parish
import pl.oaza.waw.rekolekcje.api.parish.dto.ParishPayload
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "PARISHES")
internal data class PersistedParish(
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  private val id: Long? = null,
  private val name: String? = null,
  private val address: String? = null,
  private val region: Long? = null
) : Parish {

  override fun dto(): ParishPayload {
    return ParishPayload(
      id = id,
      name = name,
      address = address,
      region = region
    )
  }
}
