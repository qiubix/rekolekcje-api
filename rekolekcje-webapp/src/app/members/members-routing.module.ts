import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MembersComponent } from './components/_root/members.component';
import { MemberDetailsComponent } from './components/details/member-details.component';
import { MemberAddEditComponent } from './components/add-edit/add-edit-page/member-add-edit.component';
import { MembersGuard } from './guards/members.guard';
import { MemberDetailsGuard } from './guards/member-details.guard';

const membersRoutes: Routes = [
  {
    path: '',
    component: MembersComponent,
    canActivate: [MembersGuard]
  },
  {
    path: 'add',
    component: MemberAddEditComponent,
    canActivate: [MembersGuard],
    data: { editing: false }
  },
  {
    path: ':id',
    component: MemberDetailsComponent,
    canActivate: [MembersGuard, MemberDetailsGuard]
  },
  {
    path: ':id/edit',
    component: MemberAddEditComponent,
    canActivate: [MembersGuard, MemberDetailsGuard],
    data: { editing: true }
  }
];

@NgModule({
  imports: [RouterModule.forChild(membersRoutes)],
  exports: [RouterModule]
})
export class MembersRoutingModule {}
