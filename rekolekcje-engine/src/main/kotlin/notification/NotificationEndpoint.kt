package pl.oaza.waw.rekolekcje.api.notification

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import pl.oaza.waw.rekolekcje.api.API_URI
import pl.oaza.waw.rekolekcje.api.notification.domain.NotificationCommandFacade
import pl.oaza.waw.rekolekcje.api.notification.dto.NotificationSummary
import javax.validation.Valid

const val NOTIFICATION_URI = "$API_URI/notification"

/**
 * This is the controller which provides operations to manage members.
 */
@RestController
@RequestMapping(NOTIFICATION_URI)
class NotificationEndpoint
@Autowired
internal constructor(
    private val notificationCommandFacade: NotificationCommandFacade
) {

  private val logger = LoggerFactory.getLogger(NotificationEndpoint::class.java)

  /**
   * Creates a new notification. When provided payload does not pass the validation, the 422
   * (Unprocessable entity) is returned.
   *
   * @param notification notification to be created
   * @return notification summary with assigned id and 201 (created)
   */
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  fun createNotification(
      @RequestBody @Valid notification: CreateNotificationRequest
  ): NotificationSummary {
    return notificationCommandFacade.create(notification)
  }
}
