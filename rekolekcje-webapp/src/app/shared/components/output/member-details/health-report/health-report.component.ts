import { Component, Input } from '@angular/core';
import { HealthReport } from '@shared/models/member-details.model';

@Component({
  selector: 'health-report',
  templateUrl: './health-report.component.html',
  styleUrls: ['../member-details-view.component.scss', './health-report.component.scss']
})
export class HealthReportComponent {
  @Input() healthReportData: HealthReport;
}
