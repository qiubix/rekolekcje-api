import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionMenuButtonsComponent } from './action-menu-buttons.component';

describe('ActionMenuButtonsComponent', () => {
  let component: ActionMenuButtonsComponent;
  let fixture: ComponentFixture<ActionMenuButtonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ActionMenuButtonsComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionMenuButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
