package pl.oaza.waw.rekolekcje.api.retreats.endpoint

import pl.oaza.waw.rekolekcje.api.core.ddd.ValueObject
import pl.oaza.waw.rekolekcje.api.retreats.domain.Turn
import pl.oaza.waw.rekolekcje.api.retreats.domain.Retreat
import pl.oaza.waw.rekolekcje.api.retreats.domain.RetreatStatus
import pl.oaza.waw.rekolekcje.api.shared.value.Stage
import java.time.LocalDate

@ValueObject
data class RetreatSummary(
  val id: Long? = null,
  val turn: Turn? = null,
  val stage: Stage? = null,
  val status: RetreatStatus = RetreatStatus.ACTIVE,
  val localization: String? = null,
  val startDate: LocalDate? = null,
  val endDate: LocalDate? = null,
  val participantsAmount: Int = 0,
  val malesParticipants: Int = 0,
  val femalesParticipants: Int = 0,
  val averageAge: Double = 0.0,
  val underageParticipants: Int = 0
) {
  constructor(domainEntity: Retreat): this(
    id = domainEntity.getId(),
    turn = domainEntity.turn,
    stage = domainEntity.stage,
    status = domainEntity.status ?: RetreatStatus.ACTIVE,
    localization = domainEntity.localization,
    startDate = domainEntity.startDate,
    endDate = domainEntity.endDate,
    participantsAmount = domainEntity.numberOfParticipants(),
    malesParticipants = domainEntity.numberOfMaleParticipants(),
    femalesParticipants = domainEntity.numberOfFemaleParticipants(),
    averageAge = domainEntity.averageParticipantsAge(),
    underageParticipants = domainEntity.numberOfUnderageParticipants()
  )
}
