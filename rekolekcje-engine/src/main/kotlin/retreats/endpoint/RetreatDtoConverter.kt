package pl.oaza.waw.rekolekcje.api.retreats.endpoint

import pl.oaza.waw.rekolekcje.api.retreats.domain.RegistrationStatus
import pl.oaza.waw.rekolekcje.api.retreats.domain.Retreat
import pl.oaza.waw.rekolekcje.api.retreats.domain.RetreatMember

fun fromDto(dto: RetreatDetails): Retreat = Retreat(
  id = dto.id,
  turn = dto.turn,
  stage = dto.stage,
  status = dto.status,
  localization = dto.localization,
  startDate = dto.startDate,
  endDate = dto.endDate,
  moderatorName = dto.moderatorName,
  moderatorPhoneNumber = dto.moderatorPhoneNumber,
  priestName = dto.priestName,
  priestPhoneNumber = dto.priestPhoneNumber,
  participantsLimit = dto.participantsLimit,
  minimumAnimatorsRequired = dto.minimumAnimatorsRequired,
  retreatMembers = (dto.participants.map { fromDto(it) } + dto.animators.map { fromDto(it) }).toMutableSet()
)

fun fromDto(dto: RetreatMemberDto): RetreatMember = RetreatMember(
  memberId = dto.memberId,
  firstName = dto.firstName,
  lastName = dto.lastName,
  sex = dto.sex,
  age = dto.age,
  paymentStatus = dto.paymentStatus,
  animator = dto.animator,
  registrationStatus = RegistrationStatus.REGISTERED
)
