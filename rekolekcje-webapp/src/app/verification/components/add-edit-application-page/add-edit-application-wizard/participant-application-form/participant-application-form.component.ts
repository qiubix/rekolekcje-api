import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { RetreatSummary } from '@retreats/models/retreat-summary.model';
import { RetreatApplication } from '@verification/model';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ContactInfo } from '@shared/models';

@Component({
  selector: 'reko-participant-application-form',
  templateUrl: './participant-application-form.component.html',
  styleUrls: ['./participant-application-form.component.scss']
})
export class ParticipantApplicationFormComponent implements OnInit {
  @Input() retreats: RetreatSummary[];
  @Input() data: RetreatApplication;
  @Output() formReady = new EventEmitter<FormGroup>();

  form: FormGroup;

  constructor(private _fb: FormBuilder) {}

  ngOnInit(): void {
    console.log('Init participant application form');
    this.form = this._fb.group({
      retreatId: [this.data ? this.data.retreatId : []]
    });

    this.formReady.emit(this.form);
  }

  onSubFormInitialized(name: string, subForm: FormGroup) {
    this.form.addControl(name, subForm);
  }

  get surety(): ContactInfo {
    return this.data ? this.data.surety : null;
  }
}
