import { AbstractControl, ValidationErrors } from '@angular/forms';

export class RekoValidators {
  public static isPhoneNumberInvalid(control: AbstractControl | null): ValidationErrors | null {
    if (!control.value) return null;

    const validPhoneNumberPattern = new RegExp('^$|^\\d{9}$');
    if (!control.value.toString().match(validPhoneNumberPattern)) {
      return {
        isPhoneNumberInvalid: true
      };
    }
    return null;
  }

  public static isEmailInvalid(control: AbstractControl | null): ValidationErrors | null {
    if (!control.value) return null;

    const validEmailPattern = new RegExp('[a-zA-Z0-9._-]+[@]+[a-zA-Z0-9-]+[.]+[a-zA-Z]{2,6}');
    if (!control.value.toString().match(validEmailPattern)) {
      return {
        isEmailInvalid: true
      };
    }
    return null;
  }
}
