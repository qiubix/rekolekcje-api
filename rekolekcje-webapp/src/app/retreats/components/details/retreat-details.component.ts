import { Component, OnDestroy, OnInit } from '@angular/core';
import { RetreatDetails, RetreatMember } from '../../models/retreat-details.model';
import { Observable, of, Subject } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Retreats } from '../../store/retreat-reducer';
import { AppSelectors } from '@core/store/app-selectors';
import { DeleteConfirmAlertDialog } from '@shared/components';
import { RetreatsSharedActions } from '../../store/retreat-actions';
import { RetreatAddEditDialog } from '../add-edit/retreat-dialog/add-edit-dialog.component';
import { MembersSharedActions } from 'app/members/state/members-actions';
import { map, skipWhile, takeUntil, withLatestFrom } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'retreat-details',
  templateUrl: './retreat-details.component.html',
  styleUrls: ['./retreat-details.component.scss'],
})
export class RetreatDetailsComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  retreatLoading$: Observable<boolean>;
  retreatData$: Observable<RetreatDetails>;
  allParticipants$: Observable<RetreatMember[]>;
  allAnimators$: Observable<RetreatMember[]>;

  constructor(
    private dialog: MatDialog,
    private router: Router,
    private retreatsStore: Store<Retreats.State>,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.retreatLoading$ = this.retreatsStore.pipe(
      select(AppSelectors.getRetreatDetailsLoading),
      takeUntil(this.ngUnsubscribe)
    );

    this.retreatData$ = this.retreatsStore.pipe(
      select(AppSelectors.getSelectedRetreat),
      takeUntil(this.ngUnsubscribe),
      skipWhile((retreatDetails) => !retreatDetails)
    );

    this.allParticipants$ = this.retreatsStore.pipe(
      select(AppSelectors.getSelectedRetreat),
      takeUntil(this.ngUnsubscribe),
      skipWhile((retreatDetails) => !retreatDetails),
      map((retreatDetails) => retreatDetails.participants)
    );

    this.allAnimators$ = this.retreatsStore.pipe(
      select(AppSelectors.getSelectedRetreat),
      takeUntil(this.ngUnsubscribe),
      skipWhile((retreatDetails) => !retreatDetails),
      map((retreatDetails) => retreatDetails.animators)
    );
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  delete(): void {
    this.dialog
      .open(DeleteConfirmAlertDialog, { disableClose: false })
      .afterClosed()
      .pipe(withLatestFrom(this.retreatData$))
      .subscribe(([result, retreat]) => {
        if (result) {
          this.router.navigate(['retreats']);
          this.retreatsStore.dispatch(new RetreatsSharedActions.DeleteRetreat(retreat.id));
        }
      });
  }

  edit(retreatData: RetreatDetails): void {
    this.dialog
      .open(RetreatAddEditDialog, {
        data: {
          dialogTitle: 'retreats.dialog.edit',
          retreatData: retreatData,
        },
        disableClose: true,
      })
      .afterClosed()
      .subscribe((result: RetreatDetails) => {
        if (result) {
          const retreatToUpdate = result;
          retreatToUpdate.id = retreatData.id;
          retreatToUpdate.participants = retreatData.participants ? retreatData.participants : [];
          retreatToUpdate.animators = retreatData.animators ? retreatData.animators : [];
          retreatToUpdate.statistics = retreatData.statistics;
          retreatToUpdate.status = retreatData.status;
          this.retreatsStore.dispatch(new RetreatsSharedActions.UpdateRetreat(retreatToUpdate));
        }
      });
  }

  goToDetails(memberId: number): void {
    this.retreatsStore.dispatch(new MembersSharedActions.SelectMemberById(memberId));
    this.router.navigateByUrl(`/members/${memberId}`);
  }

  goBack(): void {
    this.location.back();
  }
}
