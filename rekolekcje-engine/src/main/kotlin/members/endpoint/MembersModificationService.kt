package pl.oaza.waw.rekolekcje.api.members.endpoint

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pl.oaza.waw.rekolekcje.api.members.domain.MembersCommandFacade
import pl.oaza.waw.rekolekcje.api.members.query.MembersQueryFacade
import pl.oaza.waw.rekolekcje.api.shared.exceptions.MemberNotFoundException

@Service
@Transactional
internal class MembersModificationService(
  private val membersCommandFacade: MembersCommandFacade,
  private val membersQueryFacade: MembersQueryFacade
) {

  fun updateMember(memberDetails: MemberDetails): MemberSummary {
//    assignMemberToRetreats(updatedMemberDetails.id, updatedMemberDetails)
    if (memberDetails.id == null) {
      throw IllegalStateException("Can not update member without id specified")
    }
    val updatedMember = membersCommandFacade.save(fromDto(memberDetails))
    return MemberSummary(updatedMember)
  }

  fun deleteMemberById(id: Long) {
    membersCommandFacade.delete(id)
  }

}