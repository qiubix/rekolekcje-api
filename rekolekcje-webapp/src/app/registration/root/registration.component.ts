import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Retreats } from '../../retreats/store/retreat-reducer';
import { RetreatsSharedActions } from '../../retreats/store/retreat-actions';

@Component({
  selector: 'reko-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  constructor(private store: Store<Retreats.State>) {}

  ngOnInit() {
    this.store.dispatch(new RetreatsSharedActions.LoadRetreatsList());
  }
}
