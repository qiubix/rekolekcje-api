export enum MemberStatus {
  WAITING = 'WAITING',
  VERIFIED = 'VERIFIED',
  REGISTERED = 'REGISTERED',
  REJECTED = 'REJECTED',
  PROBLEM = 'PROBLEM'
}

export enum PaymentStatus {
  NOTHING = 'NOTHING',
  DOWN_PAYMENT = 'DOWN_PAYMENT',
  FULL = 'FULL'
}
