import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetreatFormComponent } from './retreat-form.component';

describe('RetreatFormComponent', () => {
  let component: RetreatFormComponent;
  let fixture: ComponentFixture<RetreatFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RetreatFormComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetreatFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
