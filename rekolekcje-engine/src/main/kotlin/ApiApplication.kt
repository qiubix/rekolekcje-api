package pl.oaza.waw.rekolekcje.api

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.scheduling.annotation.EnableScheduling

const val API_URI = "/api"

@SpringBootApplication
@EnableScheduling
class ApiApplication

fun main(args: Array<String>) {
  SpringApplication.run(ApiApplication::class.java, *args)
}
