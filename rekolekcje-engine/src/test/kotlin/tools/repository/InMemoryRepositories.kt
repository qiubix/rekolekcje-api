package pl.oaza.waw.rekolekcje.api.tools.repository

import pl.oaza.waw.rekolekcje.api.shared.domain.EntityWithId
import java.util.concurrent.ConcurrentHashMap

open class InMemoryRepository<T> {

  protected val map = ConcurrentHashMap<Long, T>()
  protected var nextId: Long = 1L

  fun findById(id: Long): T? {
    return map[id]
  }

  fun findAll(): List<T> {
    return ArrayList(map.values)
  }

  fun deleteById(id: Long) {
    map.remove(id)
  }
}

open class InMemoryReadRepository<T>(private val storage: Map<Long, T>) {

  fun findById(id: Long): T? = storage[id]

  fun findAll(): List<T> = ArrayList(storage.values)

  fun existsById(id: Long): Boolean = storage.containsKey(id)

}

open class InMemoryWriteRepository<T: EntityWithId>(private val storage: HashMap<Long, T>) {

  private var nextId = 1L

  fun save(entity: T): T {
    val entityId = entity.getId()
    return if (entityId != null && storage.containsKey(entityId)) {
      storage[entityId] = entity
      entity
    } else {
      val newMember = entity.withId(nextId) as T
      storage[nextId] = newMember
      nextId += 1
      newMember
    }
  }

  fun deleteById(id: Long) {
    storage.remove(id)
  }

}