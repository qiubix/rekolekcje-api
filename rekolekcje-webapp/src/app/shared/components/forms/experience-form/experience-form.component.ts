import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Config } from '@config';
import { Experience, ContactInfo, Stage } from '@shared/models';

const resolvedPromise = Promise.resolve(null);

@Component({
  selector: 'reko-experience-form',
  templateUrl: './experience-form.component.html',
  styleUrls: ['./experience-form.component.scss']
})
export class ExperienceFormComponent implements OnInit {
  form: FormGroup;

  @Input() experienceData: Experience;

  @Output() formReady = new EventEmitter<FormGroup>();

  stages = Stage;
  stagesKeys = Object.keys(this.stages);

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      historicalRetreats: this.fb.control(
        this.experienceData && this.experienceData.historicalRetreats
          ? this.experienceData.historicalRetreats
          : []
      )
    });
    resolvedPromise.then(() => {
      this.formReady.emit(this.form);
    });
  }

  onSubFormInitialized(name: string, subForm: FormArray) {
    this.form.addControl(name, subForm);
  }

  get animator(): ContactInfo {
    return this.experienceData ? this.experienceData.animator : {};
  }
}
