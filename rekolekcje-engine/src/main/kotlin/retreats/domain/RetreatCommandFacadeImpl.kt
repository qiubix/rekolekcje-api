package pl.oaza.waw.rekolekcje.api.retreats.domain


open class RetreatCommandFacadeImpl internal constructor(
  private val writeRepository: RetreatWriteRepository,
  private val readRepository: RetreatReadRepository,
  private val eventPublisher: RetreatEventPublisher
) : RetreatCommandFacade {

  override fun save(retreat: Retreat): Retreat {
    val retreatId = retreat.getId()
    return if (retreatId != null) {
      val storedRetreat = readRepository.findById(retreatId) ?: throw RetreatNotFoundException(retreatId)
      val updatedRetreat = retreat.copy(
        retreatMembers = ((storedRetreat.participants() + storedRetreat.animators()) as MutableSet<RetreatMember>)
      )
      writeRepository.save(updatedRetreat)
    } else {
      writeRepository.save(retreat)
    }
  }

  override fun delete(id: Long) {
    writeRepository.deleteById(id)
  }

  override fun removeMemberFromAllRetreats(memberId: Long) {
    readRepository.findAll()
      .filter { it.hasMember(memberId) }
      .forEach {
        it.removeMember(memberId)
        save(it)
      }
  }

  override fun registerMember(retreatId: Long, member: RetreatMember): Retreat {
    val retreat = readRepository.findById(retreatId) ?: throw RetreatNotFoundException(retreatId)
    retreat.registerMember(member)
    writeRepository.save(retreat)
    eventPublisher.memberRegistered(retreatId, member.memberId)
    return retreat
  }
}
