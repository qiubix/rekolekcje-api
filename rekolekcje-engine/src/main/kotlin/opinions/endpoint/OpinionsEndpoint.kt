package pl.oaza.waw.rekolekcje.api.opinions.endpoint

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import pl.oaza.waw.rekolekcje.api.API_URI
import pl.oaza.waw.rekolekcje.api.opinions.domain.OpinionsCommandFacade
import pl.oaza.waw.rekolekcje.api.opinions.domain.OpinionsQueryFacade
import pl.oaza.waw.rekolekcje.api.opinions.endpoint.OpinionsEndpoint.Companion.OPINIONS_URI
import javax.validation.Valid

@RestController
@RequestMapping(OPINIONS_URI)
class OpinionsEndpoint(
    private val opinionsCommandFacade: OpinionsCommandFacade,
    private val opinionsQueryFacade: OpinionsQueryFacade
) {

  companion object {
    const val OPINIONS_URI = "$API_URI/opinions"
    const val SUBMIT_URI = "/submit"
    const val OPINIONS_FOR_MEMBER = "/member"
  }

  @PostMapping(SUBMIT_URI)
  @ResponseStatus(HttpStatus.CREATED)
  fun submit(
      @RequestBody @Valid submitOpinionRequest: SubmitOpinionRequest
  ): OpinionSubmittedResponse {
    val createdOpinion = opinionsCommandFacade.submit(submitOpinionRequest)

    return OpinionSubmittedResponse.of(createdOpinion)
  }

  @GetMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  fun findById(@PathVariable(name = "id") id: Long): OpinionDetailsResponse {
    val opinion = opinionsQueryFacade.getById(id)

    return OpinionDetailsResponse.of(opinion)
  }

  @GetMapping("$OPINIONS_FOR_MEMBER/{memberId}")
  fun findOpinionsForMember(@PathVariable memberId: Long): List<OpinionDetailsResponse> =
    opinionsQueryFacade.getAllOpinionsForMember(memberId).map { OpinionDetailsResponse.of(it) }

}