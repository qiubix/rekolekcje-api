package pl.oaza.waw.rekolekcje.api.shared

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.jayway.jsonpath.JsonPath
import org.assertj.core.api.Assertions
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.ResultMatcher
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.io.IOException

class Response(private val resultActions: ResultActions) {

  @Throws(Exception::class)
  fun andExpect(resultMatcher: ResultMatcher): ResultActions {
    return resultActions.andExpect(resultMatcher)
  }

  fun andReturn(): MvcResult {
    return resultActions.andReturn()
  }

  inline fun <reified T: Any> getContent(): T {
    try {
      val response = andReturn().response
      val mapper = jacksonObjectMapper().registerModule(JavaTimeModule())
      return mapper.readValue(response.contentAsString, T::class.java)
    } catch (e: IOException) {
      throw IllegalStateException("Error during conversion occurred", e)
    }

  }

  inline fun <reified T: Any> getContentAsList(): List<T> {
    try {
      val response = andReturn().response
      val mapper = jacksonObjectMapper().registerModule(JavaTimeModule())
      return mapper.readValue(response.contentAsString, mapper.typeFactory.constructCollectionType(List::class.java, T::class.java))
    } catch (e: IOException) {
      throw IllegalStateException(
        "Error during conversion to list occurred",
        e
      )
    }
  }

  fun hasDataWithId() {
    val contentAsString = andReturn().response.contentAsString
    val parsedJson = JsonPath.parse(contentAsString)
    val parsedId = parsedJson.read("$.id", Long::class.java)
    Assertions.assertThat(parsedId)
      .isNotNull()
      .isNotZero()
  }

  fun hasStatusOk() = andExpect(status().isOk)

  fun hasStatusCreated() = andExpect(status().isCreated)

  fun hasStatusIsBadRequest() = andExpect(status().isBadRequest)

  fun hasStatusForbidden() = andExpect(status().isForbidden)

  fun hasStatusNotFound() = andExpect(status().isNotFound)
}
