import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { HistoricalRetreatFormComponent } from './historical-retreat-form/historical-retreat-form.component';
import { RetreatTurn } from '@shared/models/member-details.model';

const resolvedPromise = Promise.resolve(null);

@Component({
  selector: 'reko-historical-retreats-array',
  templateUrl: './historical-retreats-array.component.html',
  styleUrls: ['./historical-retreats-array.component.scss']
})
export class HistoricalRetreatsArrayComponent implements OnInit {
  retreatsFormArray: FormArray;

  @Output() formReady = new EventEmitter<FormArray>();

  addRetreat() {
    this.retreatsFormArray.push(HistoricalRetreatFormComponent.buildFormConfig(null));
  }

  removeRetreat(index) {
    this.retreatsFormArray.removeAt(index);
  }

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.retreatsFormArray = new FormArray([]);
    resolvedPromise.then(() => {
      this.formReady.emit(this.retreatsFormArray);
    });
  }

  createHistoricalRetreatForm(retreatTurn: RetreatTurn): FormGroup {
    return this.fb.group({
      stage: retreatTurn ? retreatTurn.stage : '',
      year: retreatTurn ? retreatTurn.year : ''
    });
  }

  onSubFormInitialized(subForm: FormGroup) {
    this.retreatsFormArray.push(subForm);
  }
}
