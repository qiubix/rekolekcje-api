package pl.oaza.waw.rekolekcje.api.members

internal class MembersWithRetreatsTest : MemberTestFixture() {

  /*

  @Test
  fun `should assign member to retreat for which he is applying`() {
    // given
    val retreatId = 23L
    val member = memberWithMinimalData().applyingForRetreat(retreatId)

    // when
    val memberSummary = commandFacade.save(member)

    // then
    val request = AssignMemberRequest(
      memberId = memberSummary.id,
      retreats = setOf(retreatId)
    )
    verify { retreatCommandFacadeMock.addMemberToRetreats(request) }
  }

  @Test
  fun `should add member with his historical retreats when no retreats exist`() {
    // given
    val historicalRetreats = setOf(
      RetreatTurn(stage = Stage.ODB, turn = Turn.I, year = 2015),
      RetreatTurn(stage = Stage.OND, turn = Turn.III, year = 2016)
    )
    val member = memberWithMinimalData().withNewRetreats(historicalRetreats)

    // when
    val memberSummary = commandFacade.save(member)

    // then
//    every { retreatClientMock.findHistoricalRetreatsForMember(memberSummary.id) } returns historicalRetreats
    assertThat(allInSystem).contains(member.withId(memberSummary.id).withNewRetreats(emptySet()))
    val assignMemberRequest = AssignMemberRequest(
      memberId = memberSummary.id,
      retreats = emptySet(),
      newRetreats = historicalRetreats
    )
    verify { retreatCommandFacadeMock.addMemberToRetreats(assignMemberRequest) }
  }

  @Test
  fun `should update member's historical retreats`() {
    // given
    val historicalRetreats = setOf(11L, 12L)
    val member = memberWithMinimalData().withHistoricalRetreats(historicalRetreats)
    val memberSummary = commandFacade.save(member)
    val id = memberSummary.id

    // when
    val updatedHistoricalRetreats = setOf(12L, 13L)
    val missingRetreats = setOf(
      RetreatTurn(stage = Stage.ODB, turn = Turn.I, year = 2015),
      RetreatTurn(stage = Stage.OND, turn = Turn.III, year = 2016)
    )
    val updatedMember = memberWithMinimalData()
      .withId(id)
      .withHistoricalRetreats(updatedHistoricalRetreats)
      .withNewRetreats(missingRetreats)
    commandFacade.save(updatedMember)

    // then
    val memberDetails = queryFacade.find(id)
    assertThat(allInSystem).contains(memberDetails)
    val firstRequest = AssignMemberRequest(memberId = id, retreats = historicalRetreats)
    val secondRequest = AssignMemberRequest(memberId = id, retreats = updatedHistoricalRetreats, newRetreats = missingRetreats)
    verify(exactly = 1) {
      retreatCommandFacadeMock.addMemberToRetreats(firstRequest)
      retreatCommandFacadeMock.addMemberToRetreats(secondRequest)
    }
  }

   */

}
