import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Opinion } from '@opinions/model';

@Component({
  selector: 'reko-opinion-form',
  templateUrl: './opinion-form.component.html',
  styleUrls: ['./opinion-form.component.scss']
})
export class OpinionFormComponent implements OnInit {
  @Input() encryptedMemberId: string;

  @Output() onSubmit = new EventEmitter<Opinion>();
  public form: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.form = this.fb.group({
      content: ['', Validators.required]
    });
  }

  onSubFormInitialized(name: string, subForm: FormGroup) {
    this.form.addControl(name, subForm);
  }

  get content(): FormControl {
    return this.form.get('content') as FormControl;
  }

  submit() {
    if (this.form.valid) {
      const opinion: Opinion = this.form.value;
      opinion.encryptedMemberId = this.encryptedMemberId;
      console.log('Opinion form output: ', opinion);
      this.onSubmit.emit(opinion);
    }
  }
}
