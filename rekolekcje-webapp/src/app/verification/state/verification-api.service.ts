import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import {
  ApplicationSubmission,
  ApplicationSummary,
  AssignAnimatorRequest,
  AssignAnimatorResponse,
  ChangeApplicationStatusRequest,
  ChangeApplicationStatusResponse,
  RetreatApplication,
  VerificationStatistics
} from '@verification/model';
import { Config } from '@config';
import { map, tap } from 'rxjs/operators';
import { MemberStatus, OpinionDetails } from '@shared/models';
import { of } from 'rxjs/internal/observable/of';

@Injectable({ providedIn: 'root' })
export class VerificationApiService {
  private readonly baseUrl = Config.endpoints.applicationModule;
  private readonly animatorsUrl = this.baseUrl + '/animators';
  private readonly participantsUrl = this.baseUrl + '/participants';

  constructor(private http: HttpClient) {}

  getStatistics(): Observable<VerificationStatistics> {
    //TODO: user store + guard to properly update state
    // console.log('getting stats via http...');
    return this.http.get<VerificationStatistics>(this.baseUrl + '/stats');
  }

  getPendingApplications(): Observable<ApplicationSummary[]> {
    return this.http.get<ApplicationSummary[]>(this.baseUrl + '/pending');
  }

  getVerifiedApplications(): Observable<ApplicationSummary[]> {
    return this.http.get<ApplicationSummary[]>(this.baseUrl + '/verified');
  }

  getSelectedApplication(id: number): Observable<RetreatApplication> {
    return this.http.get<RetreatApplication>(this.baseUrl + '/' + id);
  }

  submitNewApplication(payload: ApplicationSubmission): Observable<RetreatApplication> {
    return this.http.post<RetreatApplication>(this.baseUrl, payload);
  }

  updateApplication(application: RetreatApplication): Observable<RetreatApplication> {
    return this.http.put<RetreatApplication>(this.baseUrl, application);
  }

  saveApplication(payload: ApplicationSubmission, update: boolean): Observable<RetreatApplication> {
    if (update) {
      return this.http.put<RetreatApplication>(this.baseUrl + '/' + payload.id, payload);
    } else {
      return this.http.post<RetreatApplication>(this.baseUrl, payload);
    }
  }

  deleteApplication(id: number): Observable<any> {
    return this.http.delete(this.baseUrl + '/' + id);
  }

  changeApplicationStatus(
    request: ChangeApplicationStatusRequest
  ): Observable<ChangeApplicationStatusResponse> {
    return this.http.post<ChangeApplicationStatusResponse>(this.baseUrl + '/status', request);
  }

  assignAnimatorToRetreat(
    id: number,
    selectedRetreat: number,
    selectedRole: string
  ): Observable<AssignAnimatorResponse> {
    const request: AssignAnimatorRequest = {
      applicationId: id,
      selectedRetreat: selectedRetreat,
      selectedRole: selectedRole
    };
    // console.log('Sending request to API: ', request);
    return this.http.post<AssignAnimatorResponse>(this.animatorsUrl + '/assign', request);
  }
}
