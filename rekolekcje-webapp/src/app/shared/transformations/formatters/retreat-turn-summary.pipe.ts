import { Pipe, PipeTransform } from '@angular/core';
import { RetreatSummary } from 'app/retreats/models/retreat-summary.model';
import { Stage } from '../../models/stage.model';

@Pipe({
  name: 'summarizeRetreatTurn'
})
export class RetreatTurnSummaryPipe implements PipeTransform {
  stages = Stage;

  transform(retreat: RetreatSummary, args?: any): String {
    if (retreat) {
      return (
        this.stages[retreat.stage] + ': turnus ' + retreat.turn + ' (' + retreat.startDate + ')'
      );
    } else {
      return 'Turnus nie wybrany';
    }
  }
}
