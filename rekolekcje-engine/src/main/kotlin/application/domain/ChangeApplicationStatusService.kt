package pl.oaza.waw.rekolekcje.api.application.domain


internal class ChangeApplicationStatusService(
  private val applicationReadRepository: ApplicationReadRepository,
  private val applicationWriteRepository: ApplicationWriteRepository,
  private val applicationNotifier: ApplicationNotifier
) {

  fun approve(applicationId: Long): VerifiableApplication {
    return changeStatus(applicationId, { it.approve() }, { applicationNotifier.applicationVerified(applicationId) })
  }

  fun reject(id: Long, reason: String): VerifiableApplication {
    return changeStatus(id, { it.reject(reason) }, { applicationNotifier.applicationRejected(id, reason) })
  }

  fun enroll(id: Long): VerifiableApplication {
    return changeStatus(id, { it.register() })
  }

  fun resetStatus(id: Long): VerifiableApplication {
    return changeStatus(id, { it.resetToWaiting() })
  }

  fun putOnWaitingList(id: Long): VerifiableApplication {
    return changeStatus(id, { it.putOnWaitingList() })
  }

  fun resign(id: Long): VerifiableApplication {
    return changeStatus(id, { it.resign() })
  }

  fun reportProblem(id: Long): VerifiableApplication {
    return changeStatus(id, { it.reportProblem() })
  }

  private fun changeStatus(
    id: Long,
    action: (VerifiableApplication) -> VerifiableApplication,
    notifyAction: ((VerifiableApplication) -> Unit)? = null
  ): VerifiableApplication {
    val verifiableApplication = findApplication(id)
    val applicationWithChangedStatus = action(verifiableApplication)
    applicationWriteRepository.save(applicationWithChangedStatus as RetreatApplication)
    notifyAction?.let { it(applicationWithChangedStatus) }
    return applicationWithChangedStatus
  }

  private fun findApplication(id: Long): VerifiableApplication {
    val foundApplication = applicationReadRepository.findById(id)
    return foundApplication ?: throw IllegalStateException()
  }
}