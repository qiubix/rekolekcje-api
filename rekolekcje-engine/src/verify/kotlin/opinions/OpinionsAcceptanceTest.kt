package pl.oaza.waw.rekolekcje.api.opinions

import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.test.context.support.WithMockUser
import pl.oaza.waw.rekolekcje.api.members.api.MembersApiBehaviour
import pl.oaza.waw.rekolekcje.api.members.storage.MembersDatabase
import pl.oaza.waw.rekolekcje.api.members.storage.MembersStorageBehaviour
import pl.oaza.waw.rekolekcje.api.members.storage.PartialMemberFactory
import pl.oaza.waw.rekolekcje.api.opinions.domain.Submitter
import pl.oaza.waw.rekolekcje.api.opinions.endpoint.OpinionDetailsResponse
import pl.oaza.waw.rekolekcje.api.opinions.endpoint.OpinionSubmittedResponse
import pl.oaza.waw.rekolekcje.api.opinions.endpoint.OpinionsEndpoint.Companion.OPINIONS_FOR_MEMBER
import pl.oaza.waw.rekolekcje.api.opinions.endpoint.OpinionsEndpoint.Companion.OPINIONS_URI
import pl.oaza.waw.rekolekcje.api.opinions.endpoint.OpinionsEndpoint.Companion.SUBMIT_URI
import pl.oaza.waw.rekolekcje.api.opinions.endpoint.SubmitOpinionRequest
import pl.oaza.waw.rekolekcje.api.opinions.endpoint.SubmitterApiModel
import pl.oaza.waw.rekolekcje.api.opinions.storage.OpinionsDatabase
import pl.oaza.waw.rekolekcje.api.shared.BaseIntegrationTest
import java.time.Clock
import java.time.LocalDate
import java.time.LocalDateTime

class OpinionsAcceptanceTest : BaseIntegrationTest() {

  @Autowired
  private lateinit var clock: Clock

  @Autowired
  private lateinit var membersStorageBehaviour: MembersStorageBehaviour

  @Autowired
  private lateinit var membersApiBehaviour: MembersApiBehaviour

  @Autowired
  private lateinit var membersDatabase: MembersDatabase

  @Autowired
  private lateinit var opinionsDatabase: OpinionsDatabase

  @BeforeEach
  fun setup() {
    membersDatabase.clear()
    opinionsDatabase.clear()
  }

  private fun act() = Act()

  private val sampleMemberId = 141L
  private val sampleMember = PartialMemberFactory().withMinimalData(sampleMemberId)

  private fun givenOpinionRequest(): SubmitOpinionRequest {
    return SubmitOpinionRequest(
      encryptedMemberId = membersApiBehaviour.encryptedMemberIdIsSentByEmail(sampleMemberId),
      content = "Some opinion about this guy",
      submitter = SubmitterApiModel(
        fullName = "Major Yon",
        phoneNumber = "500778654",
        email = "major.yon@wp.pl",
        role = Submitter.Role.ANIMATOR
      )
    )
  }

  @Test
  fun `should add opinion`() {
    // given
    membersStorageBehaviour.existsSingleMemberWithSampleData(sampleMember)

    // when
    val response = act().`opinion is added`(givenOpinionRequest())

    // then
    response.hasStatusCreated()

    val responseBody = response.getContent<OpinionSubmittedResponse>()

    then(responseBody.id).isNotNull()
  }

  @Test
  fun `should not add opinion if member is missing`() {
    // when
    val response = act().`opinion is added`(givenOpinionRequest())

    // then
    response.hasStatusNotFound()
  }

  @Test
  fun `should add second opinion on member`() {
    // given
    val opinionRequest = givenOpinionRequest()
    membersStorageBehaviour.existsSingleMemberWithSampleData(sampleMember)

    // first opinion exists
    act().`opinion is added`(opinionRequest)

    // when
    val response = act().`opinion is added`(
      opinionRequest.copy(content = "another content")
    )

    // then
    response.hasStatusCreated()
  }

  @Test
  @WithMockUser
  fun `should fetch all opinions for member`() {
    // given
    val opinionRequest = givenOpinionRequest()
    membersStorageBehaviour.existsSingleMemberWithSampleData(sampleMember)
    val firstOpinionId = act().`opinion is added`(opinionRequest).getContent<OpinionSubmittedResponse>().id
    val anotherOpinionRequest = SubmitOpinionRequest(
      encryptedMemberId = membersApiBehaviour.encryptedMemberIdIsSentByEmail(sampleMemberId),
      content = "Different opinion about the same guy",
      submitter = SubmitterApiModel(
        fullName = "General Yen",
        phoneNumber = "600978654",
        email = "general.yen@wp.pl",
        role = Submitter.Role.MODERATOR
      )
    )
    val secondOpinionId = act().`opinion is added`(anotherOpinionRequest).getContent<OpinionSubmittedResponse>().id

    // when
    val response = act().`all opinions for member are requested`(sampleMemberId)

    // then
    response.hasStatusOk()
    val expectedOpinions = listOf(
      OpinionDetailsResponse(
        id = firstOpinionId,
        memberId = sampleMemberId,
        content = opinionRequest.content,
        createdDate = LocalDate.now(clock),
        submitter = opinionRequest.submitter
      ),
      OpinionDetailsResponse(
        id = secondOpinionId,
        memberId = sampleMemberId,
        content = anotherOpinionRequest.content,
        createdDate = LocalDate.now(clock),
        submitter = anotherOpinionRequest.submitter
      )
    )
    val opinions = response.getContentAsList<OpinionDetailsResponse>()
    then(opinions).isEqualTo(expectedOpinions)
  }

  inner class Act {
    fun `opinion is added`(opinionRequest: SubmitOpinionRequest) =
        restApi.performPost("$OPINIONS_URI/$SUBMIT_URI", opinionRequest)

    fun `all opinions for member are requested`(memberId: Long) =
      restApi.performGet("$OPINIONS_URI/$OPINIONS_FOR_MEMBER/$memberId")
  }
}
