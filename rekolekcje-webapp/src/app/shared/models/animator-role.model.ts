export enum AnimatorRole {
  MUSICIAN = 'Muzyczny',
  LITURGIST = 'Liturgiczny',
  GROUP = 'Animator grupy',
  QUARTERMASTER = 'Gospodarczy'
}
