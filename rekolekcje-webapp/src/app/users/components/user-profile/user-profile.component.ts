import { Component, OnInit } from '@angular/core';
import { UserSummary } from '../../model/user-summary.model';
import { Location } from '@angular/common';
import { select, Store } from '@ngrx/store';
import { Users } from '../../store/users-reduces';
import { AppSelectors } from '@core/store/app-selectors';
import { Observable } from 'rxjs/internal/Observable';
import { UsersSharedActions } from '../../store/users-actions';
import { SaveUserRequest } from '../../model/save-user-request.model';

@Component({
  selector: 'reko-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  currentUser$: Observable<UserSummary>;

  constructor(private readonly store: Store<Users.State>, private location: Location) {}

  ngOnInit() {
    this.currentUser$ = this.store.pipe(select(AppSelectors.getCurrentUser));
  }

  onProfileUpdate(request: SaveUserRequest) {
    if (request) {
      this.store.dispatch(new UsersSharedActions.UpdateUser(request));
    }
    this.location.back();
  }
}
