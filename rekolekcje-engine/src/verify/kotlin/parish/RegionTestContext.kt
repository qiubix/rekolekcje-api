package pl.oaza.waw.rekolekcje.api.parish

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.jdbc.core.JdbcTemplate
import pl.oaza.waw.rekolekcje.api.shared.RestApi
import pl.oaza.waw.rekolekcje.api.parish.api.RegionApiBehaviours
import pl.oaza.waw.rekolekcje.api.parish.api.RegionApiExpectations
import pl.oaza.waw.rekolekcje.api.parish.storage.RegionDatabase

@Configuration
@Profile("test")
internal class RegionTestContext {

  @Bean
  fun regionDatabase(jdbcTemplate: JdbcTemplate): RegionDatabase {
    return RegionDatabase(jdbcTemplate)
  }

  @Bean
  fun regionApiBehaviours(restApi: RestApi): RegionApiBehaviours {
    return RegionApiBehaviours(restApi)
  }

  @Bean
  fun regionApiExpectations(restApi: RestApi): RegionApiExpectations {
    return RegionApiExpectations(restApi)
  }
}
