package pl.oaza.waw.rekolekcje.api.notification.domain

import pl.oaza.waw.rekolekcje.api.notification.dto.NotificationStatus
import pl.oaza.waw.rekolekcje.api.tools.repository.InMemoryRepository
import java.util.*

internal class InMemoryNotificationRepository : InMemoryRepository<Notification>(), NotificationRepository {

  private val notificationMapper: NotificationMapper = NotificationMapper()

  override fun findByStatus(status: NotificationStatus): List<Notification> {
    return map.values.filter { notification -> notification.status == status }
  }

  override fun save(notification: Notification): Notification {
    Objects.requireNonNull(notification)
    return if (notification.id != null && map.containsKey(notification.id)) {
      map[notification.id] = notification
      notification
    } else {
      val newNotification = notificationMapper.copyWithId(notification, nextId)
      map[nextId] = newNotification
      this.nextId = this.nextId.plus(1)
      newNotification
    }
  }

  override fun deleteByApplicationId(applicationId: Long) {
    map.entries
        .filter { entry -> entry.value.applicationId == applicationId }
        .forEach{ entry -> map.remove(entry.key) }
  }
}