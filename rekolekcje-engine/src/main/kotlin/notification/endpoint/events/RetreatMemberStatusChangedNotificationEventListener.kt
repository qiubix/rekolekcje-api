package pl.oaza.waw.rekolekcje.api.notification.endpoint.events

import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationListener
import org.springframework.stereotype.Component
import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationStatus
import pl.oaza.waw.rekolekcje.api.application.query.ApplicationQueryFacade
import pl.oaza.waw.rekolekcje.api.members.query.MembersQueryFacade
import pl.oaza.waw.rekolekcje.api.notification.CreateNotificationRequest
import pl.oaza.waw.rekolekcje.api.notification.domain.NotificationCommandFacade
import pl.oaza.waw.rekolekcje.api.notification.dto.NotificationReason
import pl.oaza.waw.rekolekcje.api.retreats.endpoint.events.MemberStatusChangedEvent

@Component
internal class RetreatMemberStatusChangedNotificationEventListener(
  private val membersQueryFacade: MembersQueryFacade,
  private val applicationQueryFacade: ApplicationQueryFacade,
  private val notificationCommandFacade: NotificationCommandFacade
) : ApplicationListener<MemberStatusChangedEvent> {

  private val log = LoggerFactory.getLogger(this.javaClass)

  override fun onApplicationEvent(event: MemberStatusChangedEvent) {
    val email = membersQueryFacade.getEmail(event.memberId)
    val application = applicationQueryFacade.findByRetreatAndMember(event.retreatId, event.memberId)
    if (email != null && application != null) {
      notificationCommandFacade.create(
        CreateNotificationRequest(
          applicationId = application.getId() ?: throw IllegalStateException(),
          reason = determineNotificationReason(event.applicationStatus),
          targetEmail = email
        )
      )
    } else {
      log.error("Cannot send email to ${email} for application ${application?.getId()} on retreat status change")
    }
  }

  fun determineNotificationReason(applicationStatus: ApplicationStatus): NotificationReason =
    when(applicationStatus) {
      ApplicationStatus.REGISTERED -> NotificationReason.APPLICATION_REGISTERED
      ApplicationStatus.WAITING_LIST -> NotificationReason.APPLICATION_ON_WAITING_LIST
      else -> throw IllegalStateException("Cannot determine notification reason")
    }
}