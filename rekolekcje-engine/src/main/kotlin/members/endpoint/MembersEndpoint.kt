package pl.oaza.waw.rekolekcje.api.members.endpoint

import org.springframework.core.io.Resource
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import pl.oaza.waw.rekolekcje.api.API_URI
import pl.oaza.waw.rekolekcje.api.members.endpoint.MembersEndpoint.Companion.MEMBERS_URI
import pl.oaza.waw.rekolekcje.api.members.infrastructure.encryption.MembersEncryptionService
import javax.validation.Valid


/**
 * This is the controller which provides operations to manage members.
 */
@RestController
@RequestMapping(MEMBERS_URI)
@Transactional
class MembersEndpoint internal constructor(
  private val membersFetchService: MembersFetchService,
  private val membersCreateService: MembersCreateService,
  private val membersModificationService: MembersModificationService,
  private val membersEncryptionService: MembersEncryptionService
) {

  companion object {
    const val MEMBERS_URI = "$API_URI/members"
    const val TO_VERIFY = "/verify"
    const val VERIFICATION_STATS = "/verify/stats"
    const val ADD_MANY_URI = "/add-many"
    const val FULL_NAME_URI = "/full-name"
    const val FULL_NAME_ENCRYPTED_URI = "/full-name-encrypted"
  }

  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  fun getAllMembers(): List<MemberSummary> = membersFetchService.getAllMemberSummaries()

  /**
   * Finds single member by id. If member does not exist it returns 404 (Not found).
   *
   * @param id id of member to be found
   * @return [MemberDetails] object
   */
  @GetMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  fun getSingleMember(@PathVariable id: Long): MemberDetails = membersFetchService.getSingleMemberDetails(id)

  @GetMapping("$FULL_NAME_URI/{id}")
  fun getMemberFullName(@PathVariable id: Long): MemberPublicData = membersFetchService.getMemberFullName(id)

  @GetMapping("$FULL_NAME_ENCRYPTED_URI/{encryptedId}")
  fun getMemberFullName(@PathVariable encryptedId: String): MemberPublicData =
    //TODO jakoś po błędzie z tego serwisu przekierować na 404 albo coś
    getMemberFullName(membersEncryptionService.decryptMemberId(encryptedId))

  @GetMapping("/export")
  fun exportAllMembers(): ResponseEntity<Resource> = membersFetchService.exportAllToCsv()

  /**
   * Creates a new member. When provided payload does not pass the validation, the 422
   * (Unprocessable entity) is returned.
   *
   * @param newMemberDetails member to be saved as [MemberDetails] object
   * @return member data in [MemberDetails] with assigned id and 201 (created)
   */
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  fun saveMember(@RequestBody @Valid newMemberDetails: MemberDetails): MemberSummary =
    membersCreateService.createMember(newMemberDetails)

  @PostMapping(ADD_MANY_URI)
  @ResponseStatus(HttpStatus.CREATED)
  fun saveAll(@RequestBody @Valid members: List<MemberDetails>): List<MemberSummary> =
    membersCreateService.createManyMembers(members)

  @PostMapping("/import")
  fun importMembers(@RequestParam file: MultipartFile): List<MemberSummary> =
    membersCreateService.importFromCsv(file)

  /**
   * Deletes single member. Status 404 (Not found) is returned when there is no member
   * with given id.
   *
   * @param id id of member to be deleted
   */
  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  fun deleteSingleMember(@PathVariable id: Long) {
    membersModificationService.deleteMemberById(id)
  }

  /**
   * Updates member data. When provided payload does not pass the validation, the 422
   * (Unprocessable entity) is returned.
   *
   * @param updatedMemberDetails updated member data as [MemberDetails] object
   * @return updated member data as [MemberDetails] and 200 (OK)
   */
  @PutMapping
  @ResponseStatus(HttpStatus.OK)
  fun updateMember(@RequestBody @Valid updatedMemberDetails: MemberDetails): MemberSummary =
    membersModificationService.updateMember(updatedMemberDetails)

  // TODO: uspójnić exception handling
  @ExceptionHandler(IllegalArgumentException::class)
  fun returnBadRequestForIllegalArgumentException(): ResponseEntity<*> {
    return ResponseEntity.badRequest().build<Any>()
  }
}
