package pl.oaza.waw.rekolekcje.api.application

import org.junit.jupiter.api.Test
import org.springframework.security.test.context.support.WithMockUser
import pl.oaza.waw.rekolekcje.api.application.endpoint.VerificationStatisticsDto
import pl.oaza.waw.rekolekcje.api.application.fixture.BaseApplicationAcceptanceTest
import pl.oaza.waw.rekolekcje.api.application.fixture.animatorApplicationSubmissionDto
import pl.oaza.waw.rekolekcje.api.application.fixture.participantApplicationSubmissionDto

internal class ApplicationVerificationStatisticsAcceptanceTest : BaseApplicationAcceptanceTest() {

  @Test
  @WithMockUser
  fun `should calculate verification statistics`() {
    // given
    val firstApplication = animatorApplicationSubmissionDto()
    val secondApplication = participantApplicationSubmissionDto()
    whenInApi.alreadyExistSomeApplications(listOf(firstApplication, secondApplication))

    // when
    val response = whenInApi.verificationStatisticsAreRequested()

    // then
    val expectedStats = VerificationStatisticsDto(
      numberOfActiveApplications = 2,
      alreadyVerified = 0,
      remainingToVerify = 2,
      animatorsRemaining = 1,
      participantsRemaining = 1
    )
    thenInApi.responseHasCorrectStats(response, expectedStats)
  }

}