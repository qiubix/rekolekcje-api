import { NgModule } from '@angular/core';
import { HomeRoutingModule } from './home-routing.module';
import { SharedModule } from '../shared/shared.module';
import { AuthModule } from 'app/auth/auth.module';
import * as fromComponents from './components';

@NgModule({
  imports: [AuthModule, HomeRoutingModule, SharedModule],
  declarations: [fromComponents.components]
})
export class HomeModule {}
