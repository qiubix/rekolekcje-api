import { Component, Input, OnInit } from '@angular/core';
import { ContactInfo } from '@shared/models';

@Component({
  selector: 'reko-certification-details',
  templateUrl: './certification-details.component.html',
  styleUrls: ['./certification-details.component.scss']
})
export class CertificationDetailsComponent implements OnInit {
  @Input() isNeeded: boolean;
  @Input() contact: ContactInfo;

  constructor() {}

  ngOnInit() {}
}
