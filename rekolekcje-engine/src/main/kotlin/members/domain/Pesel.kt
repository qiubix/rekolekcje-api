package pl.oaza.waw.rekolekcje.api.members.domain

import pl.oaza.waw.rekolekcje.api.shared.value.Sex
import java.time.LocalDate
import java.time.Period
import java.time.format.DateTimeFormatter
import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
data class Pesel(
  @Column(name = "PESEL")
  val value: String
) {

  init {
    if (value.length != 11) {
      throw IllegalArgumentException("Invalid PESEL length!")
    }
  }

  val birthDate: String
    get() {
      val year = calculateYearOfBirth()
      val month = calculateMonthOfBirth()
      val day = calculateDayOfBirth()

      return buildBirthDate(day, month, year)
    }

  val sex: Sex
    get() {
      val sexDigit = singleDigit(10)
      return if (sexDigit % 2 == 0) Sex.FEMALE else Sex.MALE
    }

  fun calculateAge(): Int {
    val birthDateConverted = LocalDate.parse(birthDate, DateTimeFormatter.ofPattern("dd.MM.yyyy"))
    return Period.between(birthDateConverted, LocalDate.now()).years
  }

  private fun calculateYearOfBirth(): Int {
    val yearEnding = 10 * singleDigit(1) + singleDigit(2)
    val month = 10 * singleDigit(3) + singleDigit(4)

    return when (month) {
      in 81..92 -> 1800 + yearEnding
      in 1..12 -> 1900 + yearEnding
      in 21..32 -> 2000 + yearEnding
      in 41..52 -> 2100 + yearEnding
      in 61..72 -> 2200 + yearEnding
      else -> throw IllegalArgumentException("Invalid PESEL!")
    }
  }

  private fun calculateMonthOfBirth(): Int {
    val month = 10 * singleDigit(3) + singleDigit(4)

    return when {
      month < 13 -> month
      month in 21..32 -> month - 20
      month in 41..52 -> month - 40
      month in 61..72 -> month - 60
      month in 81..92 -> month - 80
      else -> throw IllegalArgumentException("Invalid PESEL!")
    }
  }

  private fun calculateDayOfBirth(): Int {
    return 10 * singleDigit(5) + singleDigit(6)
  }

  private fun singleDigit(position: Int): Int {
    return Character.getNumericValue(value[position - 1])
  }

  private fun buildBirthDate(day: Int, month: Int, year: Int): String {
    return "${preceedWithZero(day)}.${preceedWithZero(month)}.$year"
  }

  private fun preceedWithZero(dayOrMonth: Int): String {
    return if (dayOrMonth < 10)
      "0$dayOrMonth"
    else
      dayOrMonth.toString()
  }
}
