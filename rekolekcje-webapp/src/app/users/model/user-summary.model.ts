import { UserRole } from './role.model';

export interface UserSummary {
  id?: number;
  enabled: boolean;
  email: string;
  lastPasswordResetDate: Date;
  roles: UserRole[];
  username: string;
}
