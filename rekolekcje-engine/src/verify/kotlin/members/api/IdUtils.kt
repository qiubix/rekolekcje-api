package pl.oaza.waw.rekolekcje.api.members.api

import pl.oaza.waw.rekolekcje.api.members.endpoint.MemberDetails
import pl.oaza.waw.rekolekcje.api.members.endpoint.MemberSummary
import pl.oaza.waw.rekolekcje.api.members.utils.withId


fun assignIdsToDetailsFromMatchingSummaries(
    details: List<MemberDetails>,
    summaries: List<MemberSummary>
): List<MemberDetails> {
  return details.map { detail ->
    val correspondingSummary = summaries.find { summaryCorrespondsToDetails(it, detail) }
      ?: throw IllegalStateException("Cannot find details: $detail")
    detail.withId(correspondingSummary.id)
  }
}

private fun summaryCorrespondsToDetails(summary: MemberSummary, detail: MemberDetails)
    = summary.firstName == detail.personalData.firstName && summary.lastName == detail.personalData.lastName
