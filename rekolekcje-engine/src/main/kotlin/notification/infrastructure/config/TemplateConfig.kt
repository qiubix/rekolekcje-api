package pl.oaza.waw.rekolekcje.api.notification.infrastructure.config

import org.springframework.boot.context.properties.ConfigurationProperties
import pl.oaza.waw.rekolekcje.api.notification.domain.template.Template
import pl.oaza.waw.rekolekcje.api.notification.domain.template.TemplateProvider
import pl.oaza.waw.rekolekcje.api.notification.domain.template.extractor.ExtractorType
import pl.oaza.waw.rekolekcje.api.notification.dto.NotificationReason

//TODO: po podbiciu spring boota do 2.2.0 lub wyżej można użyć @ConstructorBinding
@Suppress("MemberVisibilityCanBePrivate")
@ConfigurationProperties("template")
class TemplateConfig : TemplateProvider {
	var notificationReasonMapping: Map<NotificationReason, TemplateParams> = emptyMap()

	override fun provideTemplate(reason: NotificationReason): Template {
		return notificationReasonMapping.getOrElse(reason) { throw IllegalStateException("Template for reason $reason should be configured") }
				.let { Template(it.subject, it.htmlTemplate, it.extractors) }
	}
}

class TemplateParams {
	var subject: String = ""
	var htmlTemplate: String = ""
	var extractors: Set<ExtractorType> = emptySet()
}
