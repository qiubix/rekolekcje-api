import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerificationStatsComponent } from './verification-stats.component';

describe('VerificationStatsComponent', () => {
  let component: VerificationStatsComponent;
  let fixture: ComponentFixture<VerificationStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VerificationStatsComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificationStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
