package pl.oaza.waw.rekolekcje.api.members.storage

import org.assertj.core.api.Assertions.assertThat
import pl.oaza.waw.rekolekcje.api.members.endpoint.MemberDetails
import java.time.LocalDate

class MembersStorageExpectations(private val database: MembersDatabase) {

  fun numberOfMembersIsEqualTo(quantity: Int) {
    val storedMemberData = database.allMemberData
    assertThat(storedMemberData).hasSize(quantity)

  }

  fun memberNoLongerExists(id: Long) {
    val idsInSystem = database.allMemberData.stream()
        .map { it.id }
    assertThat(idsInSystem).doesNotContain(id)
  }

  fun correctDataIsPersisted(dto: MemberDetails) {
    val persistedData = database.getPersistedData(dto)
    assertThat(persistedData.id).isNotNull()
    assertThat(persistedData.firstName).isEqualTo(dto.personalData.firstName)
    assertThat(persistedData.lastName).isEqualTo(dto.personalData.lastName)
    assertThat(persistedData.pesel).isEqualTo(dto.personalData.pesel)
    compareObjects(persistedData.parishId, dto.personalData.parishId)
    compareDates(dto.personalData.christeningDate, persistedData.christeningDate)
    compareObjects(persistedData.postalCode,dto.personalData.address?.postalCode)
    compareObjects(persistedData.currentTreatment,dto.healthReport?.currentTreatment)
    compareObjects(persistedData.kwcStatus,dto.experience.kwc?.status)
    compareObjects(persistedData.numberOfCommunionDays,dto.experience.formationInCommunity?.numberOfCommunionDays)
  }

  private fun compareDates(actual: LocalDate?, expected: LocalDate?) {
    if (actual == null && expected == null) {
      return
    }
    assertThat(actual).isEqualTo(expected)
  }

  private fun compareObjects(actual: Any?, expected: Any?) {
    if (actual == null && expected == null) {
      return
    }
    assertThat(actual).isEqualTo(expected)
  }

  fun numberOfContacts(): Int {
    return database.getContacts().size
  }

}
