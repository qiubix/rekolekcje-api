package pl.oaza.waw.rekolekcje.api.security

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import pl.oaza.waw.rekolekcje.api.API_URI
import pl.oaza.waw.rekolekcje.api.security.jwt.JwtToken
import pl.oaza.waw.rekolekcje.api.security.jwt.JwtTokenPayload
import pl.oaza.waw.rekolekcje.api.security.jwt.extractToken
import pl.oaza.waw.rekolekcje.api.security.jwt.generateToken
import pl.oaza.waw.rekolekcje.api.user.domain.UserAuthenticationService
import pl.oaza.waw.rekolekcje.api.user.endpoint.AuthenticationRequest
import pl.oaza.waw.rekolekcje.api.user.endpoint.UserSecurityDetails
import pl.oaza.waw.rekolekcje.api.user.query.UserQueryService
import java.time.LocalDateTime
import javax.servlet.http.HttpServletRequest

const val AUTH_URI = "$API_URI/auth"
const val SIGN_IN_URI = "/sign-in"
const val REFRESH_TOKEN_URI = "/refresh"

/**
 * Endpoint for authentication operations.
 */
@RestController
@RequestMapping(AUTH_URI)
internal class AuthenticationRestEndpoint
/**
 * Creates an instance.
 *
 * @param userAuthenticationService service responsible for authentication
 * @param userQueryService service for querying users
 */
@Autowired
constructor(
  private val userAuthenticationService: UserAuthenticationService,
  private val userQueryService: UserQueryService
) {

  private val log = LoggerFactory.getLogger(AuthenticationRestEndpoint::class.java)

  @Value("\${jwt.secret}")
  private lateinit var secret: String

  @Value("\${jwt.expiration}")
  private val expiration: Long = 0

  /**
   * Endpoint for user sign-in.
   *
   * @param authenticationRequest with sign-in details
   * @return [AuthenticationResponse] containing token
   */
  @PostMapping(SIGN_IN_URI)
  fun createAuthenticationToken(
    @RequestBody authenticationRequest: AuthenticationRequest
  ): ResponseEntity<AuthenticationResponse> {
    log.info("Trying to authenticate user {}", authenticationRequest.username)

    // Perform the security
    try {
      authenticateWithUsernameAndPassword(authenticationRequest)
    } catch (ex: BadCredentialsException) {
      return ResponseEntity(HttpStatus.UNAUTHORIZED)
    }

    // Reload password post-security, so we can generate token
    val userDetails = userQueryService.loadSecurityDetails(authenticationRequest.username)

    val token = generateNewSecurityToken(userDetails)

    // Return the token
    return ResponseEntity.ok(AuthenticationResponse(token.token))
  }

  private fun authenticateWithUsernameAndPassword(authenticationRequest: AuthenticationRequest) {
    val authenticationResponse = userAuthenticationService.authenticate(authenticationRequest)
    log.info("User {} is authenticated.", authenticationRequest.username)
    SecurityContextHolder.getContext().authentication =
      UsernamePasswordAuthenticationToken(
        authenticationResponse,
        null,
        authenticationResponse.roles.map { SimpleGrantedAuthority(it.name) }
      )
  }

  private fun generateNewSecurityToken(userDetails: UserSecurityDetails): JwtToken {
    log.info("Generating JWT for user {}", userDetails.username)
    val tokenPayload = JwtTokenPayload(
      username = userDetails.username,
      creationTime = LocalDateTime.now(),
      expirationTime = LocalDateTime.now().plusSeconds(expiration),
      roles = userDetails.roles.map { it.name }
    )
    return generateToken(tokenPayload, secret)
  }

  /**
   * Endpoint for token refresh.
   * When user is already signed-in, he might want to obtain a fresh token, when his existing one
   * is close to expiry date.
   *
   * @param request regular [HttpServletRequest] with token
   * @return [AuthenticationResponse] with a fresh token
   */
  @GetMapping(REFRESH_TOKEN_URI)
  fun refreshAndGetAuthenticationToken(
    request: HttpServletRequest
  ): ResponseEntity<AuthenticationResponse> {
    val token = extractToken(request, secret)
    val username = token.getUsername()
    val user = userQueryService.loadSecurityDetails(username)

    return if (token.canBeRefreshed(user.lastPasswordResetDate ?: throw IllegalStateException("Last password reset date is missing"))) {
      log.info("Refreshing token for user: {}", username)
      val refreshedToken = token.refresh(expiration)
      ResponseEntity.ok(AuthenticationResponse(refreshedToken.token))
    } else {
      ResponseEntity.badRequest().body(null)
    }
  }
}
