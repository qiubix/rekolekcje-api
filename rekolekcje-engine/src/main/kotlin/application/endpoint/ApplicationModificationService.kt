package pl.oaza.waw.rekolekcje.api.application.endpoint

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationCommandFacade
import pl.oaza.waw.rekolekcje.api.application.domain.AssignAnimator
import pl.oaza.waw.rekolekcje.api.application.domain.ChangeApplicationStatus
import pl.oaza.waw.rekolekcje.api.application.domain.RetreatApplication
import pl.oaza.waw.rekolekcje.api.members.endpoint.MembersFetchService
import pl.oaza.waw.rekolekcje.api.members.endpoint.MembersModificationService
import pl.oaza.waw.rekolekcje.api.members.endpoint.toDetailsDto
import pl.oaza.waw.rekolekcje.api.members.query.MembersQueryFacade
import pl.oaza.waw.rekolekcje.api.shared.exceptions.MemberNotFoundException

@Service
@Transactional
internal class ApplicationModificationService(
  private val applicationCommandFacade: ApplicationCommandFacade,
  private val membersFetchService: MembersFetchService,
  private val membersModificationService: MembersModificationService
) {

  fun changeApplicationStatus(request: ChangeApplicationStatusRequest): ChangeApplicationStatusResponse {
    val applicationStatus = applicationCommandFacade.changeApplicationStatus(
      ChangeApplicationStatus(
        id = request.id,
        status = request.status,
        reason = request.reason
      )
    )
    return ChangeApplicationStatusResponse(applicationStatus)
  }

  fun changeApplicationPaymentStatus(request: ChangePaymentStatusRequest): ChangePaymentStatusResponse {
    val paymentStatus = applicationCommandFacade.changePaymentStatus(request.applicationId, request.paymentStatus)
    return ChangePaymentStatusResponse(paymentStatus)
  }

  fun assignAnimator(request: AssignAnimatorRequest): AssignAnimatorResponse {
    applicationCommandFacade.assignAnimator(
        AssignAnimator(request.applicationId, request.selectedRetreat, request.selectedRole)
    )
    return AssignAnimatorResponse(request.selectedRetreat, request.selectedRole)
  }

  fun updateSubmission(id: Long, submissionDto: ApplicationSubmissionDto): RetreatApplicationDto {
    val updatedMember = membersModificationService.updateMember(submissionDto.memberDetails)

    val applicationToUpdate = submissionDto.toDomainEntity(
      memberId = updatedMember.id,
      firstName = updatedMember.firstName,
      lastName = updatedMember.lastName
    ).withId(id) as RetreatApplication

    val updatedApplication = applicationCommandFacade.updateApplication(applicationToUpdate)
    val memberDetails = membersFetchService.getSingleMemberDetails(updatedMember.id)

    return updatedApplication.toDto(memberDetails)
  }
}