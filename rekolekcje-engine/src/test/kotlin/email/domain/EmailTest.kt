package pl.oaza.waw.rekolekcje.api.email.domain

import io.mockk.Runs
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import org.junit.jupiter.api.BeforeEach
import org.springframework.mail.SimpleMailMessage
import org.springframework.mail.javamail.JavaMailSenderImpl
import org.springframework.test.util.ReflectionTestUtils
import org.thymeleaf.spring5.SpringTemplateEngine

abstract class EmailTest {

  private val from = "from@mail.com"

  protected val javaMailSenderMock = mockk<JavaMailSenderImpl>()
  protected val templateEngineMock = mockk<SpringTemplateEngine>()

  private val emailService = EmailConfiguration().emailService(javaMailSenderMock, templateEngineMock)

  protected val emailCommandFacade = EmailConfiguration().emailCommandFacade(emailService)

  @BeforeEach
  internal fun setUp() {
    ReflectionTestUtils.setField(emailService, "from", from)
    every { javaMailSenderMock.send(any<SimpleMailMessage>()) } just Runs
  }

  fun buildSimpleMailMessage(to: String, subject: String, body: String): SimpleMailMessage {
    val message = SimpleMailMessage()
    message.setFrom(from)
    message.setTo(to)
    message.setSubject(subject)
    message.setText(body)
    return message
  }
}
