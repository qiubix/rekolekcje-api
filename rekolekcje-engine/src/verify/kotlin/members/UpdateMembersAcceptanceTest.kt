package pl.oaza.waw.rekolekcje.api.members

import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.MethodSource
import org.springframework.security.test.context.support.WithMockUser
import pl.oaza.waw.rekolekcje.api.members.endpoint.MemberDetails
import pl.oaza.waw.rekolekcje.api.members.endpoint.MemberSummary
import pl.oaza.waw.rekolekcje.api.members.utils.getAgeFromBirthDate
import pl.oaza.waw.rekolekcje.api.members.utils.memberWithUpdatedData
import pl.oaza.waw.rekolekcje.api.members.utils.withEmergencyContactId
import pl.oaza.waw.rekolekcje.api.members.utils.withFatherContactId
import pl.oaza.waw.rekolekcje.api.members.utils.withMotherContactId
import pl.oaza.waw.rekolekcje.api.members.endpoint.PersonalDataValue
import pl.oaza.waw.rekolekcje.api.shared.value.Sex

class UpdateMembersAcceptanceTest : BaseMembersAcceptanceTest() {

  @Test
  @WithMockUser
  fun `should return member's summary with new data when single one is updated`() {
    // given
    whenInApi.singleIsAdded(sampleMember)
    val id = whenInApi.findIdOfElementWithTheSameData(sampleMember)

    // when
    val memberWithNewData = memberWithUpdatedData(id)
    val response = whenInApi.singleIsUpdated(memberWithNewData)

    // then
    response.hasStatusOk()
    thenInApi.responseHasSummaryDataMatchingDetails(response, memberWithNewData)
  }

  @Test
  @WithMockUser
  fun `should update persisted data when single member is updated`() {
    // given
    whenInApi.singleIsAdded(memberWithFullData)
    val existingMember = whenInApi.existsElementWithTheSameData(memberWithFullData)

    // when
    val memberWithNewData = memberWithUpdatedData(existingMember.id)
      .withFatherContactId(existingMember.personalData.father?.id)
      .withMotherContactId(existingMember.personalData.mother?.id)
      .withEmergencyContactId(existingMember.personalData.emergencyContact?.id)
    whenInApi.singleIsUpdated(memberWithNewData)

    // then
    thenInApi.thereIsAnExactNumberOfEntities(1)
    thenInApi.firstIsEqualTo(memberWithNewData)
  }

  @Test
  @WithMockUser
  fun `should extract information from PESEL when updating member`() {
    // given
    val member = MemberDetails(
      personalData = PersonalDataValue(
        firstName = "New",
        lastName = "Fish",
        pesel = "93020573638"
      )
    )
    val id = whenInApi.singleIsAdded(member)
      .getContent<MemberSummary>()
      .id
    val updatedMember = MemberDetails(
      id = id,
      personalData = PersonalDataValue(
        firstName = "Fresh",
        lastName = "Member",
        pesel = "92062172887",
        birthDate = "21.06.1992",
        sex = Sex.FEMALE,
        age = getAgeFromBirthDate("1992-06-21")
      )
    )

    // when
    val response = whenInApi.singleIsUpdated(updatedMember)

    // then
    thenInApi.responseHasSummaryDataMatchingDetails(response, updatedMember)
    thenInApi.exists(updatedMember)
  }

  @ParameterizedTest(name = "{index}: invalid {1}")
  @MethodSource("invalidMembers")
  @WithMockUser
  fun `should not update member with invalid data`(
      invalidPayload: MemberDetails,
      expectedErrorCode: String
  ) {
    // when
    val response = whenInApi.singleIsUpdated(invalidPayload)

    // then
    thenInApi.responseIsUnprocessableEntityAndHasCode(response, expectedErrorCode)
  }
}
