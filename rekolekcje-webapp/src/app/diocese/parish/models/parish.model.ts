export interface Parish {
  id?: number;
  name?: string;
  address?: string;
}
