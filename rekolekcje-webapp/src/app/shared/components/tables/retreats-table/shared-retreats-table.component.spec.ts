import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedRetreatsTableComponent } from './shared-retreats-table.component';

describe('SharedRetreatsTableComponent', () => {
  let component: SharedRetreatsTableComponent;
  let fixture: ComponentFixture<SharedRetreatsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SharedRetreatsTableComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedRetreatsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
