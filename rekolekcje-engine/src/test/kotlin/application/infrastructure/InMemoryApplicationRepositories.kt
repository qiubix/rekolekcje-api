package pl.oaza.waw.rekolekcje.api.application.infrastructure

import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationReadRepository
import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationWriteRepository
import pl.oaza.waw.rekolekcje.api.application.domain.RetreatApplication
import pl.oaza.waw.rekolekcje.api.tools.repository.InMemoryReadRepository
import pl.oaza.waw.rekolekcje.api.tools.repository.InMemoryWriteRepository

class InMemoryApplicationReadRepository(storage: HashMap<Long, RetreatApplication>)
  : InMemoryReadRepository<RetreatApplication>(storage), ApplicationReadRepository

class InMemoryApplicationWriteRepository(storage: HashMap<Long, RetreatApplication>)
  : InMemoryWriteRepository<RetreatApplication>(storage), ApplicationWriteRepository
