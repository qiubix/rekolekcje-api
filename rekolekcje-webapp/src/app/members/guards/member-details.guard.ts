import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Members } from '../state/members-reducer';
import { Observable, of } from 'rxjs';
import { AppSelectors } from '../../core/store/app-selectors';
import { catchError, filter, switchMap, take, tap } from 'rxjs/operators';
import { MembersSharedActions } from '../state/members-actions';
import { MemberDetails } from '@shared/models/member-details.model';

@Injectable({ providedIn: 'root' })
export class MemberDetailsGuard implements CanActivate {
  constructor(private store: Store<Members.State>) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    const memberId = +next.params['id'];
    return this.getFromStoreOrAPI(memberId).pipe(
      switchMap(() => of(true)),
      catchError(() => of(false))
    );
  }

  getFromStoreOrAPI(id: number): Observable<any> {
    return this.store.pipe(
      select(AppSelectors.getSelectedMember),
      tap((member: MemberDetails) => {
        if (!member || member.id !== id) {
          this.store.dispatch(new MembersSharedActions.SelectMemberById(id));
        }
      }),
      filter((data: MemberDetails) => data && data.id !== null && data.id === id),
      take(1)
    );
  }
}
