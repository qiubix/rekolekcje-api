package pl.oaza.waw.rekolekcje.api.members

import org.junit.jupiter.api.AfterEach
import pl.oaza.waw.rekolekcje.api.members.domain.Member
import pl.oaza.waw.rekolekcje.api.members.domain.MembersCommandFacade
import pl.oaza.waw.rekolekcje.api.members.infrastructure.InMemoryMemberReadRepository
import pl.oaza.waw.rekolekcje.api.members.infrastructure.InMemoryMemberWriteRepository
import pl.oaza.waw.rekolekcje.api.members.infrastructure.MembersConfiguration
import pl.oaza.waw.rekolekcje.api.members.query.MembersQueryFacade

abstract class MemberTestFixture {

  private val storage = HashMap<Long, Member>()
  private val memberWriteRepository = InMemoryMemberWriteRepository(storage)
  private val memberReadRepository = InMemoryMemberReadRepository(storage)

  private val membersConfiguration = MembersConfiguration()

  protected val queryFacade: MembersQueryFacade = membersConfiguration.membersQueryFacade(memberReadRepository)
  protected val commandFacade: MembersCommandFacade =
    membersConfiguration.membersCommandsFacade(memberReadRepository, memberWriteRepository)

  @AfterEach
  fun tearDown() {
    deleteAllMembers()
  }

  private fun deleteAllMembers() = storage.clear()

  protected fun getIdOfCorrespondingMemberFromSystem(member: Member): Long {
    return queryFacade.findAllMembers()
      .find { it.personalData.pesel == member.personalData.pesel }?.getId()
      ?: throw IllegalStateException("Member not found")
  }
}