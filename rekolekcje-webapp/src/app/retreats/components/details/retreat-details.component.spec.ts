import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetreatDetailsComponent } from './retreat-details.component';

describe('RetreatDetailsComponent', () => {
  let component: RetreatDetailsComponent;
  let fixture: ComponentFixture<RetreatDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RetreatDetailsComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetreatDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
