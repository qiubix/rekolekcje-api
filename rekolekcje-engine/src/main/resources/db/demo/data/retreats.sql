INSERT INTO retreats (retreat_id, turn, stage, localization, start_date, end_date, minimum_animators_required)
VALUES (1, 'I', 'ODB', 'Warsaw', '2017-07-31', '2017-08-15', 0),
       (2, 'II', 'OND', 'Shire', '2017-04-29', '2017-05-12', 0),
       (3, 'III', 'ONZ_I', 'Rajcza', '2017-04-29', '2017-05-12', 0),
       (4, 'IV', 'ONZ_II', 'Ochotnica', '2017-04-29', '2017-05-12', 0),
       (5, 'I', 'ONZ_III', 'Krościenko', '2017-04-29', '2017-05-12', 0),
       (6, 'II', 'OR_I', 'Mordor', '2017-04-29', '2017-05-12', 0),
       (7, 'III', 'OR_II', 'Gotham', '2018-05-30', '2017-06-10', 0),
       (8, 'IV', 'OR_III', 'San Andreas', '2018-05-30', '2017-06-10', 0),
       (9, 'II', 'ORAE', 'Lorien', '2018-05-30', '2017-06-10', 0),
       (10, 'II', 'ORD', 'New York', '2018-05-30', '2017-06-10', 0),
       (11, 'III', 'Triduum', 'Rzym', '2019-08-12', '2019-08-24', 0);
