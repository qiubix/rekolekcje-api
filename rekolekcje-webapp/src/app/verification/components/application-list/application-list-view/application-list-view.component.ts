import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { environment } from '@env';
import { Config } from '@config';
import { ApplicationSummary } from '@verification/model';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'reko-application-list-view',
  templateUrl: './application-list-view.component.html',
  styleUrls: ['./application-list-view.component.scss'],
})
export class ApplicationListViewComponent implements OnInit {
  private _applications: ApplicationSummary[] = [];

  readonly exportUrl = environment.apiUrl + '/members/export';
  readonly defaultFileName = Config.defaultExportFileName;

  @Input() title: string;
  @Input() displayedColumns: string[];
  @Input() dataLoading: boolean = false;
  @Input() addNewEnabled: boolean = false;

  @Input()
  set applications(applications: ApplicationSummary[]) {
    this._applications = applications;
    this.dataSource.data = applications;
    this.cd.detectChanges();
  }

  @Output() add: EventEmitter<void> = new EventEmitter<void>();
  @Output() delete: EventEmitter<number> = new EventEmitter<number>();
  @Output() edit: EventEmitter<ApplicationSummary> = new EventEmitter<ApplicationSummary>();
  @Output() select: EventEmitter<ApplicationSummary> = new EventEmitter<ApplicationSummary>();
  @Output() import: EventEmitter<void> = new EventEmitter<void>();
  @Output() import2: EventEmitter<void> = new EventEmitter<void>();

  get applications(): ApplicationSummary[] {
    return this._applications;
  }

  dataSource = new MatTableDataSource<ApplicationSummary>();

  constructor(private cd: ChangeDetectorRef) {}

  ngOnInit() {}

  onFilterChange(newData: ApplicationSummary[]) {
    this.dataSource.data = newData;
  }

  onAddNew() {
    this.add.emit();
  }

  onEdit(application: ApplicationSummary) {
    this.edit.emit(application);
  }

  onDelete(application: ApplicationSummary) {
    this.delete.emit(application.id);
  }

  onSelect(application: ApplicationSummary) {
    // console.log('Animator application selected: ', application);
    this.select.emit(application);
  }

  onImport() {
    this.import.emit();
  }

  onImport2() {
    this.import2.emit();
  }
}
