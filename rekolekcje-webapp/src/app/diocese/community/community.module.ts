import { NgModule } from '@angular/core';
import { SharedModule } from '../../shared/shared.module';
import { CommunityRoutingModule } from './community-routing.module';
import { CommunityComponent, CommunityDialogComponent } from './components';
import { CommunityGuard } from './community.guard';
import { CommunityService } from './community.service';
import * as fromComponents from './components';

@NgModule({
  imports: [SharedModule, CommunityRoutingModule],
  declarations: [fromComponents.components],
  providers: [CommunityGuard, CommunityService],
  entryComponents: [CommunityDialogComponent],
  exports: [CommunityComponent]
})
export class CommunityModule {}
