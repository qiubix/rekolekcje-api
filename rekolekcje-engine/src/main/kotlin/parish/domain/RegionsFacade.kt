package pl.oaza.waw.rekolekcje.api.parish.domain

import pl.oaza.waw.rekolekcje.api.core.ddd.ApplicationService
import pl.oaza.waw.rekolekcje.api.parish.dto.RegionNotFoundException
import pl.oaza.waw.rekolekcje.api.parish.dto.RegionPayload
import java.util.Objects.requireNonNull

/**
 * Class provides methods to manage regions.
 */
@ApplicationService
open class RegionsFacade internal constructor(private val regionsRepository: RegionsRepository) {

  /**
   * Create list of all regions.
   *
   * @return regions as a list of [RegionPayload]
   */
  fun findAll(): List<RegionPayload> {
    return regionsRepository.findAll().map { it.dto() }
  }

  /**
   * Save Region data (aka "add or update"). Regions with `id == 0` or `null`
   * are added and assigned next ID. Regions with id found in the repository are
   * updated. If no region with such ID found, exception is thrown
   *
   * @param data region's data
   * @return regions data with assigned ID, if `data.getId() == null`; object equal to
   * `data` otherwise.
   */
  fun save(data: RegionPayload): RegionPayload {
    requireNonNull(data)
    var region = Region(data.id, data.name)
    region = regionsRepository.save(region)
    return region.dto()
  }

  /**
   * Find one region by id
   *
   * @param byId ID of region to retrieve
   * @return region from the repository with `.getId() == byId`
   * @throws RegionNotFoundException if region was not found.
   */
  fun findOne(byId: Long): RegionPayload {
    val found = regionsRepository.findById(byId)
    if (found != null) {
      return found.dto()
    }
    throw RegionNotFoundException(byId)
  }

  /**
   * Delete all regions with id that occur in `ids`.
   *
   * @param ids list of IDs assigned with to-be-deleted regions
   */
  fun delete(vararg ids: Long) {
    requireNonNull(ids)
    ids.forEach { regionsRepository.deleteById(it) }
  }
}
