import com.moowork.gradle.node.npm.NpmTask

plugins {
  id("com.github.node-gradle.node") version "2.2.4"
}

// configure gradle-node-plugin
node {
  version = "16.18.0"
  npmVersion = "8.19.2"
  download = true
  workDir = file("${project.projectDir}/node")
  nodeModulesDir = file("${project.projectDir}/")
}

tasks {

  // clean node/node_modules/dist
  create<Delete>("npmClean") {
    val webDir = "${rootDir}/rekolekcje-webapp"
    delete("${webDir}/node")
    delete("${webDir}/node_modules")
    delete("${webDir}/dist")
    delete("${webDir}/coverage")
  }

  create<NpmTask>("npmCi") {
    dependsOn(npmSetup)
    setNpmCommand("ci")
    setArgs(listOf("--cache", ".npm", "--prefer-offline"))
    inputs.file("package.json")
    inputs.file("package-lock.json")
    outputs.dir("node_modules")
  }

  create<NpmTask>("buildClient") {
    dependsOn("npm_install")
    group = "build"
    description = "Compile client side folder for production"
    setArgs(listOf("run","build"))
  }

  create<NpmTask>("buildClientDev") {
    dependsOn("npmCi")
    group = "build"
    description = "Compile client side folder for development"
    setArgs(listOf("run","buildDev"))
  }

  create<NpmTask>("buildClientWatch") {
    dependsOn("npmCi")
    group = "application"
    description = "Build and watches the client side assets for rebuilding"
    setArgs(listOf("run", "buildWatch"))
  }
}



