import { Injectable } from '@angular/core';
import {
  CanActivate,
  CanActivateChild,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router
} from '@angular/router';
import { AuthService } from '../auth.service';

@Injectable({ providedIn: 'root' })
export class HasCorrectRoleGuard implements CanActivate, CanActivateChild {
  constructor(private authService: AuthService, private router: Router) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const currentUserRoles: string[] = this.authService.getCurrentUserRoles();
    const url: string = state.url;
    const domain = url.split('/').filter(it => it.length > 0)[0];
    if (currentUserRoles.includes('ADMIN')) {
      return true;
    }
    switch (domain) {
      case 'home':
      case 'registration':
        return true;
      case 'retreats':
      case 'moderation':
        return currentUserRoles.includes('DOR') || currentUserRoles.includes('MODERATOR');
      case 'verification':
        return currentUserRoles.includes('DOR');
    }

    this.router.navigate(['home']);
    return false;
  }

  canActivateChild(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.canActivate(next, state);
  }
}
