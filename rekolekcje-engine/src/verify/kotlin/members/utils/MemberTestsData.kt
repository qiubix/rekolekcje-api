package pl.oaza.waw.rekolekcje.api.members.utils

import pl.oaza.waw.rekolekcje.api.members.domain.KwcStatus
import pl.oaza.waw.rekolekcje.api.members.domain.PersonalData.SchoolType
import pl.oaza.waw.rekolekcje.api.members.endpoint.AddressValue
import pl.oaza.waw.rekolekcje.api.members.endpoint.ContactInfo
import pl.oaza.waw.rekolekcje.api.members.endpoint.DeuterocatechumenateValue
import pl.oaza.waw.rekolekcje.api.members.endpoint.ExperienceAsAnimatorValue
import pl.oaza.waw.rekolekcje.api.members.endpoint.ExperienceValue
import pl.oaza.waw.rekolekcje.api.members.endpoint.FormationInCommunityValue
import pl.oaza.waw.rekolekcje.api.members.endpoint.HealthReportValue
import pl.oaza.waw.rekolekcje.api.members.endpoint.KwcDto
import pl.oaza.waw.rekolekcje.api.members.endpoint.MemberDetails
import pl.oaza.waw.rekolekcje.api.members.endpoint.MemberSummary
import pl.oaza.waw.rekolekcje.api.members.endpoint.ParishDto
import pl.oaza.waw.rekolekcje.api.members.endpoint.PersonalDataValue
import pl.oaza.waw.rekolekcje.api.members.endpoint.RetreatTurnDto
import pl.oaza.waw.rekolekcje.api.members.endpoint.TalentsValue
import pl.oaza.waw.rekolekcje.api.shared.value.Sex
import pl.oaza.waw.rekolekcje.api.shared.value.Stage
import java.time.LocalDate
import java.time.Period


fun memberWithMinimalData(): MemberDetails = MemberDetails(
  personalData = PersonalDataValue(
    firstName = "Minimal",
    lastName = "Participant",
    pesel = "92042312348",
    birthDate = "23.04.1992",
    sex = Sex.FEMALE,
    age = getAgeFromBirthDate("1992-04-23"),
    parishId = 1L
  )
)

fun memberWithSampleData(
  firstName: String = "Sample",
  lastName: String = "Participant",
  pesel: String = "98101012349",
  sex: Sex? = Sex.FEMALE,
  birthDate: String? = "10.10.1998",
  age: Int? = getAgeFromBirthDate("1998-10-10"),
  email: String? = "a@test.com"
): MemberDetails {
  return MemberDetails(
    personalData = PersonalDataValue(
      firstName = firstName,
      lastName = lastName,
      pesel = pesel,
      sex = sex,
      age = age,
      birthDate = birthDate,
      email = email,
      christeningDate = LocalDate.of(2000, 1, 2)
    ),
    experience = ExperienceValue(
      kwc = KwcDto(
        status = KwcStatus.CANDIDATE
      )
    ),
    healthReport = HealthReportValue(
      currentTreatment = "Broken leg"
    )
  )
}

fun memberWithFullData(): MemberDetails {
  return MemberDetails(
    personalData = PersonalDataValue(
      firstName = "John",
      lastName = "Smith",
      pesel = "90042300004",
      parishId = 1L,
      communityId = 2L,
      sex = Sex.FEMALE,
      email = "john.smith@mail.com",
      phoneNumber = "998877665",
      address = AddressValue(
        streetName = "Broadway",
        streetNumber = "987/989",
        flatNumber = "13c",
        postalCode = "12-654",
        city = "New York"
      ),
      mother = ContactInfo(fullName = "Mary Smith"),
      father = ContactInfo(fullName = "Jake Smith Sr."),
      birthDate = "23.04.1990",
      age = getAgeFromBirthDate("1990-04-23"),
      placeOfBirth = "Los Angeles",
      christeningDate = LocalDate.of(1981, 2, 13),
      emergencyContact = ContactInfo(
        fullName = "Uncle Bob",
        phoneNumber = "444555666"
      ),
      schoolYear = "1 LO",
      schoolType = SchoolType.HIGH_SCHOOL,
      nameDay = "24.04"
    ),
    experience = ExperienceValue(
      kwc = KwcDto(
        status = KwcStatus.CANDIDATE,
        year = 1995
      ),
      formationInCommunity = FormationInCommunityValue(
        numberOfCommunionDays = 3,
        numberOfPrayerRetreats = 5,
        formationInSmallGroup = true,
        formationMeetingsInMonth = 3
      ),
      animator = ContactInfo(fullName = "Jason Animator"),
      deuterocatechumenate = DeuterocatechumenateValue(
        year = 2016,
        celebrationsTaken = 5,
        celebrationsPlannedThisYear = 5,
        stepsTaken = 4,
        stepsPlannedThisYear = 6,
        missionCelebrationYear = 2016,
        callByNameCelebrationYear = 2017,
        holyTriduumYear = 2017
      ),
      talents = TalentsValue(
        choir = false,
        musicalTalents = "none so far",
        altarBoy = true,
        flowerGirl = false
      ),
      experienceAsAnimator = ExperienceAsAnimatorValue(
        numberOfAnimatorDays = 0,
        diakonia = "DOR",
        leadingGroupToFormationStage = "ONŻ I"
      ),
      historicalRetreats = hashSetOf(
        RetreatTurnDto(stage = Stage.ONZ_I, year = 2013),
        RetreatTurnDto(stage = Stage.ODB, year = 2012)
      )
    ),
    healthReport = HealthReportValue(
      currentTreatment = "Standard treatment for diabetes",
      medications = "Insuline",
      allergies = "Peanuts, lactose",
      cannotHike = true,
      hasMotionSickness = true,
      hikingContraindications = "Hib implant injected",
      mentalDisorders = "None",
      other = "May be very weird sometimes"
    )
  )
}

fun memberWithUpdatedData(id: Long?): MemberDetails {
  return MemberDetails(
    id = id,
    personalData = PersonalDataValue(
      firstName = "Updated name",
      lastName = "Updated surname",
      pesel = "89112215597",
      parishId = 2L,
      address = AddressValue(
        streetName = "TD Garden",
        streetNumber = "43",
        flatNumber = "1a",
        postalCode = "32-112",
        city = "Boston"
      ),
      mother = ContactInfo(fullName = "Updated mother name and surname"),
      father = ContactInfo(fullName = "Updated father name and surname"),
      birthDate = "22.11.1989",
      age = getAgeFromBirthDate("1989-11-22"),
      sex = Sex.MALE,
      placeOfBirth = "Updated place of birth",
      christeningDate = LocalDate.of(1989, 11, 29),
      emergencyContact = ContactInfo(
        fullName = "Updated emergency contact name and surname",
        phoneNumber = "999888777",
        email = "some@email.com"
      )
    ),
    experience = ExperienceValue(
      kwc = KwcDto(
        status = KwcStatus.MEMBER,
        year = 1999
      ),
      formationInCommunity = FormationInCommunityValue(
        numberOfCommunionDays = 2,
        numberOfPrayerRetreats = 4,
        formationMeetingsInMonth = 2
      ),
      deuterocatechumenate = DeuterocatechumenateValue(
        celebrationsTaken = 10,
        celebrationsPlannedThisYear = 0,
        stepsTaken = 5,
        stepsPlannedThisYear = 2,
        year = 2013
      ),
      experienceAsAnimator = ExperienceAsAnimatorValue(
        leadingGroupToFormationStage = "ODB"
      )
    ),
    healthReport = HealthReportValue(
      currentTreatment = "None",
      medications = "Gripex",
      allergies = "Milk",
      cannotHike = false,
      hasMotionSickness = true,
      hikingContraindications = "Hib implant injected",
      mentalDisorders = "None",
      other = "Unstable"
    )
  )
}

fun getAgeFromBirthDate(isoBirthDate: String) =
  Period.between(LocalDate.parse(isoBirthDate), LocalDate.now()).years

fun MemberDetails.withId(id: Long) =
  this.copy(id = id)

fun MemberDetails.withFatherContactId(id: Long?) =
  this.copy(
    personalData = this.personalData.copy(
      father = this.personalData.father?.copy(
        id = id
      )
    )
  )

fun MemberDetails.withMotherContactId(id: Long?) =
  this.copy(
    personalData = this.personalData.copy(
      mother = this.personalData.mother?.copy(
        id = id
      )
    )
  )

fun MemberDetails.withEmergencyContactId(id: Long?) =
  this.copy(
    personalData = this.personalData.copy(
      emergencyContact = this.personalData.emergencyContact?.copy(
        id = id
      )
    )
  )

fun MemberDetails.withEmail(email: String) =
  this.copy(
    personalData = this.personalData.copy(email = email)
  )

fun MemberDetails.withPesel(pesel: String) =
  this.copy(
    personalData = this.personalData.copy(pesel = pesel)
  )

fun MemberSummary.withAge(age: Int) = this.copy(age = age)

fun MemberDetails.withAge(age: Int) =
  this.copy(
    personalData = this.personalData.copy(age = age)
  )

fun MemberDetails.withPhoneNumber(phoneNumber: String) =
  this.copy(
    personalData = this.personalData.copy(phoneNumber = phoneNumber)
  )

fun MemberDetails.withNonExistingParish() =
  this.copy(
    personalData = this.personalData.copy(
      newParish = ParishDto(
        name = "New parish",
        address = "New address"
      )
    )
  )

fun MemberDetails.withInvalidParishState() =
  this.copy(
    personalData = this.personalData.copy(
      parishId = 101,
      newParish = ParishDto(
        name = "New parish",
        address = "New address"
      )
    )
  )

fun MemberDetails.withHistoricalRetreats(retreats: Set<RetreatTurnDto>) =
  this.copy(
    experience = this.experience.copy(
      historicalRetreats = retreats
    )
  )
