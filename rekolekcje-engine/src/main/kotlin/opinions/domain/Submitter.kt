package pl.oaza.waw.rekolekcje.api.opinions.domain

import javax.persistence.Column
import javax.persistence.Embeddable
import javax.persistence.EnumType
import javax.persistence.Enumerated

@Embeddable
data class Submitter(
    @Column(name = "SUBMITTER_FULL_NAME")
    val fullName: String,

    @Column(name = "SUBMITTER_EMAIL")
    val email: String,

    @Column(name = "SUBMITTER_PHONE_NUMBER")
    val phoneNumber: String,

    @Column(name = "SUBMITTER_ROLE")
    @Enumerated(EnumType.STRING)
    val role: Role
) {
  enum class Role {
    MODERATOR,
    ANIMATOR,
    OTHER
  }
}