package pl.oaza.waw.rekolekcje.api.user

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.test.context.support.WithAnonymousUser
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import pl.oaza.waw.rekolekcje.api.shared.BaseIntegrationTest
import pl.oaza.waw.rekolekcje.api.user.api.UserApiBehaviour
import pl.oaza.waw.rekolekcje.api.user.api.UserApiExpectations
import pl.oaza.waw.rekolekcje.api.user.endpoint.ChangePasswordRequest
import pl.oaza.waw.rekolekcje.api.user.endpoint.SignUpRequest
import pl.oaza.waw.rekolekcje.api.user.endpoint.UpdateUserRequest
import pl.oaza.waw.rekolekcje.api.user.endpoint.UserRole
import pl.oaza.waw.rekolekcje.api.user.endpoint.UserSummary
import pl.oaza.waw.rekolekcje.api.user.storage.UserStorage
import pl.oaza.waw.rekolekcje.api.user.storage.UserStorageExpectations

internal class UserAcceptanceTest : BaseIntegrationTest() {

  @Autowired
  private lateinit var whenInUserApi: UserApiBehaviour
  @Autowired
  private lateinit var thenInUserApi: UserApiExpectations
  @Autowired
  private lateinit var thenInUserStorage: UserStorageExpectations

  @Autowired
  private lateinit var userStorage: UserStorage

  private val username = "testuser"
  private val password = "testpassword"
  private val email = "default@email.com"
  private val signUpRequest = SignUpRequest(username, password, email)

  @BeforeEach
  fun setup() {
    userStorage.deleteUser(username)
  }

  @Test
  @WithAnonymousUser
  fun `as an anonymous user I can register new user`() {
    // given admin user is the current user
    val signUpPayload = signUpRequest

    // when registering a new user
    whenInUserApi.newUserIsRegistered(signUpPayload)

    // then new user is added to the list of users
    thenInUserApi.existsUserWithUsername(username)
  }

  @Test
  @WithMockUser(roles = ["ADMIN"])
  fun `as an admin I cannot register a new user with existing username`() {
    // given authorized user
    val existingUsername = "admin"
    val signUpPayload = SignUpRequest(existingUsername, password, email)

    // when trying to register user with existing username
    val response = whenInUserApi.newUserIsRegistered(signUpPayload)

    // then registration fails and no user is added to the system
    response.andExpect(MockMvcResultMatchers.status().isBadRequest)
    thenInUserApi.thereIsOnlyOneUserWithUsername(existingUsername)
  }

  @Test
  @WithMockUser(roles = ["ADMIN"])
  fun `as admin I can update user`() {
    // given
    whenInUserApi.newUserIsRegistered(signUpRequest)
    val numberOfUsers = thenInUserStorage.numberOfExistingUsers()

    // when
    val userUpdateRequest = UpdateUserRequest(
      username = username,
      email = email,
      enabled = false,
      roles = setOf(UserRole.ROLE_DOR, UserRole.ROLE_MODERATOR)
    )
    val response = whenInUserApi.userIsUpdated(userUpdateRequest)

    // then
    response.hasStatusOk()
    thenInUserStorage.isCorrectNumberOfUsers(numberOfUsers)
    thenInUserStorage.correctUserIsPersisted(userUpdateRequest)
  }

  @Test
  @WithMockUser(roles = ["ADMIN"])
  fun `as admin I can change user's password`() {
    // given
    whenInUserApi.newUserIsRegistered(signUpRequest)

    // when
    val overridePasswordRequest = ChangePasswordRequest(
      username = username,
      newPassword = "new-password",
      admin = true
    )
    val response = whenInUserApi.passwordIsChanged(overridePasswordRequest)

    // then
    response.hasStatusOk()
    thenInUserStorage.userHasCorrectPassword(username, overridePasswordRequest.newPassword)
  }

  @Test
  @WithMockUser(roles = ["ADMIN"])
  fun `as an admin I can obtain a list of user summaries`() {
    // given
    whenInUserApi.newUserIsRegistered(SignUpRequest("firstUser", "pass", "first@email.com"))
    whenInUserApi.newUserIsRegistered(SignUpRequest("secondUser", "pass", "second@email.com"))

    // when
    val response = whenInUserApi.allAreRequested()

    // then
    response.hasStatusOk()
    val fetchedUsernames = response.getContentAsList<UserSummary>().map { it.username }
    assertThat(fetchedUsernames).contains("firstUser", "secondUser")
  }

  @Test
  @WithMockUser(roles = ["ADMIN"])
  fun `as an admin I can delete user`() {
    // given
    whenInUserApi.newUserIsRegistered(signUpRequest)
    val numberOfUsers = thenInUserStorage.numberOfExistingUsers()

    // when
    val response = whenInUserApi.userIsDeleted(username)

    // then
    response.hasStatusOk()
    thenInUserStorage.isCorrectNumberOfUsers(numberOfUsers - 1)
    thenInUserApi.userNoLongerExists(username)
  }

  @Test
  @WithMockUser(roles = ["ADMIN"])
  fun `as an admin I can disable user`() {
    // given
    whenInUserApi.newUserIsRegistered(signUpRequest)

    // when
    val response = whenInUserApi.userIsDisabled(username)

    // then
    response.hasStatusOk()
    thenInUserApi.userIsEnabled(username, false)
  }

  @Test
  @WithMockUser(roles = ["ADMIN"])
  fun `as an admin I can enable user`() {
    // given
    whenInUserApi.newUserIsRegistered(signUpRequest)
    whenInUserApi.userIsDisabled(username)

    // when
    val response = whenInUserApi.userIsEnabled(username)

    // then
    response.hasStatusOk()
    thenInUserApi.userIsEnabled(username, true)
  }

  @Test
  @WithMockUser
  fun `as non-admin user I cannot disable user`() {
    // given
    whenInUserApi.newUserIsRegistered(signUpRequest)

    // when
    val response = whenInUserApi.userIsDisabled(username)

    // then
    response.hasStatusForbidden()
  }
}
