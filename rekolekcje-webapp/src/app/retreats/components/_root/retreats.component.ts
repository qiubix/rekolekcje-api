import { Component, OnDestroy, OnInit } from '@angular/core';
import { RetreatDetails } from '../../models/retreat-details.model';
import { Observable, Subject } from 'rxjs';
import { Router } from '@angular/router';
import { Retreats } from '../../store/retreat-reducer';
import { select, Store } from '@ngrx/store';
import { RetreatsSharedActions } from '../../store/retreat-actions';
import { AppSelectors } from '@core/store/app-selectors';
import { RetreatSummary, RetreatStatus } from '../../models/retreat-summary.model';
import { RetreatAddEditDialog } from '../add-edit/retreat-dialog/add-edit-dialog.component';
import { filter, take, tap, map } from 'rxjs/operators';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'retreats-root',
  templateUrl: './retreats.component.html',
})
export class RetreatsComponent implements OnInit, OnDestroy {
  availableRetreats$: Observable<RetreatSummary[]>;
  historicalRetreats$: Observable<RetreatSummary[]>;

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(
    public dialog: MatDialog,
    private router: Router,
    private store: Store<Retreats.State>
  ) {}

  ngOnInit(): void {
    this.store.dispatch(new RetreatsSharedActions.LoadRetreatsList());
    this.availableRetreats$ = this.store.pipe(
      select(AppSelectors.getRetreatsList),
      map((it) => it.filter((retreat) => retreat.status === RetreatStatus.ACTIVE))
    );
    this.historicalRetreats$ = this.store.pipe(
      select(AppSelectors.getRetreatsList),
      map((it) => it.filter((retreat) => retreat.status === RetreatStatus.HISTORICAL))
    );
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  addNew(): void {
    this.dialog
      .open(RetreatAddEditDialog, {
        data: { dialogTitle: 'retreats.dialog.add' },
        disableClose: true,
      })
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.store.dispatch(new RetreatsSharedActions.CreateRetreat(result));
        }
      });
  }

  onDeleteRetreatHandler(retreatId: number): void {
    this.store.dispatch(new RetreatsSharedActions.DeleteRetreat(retreatId));
  }

  onEditRetreatHandler(id: number): void {
    const retreatDetails$ = this.store.pipe(
      select(AppSelectors.getSelectedRetreat),
      tap((retreat) => {
        if (retreat == null || retreat.id != id) {
          console.log('Dispatching select retreat event: ', id);
          this.store.dispatch(new RetreatsSharedActions.SelectRetreat(id));
        }
      }),
      filter((data: RetreatDetails) => data && data.id === id),
      take(1)
    );
    retreatDetails$.subscribe((retreatDetails) => {
      this.dialog
        .open(RetreatAddEditDialog, {
          data: {
            dialogTitle: 'retreats.dialog.edit',
            retreatData: retreatDetails,
          },
          disableClose: true,
        })
        .afterClosed()
        .subscribe((result: RetreatDetails) => {
          if (result) {
            const r = result;
            r.participants = retreatDetails.participants ? retreatDetails.participants : [];
            r.animators = retreatDetails.animators ? retreatDetails.animators : [];
            this.store.dispatch(new RetreatsSharedActions.UpdateRetreat(r));
          }
        });
    });
  }

  onSelectRetreatHandler(retreat: RetreatDetails): void {
    this.router.navigateByUrl(`retreats/${retreat.id}`);
  }
}
