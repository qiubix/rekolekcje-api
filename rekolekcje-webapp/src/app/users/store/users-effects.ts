import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action, Store } from '@ngrx/store';
import { Users } from './users-reduces';
import { UsersApiService } from './users-api.service';
import { Observable } from 'rxjs/internal/Observable';
import { UsersSharedActions } from './users-actions';
import { catchError, map, switchMap } from 'rxjs/operators';
import { UserSummary } from '../model/user-summary.model';
import { of } from 'rxjs/internal/observable/of';
import LoadUsersListSuccess = UsersSharedActions.LoadUsersListSuccess;
import LoadUsersListFail = UsersSharedActions.LoadUsersListFail;
import CreateUserSuccess = UsersSharedActions.CreateUserSuccess;
import CreateUserFail = UsersSharedActions.CreateUserFail;
import UpdateUserSuccess = UsersSharedActions.UpdateUserSuccess;
import UpdateUserFail = UsersSharedActions.UpdateUserFail;
import DeleteUserSuccess = UsersSharedActions.DeleteUserSuccess;
import DeleteUserFail = UsersSharedActions.DeleteUserFail;
import ChangePasswordSuccess = UsersSharedActions.ChangePasswordSuccess;
import ChangePasswordFail = UsersSharedActions.ChangePasswordFail;
import SelectUserSuccess = UsersSharedActions.SelectUserSuccess;
import SelectUserFail = UsersSharedActions.SelectUserFail;

@Injectable()
export class UsersEffects {
  @Effect()
  LoadUsersList: Observable<Action> = this.actions.pipe(
    ofType(UsersSharedActions.types.LoadUsersList),
    switchMap((action: UsersSharedActions.LoadUsersList) =>
      this.service.getAll().pipe(
        map((usersList: UserSummary[]) => new LoadUsersListSuccess(usersList)),
        catchError(error => of(new LoadUsersListFail(error)))
      )
    )
  );

  @Effect()
  CreateUser: Observable<Action> = this.actions.pipe(
    ofType(UsersSharedActions.types.CreateUser),
    switchMap((action: UsersSharedActions.CreateUser) =>
      this.service.createOne(action.payload).pipe(
        map((userSummary: UserSummary) => new CreateUserSuccess(userSummary)),
        catchError(error => of(new CreateUserFail(error)))
      )
    )
  );

  @Effect()
  UpdateUser: Observable<Action> = this.actions.pipe(
    ofType(UsersSharedActions.types.UpdateUser),
    switchMap((action: UsersSharedActions.UpdateUser) =>
      this.service.updateOne(action.payload).pipe(
        map((userSummary: UserSummary) => new UpdateUserSuccess(userSummary)),
        catchError(error => of(new UpdateUserFail(error)))
      )
    )
  );

  @Effect()
  DeleteUser: Observable<Action> = this.actions.pipe(
    ofType(UsersSharedActions.types.DeleteUser),
    switchMap((action: UsersSharedActions.DeleteUser) =>
      this.service.deleteOne(action.payload).pipe(
        map((userSummary: UserSummary) => new DeleteUserSuccess(userSummary.username)),
        catchError(error => of(new DeleteUserFail(error)))
      )
    )
  );

  @Effect()
  ChangePassword: Observable<Action> = this.actions.pipe(
    ofType(UsersSharedActions.types.ChangePassword),
    switchMap((action: UsersSharedActions.ChangePassword) =>
      this.service.changePassword(action.payload).pipe(
        map((notification: string) => new ChangePasswordSuccess(notification)),
        catchError(error => of(new ChangePasswordFail('Nie udało się zmienić hasła. ' + error)))
      )
    )
  );

  @Effect()
  SelectCurrentUser: Observable<Action> = this.actions.pipe(
    ofType(UsersSharedActions.types.SelectUser),
    switchMap((action: UsersSharedActions.SelectUser) =>
      this.service.getOne(action.username).pipe(
        map((user: UserSummary) => new SelectUserSuccess(user)),
        catchError(error => of(new SelectUserFail(error)))
      )
    )
  );

  constructor(
    private actions: Actions,
    private store: Store<Users.State>,
    private service: UsersApiService
  ) {}
}
