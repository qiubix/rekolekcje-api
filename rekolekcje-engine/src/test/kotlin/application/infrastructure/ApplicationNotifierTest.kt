package pl.oaza.waw.rekolekcje.api.application.infrastructure

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.springframework.context.ApplicationEventPublisher
import pl.oaza.waw.rekolekcje.api.application.endpoint.events.ApplicationRejectedEvent
import pl.oaza.waw.rekolekcje.api.application.endpoint.events.ApplicationSubmittedEvent
import pl.oaza.waw.rekolekcje.api.application.endpoint.events.ApplicationVerifiedEvent
import pl.oaza.waw.rekolekcje.api.application.endpoint.toSummary
import pl.oaza.waw.rekolekcje.api.application.infrastructure.events.ApplicationNotifierImpl
import pl.oaza.waw.rekolekcje.api.application.query.ApplicationQueryFacade
import pl.oaza.waw.rekolekcje.api.application.sampleApplication

internal class ApplicationNotifierTest {

  private val eventPublisherMock = mockk<ApplicationEventPublisher>(relaxUnitFun = true)
  private val queryFacadeMock = mockk<ApplicationQueryFacade>()

  private val notifier = ApplicationNotifierImpl(eventPublisherMock, queryFacadeMock)

  private val applicationId = 123L

  @Test
  fun `should send application submitted event`() {
    val expectedEvent = ApplicationSubmittedEvent(applicationId)

    notifier.newApplicationSubmitted(applicationId)

    verify(exactly = 1) { eventPublisherMock.publishEvent(expectedEvent) }
  }

  @Test
  fun `should send application verified event`() {
    val application = sampleApplication().copy(id = applicationId)
    every { queryFacadeMock.findApplicationById(applicationId) } returns application
    val expectedEvent = ApplicationVerifiedEvent(application.toSummary(), application.memberId)

    notifier.applicationVerified(applicationId)

    verify(exactly = 1) { eventPublisherMock.publishEvent(expectedEvent) }
  }

  @Test
  fun `should send application rejected event`() {
    val memberId = 111L
    val retreatId = 21L
    val reason = "Some reason"
    val application = sampleApplication(memberId = memberId, retreatId = retreatId).copy(id = applicationId)
    every { queryFacadeMock.findApplicationById(applicationId) } returns application
    val expectedEvent = ApplicationRejectedEvent(applicationId = applicationId, reason = reason)

    notifier.applicationRejected(applicationId, reason)

    verify(exactly = 1) { eventPublisherMock.publishEvent(expectedEvent) }
  }
}