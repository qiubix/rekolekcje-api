package pl.oaza.waw.rekolekcje.api.retreats

import pl.oaza.waw.rekolekcje.api.retreats.domain.RegistrationStatus
import pl.oaza.waw.rekolekcje.api.retreats.domain.Turn
import pl.oaza.waw.rekolekcje.api.retreats.domain.Retreat
import pl.oaza.waw.rekolekcje.api.retreats.domain.RetreatMember
import pl.oaza.waw.rekolekcje.api.retreats.domain.RetreatStatus
import pl.oaza.waw.rekolekcje.api.shared.value.PaymentStatus
import pl.oaza.waw.rekolekcje.api.shared.value.Sex
import pl.oaza.waw.rekolekcje.api.shared.value.Stage

fun sampleRetreat(
  stage: Stage = Stage.ODB,
  turn: Turn = Turn.I,
  status: RetreatStatus = RetreatStatus.ACTIVE,
  members: Set<RetreatMember> = mutableSetOf()
): Retreat = Retreat(
  turn = turn,
  stage = stage,
  status = status
).withMembers(members)

fun Retreat.withMembers(members: Set<RetreatMember>): Retreat = this.copy(retreatMembers = members.toMutableSet())

fun sampleRetreatMember(
  memberId: Long,
  firstName: String = "Sample",
  lastName: String = "Member",
  sex: Sex = Sex.MALE,
  age: Int = 0,
  paymentStatus: PaymentStatus = PaymentStatus.NOTHING,
  registrationStatus: RegistrationStatus = RegistrationStatus.REGISTERED,
  animator: Boolean = false
): RetreatMember = RetreatMember(
  memberId = memberId,
  firstName = firstName,
  lastName = lastName,
  sex = sex,
  age = age,
  paymentStatus = paymentStatus,
  registrationStatus = registrationStatus,
  animator = animator
)