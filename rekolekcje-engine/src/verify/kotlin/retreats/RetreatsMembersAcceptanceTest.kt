package pl.oaza.waw.rekolekcje.api.retreats

import org.assertj.core.api.Assertions
import org.assertj.core.api.BDDAssertions
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Import
import org.springframework.security.test.context.support.WithMockUser
import pl.oaza.waw.rekolekcje.api.application.api.ApplicationApiBehaviours
import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationStatus
import pl.oaza.waw.rekolekcje.api.application.endpoint.ApplicationSubmissionDto
import pl.oaza.waw.rekolekcje.api.application.endpoint.ApplicationSummary
import pl.oaza.waw.rekolekcje.api.application.endpoint.ChangeApplicationStatusRequest
import pl.oaza.waw.rekolekcje.api.application.endpoint.RetreatApplicationDto
import pl.oaza.waw.rekolekcje.api.application.endpoint.events.ApplicationVerifiedEvent
import pl.oaza.waw.rekolekcje.api.application.fixture.ApplicationTestContext
import pl.oaza.waw.rekolekcje.api.application.fixture.animatorApplicationSubmissionDto
import pl.oaza.waw.rekolekcje.api.application.fixture.participantApplicationSubmissionDto
import pl.oaza.waw.rekolekcje.api.application.fixture.sampleMember
import pl.oaza.waw.rekolekcje.api.application.storage.ApplicationDatabase
import pl.oaza.waw.rekolekcje.api.members.api.MembersApiBehaviour
import pl.oaza.waw.rekolekcje.api.members.domain.Pesel
import pl.oaza.waw.rekolekcje.api.members.endpoint.MemberDetails
import pl.oaza.waw.rekolekcje.api.members.storage.MembersDatabase
import pl.oaza.waw.rekolekcje.api.members.utils.memberWithSampleData
import pl.oaza.waw.rekolekcje.api.retreats.api.RetreatsApiBehaviour
import pl.oaza.waw.rekolekcje.api.retreats.api.RetreatsApiExpectations
import pl.oaza.waw.rekolekcje.api.retreats.api.RetreatsEventsBehaviour
import pl.oaza.waw.rekolekcje.api.retreats.domain.RetreatParticipantLimitExceededException
import pl.oaza.waw.rekolekcje.api.retreats.endpoint.RetreatDetails
import pl.oaza.waw.rekolekcje.api.retreats.endpoint.RetreatMemberDto
import pl.oaza.waw.rekolekcje.api.retreats.endpoint.RetreatStatisticsDto
import pl.oaza.waw.rekolekcje.api.retreats.storage.RetreatsDatabase
import pl.oaza.waw.rekolekcje.api.shared.BaseIntegrationTest

@Import(ApplicationTestContext::class)
// TODO: it's hard to test this feature in isolation, as it requires quite a lot of setup
//  (member and application for this member must exist),
//  and events are propagated automatically to different parts of the system
//  maybe whole application context can be separated for this test with some mock beans provided
internal class RetreatsMembersAcceptanceTest : BaseIntegrationTest() {

  @Autowired
  private lateinit var whenInRetreatsApi: RetreatsApiBehaviour

  @Autowired
  private lateinit var thenInRetreatsApi: RetreatsApiExpectations

  @Autowired
  private lateinit var database: RetreatsDatabase

  @Autowired
  private lateinit var membersDatabase: MembersDatabase

  @Autowired
  private lateinit var whenInMembersApi: MembersApiBehaviour

  @Autowired
  private lateinit var whenInApplicationApi: ApplicationApiBehaviours

  @Autowired
  private lateinit var applicationDatabase: ApplicationDatabase

  @Autowired
  private lateinit var whenInEventBus: RetreatsEventsBehaviour

  private val retreatOne = retreatDetailsWithSampleData1()
  private val retreatTwo = retreatDetailsWithSampleData2()

  @BeforeEach
  fun setup() {
    database.clear()
    membersDatabase.clear()
    applicationDatabase.clear()
  }

  @AfterAll
  fun tearDown() {
    database.clear()
  }

  @Test
  @WithMockUser
  fun `should register verified participant to retreat containing some participants`() {
    // given
    val existingMember = whenInMembersApi.memberExists(sampleMember())
    val existingRetreatParticipant = RetreatMemberDto(existingMember, false)
    val existingRetreatId = whenInRetreatsApi.addAndGetId(
      retreatTwo.withParticipants(setOf(existingRetreatParticipant))
    )
    val otherMember = sampleMember(firstName = "Another", lastName = "One")
    val submissionRequest = participantApplicationSubmissionDto(retreatId = existingRetreatId, member = otherMember)
    val submittedApplication = whenInApplicationApi
      .newApplicationIsSubmitted(submissionRequest)
      .getContent<RetreatApplicationDto>()
    applicationDatabase.setApplicationStatus(submittedApplication.id, ApplicationStatus.VERIFIED)

    // when
    val event = buildParticipantApplicationVerifiedEvent(
      applicationId = submittedApplication.id,
      member = submittedApplication.member,
      retreatId = existingRetreatId
    )
    whenInEventBus.eventIsPublished(event)

    // then
    val newlyRegisteredMember = RetreatMemberDto(submittedApplication.member, false)
    val expectedAverageAge = calculateAverageAgeForMembers(listOf(existingMember, otherMember))
    val expectedDetails = retreatTwo
      .withId(existingRetreatId)
      .withParticipants(setOf(existingRetreatParticipant, newlyRegisteredMember))
      .withStatistics(
        RetreatStatisticsDto(
          participantsAmount = 2,
          malesParticipants = 2,
          femalesParticipants = 0,
          averageAge = expectedAverageAge,
          underageParticipants = 0
        )
      )
    thenInRetreatsApi.exists(expectedDetails)
    val expectedSummary = retreatSummary(expectedDetails)
      .withNumberOfParticipants(2)
      .withNumberOfMaleParticipants(2)
      .withAverageAge(expectedAverageAge)
    thenInRetreatsApi.exists(expectedSummary)
  }

  @Test
  @WithMockUser
  fun `should not register participant if limit for retreat is reached`() {
    // given
    val limit = 2
    val firstMember = whenInMembersApi.memberExists(sampleMember("First", "One"))
    val secondMember = whenInMembersApi.memberExists(sampleMember("Second", "Two"))
    val thirdMember = whenInMembersApi.memberExists(sampleMember("Over", "Limit"))

    // and
    val existingRetreatId = whenInRetreatsApi.addAndGetId(
      retreatTwo
        .withLimit(limit)
        .withParticipants(setOf(
          RetreatMemberDto(firstMember, false),
          RetreatMemberDto(secondMember, false)
        ))
    )

    // when
    val ex = Assertions.catchThrowable {
      val event = buildParticipantApplicationVerifiedEvent(thirdMember, existingRetreatId)
      whenInEventBus.eventIsPublished(event)
    }

    // then
    BDDAssertions.then(ex).isInstanceOf(RetreatParticipantLimitExceededException::class.java)
  }

  @Test
  @WithMockUser
  fun `should switch enough animators flag if specific number is reached`() {
    // given
    val existingRetreatId = whenInRetreatsApi.addAndGetId(retreatOne.copy(minimumAnimatorsRequired = 1))
    val submissionRequest = animatorApplicationSubmissionDto()
    val submittedApplication = whenInApplicationApi
      .newApplicationIsSubmitted(submissionRequest)
      .getContent<RetreatApplicationDto>()
    applicationDatabase.setApplicationStatus(submittedApplication.id, ApplicationStatus.VERIFIED)
    applicationDatabase.setRetreatId(submittedApplication.id, existingRetreatId)

    // when
    val event = buildAnimatorApplicationVerifiedEvent(
      applicationId = submittedApplication.id,
      member = submittedApplication.member,
      retreatId = existingRetreatId
    )
    whenInEventBus.eventIsPublished(event)

    // then
    val retreatDetails = whenInRetreatsApi
      .singleIsRequested(existingRetreatId)
      .getContent<RetreatDetails>()
    BDDAssertions.then(retreatDetails.hasEnoughAnimators).isTrue()
  }

  private fun buildApplicationVerifiedEvent(
    member: MemberDetails,
    retreatId: Long,
    animator: Boolean = false,
    applicationId: Long = 123L
  ): ApplicationVerifiedEvent =
    ApplicationVerifiedEvent(
      application = ApplicationSummary(
        id = applicationId,
        firstName = member.personalData.firstName,
        lastName = member.personalData.lastName,
        status = ApplicationStatus.VERIFIED,
        retreatId = retreatId,
        isAnimator = animator
      ),
      memberId = member.id ?: throw IllegalStateException()
    )

  private fun buildParticipantApplicationVerifiedEvent(
    member: MemberDetails,
    retreatId: Long,
    applicationId: Long = 123L
  ): ApplicationVerifiedEvent = buildApplicationVerifiedEvent(member, retreatId, false, applicationId)

  private fun buildAnimatorApplicationVerifiedEvent(
    member: MemberDetails,
    retreatId: Long,
    applicationId: Long = 123L
  ): ApplicationVerifiedEvent = buildApplicationVerifiedEvent(member, retreatId, true, applicationId)

  private fun calculateAverageAgeForMembers(members: List<MemberDetails>): Double =
    members.map { Pesel(it.personalData.pesel).calculateAge() }.average()

  private fun animatorApplicationIsSubmittedAndVerified(
    retreatId: Long,
    firstName: String = "Bruce",
    lastName: String = "Lee"
  ): RetreatApplicationDto {
    val application = animatorApplicationSubmissionDto(
      retreatId = retreatId, member = memberWithSampleData(firstName = firstName, lastName = lastName)
    )
    return applicationIsSubmittedAndVerified(application)
  }

  private fun applicationIsSubmitted(firstName: String, lastName: String, retreatId: Long): RetreatApplicationDto {
    val submissionDto = participantApplicationSubmissionDto(
      retreatId = retreatId,
      member = memberWithSampleData(firstName = firstName, lastName = lastName)
    )
    val existingApplicationId = whenInApplicationApi.alreadyExistsSomeApplication(submissionDto)
    return whenInApplicationApi.singleApplicationIsRequested(existingApplicationId).getContent()
  }

  private fun applicationIsSubmittedAndVerified(application: ApplicationSubmissionDto): RetreatApplicationDto {
    val applicationId = whenInApplicationApi.alreadyExistsSomeApplication(application)
    val verifyRequest = ChangeApplicationStatusRequest(applicationId, ApplicationStatus.VERIFIED)
    whenInApplicationApi.applicationStatusIsChanged(verifyRequest)
    return whenInApplicationApi.singleApplicationIsRequested(applicationId).getContent()
  }
}