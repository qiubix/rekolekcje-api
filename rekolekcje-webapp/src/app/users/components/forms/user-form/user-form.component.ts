import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { SaveUserRequest } from '@users/model/save-user-request.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GroupErrorStateMatcher } from '../group-error-state-matcher';
import { UserRole } from '@users/model/role.model';

@Component({
  selector: 'reko-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {
  @Input() user: SaveUserRequest;
  @Input() showPasswordForm: Boolean;
  @Input() showActiveForm: Boolean;
  @Input() showRolesForm: Boolean;

  @Output() formOutput: EventEmitter<SaveUserRequest> = new EventEmitter();

  public form: FormGroup;
  public passwordFormGroup: FormGroup;

  public groupStateMatcher: GroupErrorStateMatcher = new GroupErrorStateMatcher();

  roles = UserRole;
  roleKeys = Object.keys(this.roles);

  constructor(private readonly fb: FormBuilder) {}

  public ngOnInit(): void {
    this.passwordFormGroup = this.fb.group(
      {
        password: this.fb.control(this.user ? this.user.password : '', Validators.required),
        confirmPassword: this.fb.control('')
      },
      {
        validators: this.checkPasswords
      }
    );

    this.form = this.fb.group({
      username: this.fb.control(this.user ? this.user.username : '', Validators.required),
      password: this.showPasswordForm ? this.passwordFormGroup : this.fb.control(''),
      email: this.fb.control(this.user ? this.user.email : '', Validators.required),
      enabled: this.fb.control(this.user ? this.user.enabled : 'true'),
      roles: this.fb.control(this.user ? this.user.roles : [])
    });
  }

  private checkPasswords(group: FormGroup): any | null {
    const pass = group.get('password').value;
    const confirmPass = group.get('confirmPassword').value;

    return pass === confirmPass ? null : { notSame: true };
  }

  cancel(): void {
    this.formOutput.emit();
  }

  submit(): void {
    if (this.form.invalid) {
      this.form.markAsDirty();
      return;
    }

    const saveUserRequest: SaveUserRequest = {
      username: this.form.get('username').value,
      password: this.showPasswordForm ? this.passwordFormGroup.get('password').value : null,
      email: this.form.get('email').value,
      enabled: this.form.get('enabled').value,
      roles: this.form.get('roles').value
    };

    this.formOutput.emit(saveUserRequest);
  }
}
