package pl.oaza.waw.rekolekcje.api.retreats

import io.mockk.Runs
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import pl.oaza.waw.rekolekcje.api.retreats.domain.Retreat
import pl.oaza.waw.rekolekcje.api.retreats.domain.RetreatEventPublisher
import pl.oaza.waw.rekolekcje.api.retreats.infrastructure.InMemoryRetreatReadRepository
import pl.oaza.waw.rekolekcje.api.retreats.infrastructure.InMemoryRetreatWriteRepository
import pl.oaza.waw.rekolekcje.api.retreats.infrastructure.RetreatConfiguration
import pl.oaza.waw.rekolekcje.api.tools.date.DateCreator

abstract class RetreatTest {

  private val storage = HashMap<Long, Retreat>()
  private val retreatReadRepository = InMemoryRetreatReadRepository(storage)
  private val retreatWriteRepository = InMemoryRetreatWriteRepository(storage)
  protected val retreatEventPublisherMock = mockk<RetreatEventPublisher>()

  protected val queryFacade = RetreatConfiguration()
    .retreatQueryFacade(retreatReadRepository)
  protected val commandFacade = RetreatConfiguration()
    .retreatCommandFacade(retreatReadRepository, retreatWriteRepository, retreatEventPublisherMock)
  protected val dateCreator = DateCreator()

  @BeforeEach
  internal fun setUp() {
    every { retreatEventPublisherMock.memberPutOnWaitingList(any(), any()) } just Runs
    every { retreatEventPublisherMock.memberRegistered(any(), any()) } just Runs
    every { retreatEventPublisherMock.memberResigned(any(), any()) } just Runs
  }

  @AfterEach
  fun tearDown() {
    storage.clear()
  }

  protected fun saveAndGetId(retreat: Retreat): Long =
    commandFacade.save(retreat).getId() ?: throw IllegalStateException("Retreat should have ID.")

  protected fun saveAll(retreats: Set<Retreat>) = retreats.forEach { commandFacade.save(it) }
}
