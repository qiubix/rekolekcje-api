import { Component, Input, OnInit } from '@angular/core';
import { VerificationStatistics } from '@verification/model';
import { RetreatSummary } from '@retreats/models/retreat-summary.model';

@Component({
  selector: 'reko-verification-stats',
  templateUrl: './verification-stats.component.html',
  styleUrls: ['./verification-stats.component.scss']
})
export class VerificationStatsComponent implements OnInit {
  @Input() stats: VerificationStatistics;
  @Input() activeRetreats: RetreatSummary[];

  constructor() {}

  ngOnInit() {}

  get numberOfActiveRetreats() {
    return this.activeRetreats.length;
  }
}
