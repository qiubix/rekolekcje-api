package pl.oaza.waw.rekolekcje.api.user.domain

class IncorrectPasswordException : Exception()