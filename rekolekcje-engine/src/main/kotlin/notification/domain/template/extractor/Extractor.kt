package pl.oaza.waw.rekolekcje.api.notification.domain.template.extractor

abstract class Extractor(val type: ExtractorType) {
	abstract fun extractValue(): String

	fun extract(): Pair<String, Any> = type.key to extractValue()
}
