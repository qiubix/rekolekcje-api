package pl.oaza.waw.rekolekcje.api.retreats.infrastructure.events

import org.springframework.context.ApplicationEventPublisher
import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationStatus
import pl.oaza.waw.rekolekcje.api.retreats.domain.RetreatEventPublisher
import pl.oaza.waw.rekolekcje.api.retreats.endpoint.events.MemberStatusChangedEvent

class RetreatEventPublisherImpl(private val eventPublisher: ApplicationEventPublisher) : RetreatEventPublisher {

  override fun memberRegistered(retreatId: Long, memberId: Long) {
    eventPublisher.publishEvent(
      MemberStatusChangedEvent(
        retreatId = retreatId,
        memberId = memberId,
        applicationStatus = ApplicationStatus.REGISTERED
      )
    )
  }

  override fun memberResigned(retreatId: Long, memberId: Long) {
    eventPublisher.publishEvent(
      MemberStatusChangedEvent(
        retreatId = retreatId,
        memberId = memberId,
        applicationStatus = ApplicationStatus.RESIGNED
      )
    )
  }

  override fun memberPutOnWaitingList(retreatId: Long, memberId: Long) {
    eventPublisher.publishEvent(
      MemberStatusChangedEvent(
        retreatId = retreatId,
        memberId = memberId,
        applicationStatus = ApplicationStatus.WAITING_LIST
      )
    )
  }
}