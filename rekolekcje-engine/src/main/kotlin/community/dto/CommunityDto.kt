package pl.oaza.waw.rekolekcje.api.community.dto

import pl.oaza.waw.rekolekcje.api.core.ddd.ValueObject

@ValueObject
data class CommunityPayload(
  val id: Long? = null,
  val name: String,
  val address: String? = null
)
