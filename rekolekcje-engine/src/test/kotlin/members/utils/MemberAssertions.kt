package pl.oaza.waw.rekolekcje.api.members.utils

import org.assertj.core.api.Assertions.assertThat
import pl.oaza.waw.rekolekcje.api.members.endpoint.MemberDetails
import pl.oaza.waw.rekolekcje.api.members.endpoint.MemberSummary

fun assertSummaryMatchesDetails(summary: MemberSummary, details: MemberDetails) {
  assertThat(summary.id).isEqualTo(details.id)
  assertThat(summary.firstName).isEqualTo(details.personalData.firstName)
  assertThat(summary.lastName).isEqualTo(details.personalData.lastName)
  assertThat(summary.age).isEqualTo(details.personalData.age)
  assertThat(summary.communityId).isEqualTo(details.personalData.communityId)
  assertThat(summary.parishId).isEqualTo(details.personalData.parishId)
}

fun assertSummariesMatchesDetails(
  summaries: List<MemberSummary>,
  details: List<MemberDetails>
) {
  assertThat(summaries).hasSameSizeAs(details)
  val sortedSummaries = summaries.sortedBy { it.id }
  val sortedDetails = details.sortedBy { it.id }
  sortedSummaries.zip(sortedDetails).forEach { assertSummaryMatchesDetails(it.first, it.second) }
}
