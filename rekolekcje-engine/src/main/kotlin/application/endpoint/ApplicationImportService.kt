package pl.oaza.waw.rekolekcje.api.application.endpoint

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationType
import pl.oaza.waw.rekolekcje.api.application.endpoint.csv.convertApplicationFromCsv
import pl.oaza.waw.rekolekcje.api.retreats.domain.Retreat
import pl.oaza.waw.rekolekcje.api.retreats.domain.Turn
import pl.oaza.waw.rekolekcje.api.retreats.query.RetreatQueryFacade
import pl.oaza.waw.rekolekcje.api.shared.value.Stage

@Service
internal class ApplicationImportService(
  private val retreatQueryFacade: RetreatQueryFacade,
  private val applicationSubmissionService: ApplicationSubmissionService
) {

  @Transactional
  fun importAnimatorsFromCsv(fileContents: String): List<RetreatApplicationDto> {
    val conversionResult = convertApplicationFromCsv(fileContents)
    val retreats = retreatQueryFacade.findAll()
    return conversionResult.convertedApplications.map {
      val availability = it.availability
        .map { retreatCode ->
          val chunks = retreatCode.split(", ")
          val turn = Turn.valueOf(chunks[0].substringAfter(" "))
          val stage = Stage.valueOf(chunks[1])
          retreats.find { r -> r.stage == stage && r.turn == turn }
        }
        .mapNotNull { retreat -> retreat?.getId() }
        .toSet()
      applicationSubmissionService.submitNewApplication(
        ApplicationSubmissionDto(
          applicationType = ApplicationType.ANIMATOR,
          memberDetails = it.member ?: throw IllegalStateException(),
          animatorRoles = it.animatorRoles,
          availability = availability,
          surety = it.surety
        )
      )
    }
  }

  @Transactional
  fun importParticipantsFromCsv(fileContents: String): List<RetreatApplicationDto> {
    val conversionResult = convertApplicationFromCsv(fileContents)
    val retreats = retreatQueryFacade.findAll()
    return conversionResult.convertedApplications.map {
      val selectedRetreat = it.selectedRetreat ?: throw IllegalStateException()
      val retreatId = extractRetreatId(selectedRetreat, retreats)
      applicationSubmissionService.submitNewApplication(
        ApplicationSubmissionDto(
          applicationType = ApplicationType.PARTICIPANT,
          memberDetails = it.member ?: throw IllegalStateException(),
          retreatId = retreatId,
          surety = it.surety
        )
      )
    }
  }

  private fun extractRetreatId(retreatDescription: String, retreats: List<Retreat>): Long? {
    val chunks = retreatDescription.split(", ")
    val turn = Turn.valueOf(chunks[0].substringAfter(" "))
    val stage = Stage.valueOf(chunks[1])
    val foundRetreat = retreats.find { r -> r.stage == stage && r.turn == turn }
    return foundRetreat?.getId()
  }
}