package pl.oaza.waw.rekolekcje.api.community.api

import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.Matchers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import pl.oaza.waw.rekolekcje.api.community.dto.CommunityPayload
import pl.oaza.waw.rekolekcje.api.shared.BaseApiExpectations
import pl.oaza.waw.rekolekcje.api.shared.Response
import pl.oaza.waw.rekolekcje.api.shared.RestApi

class CommunityApiExpectations(
  restApi: RestApi, private val baseUri: String
) : BaseApiExpectations(restApi) {

  private val all: List<CommunityPayload>
    get() = restApi.performGet(baseUri).getContentAsList()

  fun responseHasCorrectDataIgnoringId(response: Response, expectedPayload: CommunityPayload) {
    response.andExpect(status().is2xxSuccessful)
    val responseContent = response.getContent<CommunityPayload>()
    assertThat(responseContent)
      .isEqualToIgnoringGivenFields(expectedPayload, "id")
  }

  fun responseHasCorrectDataIgnoringId(response: Response, expectedPayload: List<CommunityPayload>) {
    response.andExpect(status().is2xxSuccessful)
    val returnedPayloadAsList = response.getContentAsList<CommunityPayload>()
    assertThat(returnedPayloadAsList)
      .usingRecursiveFieldByFieldElementComparator()
      .usingElementComparatorIgnoringFields("id")
      .containsAll(expectedPayload)
  }

  fun exists(payload: CommunityPayload) {
    assertThat(all).contains(payload)
  }

  fun doesNotExist(payload: CommunityPayload) {
    assertThat(all).doesNotContain(payload)
  }

  fun thereIsAnExactNumberOfEntities(size: Int) {
    assertThat(all.size).isEqualTo(size)
  }

  fun responseHasCorrectJsonValues(
    response: Response,
    payload: CommunityPayload
  ) {
    response
      .andExpect(jsonPath("$.id", Matchers.notNullValue()))
      .andExpect(jsonPath("$.name").value(payload.name))
  }
}
