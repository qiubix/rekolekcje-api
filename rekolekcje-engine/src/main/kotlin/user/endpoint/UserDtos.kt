package pl.oaza.waw.rekolekcje.api.user.endpoint

import java.util.Date

enum class UserRole {
  ROLE_USER,
  ROLE_MODERATOR,
  ROLE_DOR,
  ROLE_ADMIN
}

data class SignUpRequest(
  val username: String,
  val password: String,
  val email: String,
  val roles: Set<UserRole> = emptySet()
)

data class UserSummary(
  val username: String,
  val enabled: Boolean = true,
  val email: String = "default@email.com",
  val roles: Set<UserRole>
)

data class UserSecurityDetails(
  val username: String,
  val lastPasswordResetDate: Date,
  val roles: Set<UserRole>
)

data class UpdateUserRequest(
  val username: String,
  val email: String,
  val enabled: Boolean = true,
  val roles: Set<UserRole> = setOf()
)

data class ChangePasswordRequest(
  val username: String,
  val newPassword: String,
  val oldPassword: String? = null,
  val admin: Boolean = false
)

data class AuthenticationRequest(
  val username: String,
  val password: String
)
