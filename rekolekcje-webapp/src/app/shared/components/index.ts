import { DefaultTextInputComponent } from './input/default-text-input/default-text-input.component';
import { SectionHeaderComponent } from './input/section-header/section-header.component';
import { SharedMembersTableComponent } from './tables/members-table/shared-members-table.component';
import { SharedMembersTableFilteringComponent } from './tables/members-table/members-table-filtering/shared-members-table-filtering.component';
import { SharedRetreatsTableComponent } from './tables/retreats-table/shared-retreats-table.component';
import { ActionMenuButtonsComponent } from './buttons/action-menu-buttons/action-menu-buttons.component';
import { DetailsTextFieldComponent } from './output/details-text-field/details-text-field.component';
import { DetailsElement, DetailsRowComponent } from './output/details-row/details-row.component';
import { DatepickerComponent } from './input/datepicker/datepicker.component';
import { DateOutputComponent } from './output/date-output/date-output.component';
import { DateDetailsComponent } from './output/date-details/date-details.component';
import { StageFieldComponent } from './output/stage-field/stage-field.component';
import { ButtonComponent } from './buttons/button/button.component';
import { MemberStatusComponent } from './output/member-status/member-status.component';
import { PaymentStatusComponent } from './output/payment-status/payment-status.component';
// import { ImportDialogComponent } from './dialogs/import-dialog/import-dialog.component';
import { ContactInfoDetailsComponent } from './output/contact-info-details/contact-info-details.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { DeleteConfirmAlertDialog } from './dialogs/delete-confirm-alert/delete-confirm-alert.component';
import { CommunityPickerComponent } from './input/community-picker/community-picker.component';
import { ParishPickerComponent } from './input/parish-picker/parish-picker.component';
import { PersonalDataFormComponent } from './forms/personal-data-form/personal-data-form.component';
import { AddressFormComponent } from './forms/personal-data-form/address-form/address-form.component';
import { ContactInfoFormComponent } from './forms/contact-info-form/contact-info-form.component';
import { HealthReportFormComponent } from './forms/health-report-form/health-report-form.component';
import { ExperienceFormComponent } from './forms/experience-form/experience-form.component';
import { HistoricalRetreatsArrayComponent } from './forms/experience-form/historical-retreats-array/historical-retreats-array.component';
import { HistoricalRetreatFormComponent } from './forms/experience-form/historical-retreats-array/historical-retreat-form/historical-retreat-form.component';
import { OpinionsDetailsComponent } from './output/opinions-details/opinions-details.component';
import { MemberDetailsViewComponent } from './output/member-details/member-details-view.component';
import { CommunityDetailsComponent } from './output/member-details/community-details/community-details.component';
import { ExperienceDetailsComponent } from './output/member-details/experience-details/experience-details.component';
import { HistoricalRetreatsDetailsComponent } from './output/member-details/experience-details/historical-retreats-details/historical-retreats-details.component';
import { HistoricalRetreatsDetailsElementComponent } from './output/member-details/experience-details/historical-retreats-details/historical-retreats-details-element/historical-retreats-details-element.component';
import { HealthReportComponent } from './output/member-details/health-report/health-report.component';
import { ParishDetailsComponent } from './output/member-details/parish-details/parish-details.component';
import { PersonalDataComponent } from './output/member-details/personal-data/personal-data.component';
import { AddressDetailsComponent } from './output/member-details/personal-data/address-details/address-details.component';
import { AlternativeImportDialogComponent } from '@shared/components/dialogs/alternative-import-dialog/alternative-import-dialog.component';
import { DeuterocatechumenateDetailsComponent } from '@shared/components/output/member-details/experience-details/deuterocatechumenate-details/deuterocatechumenate-details.component';
import { TalentDetailsComponent } from '@shared/components/output/member-details/experience-details/talent-details/talent-details.component';
import { KwcDetailsComponent } from '@shared/components/output/member-details/experience-details/kwc-details/kwc-details.component';
import { FormationInCommunityDetailsComponent } from '@shared/components/output/member-details/experience-details/formation-in-community-details/formation-in-community-details.component';
import { ExperienceAsAnimatorDetailsComponent } from '@shared/components/output/member-details/experience-details/experience-as-animator-details/experience-as-animator-details.component';
import { DeuterocatechumenateFormComponent } from '@shared/components/forms/experience-form/deuterocatechumenate-form/deuterocatechumenate-form.component';
import { ExperienceAsAnimatorFormComponent } from '@shared/components/forms/experience-form/experience-as-animator-form/experience-as-animator-form.component';
import { FormationInCommunityFormComponent } from '@shared/components/forms/experience-form/formation-in-community-form/formation-in-community-form.component';
import { KwcFormComponent } from '@shared/components/forms/experience-form/kwc-form/kwc-form.component';
import { TalentFormComponent } from '@shared/components/forms/experience-form/talent-form/talent-form.component';

export const components: any[] = [
  DefaultTextInputComponent,
  SectionHeaderComponent,
  SharedMembersTableComponent,
  SharedMembersTableFilteringComponent,
  SharedRetreatsTableComponent,
  ActionMenuButtonsComponent,
  DetailsTextFieldComponent,
  DetailsRowComponent,
  DetailsElement,
  DatepickerComponent,
  DateOutputComponent,
  DateDetailsComponent,
  StageFieldComponent,
  ButtonComponent,
  MemberStatusComponent,
  ContactInfoDetailsComponent,
  OpinionsDetailsComponent,
  MemberDetailsViewComponent,
  CommunityDetailsComponent,
  ExperienceDetailsComponent,
  HistoricalRetreatsDetailsComponent,
  HistoricalRetreatsDetailsElementComponent,
  DeuterocatechumenateDetailsComponent,
  TalentDetailsComponent,
  KwcDetailsComponent,
  FormationInCommunityDetailsComponent,
  ExperienceAsAnimatorDetailsComponent,
  HealthReportComponent,
  ParishDetailsComponent,
  PersonalDataComponent,
  AddressDetailsComponent,
  PaymentStatusComponent,
  CommunityPickerComponent,
  ParishPickerComponent,
  PersonalDataFormComponent,
  AddressFormComponent,
  ContactInfoFormComponent,
  HealthReportFormComponent,
  ExperienceFormComponent,
  DeuterocatechumenateFormComponent,
  ExperienceAsAnimatorFormComponent,
  FormationInCommunityFormComponent,
  KwcFormComponent,
  TalentFormComponent,
  HistoricalRetreatsArrayComponent,
  HistoricalRetreatFormComponent,
  // ImportDialogComponent,
  DeleteConfirmAlertDialog,
  AlternativeImportDialogComponent,
  PageNotFoundComponent,
];

// export * from './dialogs/import-dialog/import-dialog.component';
export * from './output/contact-info-details/contact-info-details.component';
export * from './dialogs/delete-confirm-alert/delete-confirm-alert.component';
export * from './dialogs/alternative-import-dialog/alternative-import-dialog.component';
