package pl.oaza.waw.rekolekcje.api.application.domain

interface ApplicationReadRepository {

  fun findAll(): List<RetreatApplication>

  fun findById(id: Long): RetreatApplication?

  fun existsById(id: Long): Boolean
}

interface ApplicationWriteRepository {

  fun save(entity: RetreatApplication): RetreatApplication

  fun deleteById(id: Long)

}