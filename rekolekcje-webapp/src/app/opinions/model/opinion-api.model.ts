export interface OpinionSubmittedResponse {
  id: number;
  memberId: number;
}
