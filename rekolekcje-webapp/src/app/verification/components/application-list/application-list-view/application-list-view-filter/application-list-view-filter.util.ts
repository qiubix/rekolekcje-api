import { RetreatSummary } from '@retreats/models/retreat-summary.model';
import { ApplicationSummary } from '@verification/model';
import { ApplicationTableFilter } from './application-list-view-filter.component';

export class ApplicationListViewFilterUtil {
  static filterApplications(
    applications: ApplicationSummary[],
    retreats: RetreatSummary[],
    filterValue: ApplicationTableFilter
  ): ApplicationSummary[] {
    if (!filterValue.stage) {
      return applications;
    }
    if (filterValue.stage === 'animators') {
      return applications;
    }
    const retreatIds = retreats.filter(it => it.stage === filterValue.stage).map(it => it.id);
    return applications.filter(it => retreatIds.includes(it.retreatId));
  }
}
