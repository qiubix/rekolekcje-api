package pl.oaza.waw.rekolekcje.api.email

import com.icegreen.greenmail.configuration.GreenMailConfiguration
import com.icegreen.greenmail.junit5.GreenMailExtension
import com.icegreen.greenmail.util.ServerSetup
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.RegisterExtension
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.web.context.WebApplicationContext
import org.testcontainers.shaded.org.apache.commons.io.IOUtils
import pl.oaza.waw.rekolekcje.api.shared.RestApi
import pl.oaza.waw.rekolekcje.api.email.domain.EmailFacade
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeUtility


@ExtendWith(SpringExtension::class)
@SpringBootTest(
  webEnvironment = WebEnvironment.RANDOM_PORT,
  classes = [EmailTestConfiguration::class],
  properties = ["spring.datasource.url=jdbc:tc:postgresql:10.6-alpine://rekolekcjedb-test",
    "spring.datasource.username=" + "postgres",
    "spring.datasource.password=" + "postgres",
    "spring.datasource.driver-class-name=org.testcontainers.jdbc.ContainerDatabaseDriver"
  ]
)
@ActiveProfiles("test", "test-local", "email")
@AutoConfigureJsonTesters
@Tag("integration")
internal class EmailIntegrationTest {

  @Autowired
  protected lateinit var webApplicationContext: WebApplicationContext
  @Autowired
  private lateinit var emailFacade: EmailFacade
  @Autowired
  protected lateinit var restApi: RestApi

  companion object {

    private val serverSetup = ServerSetup(3025, "localhost", ServerSetup.PROTOCOL_SMTP)

    @JvmField
    @RegisterExtension
    val smtpServer: GreenMailExtension = GreenMailExtension(serverSetup)
      .withConfiguration(GreenMailConfiguration.aConfig().withDisabledAuthentication())

  }

  @Test
  @WithMockUser
  fun `should send single email`() {
    // given
    val from = "rekolekcje@oaza.waw.pl"
    val to = "info@somemail.com"
    val subject = "Sample subject"
    val body = "Sample email"
    val simpleEmailRequest = SendEmailRequest(
      to = to,
      subject = subject,
      body = body
    )

    // when
    emailFacade.sendEmail(simpleEmailRequest)

    // then
    smtpServer.waitForIncomingEmail(1)
    val receivedMessages = smtpServer.receivedMessages
    assertThat(receivedMessages).hasSize(1)

    val email = receivedMessages[0]
    assertThat(email.from).hasSize(1)
    assertThat(email.from[0]).isEqualTo(InternetAddress(from))

    assertThat(email.allRecipients).hasSize(1)
    assertThat(email.allRecipients[0]).isEqualTo(InternetAddress(to))
    assertThat(email.subject).isEqualTo(subject)
    assertThat(email.content.toString()).isEqualToIgnoringWhitespace(body)
  }

  @Test
  @WithMockUser
  fun `should send email with template`() {
    // given
    val from = "rekolekcje@oaza.waw.pl"
    val to = "info@somemail.com"
    val subject = "Sample subject"
    val body = "Sample email"
    val request = SendEmailWithTemplateRequest(
      to = to,
      subject = subject,
      template = "email-template.html",
      templateModel = mapOf(
        "recipient" to "Sample recipient",
        "text" to body,
        "sender" to "Sample sender"
      )
    )

    // when
    val response = restApi.performPost("$EMAIL_API_URI/template", request)

    // then
    response.hasStatusOk()
    val receivedMessages = smtpServer.receivedMessages
    assertThat(receivedMessages).hasSize(1)

    val email = receivedMessages[0]
    assertThat(email.from).hasSize(1)
    assertThat(email.from[0]).isEqualTo(InternetAddress(from))

    assertThat(email.allRecipients).hasSize(1)
    assertThat(email.allRecipients[0]).isEqualTo(InternetAddress(to))
    assertThat(email.subject).isEqualTo(subject)
    val multipartContent = IOUtils.toString(MimeUtility.decode(email.inputStream, "quoted-printable"), "UTF-8")
    assertThat(multipartContent).contains(listOf(body, "Sample recipient", "Sample sender"))
  }

  @Test
  @WithMockUser
  fun `should send registration email`() {
    // given
    val from = "rekolekcje@oaza.waw.pl"
    val to = "info@somemail.com"
    val subject = "Sample subject"

    val recipientName = "Sample recipient"
    val recipientRetreat = "ONŻ 1 - II turnus"

    val request = SendEmailWithTemplateRequest(
      to = to,
      subject = subject,
      template = "submission-confirmation.html",
      templateModel = mapOf(
        "name" to recipientName,
        "email" to to,
        "retreat" to recipientRetreat
      )
    )

    // when
    val response = restApi.performPost("$EMAIL_API_URI/template", request)

    // then
    response.hasStatusOk()
    val receivedMessages = smtpServer.receivedMessages
    assertThat(receivedMessages).hasSize(1)

    val email = receivedMessages[0]
    assertThat(email.from).hasSize(1)
    assertThat(email.from[0]).isEqualTo(InternetAddress(from))

    assertThat(email.allRecipients).hasSize(1)
    assertThat(email.allRecipients[0]).isEqualTo(InternetAddress(to))
    assertThat(email.subject).isEqualTo(subject)
    val multipartContent = IOUtils.toString(MimeUtility.decode(email.inputStream, "quoted-printable"), "UTF-8")
    assertThat(multipartContent).contains(listOf(recipientRetreat))
  }
}
