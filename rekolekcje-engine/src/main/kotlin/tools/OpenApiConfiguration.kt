package pl.oaza.waw.rekolekcje.api.tools

import io.swagger.v3.oas.models.Components
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Contact
import io.swagger.v3.oas.models.info.Info
import io.swagger.v3.oas.models.info.License
import io.swagger.v3.oas.models.security.SecurityRequirement
import io.swagger.v3.oas.models.security.SecurityScheme
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class OpenApiConfiguration {

  companion object {
    private const val securitySchemeName = "Auth JWT"
  }

  @Bean
  fun customOpenAPI(): OpenAPI? {
    return OpenAPI()
      .info(metaData())
      .addSecurityItem(SecurityRequirement().addList(Companion.securitySchemeName))
      .components(
        Components().addSecuritySchemes(
          Companion.securitySchemeName, SecurityScheme()
          .name(Companion.securitySchemeName)
          .type(SecurityScheme.Type.HTTP)
          .scheme("bearer")
          .bearerFormat("JWT")
        )
      )
  }

  private fun metaData(): Info {
    return Info()
      .title("RekoGO API")
      .description("API for the system of managing retreats applications")
      .version("1.0.0-SNAPSHOT")
      .license(apiLicence())
      .contact(apiContact())
  }

  private fun apiLicence(): License {
    return License()
      .name("MIT")
      .url("https://opensource.org/licenses/MIT")
  }

  private fun apiContact(): Contact {
    return Contact()
      .name("Oaza devs")
      .email("devs@oaza.com")
      .url("https://gitlab.com/qiubix/rekolekcje-api")
  }

}
