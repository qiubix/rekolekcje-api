import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'reko-input-text',
  templateUrl: './default-text-input.component.html',
  styleUrls: ['./default-text-input.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DefaultTextInputComponent {
  @Input() inputControl: FormControl;
  @Input() labelKey: string;
  @Input() errorMessageKey = '';
  @Input() required = false;
}
