package pl.oaza.waw.rekolekcje.api.retreats.domain

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import pl.oaza.waw.rekolekcje.api.retreats.RetreatTest
import pl.oaza.waw.rekolekcje.api.retreats.sampleRetreat
import pl.oaza.waw.rekolekcje.api.retreats.sampleRetreatMember
import pl.oaza.waw.rekolekcje.api.retreats.withMembers
import pl.oaza.waw.rekolekcje.api.shared.value.Stage

internal class UpdateRetreatTest : RetreatTest() {

  @Test
  fun `should update retreat data`() {
    // given
    val first = sampleRetreat(Stage.ODB, Turn.I)
    val second = sampleRetreat(Stage.OND, Turn.II)
    val firstId = saveAndGetId(first)
    val secondWithId = commandFacade.save(second)

    // when
    val newRetreatData = updatedRetreatData(firstId)
    val updatedRetreat = commandFacade.save(newRetreatData)

    // then
    assertThat(updatedRetreat).isEqualTo(newRetreatData)
    assertThat(queryFacade.findAll()).containsExactly(updatedRetreat, secondWithId)
  }

  @Test
  fun `should not modify participants list when updating`() {
    // given
    val member = sampleRetreatMember(memberId = 123L, animator = false)
    val participant = sampleRetreatMember(memberId = 124L, animator = false)
    val animator = sampleRetreatMember(memberId = 125L, animator = true)
    val retreat = sampleRetreat().withMembers(setOf(member, participant, animator))
    val retreatId = saveAndGetId(retreat)

    // when
    val updatedRetreat = updatedRetreatData(retreatId)
    val returnedRetreat = commandFacade.save(updatedRetreat)

    // then
    assertThat(returnedRetreat.participants()).isEqualTo(setOf(member, participant))
    assertThat(returnedRetreat.animators()).isEqualTo(setOf(animator))
  }

  private fun updatedRetreatData(id: Long) = Retreat(
    id = id,
    stage = Stage.ONZ_II,
    status = RetreatStatus.HISTORICAL,
    localization = "New location",
    turn = Turn.IV,
    startDate = dateCreator.localDate("2018-07-01"),
    endDate = dateCreator.localDate("2018-07-15")
  )
}
