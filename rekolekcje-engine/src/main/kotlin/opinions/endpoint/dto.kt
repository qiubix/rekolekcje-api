package pl.oaza.waw.rekolekcje.api.opinions.endpoint

import pl.oaza.waw.rekolekcje.api.opinions.domain.Opinion
import pl.oaza.waw.rekolekcje.api.opinions.domain.Submitter
import java.time.LocalDate
import javax.validation.Valid
import javax.validation.constraints.Email
import javax.validation.constraints.Max
import javax.validation.constraints.NotBlank

data class SubmitOpinionRequest(

  val encryptedMemberId: String,

  @NotBlank
  @Max(value = 2000)
  val content: String,

  @field:Valid
  val submitter: SubmitterApiModel
)

data class SubmitterApiModel(
  @field:NotBlank
  val fullName: String,

  @field:Email
  val email: String,

  @field:NotBlank
  val phoneNumber: String,

  val role: Submitter.Role
) {
  fun toDomain() = Submitter(
    fullName = fullName,
    email = email,
    phoneNumber = phoneNumber,
    role = role
  )
}

data class OpinionDetailsResponse(
  val id: Long,
  val memberId: Long,
  val content: String,
  val createdDate: LocalDate,
  val submitter: SubmitterApiModel
) {
  companion object {
    fun of(opinion: Opinion) = with(opinion) {
      OpinionDetailsResponse(
        id = getId() ?: throw IllegalStateException("Submitted opinion must have id"),
        memberId = memberId,
        content = content,
        createdDate = createdDate,
        submitter = submitter.toDto()
      )
    }
  }
}

fun Submitter.toDto() = SubmitterApiModel(
  fullName = fullName,
  email = email,
  phoneNumber = phoneNumber,
  role = role
)

data class OpinionSubmittedResponse(
  //TODO: czy możemy po prostu zwrócić jakieś http ok bez danych? Nie ma potrzeby wyrzucać na zewnątrz systemu jakieś IDki
  // nie mam czasu, żeby z tym teraz eksperymentować; ale memberId wyrzuciłem na wszelki wypadek.
  val id: Long
) {
  companion object {
    fun of(opinion: Opinion) = OpinionSubmittedResponse(
      id = opinion.getId() ?: throw IllegalStateException("Created opinion must have id")
    )
  }
}
