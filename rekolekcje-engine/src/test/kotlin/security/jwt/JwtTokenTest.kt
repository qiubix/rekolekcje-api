package pl.oaza.waw.rekolekcje.api.security.jwt

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import java.time.LocalDateTime

class JwtTokenTest {

  private val testUsername = "testUser"
  private val secret = "my-secret-for-tests"

  private val defaultPayload = JwtTokenPayload(
    username = testUsername,
    roles = emptyList(),
    creationTime = LocalDateTime.now(),
    expirationTime = LocalDateTime.now().plusMinutes(1)
  )

  private fun JwtTokenPayload.withCreationTime(creationTime: LocalDateTime) = this.copy(creationTime = creationTime)
  private fun JwtTokenPayload.withExpirationTime(time: LocalDateTime) = this.copy(expirationTime = time)
  private fun JwtTokenPayload.withRoles(roles: List<String>) = this.copy(roles = roles)

  @Test
  fun `should generate different tokens for different creation dates`() {
    val firstToken = generateToken(defaultPayload, secret)
    val secondToken = generateToken(defaultPayload.withCreationTime(LocalDateTime.now().plusMinutes(1)), secret)

    assertThat(firstToken).isNotEqualTo(secondToken)
  }

  @Test
  fun `should get username from token`() {
    val token = generateToken(defaultPayload, secret)

    assertThat(token.getUsername()).isEqualTo(testUsername)
  }

  @Test
  fun `should not return username when token is empty`() {
    val emptyToken = JwtToken("", secret)

    assertThatExceptionOfType(IllegalArgumentException::class.java)
      .isThrownBy { emptyToken.getUsername() }
  }

  @Test
  fun `should get roles from token`() {
    val roles = listOf("admin", "user")
    val payload = defaultPayload.withRoles(roles)
    val token = generateToken(payload, secret)

    assertThat(token.getRoles()).isEqualTo(roles)
  }

  @Test
  fun `should extract creation time`() {
    val creationTime = LocalDateTime.of(2019, 8, 20, 0, 0)
    val payload = defaultPayload.withCreationTime(creationTime)
    val token = generateToken(payload, secret)

    assertThat(token.getCreatedDate()).isEqualTo(creationTime)
  }

  @Test
  fun `should extract expiration time`() {
    val expirationTime = defaultPayload.creationTime.plusDays(3)
    val payload = defaultPayload.withExpirationTime(expirationTime)
    val token = generateToken(payload, secret)

    assertThat(token.getExpirationDate()).isEqualTo(expirationTime.withNano(0))
  }

  @Test
  fun `should token expire after some time`() {
    val payload = defaultPayload.withExpirationTime(LocalDateTime.now().plusNanos(1))
    val token = generateToken(payload, secret)

    assertThat(token.isValid()).isFalse()
  }

  private data class ValidTokenTestData(
    val testCase: String,
    val tokenValue: String,
    val secret: String,
    val isValid: Boolean
  )

  private fun validTokenDatasource() = listOf(
    ValidTokenTestData("valid token", generateToken(defaultPayload, secret).token, secret, true),
    ValidTokenTestData("empty token", "", secret, false),
    ValidTokenTestData(
      "expired token",
      generateToken(defaultPayload.withExpirationTime(LocalDateTime.now().minusDays(1)), secret).token,
      secret,
      false
    ),
    ValidTokenTestData("invalid token", "aa.bb", secret, false),
    ValidTokenTestData("invalid secret", generateToken(defaultPayload, secret).token, "another-secret", false)
  )

  @TestFactory
  fun `should return true if token is valid`() = validTokenDatasource().map { testData ->
    DynamicTest.dynamicTest(testData.testCase) {
      val token = JwtToken(testData.tokenValue, testData.secret)
      assertThat(token.isValid()).isEqualTo(testData.isValid)
    }
  }

  @Test
  fun `should not validate token if signing key has changed`() {
    val originalTokenValue = generateToken(defaultPayload, secret).token
    val tokenWithIncorrectSecret = JwtToken(originalTokenValue, "new-secret-value")

    assertThat(tokenWithIncorrectSecret.isValid()).isFalse()
  }

  private data class TokenToRefreshTestData(
    val testCase: String,
    val token: JwtToken,
    val lastPasswordResetDate: LocalDateTime,
    val canBeRefreshed: Boolean
  )

  private fun tokenToRefreshDataSource() = listOf(
    TokenToRefreshTestData(
      "can be refreshed",
      generateToken(
        defaultPayload
          .withCreationTime(LocalDateTime.now().minusHours(2))
          .withExpirationTime(LocalDateTime.now().plusDays(1)),
        secret
      ),
      LocalDateTime.now().minusHours(3),
      true
    ),
    TokenToRefreshTestData(
      "is expired",
      generateToken(
        defaultPayload
          .withCreationTime(LocalDateTime.now().minusHours(2))
          .withExpirationTime(LocalDateTime.now().minusHours(1)),
        secret
      ),
      LocalDateTime.now().minusHours(3),
      false
    ),
    TokenToRefreshTestData(
      "token was created before password was reset",
      generateToken(
        defaultPayload
          .withCreationTime(LocalDateTime.now().minusHours(2))
          .withExpirationTime(LocalDateTime.now().plusDays(1)),
        secret
      ),
      LocalDateTime.now().minusHours(1),
      false
    )
  )

  @TestFactory
  fun `should allow token refresh`() = tokenToRefreshDataSource().map { testData ->
    DynamicTest.dynamicTest(testData.testCase) {
      val canBeRefreshed = testData.token.canBeRefreshed(testData.lastPasswordResetDate)
      assertThat(canBeRefreshed).isEqualTo(testData.canBeRefreshed)
    }
  }

  @Test
  fun `should refresh token`() {
    val token = generateToken(defaultPayload, secret)
    val expectedExpirationTime = LocalDateTime.now().plusSeconds(100).withNano(0)

    val refreshedToken = token.refresh(100)

    assertThat(refreshedToken).isNotEqualTo(token)
    assertThat(refreshedToken.isValid()).isTrue()
    assertThat(refreshedToken.getExpirationDate()).isEqualTo(expectedExpirationTime)
  }
}
