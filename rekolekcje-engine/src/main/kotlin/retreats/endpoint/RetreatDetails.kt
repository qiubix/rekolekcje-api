package pl.oaza.waw.rekolekcje.api.retreats.endpoint

import pl.oaza.waw.rekolekcje.api.core.ddd.ValueObject
import pl.oaza.waw.rekolekcje.api.members.domain.Pesel
import pl.oaza.waw.rekolekcje.api.members.endpoint.MemberDetails
import pl.oaza.waw.rekolekcje.api.retreats.domain.Retreat
import pl.oaza.waw.rekolekcje.api.retreats.domain.RetreatMember
import pl.oaza.waw.rekolekcje.api.retreats.domain.RetreatStatus
import pl.oaza.waw.rekolekcje.api.retreats.domain.Turn
import pl.oaza.waw.rekolekcje.api.shared.value.PaymentStatus
import pl.oaza.waw.rekolekcje.api.shared.value.Sex
import pl.oaza.waw.rekolekcje.api.shared.value.Stage
import java.time.LocalDate

@ValueObject
data class RetreatDetails(
  val id: Long? = null,
  val turn: Turn? = null,
  val stage: Stage? = null,
  val status: RetreatStatus = RetreatStatus.ACTIVE,
  val localization: String? = null,
  val startDate: LocalDate? = null,
  val endDate: LocalDate? = null,
  val moderatorName: String? = null,
  val moderatorPhoneNumber: String? = null,
  val priestName: String? = null,
  val priestPhoneNumber: String? = null,
  val statistics: RetreatStatisticsDto = RetreatStatisticsDto(),
  val participants: Set<RetreatMemberDto> = setOf(),
  val animators: Set<RetreatMemberDto> = setOf(),
  val participantsLimit: Int? = null,
  val minimumAnimatorsRequired: Int = 0
) {
  constructor(domainEntity: Retreat) : this(
    id = domainEntity.getId(),
    turn = domainEntity.turn,
    stage = domainEntity.stage,
    status = domainEntity.status ?: RetreatStatus.ACTIVE,
    localization = domainEntity.localization,
    startDate = domainEntity.startDate,
    endDate = domainEntity.endDate,
    moderatorName = domainEntity.moderatorName,
    moderatorPhoneNumber = domainEntity.moderatorPhoneNumber,
    priestName = domainEntity.priestName,
    priestPhoneNumber = domainEntity.priestPhoneNumber,
    statistics = RetreatStatisticsDto(
      participantsAmount = domainEntity.numberOfParticipants(),
      malesParticipants = domainEntity.numberOfMaleParticipants(),
      femalesParticipants = domainEntity.numberOfFemaleParticipants(),
      averageAge = domainEntity.averageParticipantsAge(),
      underageParticipants = domainEntity.numberOfUnderageParticipants()
    ),
    participants = domainEntity.participants().map { RetreatMemberDto(it) }.toSet(),
    animators = domainEntity.animators().map { RetreatMemberDto(it) }.toSet(),
    participantsLimit = domainEntity.participantsLimit,
    minimumAnimatorsRequired = domainEntity.minimumAnimatorsRequired
  )

  val hasEnoughAnimators = animators.size >= minimumAnimatorsRequired
}

data class RetreatStatisticsDto(
  val participantsAmount: Int = 0,
  val malesParticipants: Int = 0,
  val femalesParticipants: Int = 0,
  val averageAge: Double = 0.0,
  val underageParticipants: Int = 0
)

data class RetreatMemberDto(
  val memberId: Long,
  val firstName: String,
  val lastName: String,
  val sex: Sex,
  val age: Int,
  val paymentStatus: PaymentStatus,
  val animator: Boolean
) {
  constructor(domainObject: RetreatMember) : this(
    memberId = domainObject.memberId,
    firstName = domainObject.firstName,
    lastName = domainObject.lastName,
    sex = domainObject.sex,
    age = domainObject.age,
    paymentStatus = domainObject.paymentStatus,
    animator = domainObject.animator
  )

  constructor(
    memberDetails: MemberDetails,
    animator: Boolean,
    paymentStatus: PaymentStatus = PaymentStatus.NOTHING
  ) : this(
    memberId = memberDetails.id ?: throw IllegalStateException(),
    firstName = memberDetails.personalData.firstName,
    lastName = memberDetails.personalData.lastName,
    sex = Pesel(memberDetails.personalData.pesel).sex,
    age = Pesel(memberDetails.personalData.pesel).calculateAge(),
    paymentStatus = paymentStatus,
    animator = animator
  )
}
