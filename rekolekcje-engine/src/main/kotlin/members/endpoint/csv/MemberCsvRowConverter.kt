package pl.oaza.waw.rekolekcje.api.members.endpoint.csv

import pl.oaza.waw.rekolekcje.api.members.domain.KwcStatus
import pl.oaza.waw.rekolekcje.api.members.domain.OtherRetreats
import pl.oaza.waw.rekolekcje.api.members.domain.OtherSkills
import pl.oaza.waw.rekolekcje.api.members.domain.PersonalData
import pl.oaza.waw.rekolekcje.api.members.endpoint.AddressValue
import pl.oaza.waw.rekolekcje.api.members.endpoint.ExperienceAsAnimatorValue
import pl.oaza.waw.rekolekcje.api.members.endpoint.ContactInfo
import pl.oaza.waw.rekolekcje.api.members.endpoint.DeuterocatechumenateValue
import pl.oaza.waw.rekolekcje.api.members.endpoint.ExperienceValue
import pl.oaza.waw.rekolekcje.api.members.endpoint.HealthReportValue
import pl.oaza.waw.rekolekcje.api.members.endpoint.KwcDto
import pl.oaza.waw.rekolekcje.api.members.endpoint.FormationInCommunityValue
import pl.oaza.waw.rekolekcje.api.members.endpoint.MemberDetails
import pl.oaza.waw.rekolekcje.api.members.endpoint.PersonalDataValue
import pl.oaza.waw.rekolekcje.api.members.endpoint.RetreatAsAnimatorDto
import pl.oaza.waw.rekolekcje.api.members.endpoint.RetreatTurnDto
import pl.oaza.waw.rekolekcje.api.members.endpoint.TalentsValue
import pl.oaza.waw.rekolekcje.api.shared.value.Stage

class MemberCsvRowConverter(private val fieldToHeaders: Map<String, List<String>>) {

  companion object {
    const val motherContactKey = "mother"
    const val fatherContactKey = "father"
    const val emergencyContactKey = "emergencyContact"
    const val emergencyContactPhoneKey = "emergencyContactPhone"
    const val emergencyContactEmailKey = "emergencyContactEmail"
    const val odbParticipantKey = "odbParticipantKey"
    const val ondParticipantKey = "ondParticipantKey"
    const val onzIParticipantKey = "onzIParticipantKey"
    const val onzIIParticipantKey = "onzIIParticipantKey"
    const val triduumParticipantKey = "triduumParticipantKey"
    const val onzIIIParticipantKey = "onzIIIParticipantKey"
    const val odbAnimatorKey = "odbAnimatorKey"
    const val ondAnimatorKey = "ondAnimatorKey"
    const val onzIAnimatorKey = "onzIAnimatorKey"
    const val onzIIAnimatorKey = "onzIIAnimatorKey"
    const val onzIIIAnimatorKey = "onzIIIAnimatorKey"
  }

  fun read(row: Map<String, String>): MemberDetails {
    return MemberDetails(
      personalData = extractPersonalDataValue(row),
      healthReport = extractHealthReportValue(row),
      experience = extractExperienceValue(row)
    )
  }

  private fun extractPersonalDataValue(row: Map<String, String>): PersonalDataValue {
    return PersonalDataValue(
      firstName = extractMandatoryValue(row, PersonalDataValue::firstName.name),
      lastName = extractMandatoryValue(row, PersonalDataValue::lastName.name),
      pesel = extractMandatoryValue(row, PersonalDataValue::pesel.name),
      email = extractString(row, PersonalDataValue::email.name),
      phoneNumber = extractString(row, PersonalDataValue::phoneNumber.name),
      placeOfBirth = extractString(row, PersonalDataValue::placeOfBirth.name),
      nameDay = extractString(row, PersonalDataValue::nameDay.name),
      mother = extractFamilyContactInfo(row, motherContactKey),
      father = extractFamilyContactInfo(row, fatherContactKey),
      emergencyContact = extractContactInfo(row, emergencyContactKey, emergencyContactPhoneKey, emergencyContactEmailKey),
      schoolType = extractSchoolType(row, PersonalDataValue::schoolType.name),
      schoolYear = extractString(row, PersonalDataValue::schoolYear.name),
      address = extractAddressValue(row)
    )
  }

  private fun extractAddressValue(row: Map<String, String>): AddressValue {
    return AddressValue(
      city = extractString(row, AddressValue::city.name),
      postalCode = extractString(row, AddressValue::postalCode.name),
      streetName = extractString(row, AddressValue::streetName.name),
      streetNumber = extractString(row, AddressValue::streetNumber.name),
      flatNumber = extractString(row, AddressValue::flatNumber.name)
    )
  }

  private fun extractFamilyContactInfo(row: Map<String, String>, fieldName: String): ContactInfo? {
    val rawText = extractString(row, fieldName)
    return if (rawText != null) ContactInfo(fullName = rawText) else null
  }

  private fun extractContactInfo(row: Map<String, String>, nameAndSurnameField: String, phoneNumberField: String,
                                 emailField: String): ContactInfo? {
    val rawNames = extractString(row, nameAndSurnameField)
    return if (rawNames == null) {
      null
    } else {
      ContactInfo(
        fullName = rawNames,
        phoneNumber = extractString(row, phoneNumberField),
        email = extractString(row, emailField)
      )
    }
  }

  private fun extractHealthReportValue(row: Map<String, String>): HealthReportValue {
    return HealthReportValue(
      currentTreatment = extractString(row, HealthReportValue::currentTreatment.name),
      mentalDisorders = extractString(row, HealthReportValue::mentalDisorders.name),
      allergies = extractString(row, HealthReportValue::allergies.name),
      cannotHike = extractCannotHike(row),
      hikingContraindications = extractString(row, HealthReportValue::hikingContraindications.name),
      hasMotionSickness = extractBoolean(row, HealthReportValue::hasMotionSickness.name),
      medicalDiet = extractString(row, HealthReportValue::medicalDiet.name),
      other = extractString(row, HealthReportValue::other.name)
    )
  }

  private fun extractExperienceValue(row: Map<String, String>): ExperienceValue {
    val kwcStatus = extractKwcStatus(row, KwcDto::status.name)
    val kwc = if (kwcStatus != null) KwcDto(kwcStatus, extractKwcSince(row, KwcDto::year.name)) else null
    val deuterocatechumenate = DeuterocatechumenateValue(
      stepsTaken = extractString(row, DeuterocatechumenateValue::stepsTaken.name)?.toInt(),
      stepsPlannedThisYear = extractString(row, DeuterocatechumenateValue::stepsPlannedThisYear.name)?.toInt()
    ).takeUnless { it == DeuterocatechumenateValue() }
    val lastYearFormation = FormationInCommunityValue(
      formationInSmallGroup = extractBoolean(row, FormationInCommunityValue::formationInSmallGroup.name)
    ).takeUnless { it == FormationInCommunityValue() }
    val animatorExperience = ExperienceAsAnimatorValue(
      otherRetreats = extractOtherRetreats(row),
      retreatsAsAnimator = extractRetreatsAsAnimator(row),
      otherSkills = extractOtherSkills(row),
      communityFunctions = extractString(row, ExperienceAsAnimatorValue::communityFunctions.name),
      diakonia = extractString(row, ExperienceAsAnimatorValue::diakonia.name)
    ).takeUnless { it == ExperienceAsAnimatorValue() }
    val talents = TalentsValue(
      musicalTalents = extractString(row, TalentsValue::musicalTalents.name)
    ).takeUnless { it == TalentsValue() }
    return ExperienceValue(
      kwc = kwc,
      historicalRetreats = extractHistoricalRetreats(row),
      deuterocatechumenate = deuterocatechumenate,
      formationInCommunity = lastYearFormation,
      talents = talents,
      experienceAsAnimator = animatorExperience
    )
  }

  private fun extractMandatoryValue(row: Map<String, String>, fieldName: String): String {
    return extractString(row, fieldName) ?: throw IllegalStateException("Field $fieldName not found in $row.")
  }

  private fun extractString(row: Map<String, String>, fieldName: String): String? {
    val availableHeaders = fieldToHeaders[fieldName] ?: return null
    return availableHeaders.map { row[it] }.mapNotNull { it?.trim() }.firstOrNull()
  }

  private fun extractBoolean(row: Map<String, String>, fieldName: String): Boolean? {
    val rawValue = extractString(row, fieldName)?.toLowerCase() ?: return null
    return rawValue == "tak"
  }

  private fun extractCannotHike(row: Map<String, String>): Boolean? {
    val rawValue = extractString(row, HealthReportValue::hikingContraindications.name)?.toLowerCase() ?: return null
    return rawValue != "nie"
  }

  private val otherSkillsMap: Map<String, OtherSkills> = mapOf(
    "dobrze/chętnie śpiewa" to OtherSkills.SINGS,
    "gra na instrumencie" to OtherSkills.PLAYS_INSTRUMENT,
    "prawo jazdy kat. b" to OtherSkills.DRIVERS_LICENCE,
    "książeczka sanepidu" to OtherSkills.SANEPID_BOOK
  )

  private fun extractOtherSkills(row: Map<String, String>): Set<OtherSkills> {
    val rawValue = extractString(row, ExperienceAsAnimatorValue::otherSkills.name) ?: return emptySet()
    return rawValue.split(",").mapNotNull { otherSkillsMap[it.trim()] }.toSet()
  }

  private fun extractRetreatsAsAnimator(row: Map<String, String>): Set<RetreatAsAnimatorDto> {
    return listOfNotNull(
      extractRetreatAsAnimator(row, odbAnimatorKey, Stage.ODB),
      extractRetreatAsAnimator(row, ondAnimatorKey, Stage.OND),
      extractRetreatAsAnimator(row, onzIAnimatorKey, Stage.ONZ_I),
      extractRetreatAsAnimator(row, onzIIAnimatorKey, Stage.ONZ_II),
      extractRetreatAsAnimator(row, onzIIIAnimatorKey, Stage.ONZ_III)
    )
      .filter { it.quantity > 0 }
      .toSet()
  }

  private fun extractRetreatAsAnimator(row: Map<String, String>, key: String, stage: Stage): RetreatAsAnimatorDto? {
    val rawValue = extractString(row, key)
    return if (rawValue == null) {
      null
    } else {
      RetreatAsAnimatorDto(stage, rawValue.toInt())
    }
  }

  private val otherRetreatsMap: Map<String, OtherRetreats> = mapOf(
    "KODA" to OtherRetreats.KODA,
    "Szkoła Animatora (ukończona)" to OtherRetreats.ANIMATORS_SCHOOL,
    "Szkoła Animatora (w trakcie)" to OtherRetreats.ANIMATORS_SCHOOL_IN_PROGRESS,
    "ORAE" to OtherRetreats.ORAE,
    "ORD" to OtherRetreats.ORD,
    "KAMuzO" to OtherRetreats.KAMuzO,
    "Szkoła Animatora Muzycznego" to OtherRetreats.MUSIC_ANIMATOR_SCHOOL
  )

  private fun extractOtherRetreats(row: Map<String, String>): Set<OtherRetreats> {
    val rawValue = extractString(row, ExperienceAsAnimatorValue::otherRetreats.name) ?: return emptySet()
    return rawValue.split(",").mapNotNull { otherRetreatsMap[it.trim()] }.toSet()
  }

  private fun extractHistoricalRetreats(row: Map<String, String>): Set<RetreatTurnDto> {
    return listOfNotNull(
      extractHistoricalRetreat(extractString(row, odbParticipantKey), Stage.ODB),
      extractHistoricalRetreat(extractString(row, ondParticipantKey), Stage.OND),
      extractHistoricalRetreat(extractString(row, onzIParticipantKey), Stage.ONZ_I),
      extractHistoricalRetreat(extractString(row, onzIIParticipantKey), Stage.ONZ_II),
      extractHistoricalRetreat(extractString(row, triduumParticipantKey), Stage.Triduum),
      extractHistoricalRetreat(extractString(row, onzIIIParticipantKey), Stage.ONZ_III)
    )
      .flatten()
      .toSet()
  }

  private fun extractHistoricalRetreat(value: String?, stage: Stage): List<RetreatTurnDto>? {
    return if (value == null || value == "nie dotyczy") {
      null
    } else {
      value.split(",").map { RetreatTurnDto(stage, it.trim().toInt()) }
    }
  }

  private fun extractSchoolType(row: Map<String, String>, fieldName: String): PersonalData.SchoolType? {
    return when(extractString(row, fieldName)) {
      "student" -> PersonalData.SchoolType.UNIVERSITY
      "pracujący" -> PersonalData.SchoolType.GRADUATED
      "uczeń szkoły średniej" -> PersonalData.SchoolType.HIGH_SCHOOL
      "uczeń szkoły podstawowej" -> PersonalData.SchoolType.PRIMARY
      else -> null
    }
  }

  private fun extractKwcStatus(row: Map<String, String>, fieldName: String): KwcStatus? {
    return when(extractString(row, fieldName)) {
      "Członek" -> KwcStatus.MEMBER
      "Kandydat" -> KwcStatus.CANDIDATE
      "Nie należy" -> KwcStatus.NOT_ENGAGED
      else -> null
    }
  }

  private fun extractKwcSince(row: Map<String, String>, fieldName: String): Int? {
    val rawValue = extractString(row, fieldName)?.toLowerCase() ?: return null
    return if (rawValue == "nie dotyczy") {
      null
    } else {
      rawValue.toInt()
    }
  }
}