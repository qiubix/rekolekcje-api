import { Pipe, PipeTransform } from '@angular/core';
import { MemberStatus } from '../../models/member-status.model';

@Pipe({
  name: 'colorForMemberStatus'
})
export class ColorForMemberStatusPipe implements PipeTransform {
  transform(memberStatus: MemberStatus, args?: any): String {
    switch (memberStatus) {
      case MemberStatus.PROBLEM:
        return 'red';
      case MemberStatus.VERIFIED:
        return 'blue';
      case MemberStatus.REGISTERED:
        return 'green';
      default:
        return '#ffd740';
    }
  }
}
