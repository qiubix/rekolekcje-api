import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MemberDetailsViewComponent } from './member-details-view.component';

describe('AllDataDetailsComponent', () => {
  let component: MemberDetailsViewComponent;
  let fixture: ComponentFixture<MemberDetailsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MemberDetailsViewComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MemberDetailsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
