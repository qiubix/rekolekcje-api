package pl.oaza.waw.rekolekcje.api.members.endpoint.csv

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import pl.oaza.waw.rekolekcje.api.members.MemberTestFixture
import pl.oaza.waw.rekolekcje.api.members.domain.KwcStatus
import pl.oaza.waw.rekolekcje.api.members.domain.OtherRetreats
import pl.oaza.waw.rekolekcje.api.members.domain.OtherSkills
import pl.oaza.waw.rekolekcje.api.members.domain.PersonalData
import pl.oaza.waw.rekolekcje.api.members.endpoint.AddressValue
import pl.oaza.waw.rekolekcje.api.members.endpoint.ExperienceAsAnimatorValue
import pl.oaza.waw.rekolekcje.api.members.endpoint.ContactInfo
import pl.oaza.waw.rekolekcje.api.members.endpoint.DeuterocatechumenateValue
import pl.oaza.waw.rekolekcje.api.members.endpoint.ExperienceValue
import pl.oaza.waw.rekolekcje.api.members.endpoint.HealthReportValue
import pl.oaza.waw.rekolekcje.api.members.endpoint.KwcDto
import pl.oaza.waw.rekolekcje.api.members.endpoint.FormationInCommunityValue
import pl.oaza.waw.rekolekcje.api.members.endpoint.MemberDetails
import pl.oaza.waw.rekolekcje.api.members.endpoint.PersonalDataValue
import pl.oaza.waw.rekolekcje.api.members.endpoint.RetreatAsAnimatorDto
import pl.oaza.waw.rekolekcje.api.members.endpoint.RetreatTurnDto
import pl.oaza.waw.rekolekcje.api.members.endpoint.TalentsValue
import pl.oaza.waw.rekolekcje.api.shared.endpoint.csv.CsvConversionError
import pl.oaza.waw.rekolekcje.api.shared.value.Stage
import java.nio.file.Files
import java.nio.file.Paths

internal class MemberCsvConverterTest : MemberTestFixture() {

  data class TestCase<T>(
    val name: String,
    val fileName: String,
    val testedField: ((List<MemberDetails>) -> T),
    val expectedField: T
  )

  private fun dataSource() = listOf(
    TestCase("Animator: personal data", "Animatorzy", { it[0].personalData },
      PersonalDataValue(
        firstName = "Michał",
        lastName = "Powęski",
        email = "michal.poweski@gmail.com",
        pesel = "01250941592",
        phoneNumber = "122322433",
        address = AddressValue(
          streetName = "Taneczna",
          streetNumber = "69",
          flatNumber = "1",
          postalCode = "02-829",
          city = "Warszawa"
        ),
        mother = ContactInfo(fullName = "Ada"),
        father = ContactInfo(fullName = "Marian"),
        placeOfBirth = "Warszawa",
        emergencyContact = ContactInfo(
          fullName = "Ada Krowa",
          phoneNumber = "122443223"
        ),
        schoolYear = "3",
        schoolType = PersonalData.SchoolType.UNIVERSITY,
        nameDay = "03-02"
      )
    ),
    TestCase("Participant: personal data", "ONZ-pełnoletni", { it[0].personalData },
      PersonalDataValue(
        firstName = "Test",
        lastName = "Testowy",
        email = "michal.poweski@gmail.com",
        pesel = "01220633931",
        phoneNumber = "733733732",
        address = AddressValue(
          streetName = "Eluwina",
          streetNumber = "112",
          flatNumber = "2",
          postalCode = "02-300",
          city = "Warszau"
        ),
        mother = ContactInfo(fullName = "Aleksandra Polna"),
        father = ContactInfo(fullName = "Wiktor Testowy"),
        placeOfBirth = "Warszau",
        emergencyContact = ContactInfo(
          fullName = "Kacper Kacperski",
          phoneNumber = "999200200"
        ),
        schoolYear = "3",
        schoolType = PersonalData.SchoolType.UNIVERSITY,
        nameDay = "02-02"
      )
    ),
    TestCase("Animator: health report", "Animatorzy", { it[0].healthReport },
      HealthReportValue(
        currentTreatment = "NIE",
        mentalDisorders = "tak, depresja",
        allergies = "NIE",
        medicalDiet = "bezglutenową",
        cannotHike = false,
        hikingContraindications = "NIE",
        hasMotionSickness = true,
        other = "Chorowity tak ogólnie"
      )
    ),
    TestCase("Participant: health report", "ONZ-pełnoletni", { it[0].healthReport },
      HealthReportValue(
        currentTreatment = "NIE",
        mentalDisorders = "Tak, schizofrenia",
        allergies = "NIE",
        medicalDiet = "NIE",
        cannotHike = false,
        hikingContraindications = "NIE",
        hasMotionSickness = true,
        other = "Lubię placki"
      )
    ),
    TestCase("Animator: experience", "Animatorzy", { it[0].experience },
      ExperienceValue(
        kwc = KwcDto(
          status = KwcStatus.MEMBER,
          year = 2018
        ),
        historicalRetreats = setOf(
          RetreatTurnDto(Stage.ODB, 2012),
          RetreatTurnDto(Stage.OND, 2013),
          RetreatTurnDto(Stage.OND, 2015),
          RetreatTurnDto(Stage.ONZ_I, 2016),
          RetreatTurnDto(Stage.ONZ_II, 2018),
          RetreatTurnDto(Stage.ONZ_III, 2019)
        ),
        experienceAsAnimator = ExperienceAsAnimatorValue(
          otherSkills = setOf(
            OtherSkills.DRIVERS_LICENCE,
            OtherSkills.PLAYS_INSTRUMENT,
            OtherSkills.SINGS
          ),
          retreatsAsAnimator = setOf(
            RetreatAsAnimatorDto(Stage.OND, 1)
          ),
          otherRetreats = setOf(
            OtherRetreats.ANIMATORS_SCHOOL_IN_PROGRESS,
            OtherRetreats.KODA,
            OtherRetreats.ORAE
          ),
          communityFunctions = "Animator grupy przed OND",
          diakonia = "Liturgicznej, Oaz Rekolekcyjnych"
        )
      )
    ),
    TestCase("Participant: experience", "ONZ-pełnoletni", { it[0].experience },
      ExperienceValue(
        kwc = KwcDto(
          status = KwcStatus.CANDIDATE,
          year = 2019
        ),
        historicalRetreats = setOf(
          RetreatTurnDto(Stage.OND, 2017),
          RetreatTurnDto(Stage.OND, 2018),
          RetreatTurnDto(Stage.ONZ_I, 2019)
        ),
        formationInCommunity = FormationInCommunityValue(
          formationInSmallGroup = true
        ),
        talents = TalentsValue(
          musicalTalents = "Tak"
        ),
        deuterocatechumenate = DeuterocatechumenateValue(
          stepsTaken = 10,
          stepsPlannedThisYear = 0
        )
      )
    )
  )

  @TestFactory
  fun `should import member's data`() = dataSource().map { testCase ->
    dynamicTest(testCase.name) {
      // given
      val path = Paths.get("src/test/resources/${testCase.fileName}.csv")
      val content = String(Files.readAllBytes(path), Charsets.UTF_8)

      // when
      val convertedMembers = convertMembersFromCsv(content).convertedMembers

      // then
      assertThat(testCase.testedField(convertedMembers)).isEqualTo(testCase.expectedField)
    }
  }

  @Disabled
  @Test
  fun `should import all valid members and display information about errors`() {
    // given
    val errorRowNumber = 2
    val path = Paths.get("src/test/resources/Animatorzy-errors.csv")
    val content = String(Files.readAllBytes(path), Charsets.UTF_8)

    // when
    val result = convertMembersFromCsv(content)

    // then
    val validMembers = result.convertedMembers
    assertThat(validMembers).hasSize(2)
    assertThat(result.errors).hasSize(1)
      .startsWith(
        CsvConversionError(
          row = content.split("\n")[errorRowNumber],
          problematicFields = listOf("pesel")
        )
      )
  }

  @Test
  @Disabled
  fun `should ignore unknown or irrelevant columns when importing members`() {
    // given
    val content = """
      Sygnatura czasowa,Nazwisko,Imię (imiona),Kontaktowy adres e-mail RODZICA/OPIEKUNA,Diecezja,PESEL,Rok urodzenia
      2019-03-01 11:42:53,Lane ,Louis ,parents@gmail.com,Warszawska,07270908683,1999
    """.trimIndent()

    //when
    val convertedMembers = convertMembersFromCsv(content).convertedMembers

    // then
    val expectedMember = MemberDetails(
      personalData = PersonalDataValue(
        firstName = "Louis",
        lastName = "Lane",
        pesel = "07270908683"
      ),
      healthReport = HealthReportValue(),
      experience = ExperienceValue()
    )
    assertThat(convertedMembers[0]).isEqualTo(expectedMember)
  }

  @Test
  @Disabled
  fun `should import data of members correctly from file`() {
    // given
    val path = Paths.get("src/test/resources/Kopia-ODB-Odpowiedzi.csv")
    val content = String(Files.readAllBytes(path), Charsets.UTF_8)

    // when
    val convertedMembers = convertMembersFromCsv(content).convertedMembers

    // then
    val expectedDetails = MemberDetails(
      personalData = PersonalDataValue(
        firstName = "Mała",
        lastName = "Dziewczynka",
        email = "tata@dad.pl",
        pesel = "01320751383",
        placeOfBirth = "Chicago",
        nameDay = "02-02",
        phoneNumber = "444555666",
        address = AddressValue(
          city = "Chicago",
          postalCode = "22-345",
          streetName = "Amerykańska",
          streetNumber = "2345M",
          flatNumber = "345"
        ),
        father = ContactInfo(fullName = "Tata Małej"),
        mother = ContactInfo(fullName = "Mama Małej")
      ),
      healthReport = HealthReportValue(
        other = ""
      )
    )
    assertThat(convertedMembers[0]).isEqualTo(expectedDetails)
  }

}
