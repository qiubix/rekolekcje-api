
import com.adarshr.gradle.testlogger.TestLoggerExtension
import com.adarshr.gradle.testlogger.theme.ThemeType
import org.gradle.api.tasks.testing.logging.TestExceptionFormat

plugins {
  idea
  id("com.adarshr.test-logger")
  kotlin("jvm")
}

val mainSourceSet: SourceSet = project.the<SourceSetContainer>()["main"]
val testSourceSet: SourceSet = project.the<SourceSetContainer>()["test"]

sourceSets {
  create("verify") {
    compileClasspath += sourceSets.main.get().output + testSourceSet.output + testSourceSet.compileClasspath
    runtimeClasspath += sourceSets.main.get().output + testSourceSet.runtimeClasspath
  }
}

val verifyImplementation by configurations.getting {
  extendsFrom(configurations.implementation.get())
}

configurations["verifyRuntimeOnly"].extendsFrom(configurations.runtimeOnly.get())


val verifySourceSet: SourceSet = project.the<SourceSetContainer>()["verify"]

val verify = task<Test>("verify") {
  description = "Runs the acceptance and integration tests"
  group = "verification"
  testClassesDirs = verifySourceSet.output.classesDirs
  classpath = verifySourceSet.runtimeClasspath
  mustRunAfter(tasks["test"])
  useJUnitPlatform()
}

tasks["check"].dependsOn(verify)

tasks.withType<Test> {
  useJUnitPlatform()
  outputs.upToDateWhen { false }
  testLogging {
    events("passed", "skipped", "failed")
    exceptionFormat = TestExceptionFormat.FULL
    // TODO: log tests summary. See https://technology.lastminute.com/junit5-kotlin-and-gradle-dsl/
  }
}

extensions.getByType<TestLoggerExtension>().run {
  theme = ThemeType.STANDARD_PARALLEL
  slowThreshold = 500
}

idea {
  module {
    inheritOutputDirs = false
    outputDir = file(project.buildDir.toString() + "/classes/main/")

    testSourceDirs.add(file("verify"))
    testSourceDirs.addAll(verifySourceSet.allSource.srcDirs)
  }
}
