package pl.oaza.waw.rekolekcje.api.application.domain

enum class ApplicationType {
  PARTICIPANT,
  ANIMATOR
}