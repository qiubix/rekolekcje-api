import { Component, Input, OnInit } from '@angular/core';
import { Deuterocatechumenate } from '@shared/models';

@Component({
  selector: 'reko-deuterocatechumenate-details',
  templateUrl: './deuterocatechumenate-details.component.html',
  styleUrls: ['./deuterocatechumenate-details.component.scss']
})
export class DeuterocatechumenateDetailsComponent implements OnInit {
  _data: Deuterocatechumenate;

  @Input() data: Deuterocatechumenate;

  ngOnInit() {
    this._data = this.data ? this.data : {};
  }

  get stepsTakenOrPlanned() {
    let stepsTaken = this._data.stepsTaken ? this._data.stepsTaken : 0;
    let stepsPlanned = this._data.stepsPlannedThisYear ? this._data.stepsPlannedThisYear : 0;
    return stepsTaken + ' / ' + stepsPlanned;
  }

  get celebrationsTakenOrPlanned() {
    let celebrationsTaken = this._data.celebrationsTaken ? this._data.celebrationsTaken : 0;
    let celebrationsPlanned = this._data.celebrationsPlannedThisYear
      ? this._data.celebrationsPlannedThisYear
      : 0;
    return celebrationsTaken + ' / ' + celebrationsPlanned;
  }
}
