import { MemberStatus, PaymentStatus } from '@shared/models';

export interface MemberSummary {
  id?: number;
  firstName?: string;
  lastName?: string;
  age?: number;
  parishId?: number;
  communityId?: number;
  applyingForRetreatTurn?: number;
  animator?: boolean;
  memberStatus?: MemberStatus;
  paymentStatus?: PaymentStatus;
}

export interface MemberPublicData {
  fullName: string;
}
