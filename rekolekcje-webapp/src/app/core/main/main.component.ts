import { Component } from '@angular/core';

@Component({
  selector: 'reko-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent {}
