package pl.oaza.waw.rekolekcje.api.parish.domain

import org.springframework.data.repository.Repository
import pl.oaza.waw.rekolekcje.api.core.ddd.DomainRepository

@DomainRepository
internal interface RegionsRepository : Repository<Region, Long> {

  fun findById(id: Long): Region?

  fun findAll(): List<Region>

  fun save(region: Region): Region

  fun deleteById(id: Long)
}
