import { Community } from '../models/community.model';
import { CommunityActions } from './community-actions';

export namespace Communities {
  export interface State {
    communitiesList: Community[];
    communitiesListLoading: boolean;
    selectedCommunity: Community;
  }

  export const initialState: Communities.State = {
    communitiesList: [],
    communitiesListLoading: false,
    selectedCommunity: null
  };
}

export namespace CommunitiesReducer {
  export function reducer(
    state: Communities.State = Communities.initialState,
    action
  ): Communities.State {
    switch (action.type) {
      case CommunityActions.types.CreateCommunitySuccess: {
        return {
          ...state,
          communitiesList: [...state.communitiesList, action.payload]
        };
      }

      case CommunityActions.types.LoadCommunitiesList: {
        return {
          ...state,
          communitiesList: [],
          communitiesListLoading: true
        };
      }

      case CommunityActions.types.LoadCommunitiesListFail: {
        return {
          ...state,
          communitiesListLoading: false
        };
      }

      case CommunityActions.types.LoadCommunitiesListSuccess: {
        return {
          ...state,
          communitiesList: action.payload,
          communitiesListLoading: false
        };
      }

      case CommunityActions.types.DeleteCommunitySuccess: {
        return {
          ...state,
          communitiesList: state.communitiesList.filter(
            (community: Community) => community.id !== action.payload
          )
        };
      }

      case CommunityActions.types.UpdateCommunitySuccess: {
        return {
          ...state,
          communitiesList: state.communitiesList.map((community: Community) =>
            community.id === action.payload.id ? action.payload : community
          )
        };
      }

      default:
        return state;
    }
  }
}
