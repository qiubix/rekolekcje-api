package pl.oaza.waw.rekolekcje.api.members.domain

import javax.persistence.Embeddable

@Embeddable
data class MemberContact(
  val firstName: String? = null,
  val lastName: String? = null,
  val phoneNumber: String? = null,
  val email: String? = null
)