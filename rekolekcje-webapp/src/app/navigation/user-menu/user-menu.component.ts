import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'reko-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss']
})
export class UserMenuComponent implements OnInit {
  isAuthenticated: boolean;

  constructor(public authService: AuthService, private router: Router) {}

  ngOnInit(): void {
    this.isAuthenticated = this.authService.isAuthenticated();
  }

  get username(): string {
    return this.authService.getCurrentUsername();
  }

  userProfile() {
    this.router.navigate(['users', 'profile']);
  }

  onLogout(): void {
    this.authService.logout();
    this.router.navigate(['auth', 'login']);
  }

  navigateToUserList(): void {
    this.router.navigate(['users']);
  }
}
