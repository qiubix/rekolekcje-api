package pl.oaza.waw.rekolekcje.api.email.domain

import pl.oaza.waw.rekolekcje.api.email.SendEmailRequest
import pl.oaza.waw.rekolekcje.api.email.SendEmailWithTemplateRequest

internal class EmailFacadeImpl(private val emailService: EmailService) : EmailFacade {

  override fun sendEmail(email: SendEmailRequest) {
    emailService.sendSimpleMessage(
      email.to,
      email.subject,
      email.body
    )
  }

  override fun sendEmailWithTemplate(email: SendEmailWithTemplateRequest) {
    emailService.sendMessageWithTemplate(email.to, email.subject, email.template, email.templateModel)
  }
}
