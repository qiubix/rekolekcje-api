import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AnimatorAssignment, RetreatApplication, StatusWithReason } from '@verification/model';
import { MemberStatus, PaymentStatus } from '@shared/models';

@Component({
  selector: 'reko-application-details-view',
  templateUrl: './application-details-view.component.html',
  styleUrls: ['./application-details-view.component.scss']
})
export class ApplicationDetailsViewComponent implements OnInit {
  @Input() application: RetreatApplication;
  @Input() animator: boolean;

  @Output() goBack = new EventEmitter();
  @Output() edit = new EventEmitter<number>();
  @Output() delete = new EventEmitter<number>();
  @Output() changeApplicationStatus = new EventEmitter<StatusWithReason>();
  @Output() assignAnimator = new EventEmitter<AnimatorAssignment>();

  ngOnInit(): void {}

  onChangeApplicationStatus(status: StatusWithReason) {
    // console.log('Changing status to: ', status);
    // this.membersStore.dispatch(new MembersSharedActions.ChangeMemberStatus(this.member.id, status));
    this.changeApplicationStatus.emit(status);
  }

  onChangePaymentStatus(newStatus: string) {
    console.log('Changing payment status to: ', newStatus);
    // this.membersStore.dispatch(
    //   new MembersSharedActions.ChangePaymentStatus(this.member.id, newStatus)
    // );
  }

  onSelectRetreat(animatorAssignment: AnimatorAssignment) {
    // console.log('Selecting retreat: ', animatorAssignment.selectedRetreat);
    // TODO: dispatch proper action to inform backend about it
    this.application.retreatId = animatorAssignment.selectedRetreat;
    this.assignAnimator.emit(animatorAssignment);
  }

  get currentMemberStatus(): MemberStatus {
    return this.application.status;
  }

  get currentPaymentStatus(): PaymentStatus {
    return this.application.paymentStatus;
  }

  onGoBack() {
    this.goBack.emit();
  }

  onEdit() {
    this.edit.emit(this.application.id);
  }

  onDelete() {
    this.delete.emit(this.application.id);
  }
}
