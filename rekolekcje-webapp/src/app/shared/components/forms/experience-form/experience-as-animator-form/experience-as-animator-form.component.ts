import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import {
  Diakonia,
  ExperienceAsAnimator,
  OtherRetreats,
  OtherSkills,
  Stage,
  Talents
} from '@shared/models';

const resolvedPromise = Promise.resolve(null);

@Component({
  selector: 'reko-experience-as-animator-form',
  templateUrl: './experience-as-animator-form.component.html',
  styleUrls: ['./experience-as-animator-form.component.scss']
})
export class ExperienceAsAnimatorFormComponent implements OnInit {
  form: FormGroup;
  stages = Stage;
  stagesKeys = Object.keys(this.stages);
  otherRetreats = OtherRetreats;
  otherRetreatsKeys = Object.keys(this.otherRetreats);
  otherSkills = OtherSkills;
  otherSkillsKeys = Object.keys(this.otherSkills);
  diakonias = Diakonia;
  diakoniasKeys = Object.keys(this.diakonias);

  @Input() data: ExperienceAsAnimator;
  @Output() formReady = new EventEmitter<FormGroup>();

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.form = this.fb.group({
      communityFunctions: this.data ? this.data.communityFunctions : null,
      leadingGroupToFormationStage: this.data ? this.data.leadingGroupToFormationStage : null,
      retreatsAsAnimator: this.data ? this.data.retreatsAsAnimator : null,
      numberOfAnimatorDays: this.data ? this.data.numberOfAnimatorDays : null,
      diakonia: this.data ? this.data.diakonia : null,
      otherRetreats: this.data ? this.data.otherRetreats : null,
      otherSkills: this.data ? this.data.otherSkills : null
    });
    resolvedPromise.then(() => {
      this.formReady.emit(this.form);
    });
  }
}
