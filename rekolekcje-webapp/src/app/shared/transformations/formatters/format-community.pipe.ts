import { Pipe, PipeTransform } from '@angular/core';
import { Community } from '../../../diocese/community/models/community.model';

@Pipe({
  name: 'formatCommunity'
})
export class FormatCommunityPipe implements PipeTransform {
  transform(community: Community, args?: any): string {
    return community ? community.name : '';
  }
}
