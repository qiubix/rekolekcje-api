package pl.oaza.waw.rekolekcje.api.user.domain

enum class Role {
  ROLE_DOR,
  ROLE_USER,
  ROLE_ADMIN,
  ROLE_MODERATOR
}
