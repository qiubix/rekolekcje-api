import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  Output,
  ViewChild,
} from '@angular/core';
import { RetreatDetails } from '../../models/retreat-details.model';
import { DeleteConfirmAlertDialog } from '@shared/components';
import { Store } from '@ngrx/store';
import { Retreats } from '../../store/retreat-reducer';
import { Subject } from 'rxjs';
import { RetreatSummary } from '../../models/retreat-summary.model';
import { Stage } from '@shared/models';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'retreats-list',
  templateUrl: './retreats-list.component.html',
  styleUrls: ['./retreats-list.component.scss'],
})
export class RetreatsListComponent implements OnChanges, AfterViewInit, OnDestroy {
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  @Input() retreatsSummary: RetreatSummary[];

  @Output() deleteRetreat: EventEmitter<number> = new EventEmitter<number>();
  @Output() editRetreat: EventEmitter<number> = new EventEmitter<number>();
  @Output() onSelectRetreat: EventEmitter<RetreatDetails> = new EventEmitter<RetreatDetails>();

  dataSource: MatTableDataSource<RetreatSummary>;

  // @TODO choose only important columns to display
  displayedColumns = [
    'turn',
    'stage',
    'localization',
    'participantsAmount',
    'malesParticipants',
    'femalesParticipants',
    'dates',
    'status',
    'options',
  ];

  stages = Stage;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(public dialog: MatDialog, private retreatsStore: Store<Retreats.State>) {}

  ngOnChanges(): void {
    this.dataSource = new MatTableDataSource<RetreatSummary>(this.retreatsSummary);
  }

  // @TODO refactor this to set input
  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  goToDetails(retreat: RetreatSummary): void {
    this.onSelectRetreat.emit(retreat);
  }

  edit(retreatId: number): void {
    this.editRetreat.emit(retreatId);
  }

  openConfirmDeleteAlert(id: number) {
    this.dialog
      .open(DeleteConfirmAlertDialog, { disableClose: false })
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.deleteRetreat.emit(id);
        }
      });
  }
}
