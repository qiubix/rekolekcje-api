package pl.oaza.waw.rekolekcje.api.email.domain

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.mail.javamail.JavaMailSender
import org.thymeleaf.spring5.SpringTemplateEngine

@Configuration
internal class EmailConfiguration {

  @Bean
  fun emailService(emailSender: JavaMailSender, templateEngine: SpringTemplateEngine): EmailService {
    return EmailServiceImpl(emailSender, templateEngine)
  }

  @Bean
  fun emailCommandFacade(emailService: EmailService): EmailFacade {
    return EmailFacadeImpl(emailService)
  }
}
