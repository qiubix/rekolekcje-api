package pl.oaza.waw.rekolekcje.api.application

import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.DynamicTest.dynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import org.springframework.security.test.context.support.WithMockUser
import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationStatus
import pl.oaza.waw.rekolekcje.api.application.endpoint.ApplicationSummary
import pl.oaza.waw.rekolekcje.api.application.endpoint.ChangeApplicationStatusRequest
import pl.oaza.waw.rekolekcje.api.application.endpoint.ChangePaymentStatusRequest
import pl.oaza.waw.rekolekcje.api.application.fixture.BaseApplicationAcceptanceTest
import pl.oaza.waw.rekolekcje.api.application.fixture.animatorApplicationSubmissionDto
import pl.oaza.waw.rekolekcje.api.application.fixture.sampleMember
import pl.oaza.waw.rekolekcje.api.shared.value.PaymentStatus

internal class ApplicationStatusAcceptanceTest : BaseApplicationAcceptanceTest() {

  @Test
  @WithMockUser
  fun `should change payment status`() {
    // given
    val applicationId = someApplicationExists()

    // when
    val request = ChangePaymentStatusRequest(
      applicationId = applicationId,
      paymentStatus = PaymentStatus.DOWN_PAYMENT
    )
    val response = whenInApi.paymentStatusIsChanged(request)

    // then
    response.hasStatusOk()
    thenInApi.applicationHasCorrectStatus(applicationId, PaymentStatus.DOWN_PAYMENT)
  }

  @Test
  @WithMockUser
  // TODO
  @Disabled
  fun `should return 'NOT_FOUND' when trying to change status of non-existing application`() {
    // given
    val nonExistingApplicationId = 999L

    // when
    val request = ChangePaymentStatusRequest(
      applicationId = nonExistingApplicationId,
      paymentStatus = PaymentStatus.FULL
    )
    val response = whenInApi.paymentStatusIsChanged(request)

    // then
    response.hasStatusNotFound()
  }

  data class TestCase(
    val name: String,
    val initialStatus: ApplicationStatus,
    val targetStatus: ApplicationStatus,
    val reason: String? = null
  )

  private fun applicationStatusDataSource() = listOf(
    TestCase( "'WAITING' -> 'VERIFIED'", ApplicationStatus.WAITING, ApplicationStatus.VERIFIED),
    TestCase( "'WAITING' -> 'PROBLEM'", ApplicationStatus.WAITING, ApplicationStatus.PROBLEM),
    TestCase( "'WAITING' -> 'REJECTED'", ApplicationStatus.WAITING, ApplicationStatus.REJECTED, "Good reason"),
    TestCase( "'VERIFIED' -> 'REGISTERED'", ApplicationStatus.VERIFIED, ApplicationStatus.REGISTERED),
    TestCase( "'REJECTED' -> 'WAITING'", ApplicationStatus.REJECTED, ApplicationStatus.WAITING)
  )

  @TestFactory
  @WithMockUser
  fun `should change application's status`() = applicationStatusDataSource().map { testCase ->
    dynamicTest(testCase.name) {
      // given
      val id = someApplicationExistsWithStatus(testCase.initialStatus)

      // when
      val request = ChangeApplicationStatusRequest(id, testCase.targetStatus, testCase.reason)
      val response = whenInApi.applicationStatusIsChanged(request)

      // then
      response.hasStatusOk()
      val expectedSummary = ApplicationSummary(
        id = id,
        firstName = sampleMember().personalData.firstName,
        lastName = sampleMember().personalData.lastName,
        status = testCase.targetStatus,
        isAnimator = true
      )
      thenInApi.exists(expectedSummary)
    }
  }

  private fun emptyReasons() = listOf(
    "empty reason" to "",
    "null reason" to null
  )

  @TestFactory
  @WithMockUser
  fun `should reject request to reject application without reason`() = emptyReasons().map { (testCase, reason) ->
    dynamicTest(testCase) {
      // given
      val id = someApplicationExists()

      // when
      val request = ChangeApplicationStatusRequest(id, ApplicationStatus.REJECTED, reason)
      val response = whenInApi.applicationStatusIsChanged(request)

      // then
      response.hasStatusIsBadRequest()
    }
  }

  @Test
  @WithMockUser
  fun `should reject request to change application status to unknown value`() {
    // given
    val id = someApplicationExists()

    // when
    val response = whenInApi.applicationStatusIsChanged(id, "non-existing-status")

    // then
    response.hasStatusIsBadRequest()
  }

  private fun someApplicationExists(): Long {
    val application = animatorApplicationSubmissionDto(member = sampleMember())
    return whenInApi.alreadyExistsSomeApplication(application)
  }

  private fun someApplicationExistsWithStatus(status: ApplicationStatus): Long {
    val id = someApplicationExists()
    updateStatus(id, status)
    return id
  }
}