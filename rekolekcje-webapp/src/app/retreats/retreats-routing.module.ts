import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RetreatDetailsComponent } from './components/details/retreat-details.component';
import { RetreatsComponent } from './components/_root/retreats.component';
import { RetreatDetailsGuard } from './components/details/retreat-details.guard';

const retreatsRoutes: Routes = [
  {
    path: '',
    component: RetreatsComponent
  },
  {
    path: ':id',
    component: RetreatDetailsComponent,
    canActivate: [RetreatDetailsGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(retreatsRoutes)],
  exports: [RouterModule],
  providers: [RetreatDetailsGuard]
})
export class RetreatsRoutingModule {}
