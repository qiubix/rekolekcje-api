import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';
import { catchError } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class HttpRequestInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // Clone the request to add the new header.
    const authReq = req.clone({
      setHeaders: { Authorization: 'Bearer ' + this.authService.getToken() }
    });

    return next.handle(authReq).pipe(
      catchError((error, caught) => {
        //intercept the response error and displace it to the console
        console.log('Error Occurred');
        console.log(error);
        //return the error to the method that called it
        throw error;
      })
    );
  }
}
