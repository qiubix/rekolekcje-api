import { Component, Inject } from '@angular/core';
import { Parish } from '../../models/parish.model';
import { Observable } from 'rxjs/internal/Observable';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'parish-dialog',
  templateUrl: './parish-dialog.component.html',
  styleUrls: ['./parish-dialog.component.scss'],
})
export class ParishAddEditDialogComponent {
  dialogTitle: Observable<string>;
  formData: Parish;

  constructor(
    public dialogRef: MatDialogRef<ParishAddEditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.dialogTitle = data.dialogTitle;
    this.formData = data.formData ? data.formData : {};
  }

  onFormOutput(parish: Parish): void {
    if (parish) {
      parish.id = this.formData.id;
    }
    this.dialogRef.close(parish);
  }
}
