package pl.oaza.waw.rekolekcje.api.application.endpoint

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus
import java.lang.RuntimeException

@ResponseStatus(code = HttpStatus.NOT_FOUND)
class ApplicationNotFoundException(message: String) : RuntimeException(message)