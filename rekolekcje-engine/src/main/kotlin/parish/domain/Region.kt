package pl.oaza.waw.rekolekcje.api.parish.domain

import pl.oaza.waw.rekolekcje.api.core.ddd.AggregateRoot
import pl.oaza.waw.rekolekcje.api.parish.dto.RegionPayload
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@AggregateRoot
@Entity
@Table(name = "REGIONS")
internal data class Region(
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  val id: Long? = null,
  val name: String? = null
) {

  fun dto(): RegionPayload = RegionPayload(
    id = id,
    name = name
  )
}
