package pl.oaza.waw.rekolekcje.api.members.storage

import pl.oaza.waw.rekolekcje.api.members.domain.KwcStatus
import pl.oaza.waw.rekolekcje.api.members.endpoint.MemberDetails
import pl.oaza.waw.rekolekcje.api.members.utils.getAgeFromBirthDate
import pl.oaza.waw.rekolekcje.api.members.endpoint.AddressValue
import pl.oaza.waw.rekolekcje.api.members.endpoint.ExperienceValue
import pl.oaza.waw.rekolekcje.api.members.endpoint.HealthReportValue
import pl.oaza.waw.rekolekcje.api.members.endpoint.KwcDto
import pl.oaza.waw.rekolekcje.api.members.endpoint.PersonalDataValue
import pl.oaza.waw.rekolekcje.api.shared.value.Sex
import java.time.LocalDate

class PartialMemberFactory {

  fun withMinimalData(id: Long?): MemberDetails {
    return MemberDetails(
      id = id,
      personalData = PersonalDataValue(
        firstName = "Minimal",
        lastName = "Participant",
        pesel = "92042312348",
        birthDate = "23.04.1992",
        age = getAgeFromBirthDate("1992-04-23"),
        sex = Sex.FEMALE,
        parishId = 1L
      )
    )
  }

  fun withSampleData(id: Long?): MemberDetails {
    return MemberDetails(
      id = id,
      personalData = PersonalDataValue(
        firstName = "Sample",
        lastName = "Participant",
        pesel = "98101012349",
        parishId = 1L,
        christeningDate = LocalDate.of(1991, 11, 21),
        sex = Sex.FEMALE,
        birthDate = "10.10.1998",
        age = getAgeFromBirthDate("1998-10-10"),
        address = AddressValue(
          postalCode = "AB 321"
        )
      )
      ,
      experience = ExperienceValue(
        kwc = KwcDto(
          status = KwcStatus.CANDIDATE
        )
      ),
      healthReport = HealthReportValue(
        currentTreatment = "Broken leg"
      )
    )
  }
}
