package pl.oaza.waw.rekolekcje.api.members.domain

import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
data class Deuterocatechumenate(
  @Column(name = "DEUTEROCATECHUMENATE_YEAR")
  val year: Int? = null,
  val stepsTaken: Int? = null,
  val stepsPlannedThisYear: Int? = null,
  val celebrationsTaken: Int? = null,
  val celebrationsPlannedThisYear: Int? = null,
  val missionCelebrationYear: Int? = null,
  val callByNameCelebrationYear: Int? = null,
  val holyTriduumYear: Int? = null
)