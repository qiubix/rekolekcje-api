import org.sonarqube.gradle.SonarQubeExtension

plugins {
  id("org.sonarqube")
}

extensions.getByType<SonarQubeExtension>().run {
  properties {
//    properties["sonar.tests"] += sourceSets.verify.allSource.srcDirs.findAll { it.exists() }
    property("sonar.host.url", "https://sonarcloud.io")
    property("sonar.jacoco.reportPath", "${project.buildDir}/jacoco/merged.exec")
    property("sonar.kotlin.coveragePlugin", "jacoco")
    property("sonar.junit.reportsPath", "${project.buildDir}/reports")
    property("sonar.verbose", "true")
  }
}

project.tasks["sonarqube"].dependsOn("check")
