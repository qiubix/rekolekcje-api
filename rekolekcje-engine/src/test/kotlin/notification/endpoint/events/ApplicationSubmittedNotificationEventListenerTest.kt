package pl.oaza.waw.rekolekcje.api.notification.endpoint.events

import io.mockk.Called
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import notification.endpoint.events.ApplicationSubmittedNotificationEventListener
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import pl.oaza.waw.rekolekcje.api.application.domain.RetreatApplication
import pl.oaza.waw.rekolekcje.api.application.endpoint.events.ApplicationSubmittedEvent
import pl.oaza.waw.rekolekcje.api.application.query.ApplicationQueryFacade
import pl.oaza.waw.rekolekcje.api.application.sampleApplication
import pl.oaza.waw.rekolekcje.api.members.query.MembersQueryFacade
import pl.oaza.waw.rekolekcje.api.notification.CreateNotificationRequest
import pl.oaza.waw.rekolekcje.api.notification.domain.NotificationCommandFacade
import pl.oaza.waw.rekolekcje.api.notification.dto.NotificationReason

internal class ApplicationSubmittedNotificationEventListenerTest {
  private val notificationCommandFacadeMock = mockk<NotificationCommandFacade>()
  private val applicationQueryFacadeMock = mockk<ApplicationQueryFacade>()
  private val membersQueryFacadeMock = mockk<MembersQueryFacade>()
  private val eventListener = ApplicationSubmittedNotificationEventListener(
    notificationCommandFacadeMock,
    applicationQueryFacadeMock,
    membersQueryFacadeMock
  )

  private val applicationId = 321L
  private val memberId = 123L
  private val targetEmail = "some@email"
  private val sampleApplication = sampleApplication(memberId = memberId).withId(applicationId) as RetreatApplication
  private val event = ApplicationSubmittedEvent(
    applicationId = applicationId
  )

  @BeforeEach
  internal fun setUp() {
    every { notificationCommandFacadeMock.create(any()) } returns mockk()
    every { applicationQueryFacadeMock.findApplicationById(applicationId) } returns sampleApplication
  }

  @Test
  fun `should send notification to member when application is submitted`() {
    // given
    every { membersQueryFacadeMock.getEmail(memberId) } returns targetEmail

    // when
    eventListener.onApplicationEvent(event)

    // then
    val expectedNotificationRequest = CreateNotificationRequest(
      applicationId = applicationId,
      reason = NotificationReason.APPLICATION_SUBMITTED,
      targetEmail = targetEmail
    )
    verify { notificationCommandFacadeMock.create(expectedNotificationRequest) }
  }

  @Test
  fun `should not send notification if member has no email`() {
    // given
    every { membersQueryFacadeMock.getEmail(memberId) } returns null

    // when
    eventListener.onApplicationEvent(event)

    // then
    verify { notificationCommandFacadeMock wasNot Called }
  }

  @Test
  fun `should ask surety for opinion`() {
    // given
    every { membersQueryFacadeMock.getEmail(memberId) } returns targetEmail

    // when
    eventListener.onApplicationEvent(event)

    // then
    val expectedNotificationRequest = CreateNotificationRequest(
      applicationId = applicationId,
      reason = NotificationReason.OPINION_NEEDED,
      targetEmail = sampleApplication.surety?.email ?: throw IllegalStateException()
    )
    verify(exactly = 1) { notificationCommandFacadeMock.create(expectedNotificationRequest) }
  }

  @Test
  fun `should not send notification to if surety's email is not set`() {
    // given
    every { membersQueryFacadeMock.getEmail(memberId) } returns targetEmail
    every { applicationQueryFacadeMock.findApplicationById(applicationId) } returns
        sampleApplication(memberId = memberId, suretyEmail = null).withId(applicationId) as RetreatApplication

    // when
    eventListener.onApplicationEvent(event)

    // then send only notification to member
    verify(exactly = 1) { notificationCommandFacadeMock.create(any()) }
  }
}