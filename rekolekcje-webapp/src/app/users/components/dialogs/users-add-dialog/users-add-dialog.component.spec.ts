import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersAddEditDialogComponent } from './users-add-dialog.component';

describe('UsersAddDialogComponent', () => {
  let component: UsersAddEditDialogComponent;
  let fixture: ComponentFixture<UsersAddEditDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UsersAddEditDialogComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersAddEditDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
