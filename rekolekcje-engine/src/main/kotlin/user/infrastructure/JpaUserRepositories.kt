package pl.oaza.waw.rekolekcje.api.user.infrastructure

import org.springframework.data.repository.Repository
import pl.oaza.waw.rekolekcje.api.user.domain.User
import pl.oaza.waw.rekolekcje.api.user.domain.UserReadRepository
import pl.oaza.waw.rekolekcje.api.user.domain.UserWriteRepository

interface JpaUserReadRepository : UserReadRepository, Repository<User, Long>

interface JpaUserWriteRepository : UserWriteRepository, Repository<User, Long>
