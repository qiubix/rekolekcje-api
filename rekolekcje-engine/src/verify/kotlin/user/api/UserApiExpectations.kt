package pl.oaza.waw.rekolekcje.api.user.api

import org.assertj.core.api.Assertions.assertThat
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user
import pl.oaza.waw.rekolekcje.api.shared.BaseApiExpectations
import pl.oaza.waw.rekolekcje.api.shared.RestApi
import pl.oaza.waw.rekolekcje.api.user.domain.UserNotFoundException
import pl.oaza.waw.rekolekcje.api.user.endpoint.UserSummary

class UserApiExpectations(restApi: RestApi, private val baseUri: String): BaseApiExpectations(restApi) {

  fun thereIsOnlyOneUserWithUsername(existingUsername: String) {
    val allUsers = restApi.performGet(baseUri).getContentAsList<UserSummary>()
    assertThat(allUsers.map { it.username }).containsOnlyOnce(existingUsername)
  }

  fun existsUserWithUsername(username: String) {
    val allUsers = restApi.performGet(baseUri, user("admin").roles("ADMIN")).getContentAsList<UserSummary>()
    val user = allUsers.find { it.username == username }
    assertThat(user).isNotNull
  }

  fun userNoLongerExists(username: String) {
    val allUsernames = restApi.performGet(baseUri).getContentAsList<UserSummary>().map { it.username }
    assertThat(allUsernames).doesNotContain(username)
  }

  fun userIsEnabled(username: String, enabled: Boolean) {
    val allUsers = restApi.performGet(baseUri).getContentAsList<UserSummary>()
    val user = allUsers.find { it.username == username } ?: throw UserNotFoundException(username)
    assertThat(user.enabled).isEqualTo(enabled)
  }
}
