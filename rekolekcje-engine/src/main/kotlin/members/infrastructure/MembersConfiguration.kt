package pl.oaza.waw.rekolekcje.api.members.infrastructure

import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import pl.oaza.waw.rekolekcje.api.members.domain.MemberReadRepository
import pl.oaza.waw.rekolekcje.api.members.domain.MemberWriteRepository
import pl.oaza.waw.rekolekcje.api.members.domain.MembersCommandFacade
import pl.oaza.waw.rekolekcje.api.members.domain.MembersCommandsFacadeImpl
import pl.oaza.waw.rekolekcje.api.members.infrastructure.encryption.MembersEncryptionService
import pl.oaza.waw.rekolekcje.api.members.query.MembersQueryFacade
import pl.oaza.waw.rekolekcje.api.members.query.MembersQueryFacadeImpl
import pl.oaza.waw.rekolekcje.api.members.query.OpinionLinkQueryFacade
import pl.oaza.waw.rekolekcje.api.members.query.OpinionLinkQueryFacadeImpl

@Configuration
internal class MembersConfiguration {

  @Bean
  fun membersQueryFacade(memberReadRepository: MemberReadRepository): MembersQueryFacade {
    return MembersQueryFacadeImpl(memberReadRepository)
  }

  @Bean
  fun opinionLinkQueryFacade(membersEncryptionService: MembersEncryptionService): OpinionLinkQueryFacade {
    return OpinionLinkQueryFacadeImpl(membersEncryptionService)
  }

  @Bean
  fun membersCommandsFacade(
    memberReadRepository: MemberReadRepository,
    memberWriteRepository: MemberWriteRepository
  ): MembersCommandFacade =
    MembersCommandsFacadeImpl(
      memberReadRepository,
      memberWriteRepository
    )
}
