import { ContactInfo, MemberDetails, MemberStatus, PaymentStatus } from '@shared/models';

export enum ApplicationType {
  PARTICIPANT = 'PARTICIPANT',
  ANIMATOR = 'ANIMATOR'
}

export interface ApplicationSubmission {
  id?: number;
  applicationType: ApplicationType;
  memberDetails: MemberDetails;
  surety?: ContactInfo;
  animatorRoles?: string[];
  availability?: number[];
  retreatId?: number;
  other?: string;
  transportation?: Transportation;
  certificationNeeded?: boolean;
  certificationContact?: ContactInfo;
}

export interface RetreatApplication {
  id: number;
  applicationType: ApplicationType;
  member: MemberDetails;
  animatorRoles?: string[];
  availability?: number[];
  surety?: ContactInfo;
  status: MemberStatus;
  paymentStatus: PaymentStatus;
  retreatId?: number;
  selectedRole?: string;
  other?: string;
  transportation?: Transportation;
  certificationNeeded?: boolean;
  certificationContact?: ContactInfo;
}

export interface ApplicationSummary {
  id: number;
  firstName: string;
  lastName: string;
  status: string;
  paymentStatus: string;
  isAnimator: boolean;
  retreatId?: number;
}

export interface AnimatorAssignment {
  selectedRetreat: number;
  selectedRole: string;
}

export interface Transportation {
  to?: string;
  from?: string;
}
