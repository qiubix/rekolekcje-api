// import { Component, EventEmitter, Inject } from '@angular/core';
// import { UploaderOptions, UploadFile, UploadInput, UploadOutput } from 'ngx-uploader';
// import { environment } from '@env';
// import { TranslateService } from '@ngx-translate/core';
// import { Config } from '@config';
// import { AuthService } from 'app/auth/auth.service';
// import { Members } from '@members/state/members-reducer';
// import { Store } from '@ngrx/store';
// import { MembersSharedActions } from '@members/state/members-actions';
// import { VerificationActions } from '@verification/state/verification-actions';
// import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
// import { MatSnackBar } from '@angular/material/snack-bar';
//
// @Component({
//   selector: 'reko-import-dialog',
//   templateUrl: './import-dialog.component.html',
//   styleUrls: ['./import-dialog.component.scss'],
// })
// export class ImportDialogComponent {
//   files: UploadFile[] = [];
//   fileInvalid = true;
//   uploadInput: EventEmitter<UploadInput> = new EventEmitter<UploadInput>();
//   uploadOptions: UploaderOptions = {
//     concurrency: 1,
//     maxUploads: 1,
//     allowedContentTypes: ['text/csv'],
//   };
//   importUrl: string;
//
//   constructor(
//     private dialogRef: MatDialogRef<ImportDialogComponent>,
//     private snackBar: MatSnackBar,
//     private translate: TranslateService,
//     private store: Store<Members.State>,
//     private authService: AuthService,
//     @Inject(MAT_DIALOG_DATA) private data?: any
//   ) {
//     this.importUrl = data ? data.importUrl : '/members/import';
//   }
//
//   onUploadOutput(output: UploadOutput): void {
//     console.log('onUploadOutput: ', output);
//     this.fileInvalid = this.isFileInvalid();
//
//     switch (output.type) {
//       case 'addedToQueue':
//         console.log('Added file to queue...');
//         if (typeof output.file !== 'undefined') {
//           this.files.push(output.file);
//           console.log('Added file to list of files: ', output.file);
//         }
//         break;
//       case 'uploading':
//         console.log('Uploading file...');
//         if (typeof output.file !== 'undefined') {
//           // update current data in files array for uploading file
//           const index = this.files.findIndex(
//             (file) => typeof output.file !== 'undefined' && file.id === output.file.id
//           );
//           this.files[index] = output.file;
//         }
//         break;
//       case 'removed':
//         this.files = this.files.filter((file: UploadFile) => file !== output.file);
//         break;
//       case 'done':
//         // File uploaded
//         this.snackBar.open(
//           this.translate.instant('members.list.import.success'),
//           this.translate.instant('shared.ok'),
//           { duration: Config.notificationDuration }
//         );
//         this.store.dispatch(new MembersSharedActions.LoadMembersList());
//         this.store.dispatch(new VerificationActions.GetPendingApplications());
//         this.dialogRef.close();
//         break;
//     }
//   }
//
//   submit(): void {
//     console.log('Trying to upload file: ', this.files[0]);
//     const event: UploadInput = {
//       type: 'uploadFile',
//       url: environment.apiUrl + this.importUrl,
//       method: 'POST',
//       headers: { Authorization: 'Bearer ' + this.authService.getToken() },
//       file: this.files[0],
//     };
//
//     this.uploadInput.emit(event);
//   }
//
//   private isFileInvalid(): boolean {
//     console.log('Selected files: ', this.files);
//     return !this.files.length || !['csv'].includes(this.files[0].name.split('.')[1]);
//   }
// }
