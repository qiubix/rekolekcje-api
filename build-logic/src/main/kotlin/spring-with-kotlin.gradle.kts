plugins {
  id("org.springframework.boot")
  id("io.spring.dependency-management")
  kotlin("jvm")
  kotlin("plugin.spring")
  kotlin("plugin.noarg")
  kotlin("plugin.allopen")
  kotlin("plugin.jpa")
}

project.dependencies {
  add("implementation", Libs.Spring.actuator)
  add("implementation", Libs.Spring.web)
  add("implementation", Libs.Spring.dataJpa)
//  add("implementation", Libs.Spring.dataRest)
  add("implementation", Libs.Spring.jdbc)
  add("implementation", Libs.Spring.validation)
  add("implementation", Libs.Spring.security)
  add("implementation", Libs.Spring.mail)
  add("implementation", Libs.Spring.thymeleaf)

  add("implementation", Libs.Kotlin.sdtLib)
  add("implementation", Libs.Kotlin.reflect)
  add("implementation", Libs.Kotlin.jacksonModule)
  add("implementation", Libs.Kotlin.csv)

  add("runtimeOnly", RuntimeLibs.Spring.devTools)

  add("testImplementation", TestLibs.Spring.test) {
    exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
    exclude(group = "com.vaadin.external.google", module = "android-json")
  }
  add("testImplementation", TestLibs.Spring.security) {
    exclude(group = "com.vaadin.external.google", module = "android-json")
    exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
  }

}
