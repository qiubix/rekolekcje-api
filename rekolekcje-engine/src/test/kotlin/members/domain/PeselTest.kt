package pl.oaza.waw.rekolekcje.api.members.domain

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import pl.oaza.waw.rekolekcje.api.shared.value.Sex
import java.time.LocalDate
import java.time.temporal.ChronoUnit

internal class PeselTest {

  private fun peselToBirthDateData() = listOf(
    "93103002331" to "30.10.1993",
    "00212011230" to "20.01.2000",
    "85022831221" to "28.02.1985",
    "05311033544" to "10.11.2005",
    "44812233544" to "22.01.1844",
    "91441533544" to "15.04.2191",
    "25720933544" to "09.12.2225",
    "99090912324" to "09.09.1999"
  )

  @TestFactory
  fun `should conversion method return not null`() = peselToBirthDateData().map { (value, _) ->
    DynamicTest.dynamicTest("parse $value") {
      val pesel = Pesel(value)
      assertThat(pesel.birthDate).isNotBlank()
    }
  }

  @TestFactory
  fun `should convert PESEL to correct birth date`() = peselToBirthDateData().map { (value, correspondingBirthDate) ->
    DynamicTest.dynamicTest("convert $value to $correspondingBirthDate") {
      val pesel = Pesel(value)
      assertThat(pesel.birthDate).isEqualTo(correspondingBirthDate)
    }
  }

  @Test
  fun `should not allow too short PESEL`() {
    val tooShortPeselValue = "990101223"

    assertThatExceptionOfType(IllegalArgumentException::class.java)
      .isThrownBy { Pesel(tooShortPeselValue) }
  }

  @TestFactory
  fun `should not allow invalid PESEL`() = listOf(
    "35132100001",
    "35352100001",
    "99542100001",
    "99772100001"
  ).map { invalidPeselValue ->
    DynamicTest.dynamicTest("invalid pesel: $invalidPeselValue") {
      val pesel = Pesel(invalidPeselValue)
      assertThatExceptionOfType(IllegalArgumentException::class.java)
        .isThrownBy { pesel.birthDate }
    }
  }

  private fun ageFromPeselData() = listOf(
    "97011055994" to LocalDate.of(1997, 1, 10),
    "03240324729" to LocalDate.of(2003, 4, 3),
    "91121055378" to LocalDate.of(1991, 12, 10)
  )

  @TestFactory
  fun `should calculate age from PESEL`() = ageFromPeselData()
    .map { (peselValue, birthDate) ->
      DynamicTest.dynamicTest("for pesel $peselValue") {
        val expectedAge = ChronoUnit.YEARS.between(birthDate, LocalDate.now())
        val pesel = Pesel(peselValue)

        assertThat(pesel.calculateAge()).isEqualTo(expectedAge)
      }
    }

  private fun peselToSex() = listOf(
    "75062723635" to Sex.MALE,
    "49032118664" to Sex.FEMALE,
    "94042182912" to Sex.MALE,
    "61091826458" to Sex.MALE,
    "92061372639" to Sex.MALE,
    "79112043584" to Sex.FEMALE,
    "66120467424" to Sex.FEMALE
  )

  @TestFactory
  fun `should return correct sex calculated from PESEL`() = peselToSex().map { (peselValue, expectedSex) ->
    DynamicTest.dynamicTest("should extract $expectedSex from pesel $peselValue") {
      val pesel = Pesel(peselValue)
      assertThat(pesel.sex).isEqualTo(expectedSex)
    }
  }
}
