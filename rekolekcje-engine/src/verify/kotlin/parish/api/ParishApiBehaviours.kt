package pl.oaza.waw.rekolekcje.api.parish.api

import pl.oaza.waw.rekolekcje.api.shared.BaseApiBehaviour
import pl.oaza.waw.rekolekcje.api.shared.RestApi
import pl.oaza.waw.rekolekcje.api.parish.PARISH_API_URI
import pl.oaza.waw.rekolekcje.api.parish.dto.ParishNotFoundException
import pl.oaza.waw.rekolekcje.api.parish.dto.ParishPayload

class ParishApiBehaviours(restApi: RestApi) :
  BaseApiBehaviour<ParishPayload>(restApi, PARISH_API_URI) {

  fun resourceAmountIsRequested(): Int {
    return restApi.performGet(baseUri)
      .getContentAsList<ParishPayload>()
      .size
  }

  fun existsElementWithTheSameData(payload: ParishPayload): ParishPayload {
    val response = restApi.performGet(baseUri)
    val allElements = response.getContentAsList<ParishPayload>()
    val foundElement = allElements.find { haveTheSameData(payload, it) }
    return foundElement ?: throw getNotFoundException(payload)
  }

  override fun haveTheSameData(first: ParishPayload, second: ParishPayload): Boolean {
    return (first.name == second.name
        && first.address == second.address
        && first.region == second.region)
  }

  override fun getNotFoundException(payload: ParishPayload): RuntimeException {
    return ParishNotFoundException(payload)
  }
}
