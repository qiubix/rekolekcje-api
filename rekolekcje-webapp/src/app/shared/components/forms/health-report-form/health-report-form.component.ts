import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HealthReport } from '@shared/models/member-details.model';

@Component({
  selector: 'reko-health-report-form',
  templateUrl: './health-report-form.component.html',
  styleUrls: ['./health-report-form.component.scss']
})
export class HealthReportFormComponent implements OnInit {
  form: FormGroup;

  @Input() healthReportData: HealthReport;
  @Output() formReady = new EventEmitter<FormGroup>();

  constructor(private _fb: FormBuilder) {}

  ngOnInit(): void {
    this.form = this._fb.group({
      currentTreatment: this.healthReportData ? this.healthReportData.currentTreatment : '',
      mentalDisorders: this.healthReportData ? this.healthReportData.mentalDisorders : '',
      medications: this.healthReportData ? this.healthReportData.medications : '',
      allergies: this.healthReportData ? this.healthReportData.allergies : '',
      medicalDiet: this.healthReportData ? this.healthReportData.medicalDiet : '',
      cannotHike: this.healthReportData ? this.healthReportData.cannotHike : false,
      illnessHistory: this.healthReportData ? this.healthReportData.hikingContraindications : '',
      hasMotionSickness: this.healthReportData ? this.healthReportData.hasMotionSickness : false,
      other: this.healthReportData ? this.healthReportData.other : ''
    });
    this.formReady.emit(this.form);
  }
}
