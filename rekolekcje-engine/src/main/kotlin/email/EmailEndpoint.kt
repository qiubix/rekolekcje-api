package pl.oaza.waw.rekolekcje.api.email

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*
import pl.oaza.waw.rekolekcje.api.API_URI
import pl.oaza.waw.rekolekcje.api.email.domain.EmailFacade

const val EMAIL_API_URI = "$API_URI/email"

@RestController
@RequestMapping(EMAIL_API_URI)
class EmailEndpoint(private val emailFacade: EmailFacade) {

  @PostMapping
  @ResponseStatus(HttpStatus.OK)
  fun send(@RequestBody sendEmailRequest: SendEmailRequest) {
    emailFacade.sendEmail(sendEmailRequest)
  }

  @PostMapping("/template")
  @ResponseStatus(HttpStatus.OK)
  fun sendWithTemplate(@RequestBody sendEmailRequest: SendEmailWithTemplateRequest) {
    emailFacade.sendEmailWithTemplate(sendEmailRequest)
  }
}
