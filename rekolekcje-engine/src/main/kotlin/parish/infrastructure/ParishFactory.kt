package pl.oaza.waw.rekolekcje.api.parish.infrastructure

import pl.oaza.waw.rekolekcje.api.parish.domain.Parish
import pl.oaza.waw.rekolekcje.api.parish.dto.ParishPayload

internal class ParishFactory {
  fun from(parishPayload: ParishPayload): Parish {
    return PersistedParish(
      id = parishPayload.id,
      name = parishPayload.name,
      address = parishPayload.address
    )
  }
}
