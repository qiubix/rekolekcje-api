package pl.oaza.waw.rekolekcje.api.security

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import pl.oaza.waw.rekolekcje.api.shared.RestApi

@Configuration
@Profile("test")
internal class SecurityTestContext {

  @Bean
  fun authApi(restApi: RestApi): AuthApi {
    return AuthApi(restApi)
  }
}
