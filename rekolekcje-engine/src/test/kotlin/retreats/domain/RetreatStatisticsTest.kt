package pl.oaza.waw.rekolekcje.api.retreats.domain

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import pl.oaza.waw.rekolekcje.api.retreats.RetreatTest
import pl.oaza.waw.rekolekcje.api.retreats.sampleRetreat
import pl.oaza.waw.rekolekcje.api.retreats.sampleRetreatMember
import pl.oaza.waw.rekolekcje.api.shared.value.Sex

internal class RetreatStatisticsTest : RetreatTest() {

  val members = setOf(
    sampleRetreatMember(memberId = 2L, firstName = "Aaa", lastName = "One", age = 10, sex = Sex.FEMALE, animator = false),
    sampleRetreatMember(memberId = 4L, firstName = "Bbb", lastName = "One", age = 20, sex = Sex.MALE, animator = false),
    sampleRetreatMember(memberId = 7L, firstName = "Ccc", lastName = "One", age = 18, sex = Sex.FEMALE, animator = false),
    sampleRetreatMember(memberId = 8L, firstName = "Ddd", lastName = "One", age = 19, sex = Sex.MALE, animator = true),
    sampleRetreatMember(memberId = 9L, firstName = "Eee", lastName = "One", age = 17, sex = Sex.FEMALE, animator = false)
  )

  @Test
  fun `should calculate number of participants`() {
    // when
    val retreat = sampleRetreat(members = members)

    // then
    assertThat(retreat.numberOfParticipants()).isEqualTo(members.size - 1)
  }

  @Test
  fun `should calculate number of underage participants`() {
    // when
    val retreat = sampleRetreat(members = members)

    // then
    assertThat(retreat.numberOfUnderageParticipants()).isEqualTo(2)
  }

  @Test
  fun `should calculate number of female participants`() {
    // when
    val retreat = sampleRetreat(members = members)

    // then
    assertThat(retreat.numberOfFemaleParticipants()).isEqualTo(3)
  }

  @Test
  fun `should calculate number of male participants`() {
    // when
    val retreat = sampleRetreat(members = members)

    // then
    assertThat(retreat.numberOfMaleParticipants()).isEqualTo(1)
  }

  @Test
  fun `should calculate average age`() {
    // given
    val members = setOf(
      sampleRetreatMember(memberId = 2L, firstName = "Aaa", lastName = "One", age = 10),
      sampleRetreatMember(memberId = 4L, firstName = "Bbb", lastName = "One", age = 20),
      sampleRetreatMember(memberId = 7L, firstName = "Ccc", lastName = "One", age = 30),
      sampleRetreatMember(memberId = 8L, firstName = "Ddd", lastName = "One", age = 30),
      sampleRetreatMember(memberId = 9L, firstName = "Eee", lastName = "One", age = 10)
    )

    // when
    val retreat = sampleRetreat(members = members)

    // then
    assertThat(retreat.averageParticipantsAge()).isEqualTo(20.0)
  }

  @Test
  fun `should return 0 as average age when no members registered`() {
    // when
    val retreat = sampleRetreat(members = emptySet())

    // then
    assertThat(retreat.averageParticipantsAge()).isEqualTo(0.0)
  }

  @Test
  fun `should get only participants`() {
    // when
    val retreat = sampleRetreat(members = members)

    // then
    val expectedParticipants = members.filter { !it.animator }.toSet()
    assertThat(retreat.participants()).isEqualTo(expectedParticipants)
  }

  @Test
  fun `should get only animators`() {
    // when
    val retreat = sampleRetreat(members = members)

    // then
    val expectedAnimators = members.filter { it.animator }.toSet()
    assertThat(retreat.animators()).isEqualTo(expectedAnimators)
  }
}