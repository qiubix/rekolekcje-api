package pl.oaza.waw.rekolekcje.api.parish

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import pl.oaza.waw.rekolekcje.api.parish.domain.RegionsFacade
import pl.oaza.waw.rekolekcje.api.parish.dto.RegionPayload

const val REGIONS_API_URI = "$PARISH_API_URI/regions"

@RestController
@RequestMapping(REGIONS_API_URI)
class RegionEndpoint internal constructor(private val regionsFacade: RegionsFacade) {

  /**
   * Returns all regions in the system as list of [RegionPayload] objects.
   *
   * @return list of RegionPayload objects
   * @see RegionPayload
   */
  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  fun getAllRegions() = regionsFacade.findAll()

  /**
   * Finds region in repository by id.
   * The 404 (NOT FOUND) status code is returned when no such region exists.
   *
   * @param id id of region to be found
   * @return region data as [RegionPayload]
   */
  @GetMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  fun getOneRegion(@PathVariable id: Long): RegionPayload {
    return regionsFacade.findOne(id)
  }


  /**
   * This method provides saving regions to repository.
   *
   * @param newRegionData region data to be saved as [RegionPayload] object
   * @return region data RegionPayload object and 201 code status
   */
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  fun createRegion(@RequestBody newRegionData: RegionPayload): RegionPayload {
    return regionsFacade.save(newRegionData)
  }

  /**
   * This method allows to update region data.
   *
   * @param updatedRegionData updated region data as [RegionPayload] object
   * @return updated region data as RegionPayload object
   */
  @PutMapping
  @ResponseStatus(HttpStatus.OK)
  fun updateRegion(@RequestBody updatedRegionData: RegionPayload): RegionPayload {
    return regionsFacade.save(updatedRegionData)
  }

  /**
   * This method allows to delete region from repository.
   * The 404 status is returned if there is no region with the given id.
   *
   * @param id id of region to be deleted
   */
  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  fun deleteRegion(@PathVariable id: Long?) {
    regionsFacade.delete(id!!)
  }
}
