package pl.oaza.waw.rekolekcje.api.application.query

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import pl.oaza.waw.rekolekcje.api.application.ApplicationTestFixture
import pl.oaza.waw.rekolekcje.api.application.animatorApplication
import pl.oaza.waw.rekolekcje.api.application.domain.AnimatorRole
import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationStatus
import pl.oaza.waw.rekolekcje.api.application.participantApplication
import pl.oaza.waw.rekolekcje.api.shared.value.PaymentStatus

internal class FindApplicationTest : ApplicationTestFixture() {

  private val firstAnimatorApplication = animatorApplication(101L, ApplicationStatus.WAITING)
  private val secondAnimatorApplication = animatorApplication(102L, ApplicationStatus.VERIFIED)
  private val thirdAnimatorApplication = animatorApplication(103L, ApplicationStatus.REGISTERED)
  private val fourthAnimatorApplication = animatorApplication(104L, ApplicationStatus.PROBLEM)
  private val animatorApplications =
    listOf(
      firstAnimatorApplication, secondAnimatorApplication, thirdAnimatorApplication, fourthAnimatorApplication
    )
  private val firstParticipantApplication = participantApplication(201L, ApplicationStatus.WAITING, 1L)
  private val secondParticipantApplication = participantApplication(202L, ApplicationStatus.REGISTERED, 1L)
  private val thirdParticipantApplication = participantApplication(203L, ApplicationStatus.REJECTED, 2L)
  private val fourthParticipantApplication = participantApplication(204L, ApplicationStatus.RESIGNED, 2L)
  private val fifthParticipantApplication = participantApplication(101L, ApplicationStatus.REGISTERED, 2L)
  private val participantApplications =
    listOf(
      firstParticipantApplication, secondParticipantApplication, thirdParticipantApplication,
      fourthParticipantApplication, fifthParticipantApplication
    )

  @BeforeEach
  internal fun setUp() {
    persistAllApplications(listOf(
      firstAnimatorApplication, secondAnimatorApplication, thirdAnimatorApplication, fourthAnimatorApplication,
      firstParticipantApplication, secondParticipantApplication, thirdParticipantApplication,
      fourthParticipantApplication, fifthParticipantApplication
    ))
  }

  @Test
  fun `should find all applications`() {
    val foundApplications = queryFacade.findAllApplications()

    assertThat(foundApplications)
      .usingRecursiveComparison()
      .ignoringFields("id")
      .isEqualTo(animatorApplications + participantApplications)
  }

  @Test
  fun `should find all animator applications`() {
    val foundApplications = queryFacade.findAnimatorApplications()

    assertThat(foundApplications)
      .usingElementComparatorIgnoringFields("id")
      .isEqualTo(animatorApplications)
  }

  @Test
  fun `should find single animator application`() {
    val application = animatorApplication(
      paymentStatus = PaymentStatus.NOTHING,
      animatorRoles = setOf(AnimatorRole.LITURGIST)
    )
    val applicationId = persistApplication(application)

    val foundApplication = queryFacade.findApplicationById(applicationId)

    assertThat(foundApplication).isEqualTo(application.withId(applicationId))
  }

  @Test
  fun `should find all participant applications`() {
    val foundApplications = queryFacade.findParticipantApplications()

    assertThat(foundApplications)
      .usingElementComparatorIgnoringFields("id")
      .isEqualTo(participantApplications)
  }

  @Test
  fun `should find single participant applications`() {
    val application = participantApplication(321L)
    val applicationId = persistApplication(application)

    val foundApplication = queryFacade.findApplicationById(applicationId)

    assertThat(foundApplication).isEqualTo(application.withId(applicationId))
  }

  @Test
  fun `should find all pending applications`() {
    val foundApplications = queryFacade.findPendingApplications()

    val expectedApplications = listOf(firstAnimatorApplication, fourthAnimatorApplication, firstParticipantApplication)
    assertThat(foundApplications)
      .usingRecursiveComparison()
      .ignoringFields("id")
      .isEqualTo(expectedApplications)
  }

  @Test
  fun `should find all ready applications`() {
    val foundApplications = queryFacade.findReadyApplications()

    val expectedApplications = listOf(
      secondAnimatorApplication,
      thirdAnimatorApplication,
      secondParticipantApplication,
      thirdParticipantApplication,
      fourthParticipantApplication,
      fifthParticipantApplication
    )
    assertThat(foundApplications)
      .usingRecursiveComparison()
      .ignoringFields("id")
      .isEqualTo(expectedApplications)
  }

  @Test
  fun `should find application by retreat and member ids`() {
    val foundApplication = queryFacade.findByRetreatAndMember(2L, 204L)

    assertThat(foundApplication)
      .isNotNull
      .usingRecursiveComparison()
      .ignoringFields("id")
      .isEqualTo(fourthParticipantApplication)
  }
}