import { NgModule } from '@angular/core';
import { RetreatAddEditDialog } from './components/add-edit/retreat-dialog/add-edit-dialog.component';
import { RetreatsComponent } from './components/_root/retreats.component';
import { RetreatsRoutingModule } from './retreats-routing.module';
import { SharedModule } from '../shared/shared.module';
import { RetreatDetailsGuard } from './components/details/retreat-details.guard';
import { RetreatsService } from './store/retreats.service';

import * as fromComponents from './components';
import { RetreatDetailsViewComponent } from './components/details/retreat-details-view/retreat-details-view.component';

@NgModule({
  imports: [SharedModule, RetreatsRoutingModule],
  declarations: [fromComponents.components, RetreatDetailsViewComponent],
  providers: [RetreatDetailsGuard, RetreatsService],
  exports: [RetreatsComponent],
  entryComponents: [RetreatAddEditDialog]
})
export class RetreatsModule {}
