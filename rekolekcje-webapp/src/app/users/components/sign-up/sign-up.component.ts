import { Component, OnInit } from '@angular/core';
import { SaveUserRequest } from '@users/model/save-user-request.model';
import { UsersSharedActions } from '@users/store/users-actions';
import { Store } from '@ngrx/store';
import { Users } from '@users/store/users-reduces';
import { Location } from '@angular/common';

@Component({
  selector: 'reko-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  constructor(private readonly store: Store<Users.State>, private location: Location) {}

  ngOnInit() {}

  onSubmit(request: SaveUserRequest) {
    if (request) {
      this.store.dispatch(new UsersSharedActions.CreateUser(request));
    }
    this.location.back();
  }

  onCancel() {
    this.location.back();
  }
}
