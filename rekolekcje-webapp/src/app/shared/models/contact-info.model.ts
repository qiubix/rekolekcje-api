export interface ContactInfo {
  id?: number;
  fullName?: string;
  phoneNumber?: number;
  email?: string;
}
