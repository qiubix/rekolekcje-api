import { Component, Inject } from '@angular/core';
import { ChangePasswordRequest } from '@users/model/change-password-request.model';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'reko-change-password-dialog',
  templateUrl: './change-password-dialog.component.html',
  styleUrls: ['./change-password-dialog.component.scss'],
})
export class ChangePasswordDialogComponent {
  username: string;

  constructor(
    public dialogRef: MatDialogRef<ChangePasswordDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    if (data && data.username) {
      this.username = data.username;
    } else {
      this.dialogRef.close();
    }
  }

  onFormOutput(request: ChangePasswordRequest): void {
    if (request) {
      this.dialogRef.close(request);
    } else {
      this.dialogRef.close();
    }
  }
}
