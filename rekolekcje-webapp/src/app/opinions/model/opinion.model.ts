export interface Opinion {
  encryptedMemberId: string;
  content: string;
  submitter: Submitter;
}

export interface Submitter {
  fullName: string;
  email: string;
  phoneNumber: string;
  role: string;
}
