import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
  `java-gradle-plugin`
  `kotlin-dsl`
  `kotlin-dsl-precompiled-script-plugins`
}

repositories {
  mavenCentral()
  maven(url = "https://plugins.gradle.org/m2/")
}

java {
  sourceCompatibility = JavaVersion.VERSION_17
  targetCompatibility = JavaVersion.VERSION_17
}

tasks.withType<KotlinCompile> {
  kotlinOptions {
    freeCompilerArgs = listOf("-Xjsr305=strict")
    jvmTarget = "17"
  }
}

fun plugin(id: String, version: String) = "$id:$id.gradle.plugin:$version"

val sonarqubeVersion = "2.8"
val testloggerVersion = "2.0.0"
val kotlinVersion = "1.6.10"
val springBootVersion = "2.7.5"
val springDependencyManagementVersion = "1.0.15.RELEASE"

dependencies {
//  implementation(gradleApi())
  implementation("com.adarshr:gradle-test-logger-plugin:$testloggerVersion")
  implementation("org.sonarsource.scanner.gradle:sonarqube-gradle-plugin:$sonarqubeVersion")
  implementation("org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlinVersion")
  implementation("org.jetbrains.kotlin:kotlin-noarg:$kotlinVersion")
  implementation("org.jetbrains.kotlin:kotlin-allopen:$kotlinVersion")
  implementation("org.jetbrains.kotlin:kotlin-reflect:$kotlinVersion")
  implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:$kotlinVersion")
  implementation("org.springframework.boot:spring-boot-gradle-plugin:$springBootVersion")
  implementation("io.spring.gradle:dependency-management-plugin:$springDependencyManagementVersion")
}
