import { Action } from '@ngrx/store';
import { RetreatDetails } from '../models/retreat-details.model';
import { RetreatSummary } from '../models/retreat-summary.model';

export namespace RetreatsSharedActions {
  export const types = {
    CreateRetreat: '[Retreats] Create Retreat',
    CreateRetreatFail: '[Retreats] Create Retreat Fail',
    CreateRetreatSuccess: '[Retreats] Create Retreat Success',
    DeleteRetreat: '[Retreats] Delete Retreat',
    DeleteRetreatFail: '[Retreats] Delete Retreat Fail',
    DeleteRetreatSuccess: '[Retreats] Delete Retreat Success',
    LoadRetreatsList: '[Retreats] Load Retreats List',
    LoadRetreatsListFail: '[Retreats] Load Retreats List Fail',
    LoadRetreatsListSuccess: '[Retreats] Load Retreats List Success',
    SelectRetreat: '[Retreats] Select Retreat',
    SelectRetreatFail: '[Retreats] Select Retreats Fail',
    SelectRetreatSuccess: '[Retreats] Select Retreats Success',
    UpdateRetreat: '[Retreats] Update Retreats',
    UpdateRetreatFail: '[Retreats] Update Retreats Fail',
    UpdateRetreatSuccess: '[Retreats] Update Retreats Success'
  };

  /**
   * Create retreat actions
   */
  export class CreateRetreat implements Action {
    type = types.CreateRetreat;

    constructor(public payload: RetreatDetails) {}
  }

  export class CreateRetreatFail implements Action {
    type = types.CreateRetreatFail;

    constructor(public payload: any) {}
  }

  export class CreateRetreatSuccess implements Action {
    type = types.CreateRetreatSuccess;

    constructor(public payload: RetreatDetails) {}
  }

  /**
   * Delete retreat actions
   */
  export class DeleteRetreat implements Action {
    type = types.DeleteRetreat;

    constructor(public payload: number) {}
  }

  export class DeleteRetreatFail implements Action {
    type = types.DeleteRetreatFail;

    constructor(public payload: any) {}
  }

  export class DeleteRetreatSuccess implements Action {
    type = types.DeleteRetreatSuccess;

    constructor(public payload: number) {}
  }

  /**
   * Load retreat list actions
   */
  export class LoadRetreatsList implements Action {
    type = types.LoadRetreatsList;

    constructor(public payload?: any) {}
  }

  export class LoadRetreatsListFail implements Action {
    type = types.LoadRetreatsListFail;

    constructor(public payload: any) {}
  }

  export class LoadRetreatsListSuccess implements Action {
    type = types.LoadRetreatsListSuccess;

    constructor(public payload: RetreatSummary[]) {}
  }

  /**
   * Select retreat actions
   */
  export class SelectRetreat implements Action {
    type = types.SelectRetreat;

    constructor(public payload: number) {}
  }

  export class SelectRetreatFail implements Action {
    type = types.SelectRetreatFail;

    constructor(public payload: any) {}
  }

  export class SelectRetreatSuccess implements Action {
    type = types.SelectRetreatSuccess;

    constructor(public payload: RetreatDetails) {}
  }

  /**
   * Update retreat actions
   */
  export class UpdateRetreat implements Action {
    type = types.UpdateRetreat;

    constructor(public payload: RetreatDetails) {}
  }

  export class UpdateRetreatFail implements Action {
    type = types.UpdateRetreatFail;

    constructor(public payload: any) {}
  }

  export class UpdateRetreatSuccess implements Action {
    type = types.UpdateRetreatSuccess;

    constructor(public payload: RetreatDetails) {}
  }
}
