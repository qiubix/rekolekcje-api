plugins {
  jacoco
}

val verifySourceSet: SourceSet = project.the<SourceSetContainer>()["verify"]
val mainSourceSet: SourceSet = project.the<SourceSetContainer>()["main"]

val verifyTask: Task = tasks["verify"]

val jacocoVerifyTask = tasks.create<JacocoReport>("jacocoVerifyTestReport") {
  executionData(verifyTask)
  sourceSets(mainSourceSet)
  dependsOn(verifyTask)
}

tasks.withType<JacocoReport> {
  reports {
    xml.required.set(true)
  }
}

verifyTask.setFinalizedBy(setOf(jacocoVerifyTask))

val testSourceSet: SourceSet = project.the<SourceSetContainer>()["verify"]
val testTask: Task = tasks["test"]

//val jacocoMergeTask = tasks.create<JacocoMerge>("jacocoMerge") {
//  executionData(testSourceSet, verifySourceSet)
//  destinationFile = file("$buildDir/jacoco/merged.exec")
//  dependsOn(testTask, verifyTask)
//}

tasks.create<JacocoReport>("mergedReport") {
  executionData(testSourceSet, verifySourceSet)
//  executionData(jacocoMergeTask.destinationFile)
  sourceDirectories.setFrom(files(mainSourceSet.java.srcDirs))
  classDirectories.setFrom(files(mainSourceSet.output.classesDirs))
  reports {
    xml.required.set(false)
    html.required.set(true)
  }
//  dependsOn(jacocoMergeTask)
  dependsOn(testTask, verifyTask)
}

project.tasks["check"].dependsOn("jacocoTestReport")
