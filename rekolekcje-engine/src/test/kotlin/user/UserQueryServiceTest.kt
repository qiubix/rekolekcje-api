package pl.oaza.waw.rekolekcje.api.user

import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.Test
import pl.oaza.waw.rekolekcje.api.user.domain.UserNotFoundException
import pl.oaza.waw.rekolekcje.api.user.infrastructure.UserTestFixture

class UserQueryServiceTest : UserTestFixture() {

  @Test
  fun `should throw 'UserNotFoundException' when trying to get security details of non-existing user`() {
    assertThatExceptionOfType(UserNotFoundException::class.java)
      .isThrownBy { userQueryService.loadSecurityDetails("nonExistingUser") }
  }
}
