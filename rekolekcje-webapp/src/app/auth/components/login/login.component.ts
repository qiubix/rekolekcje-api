import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { Config } from '../../../../config/config';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from '../../auth.service';

@Component({
  selector: 'reko-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  authorizationInProgress = false;
  errorMessage: string;
  form: FormGroup;
  message: string;

  @ViewChild('loginForm') ngFormDirective: NgForm;

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(
    public authService: AuthService,
    public router: Router,
    private fb: FormBuilder,
    private translate: TranslateService
  ) {
    this.setMessage();
  }

  ngOnInit() {
    this.form = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    this.form.valueChanges
      .pipe(
        distinctUntilChanged(),
        debounceTime(Config.inputDebounceTime),
        takeUntil(this.ngUnsubscribe)
      )
      .subscribe(text => delete this.errorMessage);
  }

  setMessage() {
    if (this.authService.isAuthenticated()) {
      this.translate.get('auth.messages.logged-in').subscribe(result => (this.message = result));
    } else {
      this.translate.get('auth.messages.logged-out').subscribe(result => (this.message = result));
    }
  }

  authorize() {
    this.translate.get('auth.messages.trying').subscribe(result => (this.message = result));
    if (!this.form.valid) {
      this.translate.get('auth.messages.error').subscribe(result => (this.errorMessage = result));
      return;
    }

    if (this.authorizationInProgress) {
      return;
    } else {
      this.authorizationInProgress = true;
    }

    this.authService
      .authorize(this.form.value)
      .pipe(takeUntil(this.ngUnsubscribe))
      .subscribe(
        data => {
          this.translate
            .get('auth.messages.successful')
            .subscribe(result => (this.message = result));
          const redirectUrl = this.authService.redirectUrl ? this.authService.redirectUrl : '/';
          this.router.navigate([redirectUrl]);
        },
        error => {
          this.authorizationInProgress = false;
          this.translate.get('auth.messages.error').subscribe(result => (this.message = result));
        }
      );
  }

  logout() {
    this.authService.logout();
    this.setMessage();
  }
}
