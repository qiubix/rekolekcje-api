import { RetreatDetails } from '../models/retreat-details.model';
import { RetreatsSharedActions } from './retreat-actions';
import { RetreatSummary } from '../models/retreat-summary.model';

export namespace Retreats {
  export interface State {
    retreatsList: RetreatSummary[];
    retreatsLoading: boolean;
    selectedRetreat: RetreatDetails;
    retreatDetailsLoading: boolean;
  }

  export const initialState: Retreats.State = {
    retreatsList: [],
    retreatsLoading: false,
    selectedRetreat: null,
    retreatDetailsLoading: false
  };
}

export namespace RetreatsReducer {
  export function reducer(state: Retreats.State = Retreats.initialState, action): Retreats.State {
    switch (action.type) {
      case RetreatsSharedActions.types.CreateRetreatFail: {
        return { ...state };
      }

      case RetreatsSharedActions.types.CreateRetreatSuccess: {
        const list: RetreatSummary[] = [...state.retreatsList];
        list.push(action.payload);
        return { ...state, retreatsList: list };
      }

      case RetreatsSharedActions.types.DeleteRetreatFail: {
        return { ...state };
      }

      case RetreatsSharedActions.types.DeleteRetreatSuccess: {
        return {
          ...state,
          retreatsList: state.retreatsList.filter(
            (retreat: RetreatSummary) => retreat.id !== action.payload
          )
        };
      }

      case RetreatsSharedActions.types.LoadRetreatsListFail: {
        return { ...state };
      }

      case RetreatsSharedActions.types.LoadRetreatsListSuccess: {
        return { ...state, retreatsList: action.payload };
      }

      case RetreatsSharedActions.types.SelectRetreat: {
        return { ...state, retreatDetailsLoading: true };
      }

      case RetreatsSharedActions.types.SelectRetreatSuccess: {
        return { ...state, selectedRetreat: action.payload, retreatDetailsLoading: false };
      }

      case RetreatsSharedActions.types.SelectRetreatFail: {
        return { ...state, retreatDetailsLoading: false };
      }

      case RetreatsSharedActions.types.UpdateRetreatFail: {
        return { ...state };
      }

      case RetreatsSharedActions.types.UpdateRetreatSuccess: {
        return {
          ...state,
          retreatsList: state.retreatsList.map((retreat: RetreatSummary) => {
            if (retreat.id === action.payload.id) {
              return action.payload;
            } else {
              return retreat;
            }
          })
        };
      }

      default:
        return state;
    }
  }
}
