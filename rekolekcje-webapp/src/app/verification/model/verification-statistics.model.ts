export interface VerificationStatistics {
  numberOfActiveApplications: number;
  alreadyVerified: number;
  remainingToVerify: number;
  animatorsRemaining: number;
  participantsRemaining: number;
}
