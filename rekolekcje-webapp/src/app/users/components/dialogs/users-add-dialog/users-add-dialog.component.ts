import { Component, Inject } from '@angular/core';
import { SaveUserRequest } from '@users/model/save-user-request.model';
import { Observable } from 'rxjs/internal/Observable';
import { TranslateService } from '@ngx-translate/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'reko-users-add-dialog',
  templateUrl: './users-add-dialog.component.html',
  styleUrls: ['./users-add-dialog.component.scss'],
})
export class UsersAddEditDialogComponent {
  public dialogTitle$: Observable<string>;
  public user: SaveUserRequest;
  public showPasswordForm: Boolean;

  constructor(
    public dialogRef: MatDialogRef<UsersAddEditDialogComponent>,
    private readonly translate: TranslateService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.user = {
      username: data ? data.user.username : '',
      password: '',
      email: data ? data.user.email : '',
      enabled: data ? data.user.enabled : true,
      roles: data ? data.user.roles : [],
    };
    this.showPasswordForm = data ? data.showPasswordForm : true;
    const titleKey = data ? 'editUserTitle' : 'addUserTitle';
    this.dialogTitle$ = translate.get('usersManagement.dialog.' + titleKey);
  }

  onFormOutput(request: SaveUserRequest): void {
    if (request) {
      this.dialogRef.close(request);
    } else {
      this.dialogRef.close();
    }
  }
}
