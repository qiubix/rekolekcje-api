package pl.oaza.waw.rekolekcje.api.application.api

import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationStatus
import pl.oaza.waw.rekolekcje.api.application.endpoint.ApplicationEndpoint.Companion.ANIMATORS_URI
import pl.oaza.waw.rekolekcje.api.application.endpoint.ApplicationEndpoint.Companion.APPLICATION_URI
import pl.oaza.waw.rekolekcje.api.application.endpoint.ApplicationEndpoint.Companion.ASSIGN_URI
import pl.oaza.waw.rekolekcje.api.application.endpoint.ApplicationEndpoint.Companion.PARTICIPANTS_URI
import pl.oaza.waw.rekolekcje.api.application.endpoint.ApplicationEndpoint.Companion.PAYMENT_URI
import pl.oaza.waw.rekolekcje.api.application.endpoint.ApplicationEndpoint.Companion.STATS_URI
import pl.oaza.waw.rekolekcje.api.application.endpoint.ApplicationEndpoint.Companion.STATUS_URI
import pl.oaza.waw.rekolekcje.api.application.endpoint.ApplicationSubmissionDto
import pl.oaza.waw.rekolekcje.api.application.endpoint.AssignAnimatorRequest
import pl.oaza.waw.rekolekcje.api.application.endpoint.ChangeApplicationStatusRequest
import pl.oaza.waw.rekolekcje.api.application.endpoint.ChangePaymentStatusRequest
import pl.oaza.waw.rekolekcje.api.application.endpoint.RetreatApplicationDto
import pl.oaza.waw.rekolekcje.api.application.fixture.SampleApplicationData
import pl.oaza.waw.rekolekcje.api.application.fixture.animatorApplicationSubmissionDto
import pl.oaza.waw.rekolekcje.api.application.fixture.participantApplicationSubmissionDto
import pl.oaza.waw.rekolekcje.api.shared.Response
import pl.oaza.waw.rekolekcje.api.shared.RestApi

class ApplicationApiBehaviours(private val restApi: RestApi) {

  companion object {
    const val BASE_URI = APPLICATION_URI
    const val BASE_ANIMATOR_URI = "$BASE_URI$ANIMATORS_URI"
    const val BASE_PARTICIPANT_URI = "$BASE_URI$PARTICIPANTS_URI"
    const val ANIMATOR_IMPORT_URI = "$BASE_ANIMATOR_URI/import"
    const val PARTICIPANT_IMPORT_URI = "$BASE_PARTICIPANT_URI/import"
  }

  fun allAnimatorApplicationsAreRequested(): Response = restApi.performGet(BASE_ANIMATOR_URI)

  fun singleApplicationIsRequested(id: Long): Response = restApi.performGet("$BASE_URI/$id")

  fun singleApplicationIsDeleted(id: Long): Response = restApi.performDelete("$BASE_URI/$id")

  fun newApplicationIsSubmitted(payload: ApplicationSubmissionDto): Response =
    restApi.performPost(BASE_URI, payload)

  fun alreadyExistSomeApplications(data: List<SampleApplicationData>) {
    submitAndChangeStatus(
      data.filter { it.isAnimator },
      { animatorApplicationSubmissionDto(member = it.member) },
      { alreadyExistsSomeApplication(it) }
    )

    submitAndChangeStatus(
      data.filter { !it.isAnimator },
      { participantApplicationSubmissionDto(member = it.member) },
      { alreadyExistsSomeApplication(it) }
    )
  }

  private fun <T> submitAndChangeStatus(
    data: List<SampleApplicationData>,
    mapper: ((SampleApplicationData) -> T),
    submitFunction: ((T) -> Long)
  ) {
    data.map {
      val submissionDto = mapper.invoke(it)
      val applicationId = submitFunction.invoke(submissionDto)
      if (it.status != ApplicationStatus.WAITING) {
        applicationStatusIsChanged(
          ChangeApplicationStatusRequest(id = applicationId, status = it.status)
        )
      }
      applicationId
    }
  }

  fun alreadyExistSomeApplications(applications: List<ApplicationSubmissionDto>): List<Long> {
    return applications.map { alreadyExistsSomeApplication(it) }
  }

  fun alreadyExistsSomeApplication(application: ApplicationSubmissionDto): Long {
    return restApi.performPost(BASE_URI, application).getContent<RetreatApplicationDto>().id
  }

  fun fileIsUploadedForAnimators(fileContent: String): Response =
    restApi.performFileUpload(ANIMATOR_IMPORT_URI, fileContent)

  fun fileIsUploadedForParticipants(fileContent: String): Response =
    restApi.performFileUpload(PARTICIPANT_IMPORT_URI, fileContent)

  fun paymentStatusIsChanged(request: ChangePaymentStatusRequest): Response =
    restApi.performPost("$BASE_URI$PAYMENT_URI", request)

  fun applicationStatusIsChanged(request: ChangeApplicationStatusRequest): Response =
    restApi.performPost("$BASE_URI$STATUS_URI", request)

  fun applicationStatusIsChanged(id: Long, status: String): Response =
    restApi.performPost("$BASE_URI$STATUS_URI", object { val id = id; val status = status})

  fun verificationStatisticsAreRequested(): Response = restApi.performGet("$BASE_URI/$STATS_URI")

  fun allParticipantApplicationsAreRequested(): Response = restApi.performGet(BASE_PARTICIPANT_URI)

  fun allPendingApplicationsAreRequested(): Response {
    TODO("Not yet implemented")
  }

  fun allReadyApplicationsAreRequested(): Response {
    TODO("Not yet implemented")
  }

  fun updatedApplicationIsSubmitted(id: Long, application: ApplicationSubmissionDto): Response =
    restApi.performPut("$BASE_URI/$id", application)

  fun animatorIsAssignedToRetreat(request: AssignAnimatorRequest): Response =
      restApi.performPost("$BASE_ANIMATOR_URI$ASSIGN_URI", request)

}