package pl.oaza.waw.rekolekcje.api.members.infrastructure.encryption

import org.springframework.beans.factory.annotation.Value
import org.springframework.security.crypto.codec.Hex
import org.springframework.stereotype.Service
import java.nio.ByteBuffer
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import java.security.SecureRandom
import java.security.spec.KeySpec
import java.util.*
import javax.crypto.Cipher
import javax.crypto.SecretKey
import javax.crypto.SecretKeyFactory
import javax.crypto.spec.GCMParameterSpec
import javax.crypto.spec.PBEKeySpec
import javax.crypto.spec.SecretKeySpec


@Service
internal class MembersEncryptionService(
    @Value("\${application.security.salt}") private val saltHex: String,
    @Value("\${application.security.password}") private val password: String
) {

  private val ENCRYPT_ALGO = "AES/GCM/NoPadding"
  private val TAG_LENGTH_BIT = 128
  private val IV_LENGTH = 16

  private val UTF_8: Charset = StandardCharsets.UTF_8

  // AES key derived from a password
  private fun getKey(): SecretKey {
    val factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256")
    val salt = Hex.decode(saltHex);
    val spec: KeySpec = PBEKeySpec(password.toCharArray(), salt, 65536, 256)
    return SecretKeySpec(factory.generateSecret(spec).encoded, "AES")
  }

  // AES-GCM needs GCMParameterSpec
  private fun encrypt(plainText: ByteArray, secret: SecretKey, iv: ByteArray): ByteArray {
    val cipher: Cipher = Cipher.getInstance(ENCRYPT_ALGO)
    cipher.init(Cipher.ENCRYPT_MODE, secret, GCMParameterSpec(TAG_LENGTH_BIT, iv))
    return cipher.doFinal(plainText)
  }


  // prefix IV length + IV bytes to cipher text
  private fun encryptWithPrefixIV(plainText: ByteArray, secret: SecretKey, iv: ByteArray): ByteArray {
    val cipherText = encrypt(plainText, secret, iv)
    return ByteBuffer.allocate(iv.size + cipherText.size)
        .put(iv)
        .put(cipherText)
        .array()
  }

  private fun decrypt(cipherText: ByteArray, secret: SecretKey, iv: ByteArray): String {
    val cipher = Cipher.getInstance(ENCRYPT_ALGO)
    cipher.init(Cipher.DECRYPT_MODE, secret, GCMParameterSpec(TAG_LENGTH_BIT, iv))
    val plainText = cipher.doFinal(cipherText)
    return String(plainText, UTF_8)
  }

  private fun getRandomInitializationVector(): ByteArray {
    val nonce = ByteArray(IV_LENGTH)
    SecureRandom().nextBytes(nonce)
    return nonce
  }

  fun decryptWithPrefixIniatilizationVector(encryptedInput: ByteArray, secret: SecretKey): String {
    val bb = ByteBuffer.wrap(encryptedInput)
    val iv = ByteArray(IV_LENGTH)
    bb[iv]
    val cipherText = ByteArray(bb.remaining())
    bb[cipherText]
    return decrypt(cipherText, secret, iv)
  }

  fun encryptMemberId(id: Long): String {
    val encryptedId = encryptWithPrefixIV(id.toString().toByteArray(UTF_8), getKey(), getRandomInitializationVector())
    return Base64.getUrlEncoder().encodeToString(encryptedId)
  }

  fun decryptMemberId(encryptedId: String): Long {
    val decodedBytes = Base64.getUrlDecoder().decode(encryptedId)
    val decryptedString = decryptWithPrefixIniatilizationVector(decodedBytes, getKey())
    return decryptedString.toLong()
  }
}
