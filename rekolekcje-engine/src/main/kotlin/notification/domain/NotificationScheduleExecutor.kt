package pl.oaza.waw.rekolekcje.api.notification.domain

import org.springframework.beans.factory.annotation.Value
import org.springframework.scheduling.annotation.Scheduled

internal open class NotificationScheduleExecutor(
    private val notificationSender: NotificationSender) {

  @Value("\${notification.enabled}")
  private val notificationEnabled: Boolean = false

  @Scheduled(cron = "\${notification.cron}")
  open fun sendNotifications() {
    if (notificationEnabled) {
      val notifications = notificationSender.findNotificationsToSend();
      notifications.forEach(notificationSender::sendNotification)
    }
  }
}
