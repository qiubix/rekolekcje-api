import { Pipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';
import { MembersApiService } from '@members/state/members-api.service';

@Pipe({
  name: 'findMemberFullName'
})
export class FindMemberFullNamePipe implements PipeTransform {
  constructor(private _membersApiService: MembersApiService) {}

  transform(memberId: number, args?: any): Observable<string> {
    return this._membersApiService.getMemberFullName(memberId).pipe(map(it => it.fullName));
  }
}
