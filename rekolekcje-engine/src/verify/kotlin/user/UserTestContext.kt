package pl.oaza.waw.rekolekcje.api.user

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.security.crypto.password.PasswordEncoder
import pl.oaza.waw.rekolekcje.api.shared.RestApi
import pl.oaza.waw.rekolekcje.api.user.api.UserApiBehaviour
import pl.oaza.waw.rekolekcje.api.user.api.UserApiExpectations
import pl.oaza.waw.rekolekcje.api.user.endpoint.UserEndpoint.Companion.USERS_URI
import pl.oaza.waw.rekolekcje.api.user.storage.UserStorage
import pl.oaza.waw.rekolekcje.api.user.storage.UserStorageExpectations

@Configuration
@Profile("test")
internal class UserTestContext {

  @Bean
  fun userStorage(jdbcTemplate: JdbcTemplate): UserStorage = UserStorage(jdbcTemplate)

  @Bean
  fun userStorageExpectations(userStorage: UserStorage, passwordEncoder: PasswordEncoder): UserStorageExpectations =
    UserStorageExpectations(userStorage, passwordEncoder)

  @Bean
  fun userApiBehaviour(restApi: RestApi): UserApiBehaviour = UserApiBehaviour(restApi, USERS_URI)

  @Bean
  fun userApiExpectations(restApi: RestApi): UserApiExpectations = UserApiExpectations(restApi, USERS_URI)

}
