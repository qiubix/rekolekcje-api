package pl.oaza.waw.rekolekcje.api.community

import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.test.context.support.WithMockUser
import pl.oaza.waw.rekolekcje.api.community.api.CommunityApiBehaviour
import pl.oaza.waw.rekolekcje.api.community.api.CommunityApiExpectations
import pl.oaza.waw.rekolekcje.api.community.dto.CommunityPayload
import pl.oaza.waw.rekolekcje.api.community.storage.CommunityDatabase
import pl.oaza.waw.rekolekcje.api.shared.BaseIntegrationTest

internal class CommunityAcceptanceTest : BaseIntegrationTest() {

  @Autowired
  private lateinit var database: CommunityDatabase

  @Autowired
  private lateinit var whenInApi: CommunityApiBehaviour

  @Autowired
  private lateinit var thenInApi: CommunityApiExpectations

  private val first = CommunityPayload(
    name = "Name of the first",
    address = "Address of the first"
  )
  private val second = CommunityPayload(
    name = "Name of the second",
    address = "Second address"
  )
  private val communities = listOf(first, second)

  @BeforeEach
  fun setup() {
    database.clear()
  }

  @Test
  @WithMockUser
  @Throws(Exception::class)
  fun `should add a single community`() {
    // when some community is added
    val response = whenInApi.singleIsAdded(first)

    // then API returns this retreat with id
    thenInApi.responseStatusIsCreated(response)
    thenInApi.responseHasCorrectJsonValues(response, first)
  }

  @Test
  @WithMockUser
  @Throws(Exception::class)
  fun `should update a single community`() {
    // given some community has already been added
    whenInApi.allAreAdded(communities)
    val (id) = whenInApi.existsElementWithTheSameData(first)

    // when this community is updated
    val updatedCommunity = CommunityPayload(
      id = id,
      name = "A brand new community name"
    )
    val response = whenInApi.singleIsUpdated(updatedCommunity)

    // then API returns the same number of communities with updated data
    thenInApi.responseHasCorrectDataIgnoringId(response, updatedCommunity)
    thenInApi.thereIsAnExactNumberOfEntities(communities.size)
    thenInApi.exists(updatedCommunity)
  }

  @Test
  @WithMockUser
  @Throws(Exception::class)
  fun `should return 'not found' when retrieving non-existing community`() {
    // given some communities were already added
    whenInApi.allAreAdded(communities)
    val incorrectId = 654321L

    // when trying to retrieve a community with incorrect id
    val response = whenInApi.singleIsRequested(incorrectId)

    // then 'not found' is returned
    thenInApi.responseStatusIsNotFound(response)
  }

  @Test
  @WithMockUser
  @Throws(Exception::class)
  fun `should get one community`() {
    // given some communities were already added
    whenInApi.allAreAdded(communities)
    val (id) = whenInApi.existsElementWithTheSameData(first)

    // when trying to retrieve one with correct id
    val response = whenInApi.singleIsRequested(id)

    // then community with correct data is returned
    thenInApi.responseHasCorrectDataIgnoringId(response, first.copy(id = id))
  }

  @Test
  @WithMockUser
  @Throws(Exception::class)
  fun `should get all communities`() {
    // given
    whenInApi.allAreAdded(communities)

    // when
    val response = whenInApi.allAreRequested()

    // then
    thenInApi.responseHasCorrectDataIgnoringId(response, communities)
  }

  @Test
  @WithMockUser
  @Throws(Exception::class)
  fun `should delete a community`() {
    // given some communities were already added
    whenInApi.allAreAdded(communities)
    val (id) = whenInApi.existsElementWithTheSameData(first)

    // when community is deleted
    val response = whenInApi.singleIsDeleted(id!!)

    // then API returns communities without the deleted one
    thenInApi.responseStatusIsOk(response)
    thenInApi.thereIsAnExactNumberOfEntities(communities.size - 1)
    thenInApi.doesNotExist(first)
  }
}
