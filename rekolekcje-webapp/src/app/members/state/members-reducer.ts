import { MembersSharedActions } from './members-actions';
import { MemberSummary } from '../models/member-summary.model';
import { CurrentApplication, MemberDetails } from '@shared/models/member-details.model';

export namespace Members {
  export interface State {
    membersList: MemberSummary[];
    membersToVerify: MemberSummary[];
    membersLoading: boolean;
    selectedMember: MemberDetails;
    recentlyCreatedMemberId: number;
    currentApplicationStage: string;
  }

  export const initialState: Members.State = {
    membersList: [],
    membersToVerify: [],
    membersLoading: false,
    selectedMember: null,
    recentlyCreatedMemberId: null,
    currentApplicationStage: null
  };
}

export namespace MembersReducer {
  export function reducer(state: Members.State = Members.initialState, action): Members.State {
    switch (action.type) {
      case MembersSharedActions.types.CreateMemberSuccess: {
        return {
          ...state,
          membersList: [...state.membersList, action.payload],
          recentlyCreatedMemberId: action.payload.id
        };
      }

      case MembersSharedActions.types.DeleteMemberSuccess: {
        return {
          ...state,
          membersList: state.membersList.filter(
            (member: MemberSummary) => member.id !== action.payload
          )
        };
      }

      case MembersSharedActions.types.LoadMembersToVerify:
      case MembersSharedActions.types.LoadMembersList: {
        return {
          ...state,
          membersLoading: true
        };
      }

      case MembersSharedActions.types.LoadMembersToVerifyFail:
      case MembersSharedActions.types.LoadMembersListFail: {
        return {
          ...state,
          membersLoading: false
        };
      }

      case MembersSharedActions.types.LoadMembersListSuccess: {
        return {
          ...state,
          membersList: action.payload,
          membersLoading: false
        };
      }

      case MembersSharedActions.types.LoadMembersToVerifySuccess: {
        return {
          ...state,
          membersToVerify: action.payload,
          membersLoading: false
        };
      }

      case MembersSharedActions.types.SelectMemberSuccess: {
        return {
          ...state,
          selectedMember: action.payload
        };
      }

      case MembersSharedActions.types.UpdateMemberSuccess: {
        return {
          ...state,
          selectedMember: null,
          membersList: state.membersList.map((member: MemberSummary) => {
            return member.id === action.payload.id ? action.payload : member;
          })
        };
      }

      case MembersSharedActions.types.ChangeMemberStatusSuccess: {
        return updateStatus(state, action, { memberStatus: action.payload.memberStatus });
      }

      case MembersSharedActions.types.ChangePaymentStatusSuccess: {
        return updateStatus(state, action, { paymentStatus: action.payload.paymentStatus });
      }

      case MembersSharedActions.types.SetApplicationCurrentStage: {
        return {
          ...state,
          currentApplicationStage: action.payload
        };
      }

      default:
        return state;
    }
  }

  function updateStatus(
    state: Members.State,
    action,
    modification: CurrentApplication
  ): Members.State {
    // const updatedCurrentApplication = Object.assign(
    //   state.selectedMember.currentApplication,
    //   modification
    // );
    // const updatedDetails = Object.assign(state.selectedMember, {
    //   currentApplication: updatedCurrentApplication
    // });
    const updatedDetails = state.selectedMember;
    return {
      ...state,
      selectedMember: updatedDetails,
      membersList: state.membersList.map((member: MemberSummary) => {
        return member.id === action.payload.id ? action.payload : member;
      }),
      membersToVerify: state.membersList.map((member: MemberSummary) => {
        return member.id === action.payload.id ? action.payload : member;
      })
    };
  }
}
