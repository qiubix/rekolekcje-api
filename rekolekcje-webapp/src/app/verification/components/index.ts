import { VerificationComponent } from './_root/verification.component';
import { ApplicationListComponent } from './application-list/application-list.component';
import { ApplicationDetailsComponent } from './application-details/application-details.component';
import { ApplicationListViewComponent } from './application-list/application-list-view/application-list-view.component';
import { VerificationStatsComponent } from './statistics/verification-stats.component';
import { SingleStatComponent } from './statistics/single-stat/single-stat.component';
import { CurrentApplicationDetailsViewComponent } from './application-details/application-details-view/current-application-details/current-application-details-view.component';
import { ApplicationDetailsViewComponent } from './application-details/application-details-view/application-details-view.component';
import { ApplicationStatusActionsComponent } from './application-details/application-details-view/application-status-actions/application-status-actions.component';
import { PaymentStatusActionsComponent } from './application-details/application-details-view/payment-status-actions/payment-status-actions.component';
import { RetreatApplicationBadgesComponent } from './application-details/application-details-view/retreat-application-badges/retreat-application-badges.component';
import { AddEditApplicationPageComponent } from './add-edit-application-page/add-edit-application-page.component';
import { AddEditApplicationWizardComponent } from './add-edit-application-page/add-edit-application-wizard/add-edit-application-wizard.component';
import { AnimatorApplicationFormComponent } from './add-edit-application-page/add-edit-application-wizard/animator-application-form/animator-application-form.component';
import { WizardButtonsComponent } from './add-edit-application-page/add-edit-application-wizard/wizard-buttons/wizard-buttons.component';
import { ApplicationListViewFilterComponent } from './application-list/application-list-view/application-list-view-filter/application-list-view-filter.component';
import { ParticipantApplicationFormComponent } from './add-edit-application-page/add-edit-application-wizard/participant-application-form/participant-application-form.component';
import { ParticipantApplicationDetailsComponent } from './application-details/application-details-view/participant-application-details/participant-application-details.component';
import { AnimatorVsParticipantDialogComponent } from './animator-vs-participant-dialog/animator-vs-participant-dialog.component';
import { RejectionReasonDialogComponent } from './application-details/application-details-view/rejection-reason-dialog/rejection-reason-dialog.component';
import { ApplicationSummaryFormComponent } from '@verification/components/add-edit-application-page/add-edit-application-wizard/application-summary-form/application-summary-form.component';
import { TransportationFormComponent } from './add-edit-application-page/add-edit-application-wizard/transportation-form/transportation-form.component';
import { CertificationDetailsComponent } from './application-details/application-details-view/current-application-details/certification-details/certification-details.component';
import { TransportationDetailsComponent } from './application-details/application-details-view/current-application-details/transportation-details/transportation-details.component';

export const components: any[] = [
  VerificationComponent,
  ApplicationListComponent,
  ApplicationDetailsComponent,
  ApplicationDetailsViewComponent,
  ApplicationListViewFilterComponent,
  CurrentApplicationDetailsViewComponent,
  ApplicationListViewComponent,
  VerificationStatsComponent,
  SingleStatComponent,
  ApplicationStatusActionsComponent,
  PaymentStatusActionsComponent,
  RetreatApplicationBadgesComponent,
  AddEditApplicationPageComponent,
  AddEditApplicationWizardComponent,
  AnimatorApplicationFormComponent,
  ParticipantApplicationFormComponent,
  ParticipantApplicationDetailsComponent,
  WizardButtonsComponent,
  AnimatorVsParticipantDialogComponent,
  RejectionReasonDialogComponent,
  ApplicationSummaryFormComponent,
  TransportationFormComponent,
  CertificationDetailsComponent,
  TransportationDetailsComponent
];

export * from './_root/verification.component';
export * from './application-details/application-details.component';
export * from './add-edit-application-page/add-edit-application-page.component';
export * from './animator-vs-participant-dialog/animator-vs-participant-dialog.component';
export * from './application-details/application-details-view/rejection-reason-dialog/rejection-reason-dialog.component';
