import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import {
  AnimatorAssignment,
  AssignAnimatorRequest,
  ChangeApplicationStatusRequest,
  ChangePaymentStatusRequest,
  RetreatApplication,
  StatusWithReason,
} from '@verification/model';
import { select, Store } from '@ngrx/store';
import { Verification } from '../../state/verification-reducer';
import { AppSelectors } from '@core/store/app-selectors';
import { ActivatedRoute, Router } from '@angular/router';
import { DeleteConfirmAlertDialog } from '@shared/components';
import { Location } from '@angular/common';
import { VerificationActions } from '@verification/state/verification-actions';
import { Config } from '@config';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'reko-application-details',
  templateUrl: './application-details.component.html',
  styleUrls: ['./application-details.component.scss'],
})
export class ApplicationDetailsComponent implements OnInit {
  retreatApplication$: Observable<RetreatApplication>;
  animator: boolean = false;
  applicationId: number;

  constructor(
    private store: Store<Verification.State>,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private location: Location
  ) {}

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.animator = JSON.parse(params.animator);
    });
    this.applicationId = +this.activatedRoute.snapshot.params['id'];
    this.retreatApplication$ = this.store.pipe(select(AppSelectors.getSelectedApplication));
  }

  onGoBack() {
    this.location.back();
  }

  onEdit(applicationId: number) {
    this.router.navigate(['verification', `${applicationId}`, 'edit'], {
      queryParams: { animator: this.animator },
    });
  }

  onDelete(id: number) {
    this.dialog
      .open(DeleteConfirmAlertDialog, { disableClose: false })
      .afterClosed()
      .subscribe((result) => {
        if (result) {
          this.router.navigate(['verification']);
          this.store.dispatch(new VerificationActions.DeleteApplication(id));
        }
      });
  }

  onChangePaymentStatus(applicationId: number, paymentStatus: string) {
    const request: ChangePaymentStatusRequest = {
      applicationId: applicationId,
      paymentStatus: paymentStatus,
    };
    console.log('Sending change payment status request: ', request);
  }

  onChangeApplicationStatus(applicationStatus: StatusWithReason) {
    const request: ChangeApplicationStatusRequest = {
      id: this.applicationId,
      status: applicationStatus.status,
      reason: applicationStatus.reason,
    };
    this.snackBar.open('Zmieniono status zgłoszenia', applicationStatus.status, {
      duration: Config.notificationDuration,
    });
    this.store.dispatch(new VerificationActions.ChangeApplicationStatus(request));
  }

  onAssignAnimator(assignment: AnimatorAssignment) {
    const request: AssignAnimatorRequest = {
      applicationId: this.applicationId,
      selectedRetreat: assignment.selectedRetreat,
      selectedRole: assignment.selectedRole,
    };
    this.snackBar.open('Animator został przypisany do turnusu', 'OK', {
      duration: Config.notificationDuration,
    });
    this.store.dispatch(new VerificationActions.AssignAnimator(request));
  }
}
