import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PaymentStatus } from '@shared/models';

@Component({
  selector: 'reko-payment-status-actions',
  templateUrl: './payment-status-actions.component.html',
  styleUrls: ['./payment-status-actions.component.scss']
})
export class PaymentStatusActionsComponent implements OnInit {
  @Input() paymentStatus: PaymentStatus;
  @Output() onChangeStatus = new EventEmitter<string>();

  constructor() {}

  ngOnInit() {}

  memberPaidNothing(): boolean {
    return this.paymentStatus === PaymentStatus.NOTHING;
  }

  memberMadeDownPayment(): boolean {
    return this.paymentStatus === PaymentStatus.DOWN_PAYMENT;
  }

  memberPaidFull(): boolean {
    return this.paymentStatus === PaymentStatus.FULL;
  }

  changePaymentStatus(newStatus: string) {
    this.onChangeStatus.emit(newStatus);
  }
}
