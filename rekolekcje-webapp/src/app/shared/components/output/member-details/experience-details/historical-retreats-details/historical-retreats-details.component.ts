import { Component, Input, OnInit } from '@angular/core';
import { RetreatTurn } from '@shared/models/member-details.model';

@Component({
  selector: 'reko-historical-retreats-details',
  templateUrl: './historical-retreats-details.component.html',
  styleUrls: ['./historical-retreats-details.component.scss']
})
export class HistoricalRetreatsDetailsComponent {
  @Input() historicalRetreats: RetreatTurn[];
}
