import { Component, OnInit, ContentChildren, QueryList, Input, Directive } from '@angular/core';

@Directive({ selector: 'reko-details-row-element' })
export class DetailsElement {
  @Input() label: string;
  @Input() value: string;
  @Input() icon: string;
}

@Component({
  selector: 'reko-details-row',
  templateUrl: './details-row.component.html',
  styleUrls: ['./details-row.component.scss']
})
export class DetailsRowComponent implements OnInit {
  @ContentChildren(DetailsElement) elements: QueryList<DetailsElement>;

  constructor() {}

  ngOnInit() {}

  get detailsElements(): DetailsElement[] {
    return this.elements.toArray();
  }
}
