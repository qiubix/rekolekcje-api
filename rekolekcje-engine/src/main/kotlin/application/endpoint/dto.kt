package pl.oaza.waw.rekolekcje.api.application.endpoint

import com.fasterxml.jackson.annotation.JsonProperty
import pl.oaza.waw.rekolekcje.api.application.domain.*
import pl.oaza.waw.rekolekcje.api.members.endpoint.MemberDetails
import pl.oaza.waw.rekolekcje.api.shared.endpoint.ContactDto
import pl.oaza.waw.rekolekcje.api.shared.endpoint.toDto
import pl.oaza.waw.rekolekcje.api.shared.value.PaymentStatus

data class ApplicationSubmissionDto(
  val applicationType: ApplicationType,
  val memberDetails: MemberDetails,
  val surety: ContactDto? = null,
  val animatorRoles: Set<AnimatorRole>? = null,
  val availability: Set<Long>? = null,
  val retreatId: Long? = null
) {
  fun toDomainEntity(memberId: Long, firstName: String, lastName: String) = RetreatApplication(
    applicationType = applicationType,
    firstName = firstName,
    lastName = lastName,
    memberId = memberId,
    surety = surety?.toDomainEntity(),
    animatorRoles = animatorRoles,
    availability = availability,
    retreatId = retreatId
  )
}

data class RetreatApplicationDto(
  val id: Long,
  val type: ApplicationType,
  val member: MemberDetails,
  val animatorRoles: Set<AnimatorRole>? = null,
  val availability: Set<Long>? = null,
  val surety: ContactDto? = null,
  val status: ApplicationStatus = ApplicationStatus.WAITING,
  val paymentStatus: PaymentStatus = PaymentStatus.NOTHING,
  val retreatId: Long? = null,
  val selectedRole: AnimatorRole? = null
)

fun RetreatApplication.toDto(member: MemberDetails) = RetreatApplicationDto(
  id = getId() ?: throw IllegalStateException(),
  type = getType(),
  member = member,
  retreatId = retreatId,
  status = status,
  paymentStatus = paymentStatus,
  animatorRoles = animatorRoles,
  availability = availability,
  surety = surety?.toDto(),
  selectedRole = selectedRole
)

data class ChangePaymentStatusRequest(
  val applicationId: Long,
  val paymentStatus: PaymentStatus
)

data class ChangePaymentStatusResponse(
  val paymentStatus: PaymentStatus
)

data class ChangeApplicationStatusRequest(
  val id: Long,
  val status: ApplicationStatus,
  val reason: String? = null
)

data class ChangeApplicationStatusResponse(
  val status: ApplicationStatus
)

data class AssignAnimatorRequest(
  val applicationId: Long,
  val selectedRetreat: Long,
  val selectedRole: AnimatorRole
)

data class AssignAnimatorResponse(
  val selectedRetreat: Long,
  val selectedRole: AnimatorRole
)

data class ApplicationSummary(
  val id: Long,
  val firstName: String,
  val lastName: String,
  val status: ApplicationStatus = ApplicationStatus.WAITING,
  val paymentStatus: PaymentStatus = PaymentStatus.NOTHING,
  @get:JsonProperty("isAnimator")
  val isAnimator: Boolean = false,
  val retreatId: Long? = null
)

fun RetreatApplication.toSummary() = ApplicationSummary(
  id = getId() ?: throw IllegalStateException(),
  firstName = firstName,
  lastName = lastName,
  status = status,
  paymentStatus = paymentStatus,
  retreatId = retreatId,
  isAnimator = applicationType == ApplicationType.ANIMATOR
)

fun RetreatApplicationDto.toSummary() = ApplicationSummary(
  id = id,
  firstName = member.personalData.firstName,
  lastName = member.personalData.lastName,
  status = status,
  paymentStatus = paymentStatus,
  isAnimator = type == ApplicationType.ANIMATOR,
  retreatId = retreatId
)

data class VerificationStatisticsDto(
  val numberOfActiveApplications: Int,
  val alreadyVerified: Int,
  val remainingToVerify: Int,
  val animatorsRemaining: Int,
  val participantsRemaining: Int
) {
  constructor(domainObject: VerificationStatistics) : this(
    numberOfActiveApplications = domainObject.numberOfActiveApplications,
    alreadyVerified = domainObject.alreadyVerified,
    remainingToVerify = domainObject.remainingToVerify,
    animatorsRemaining = domainObject.animatorsRemaining,
    participantsRemaining = domainObject.participantsRemaining
  )
}
