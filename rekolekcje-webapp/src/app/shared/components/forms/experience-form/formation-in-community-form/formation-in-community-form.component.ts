import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FormationInCommunity, Stage, Talents } from '@shared/models';

const resolvedPromise = Promise.resolve(null);

@Component({
  selector: 'reko-formation-in-community-form',
  templateUrl: './formation-in-community-form.component.html',
  styleUrls: ['./formation-in-community-form.component.scss']
})
export class FormationInCommunityFormComponent implements OnInit {
  form: FormGroup;

  @Input() data: FormationInCommunity;
  @Output() formReady = new EventEmitter<FormGroup>();

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.form = this.fb.group({
      formationInSmallGroup: this.data ? this.data.formationInSmallGroup : null,
      formationMeetingsInMonth: this.data ? this.data.formationMeetingsInMonth : null,
      numberOfCommunionDays: this.data ? this.data.numberOfCommunionDays : null,
      numberOfPrayerRetreats: this.data ? this.data.numberOfPrayerRetreats : null
    });
    resolvedPromise.then(() => {
      this.formReady.emit(this.form);
    });
  }
}
