import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Submitter } from '@opinions/model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

const resolvedPromise = Promise.resolve(null);

@Component({
  selector: 'reko-opinion-submitter',
  templateUrl: './opinion-submitter.component.html',
  styleUrls: ['./opinion-submitter.component.scss']
})
export class OpinionSubmitterComponent implements OnInit {
  @Input() submitter: Submitter;
  @Output() formReady = new EventEmitter<FormGroup>();
  public form: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.form = this.fb.group({
      fullName: [this.submitter ? this.submitter.fullName : '', Validators.required],
      email: [this.submitter ? this.submitter.email : '', [Validators.required, Validators.email]],
      phoneNumber: [this.submitter ? this.submitter.phoneNumber : '', Validators.required],
      role: [this.submitter ? this.submitter.role : 'OTHER']
    });
    resolvedPromise.then(() => {
      this.formReady.emit(this.form);
    });
  }
}
