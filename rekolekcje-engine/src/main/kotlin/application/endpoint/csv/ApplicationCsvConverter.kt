package pl.oaza.waw.rekolekcje.api.application.endpoint.csv

import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import pl.oaza.waw.rekolekcje.api.application.domain.AnimatorRole
import pl.oaza.waw.rekolekcje.api.members.endpoint.MemberDetails
import pl.oaza.waw.rekolekcje.api.members.endpoint.csv.MemberCsvRowConverter
import pl.oaza.waw.rekolekcje.api.members.endpoint.csv.memberFieldToHeaders
import pl.oaza.waw.rekolekcje.api.shared.endpoint.ContactDto
import pl.oaza.waw.rekolekcje.api.shared.endpoint.csv.CsvConversionError

fun convertApplicationFromCsv(content: String): ApplicationCsvConversionResult {
  val rawData: List<Map<String, String>> = csvReader().readAllWithHeader(content)
  val applicationCsvRowConverter = ApplicationCsvRowConverter(applicationFieldToHeaders)
  val memberCsvRowConverter = MemberCsvRowConverter(memberFieldToHeaders)
  val convertedApplications = rawData.map { row ->
    val member = memberCsvRowConverter.read(row)
    applicationCsvRowConverter.read(row.mapKeys { (k, _) -> k.trim() }, member)
  }
  return ApplicationCsvConversionResult(
    convertedApplications = convertedApplications,
    errors = emptyList()
  )
}

data class ApplicationCsvConversionResult(
  val convertedApplications: List<ConvertedApplicationData>,
  val errors: List<CsvConversionError>
)

data class ConvertedApplicationData(
  val animatorRoles: Set<AnimatorRole> = emptySet(),
  val availability: Set<String> = emptySet(),
  val surety: ContactDto? = null,
  val selectedRetreat: String? = null,
  val member: MemberDetails? = null
)