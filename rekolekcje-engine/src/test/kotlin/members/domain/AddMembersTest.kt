package pl.oaza.waw.rekolekcje.api.members.domain

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import pl.oaza.waw.rekolekcje.api.members.MemberTestFixture

internal class AddMembersTest : MemberTestFixture() {

  @Test
  fun `should add single member to empty system`() {
    // given
    val member = sampleMember()

    // when
    commandFacade.save(member)

    // then
    val allMembers = queryFacade.findAllMembers()
    assertThat(allMembers)
      .usingElementComparatorIgnoringFields("id")
      .contains(member)
  }

  @Test
  fun `should add single member when some already exist`() {
    // given
    val existingMember = minimalMember()
    commandFacade.save(existingMember)
    val newMember = sampleMember()

    // when
    commandFacade.save(newMember)

    // then
    val allMembers = queryFacade.findAllMembers()
    assertThat(allMembers)
      .hasSize(2)
      .usingElementComparatorIgnoringFields("id")
      .isEqualTo(listOf(existingMember, newMember))
  }

  @Test
  fun `should assign ID when adding new member`() {
    // given
    val memberWithoutId = sampleMember()

    // when
    val addedMember = commandFacade.save(memberWithoutId)

    // then
    assertThat(addedMember.getId())
      .isNotNull()
      .isNotZero()
  }

  @Test
  fun `should add multiple members at once`() {
    val first = minimalMember()
    val second = sampleMember()
    val members = listOf(first, second)

    // when
    commandFacade.saveAll(members)

    // then
    assertThat(queryFacade.findAllMembers())
      .usingElementComparatorIgnoringFields("id")
      .containsAll(members)
  }

}