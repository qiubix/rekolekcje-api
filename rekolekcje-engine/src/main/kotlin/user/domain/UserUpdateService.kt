package pl.oaza.waw.rekolekcje.api.user.domain

import org.springframework.security.crypto.password.PasswordEncoder
import pl.oaza.waw.rekolekcje.api.user.endpoint.ChangePasswordRequest
import pl.oaza.waw.rekolekcje.api.user.endpoint.UpdateUserRequest
import pl.oaza.waw.rekolekcje.api.user.endpoint.UserRole
import pl.oaza.waw.rekolekcje.api.user.endpoint.UserSummary

class UserUpdateService(
  private val userReadRepository: UserReadRepository,
  private val userWriteRepository: UserWriteRepository,
  private val passwordEncoder: PasswordEncoder
) {

  fun deleteByUsername(username: String) {
    val user = getUser(username)
    userWriteRepository.deleteById(user.id ?: throw IllegalStateException("Cannot delete user without id."))
  }

  fun update(userUpdateRequest: UpdateUserRequest): UserSummary {
    val user = getUser(userUpdateRequest.username)
    val updatedUser = User(
      id = user.id,
      username = userUpdateRequest.username,
      password = user.password,
      email = userUpdateRequest.email,
      enabled = userUpdateRequest.enabled,
      roles = mapRoles(userUpdateRequest.roles)
    )
    userWriteRepository.save(updatedUser)
    return updatedUser.toDto()
  }

  private fun mapRoles(userRoles: Set<UserRole>) =
    (userRoles.map { Role.valueOf(it.name) } + setOf(Role.ROLE_USER)).toSet()

  fun disableUser(username: String) {
    val user = getUser(username)
    userWriteRepository.save(user.copy(enabled = false))
  }

  fun enableUser(username: String) {
    val user = getUser(username)
    userWriteRepository.save(user.copy(enabled = true))
  }

  private fun getUser(username: String): User {
    return userReadRepository.findByUsername(username) ?: throw UserNotFoundException(username)
  }

  fun changePassword(changePasswordRequest: ChangePasswordRequest): UserSummary {
    val user = getUser(changePasswordRequest.username)
    return if (changePasswordRequest.admin) {
      storeUsersPassword(user, changePasswordRequest)
    } else if (changePasswordRequest.oldPassword == null || !oldPasswordIsCorrect(changePasswordRequest.oldPassword, user.password)) {
      throw IncorrectPasswordException()
    } else {
      storeUsersPassword(user, changePasswordRequest)
    }
  }

  private fun storeUsersPassword(
    user: User,
    changePasswordRequest: ChangePasswordRequest
  ): UserSummary {
    val userWithNewPassword = user.copy(password = passwordEncoder.encode(changePasswordRequest.newPassword))
    userWriteRepository.save(userWithNewPassword)
    return userWithNewPassword.toDto()
  }

  private fun oldPasswordIsCorrect(oldPassword: String, storedPassword: String) =
    passwordEncoder.matches(oldPassword, storedPassword)

}
