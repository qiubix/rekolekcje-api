package pl.oaza.waw.rekolekcje.api.retreats.endpoint.events

import org.springframework.context.ApplicationEvent
import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationStatus

data class MemberStatusChangedEvent(
  val retreatId: Long,
  val memberId: Long,
  val applicationStatus: ApplicationStatus
) : ApplicationEvent("")