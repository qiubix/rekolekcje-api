import { Action, Store } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { Members } from './members-reducer';
import { MembersSharedActions } from './members-actions';
import { Injectable } from '@angular/core';
import { catchError, map, switchMap } from 'rxjs/operators';
import { MemberSummary } from '../models/member-summary.model';
import { MemberDetails } from '@shared/models/member-details.model';
import { MembersApiService } from './members-api.service';
import CreateMemberSuccess = MembersSharedActions.CreateMemberSuccess;
import DeleteMemberSuccess = MembersSharedActions.DeleteMemberSuccess;
import UpdateMemberSuccess = MembersSharedActions.UpdateMemberSuccess;
import SelectMemberSuccess = MembersSharedActions.SelectMemberSuccess;
import LoadMembersListSuccess = MembersSharedActions.LoadMembersListSuccess;
import LoadMembersListFail = MembersSharedActions.LoadMembersListFail;
import ChangeMemberStatusSuccess = MembersSharedActions.ChangeMemberStatusSuccess;
import ChangePaymentStatusSuccess = MembersSharedActions.ChangePaymentStatusSuccess;
import MemberActionFail = MembersSharedActions.MemberActionFail;
import LoadMembersToVerifySuccess = MembersSharedActions.LoadMembersToVerifySuccess;
import LoadMembersToVerifyFail = MembersSharedActions.LoadMembersToVerifyFail;

@Injectable()
export class MembersEffects {
  @Effect()
  CreateMember: Observable<Action> = this.actions.pipe(
    ofType(MembersSharedActions.types.CreateMember),
    switchMap((action: MembersSharedActions.CreateMember) =>
      this.service.addNew(action.payload).pipe(
        map(data => new CreateMemberSuccess(data)),
        catchError(error => of(new MemberActionFail(error)))
      )
    )
  );

  @Effect()
  DeleteMember: Observable<Action> = this.actions.pipe(
    ofType(MembersSharedActions.types.DeleteMember),
    switchMap((action: MembersSharedActions.DeleteMember) =>
      this.service.deleteOne(action.payload).pipe(
        map(() => new DeleteMemberSuccess(action.payload)),
        catchError(error => of(new MemberActionFail(error)))
      )
    )
  );

  @Effect()
  LoadMembersList: Observable<Action> = this.actions.pipe(
    ofType(MembersSharedActions.types.LoadMembersList),
    switchMap((action: MembersSharedActions.LoadMembersList) =>
      this.service.getAll().pipe(
        map((data: MemberSummary[]) => new LoadMembersListSuccess(data)),
        catchError(error => of(new LoadMembersListFail(error)))
      )
    )
  );

  @Effect()
  LoadMembersToVerify: Observable<Action> = this.actions.pipe(
    ofType(MembersSharedActions.types.LoadMembersToVerify),
    switchMap((action: MembersSharedActions.LoadMembersToVerify) =>
      this.service.getMembersToVerify().pipe(
        map((data: MemberSummary[]) => new LoadMembersToVerifySuccess(data)),
        catchError(error => of(new LoadMembersToVerifyFail(error)))
      )
    )
  );

  @Effect()
  SelectMemberById: Observable<Action> = this.actions.pipe(
    ofType(MembersSharedActions.types.SelectMemberById),
    switchMap((action: MembersSharedActions.SelectMemberById) => {
      // console.log('getting member from server. Id: ', action.payload);
      return this.service.getOne(action.payload).pipe(
        map((data: MemberDetails) => new SelectMemberSuccess(data)),
        catchError(error => of(new MemberActionFail(error)))
      );
    })
  );

  @Effect()
  UpdateMember: Observable<Action> = this.actions.pipe(
    ofType(MembersSharedActions.types.UpdateMember),
    switchMap((action: MembersSharedActions.UpdateMember) => {
      console.log('Sending PUT request with updated member... Payload: ', action.payload);
      return this.service.update(action.payload).pipe(
        map((data: MemberSummary) => new UpdateMemberSuccess(data)),
        catchError(error => of(new MemberActionFail(error)))
      );
    })
  );

  @Effect()
  ChangeMemberStatus: Observable<Action> = this.actions.pipe(
    ofType(MembersSharedActions.types.ChangeMemberStatus),
    switchMap((action: MembersSharedActions.ChangeMemberStatus) => {
      return this.service.changeMemberStatus(action.id, action.payload).pipe(
        map((data: MemberSummary) => new ChangeMemberStatusSuccess(data)),
        catchError(error => of(new MemberActionFail(error)))
      );
    })
  );

  @Effect()
  ChangePaymentStatus: Observable<Action> = this.actions.pipe(
    ofType(MembersSharedActions.types.ChangePaymentStatus),
    switchMap((action: MembersSharedActions.ChangePaymentStatus) => {
      return this.service.changePaymentStatus(action.id, action.payload).pipe(
        map((data: MemberSummary) => new ChangePaymentStatusSuccess(data)),
        catchError(error => of(new MemberActionFail(error)))
      );
    })
  );

  constructor(
    private actions: Actions,
    private store: Store<Members.State>,
    private service: MembersApiService
  ) {}
}
