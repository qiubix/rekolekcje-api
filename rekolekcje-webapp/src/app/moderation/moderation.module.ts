import { NgModule } from '@angular/core';

import { ModerationRoutingModule } from './moderation-routing.module';
import { ModerationComponent } from './root/moderation.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [ModerationComponent],
  imports: [SharedModule, ModerationRoutingModule]
})
export class ModerationModule {}
