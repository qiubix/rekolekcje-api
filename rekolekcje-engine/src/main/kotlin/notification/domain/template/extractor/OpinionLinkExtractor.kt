package pl.oaza.waw.rekolekcje.api.notification.domain.template.extractor

import pl.oaza.waw.rekolekcje.api.application.domain.RetreatApplication
import pl.oaza.waw.rekolekcje.api.members.query.OpinionLinkQueryFacade

internal class OpinionLinkExtractor(
  private val application: RetreatApplication,
  private val opinionLinkQueryFacade: OpinionLinkQueryFacade
) : Extractor(ExtractorType.OPINION_LINK) {
    override fun extractValue(): String {
      return opinionLinkQueryFacade.generateLink(application.memberId)
    }
  }
