package pl.oaza.waw.rekolekcje.api.application

import org.junit.jupiter.api.Test
import org.springframework.security.test.context.support.WithMockUser
import pl.oaza.waw.rekolekcje.api.application.domain.AnimatorRole
import pl.oaza.waw.rekolekcje.api.application.endpoint.AssignAnimatorRequest
import pl.oaza.waw.rekolekcje.api.application.fixture.BaseApplicationAcceptanceTest
import pl.oaza.waw.rekolekcje.api.application.fixture.animatorApplicationSubmissionDto
import pl.oaza.waw.rekolekcje.api.application.fixture.fullApplicationFromSubmission
import pl.oaza.waw.rekolekcje.api.application.fixture.sampleMember

internal class ApplicationAnimatorAssignmentAcceptanceTest : BaseApplicationAcceptanceTest() {

  @Test
  @WithMockUser
  fun `should assign animator application to retreat with specific role`() {
    val member = sampleMember()
    val submissionDto = animatorApplicationSubmissionDto(member = member)
    val applicationId = whenInApi.alreadyExistsSomeApplication(submissionDto)
    val retreatId = 12L
    val role = AnimatorRole.QUARTERMASTER

    val request = AssignAnimatorRequest(applicationId, retreatId, role)
    val response = whenInApi.animatorIsAssignedToRetreat(request)

    response.hasStatusOk()
    val existingMember = thenInMembersApi.existsMemberWithMatchingData(member)
    val expectedExistingApplication = fullApplicationFromSubmission(
        submissionDto = submissionDto,
        applicationId = applicationId,
        member = existingMember
    )
        .copy(retreatId = retreatId, selectedRole = role)
    thenInApi.exists(expectedExistingApplication)
  }
}