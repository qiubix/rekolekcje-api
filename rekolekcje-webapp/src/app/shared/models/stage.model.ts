export enum Stage {
  ODB = 'ODB',
  OND = 'OND',
  ONZ_I = 'ONŻ I',
  ONZ_II = 'ONŻ II',
  ONZ_III = 'ONŻ III',
  OR_I = 'OR I',
  OR_II = 'OR II',
  OR_III = 'OR III',
  ORAE = 'ORAE',
  ORD = 'ORD',
  Triduum = 'Triduum'
}
