import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Transportation } from '@verification/model';

@Component({
  selector: 'reko-transportation-form',
  templateUrl: './transportation-form.component.html',
  styleUrls: ['./transportation-form.component.scss']
})
export class TransportationFormComponent implements OnInit {
  form: FormGroup;

  @Input() data: Transportation;

  @Output() formReady = new EventEmitter<FormGroup>();

  constructor(private _fb: FormBuilder) {}

  ngOnInit() {
    this.form = this._fb.group({
      from: [this.data ? this.data.from : ''],
      to: [this.data ? this.data.to : '']
    });
    this.formReady.emit(this.form);
  }
}
