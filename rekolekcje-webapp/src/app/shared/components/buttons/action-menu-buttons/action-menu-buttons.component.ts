import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'reko-action-menu-buttons',
  templateUrl: './action-menu-buttons.component.html',
  styleUrls: ['./action-menu-buttons.component.scss']
})
export class ActionMenuButtonsComponent {
  @Input()
  isDelete = false;
  @Input()
  isDetails = false;
  @Input()
  isEdit = false;

  @Output()
  details = new EventEmitter<void>();
  @Output()
  delete = new EventEmitter<void>();
  @Output()
  edit = new EventEmitter<void>();

  onDelete(): void {
    this.delete.emit();
  }

  onDetails(): void {
    this.details.emit();
  }

  onEdit(): void {
    this.edit.emit();
  }
}
