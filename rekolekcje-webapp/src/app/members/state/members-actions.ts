import { Action } from '@ngrx/store';
import { MemberSummary } from '../models/member-summary.model';
import { MemberDetails } from '@shared/models/member-details.model';

export namespace MembersSharedActions {
  export const types = {
    MemberActionFail: '[Members] Action failed',
    CreateMember: '[Members] Create Member',
    CreateMemberSuccess: '[Members] Create Member Success',
    DeleteMember: '[Members] Delete Member',
    DeleteMemberSuccess: '[Members] Delete Member Success',
    LoadMembersList: '[Members] Load Members List',
    LoadMembersListFail: '[Members] Load Members List Fail',
    LoadMembersListSuccess: '[Members] Load Members List Success',
    LoadMembersToVerify: '[Members] Load members to verify',
    LoadMembersToVerifySuccess: '[Members] Load members to verify success',
    LoadMembersToVerifyFail: '[Members] Load members to verify fail',
    SelectMember: '[Members] Select Member',
    SelectMemberById: '[Members] Select Member By Id',
    SelectMemberSuccess: '[Members] Select Member Success',
    UpdateMember: '[Members] Update Member',
    UpdateMemberSuccess: '[Members] Update Member Success',
    ChangeMemberStatus: '[Members] Change Member status',
    ChangeMemberStatusSuccess: '[Members] Change Member status Success',
    ChangePaymentStatus: '[Members] Change payment status',
    ChangePaymentStatusSuccess: '[Members] Change payment status success',
    SetApplicationCurrentStage: '[Members] Set current application stage'
  };

  export class MemberActionFail implements Action {
    type = types.MemberActionFail;

    constructor(public payload: any) {}
  }

  /**
   * Create member actions
   */
  export class CreateMember implements Action {
    type = types.CreateMember;

    constructor(public payload: MemberDetails) {}
  }

  export class CreateMemberSuccess implements Action {
    type = types.CreateMemberSuccess;

    constructor(public payload: MemberSummary) {}
  }

  /**
   * Delete member actions
   */
  export class DeleteMember implements Action {
    type = types.DeleteMember;

    constructor(public payload: number) {}
  }

  export class DeleteMemberSuccess implements Action {
    type = types.DeleteMemberSuccess;

    constructor(public payload: number) {}
  }

  /**
   * Load members list actions
   */
  export class LoadMembersList implements Action {
    type = types.LoadMembersList;

    constructor(public payload?: any) {}
  }

  export class LoadMembersListFail implements Action {
    type = types.LoadMembersListFail;

    constructor(public payload: any) {}
  }

  export class LoadMembersListSuccess implements Action {
    type = types.LoadMembersListSuccess;

    constructor(public payload: MemberSummary[]) {}
  }

  export class LoadMembersToVerify implements Action {
    type = types.LoadMembersToVerify;

    constructor(public payload?: any) {}
  }

  export class LoadMembersToVerifyFail implements Action {
    type = types.LoadMembersToVerifyFail;

    constructor(public payload: any) {}
  }

  export class LoadMembersToVerifySuccess implements Action {
    type = types.LoadMembersToVerifySuccess;

    constructor(public payload: MemberSummary[]) {}
  }

  /**
   * Select member actions
   */
  export class SelectMemberById implements Action {
    type = types.SelectMemberById;

    constructor(public payload: number) {}
  }

  export class SelectMemberSuccess implements Action {
    type = types.SelectMemberSuccess;

    constructor(public payload: MemberDetails) {}
  }

  /**
   * Update member actions
   */
  export class UpdateMember implements Action {
    type = types.UpdateMember;

    constructor(public payload: MemberDetails) {}
  }

  export class UpdateMemberSuccess implements Action {
    type = types.UpdateMemberSuccess;

    constructor(public payload: MemberSummary) {}
  }

  export class ChangeMemberStatus implements Action {
    type = types.ChangeMemberStatus;

    constructor(public id: number, public payload: string) {}
  }

  export class ChangeMemberStatusSuccess implements Action {
    type = types.ChangeMemberStatusSuccess;

    constructor(public payload: MemberSummary) {}
  }
  export class ChangePaymentStatus implements Action {
    type = types.ChangePaymentStatus;

    constructor(public id: number, public payload: string) {}
  }

  export class ChangePaymentStatusSuccess implements Action {
    type = types.ChangePaymentStatusSuccess;

    constructor(public payload: MemberSummary) {}
  }

  export class SetCurrentApplicationStage implements Action {
    type = types.SetApplicationCurrentStage;

    constructor(public payload: string) {}
  }
}
