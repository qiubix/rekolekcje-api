package pl.oaza.waw.rekolekcje.api.application.fixture

import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.jdbc.core.JdbcTemplate
import pl.oaza.waw.rekolekcje.api.application.api.ApplicationApiBehaviours
import pl.oaza.waw.rekolekcje.api.application.api.ApplicationApiExpectations
import pl.oaza.waw.rekolekcje.api.application.storage.ApplicationDatabase
import pl.oaza.waw.rekolekcje.api.shared.RestApi

@TestConfiguration
internal class ApplicationTestContext {

  @Bean
  fun applicationDatabase(jdbcTemplate: JdbcTemplate) = ApplicationDatabase(jdbcTemplate)

  @Bean
  fun applicationApiBehaviours(restApi: RestApi) = ApplicationApiBehaviours(restApi)

  @Bean
  fun applicationApiExpectations(restApi: RestApi) = ApplicationApiExpectations(restApi)

}