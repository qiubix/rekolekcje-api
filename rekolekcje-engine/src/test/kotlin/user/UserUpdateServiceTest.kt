package pl.oaza.waw.rekolekcje.api.user

import io.mockk.every
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import pl.oaza.waw.rekolekcje.api.user.domain.IncorrectPasswordException
import pl.oaza.waw.rekolekcje.api.user.domain.UserNotFoundException
import pl.oaza.waw.rekolekcje.api.user.endpoint.ChangePasswordRequest
import pl.oaza.waw.rekolekcje.api.user.endpoint.UpdateUserRequest
import pl.oaza.waw.rekolekcje.api.user.endpoint.UserRole
import pl.oaza.waw.rekolekcje.api.user.endpoint.UserSummary
import pl.oaza.waw.rekolekcje.api.user.infrastructure.UserTestFixture

class UserUpdateServiceTest : UserTestFixture() {

  @BeforeEach
  internal fun setUp() {
    userRegisterService.registerUser(signUpRequest)
  }

  @Test
  fun `should delete user by username`() {
    // when
    userUpdateService.deleteByUsername(username)

    // then
    val allUsernames = userQueryService.findAll().map { it.username }
    assertThat(allUsernames).doesNotContain(username)
  }

  @Test
  fun `should throw 'UserNotFoundException' when trying to delete non-existing user`() {
    assertThatExceptionOfType(UserNotFoundException::class.java)
      .isThrownBy { userUpdateService.deleteByUsername("non-existing-username") }
  }

  private fun rolesDataSource() = listOf(
    setOf(UserRole.ROLE_USER, UserRole.ROLE_MODERATOR) to setOf(UserRole.ROLE_USER, UserRole.ROLE_MODERATOR),
    setOf<UserRole>() to setOf(UserRole.ROLE_USER),
    setOf(UserRole.ROLE_DOR) to setOf(UserRole.ROLE_DOR, UserRole.ROLE_USER)
  )

  @TestFactory
  fun `should update user's roles`() = rolesDataSource().map { (givenRoles, expectedRoles) ->
    DynamicTest.dynamicTest("given roles $givenRoles") {
      // given
      val userUpdateRequest = UpdateUserRequest(
        username = username,
        email = email,
        roles = givenRoles
      )

      // when
      userUpdateService.update(userUpdateRequest)

      // then
      val foundUser = userQueryService.findByUsername(username)
      assertThat(foundUser).isNotNull
      assertThat(foundUser?.roles).isEqualTo(expectedRoles)
    }
  }

  @Test
  fun `should disable user`() {
    // when
    userUpdateService.disableUser(username)

    // then
    val userSummary = userQueryService.findByUsername(username) ?: throw IllegalStateException("User should be present")
    assertThat(userSummary.enabled).isFalse()
  }

  @Test
  fun `should enable user`() {
    // given
    userUpdateService.disableUser(username)

    // when
    userUpdateService.enableUser(username)

    // then
    val userSummary = userQueryService.findByUsername(username) ?: throw IllegalStateException("User should be present")
    assertThat(userSummary.enabled).isTrue()
  }

  @Test
  fun `should change user's password`() {
    // given
    val requestWithNewPassword = ChangePasswordRequest(
      username = username,
      oldPassword = password,
      newPassword = "brand-n3w-secure_password"
    )
    val newPasswordEncoded = "new-password-encoded"
    every { passwordEncoderMock.encode(requestWithNewPassword.newPassword) } returns newPasswordEncoded
    every { passwordEncoderMock.matches(requestWithNewPassword.newPassword, newPasswordEncoded) } returns true

    // when
    userUpdateService.changePassword(requestWithNewPassword)

    // then
    val storedPassword = findPasswordForUser(username)
    assertThat(storedPassword).isEqualTo(newPasswordEncoded)
  }

  @Test
  fun `should not change user's data when changing password`() {
    // given
    val changePasswordRequest = ChangePasswordRequest(
      username = username,
      oldPassword = password,
      newPassword = "brand-n3w-secure_password"
    )
    val newPasswordEncoded = "new-password-encoded"
    every { passwordEncoderMock.encode(changePasswordRequest.newPassword) } returns newPasswordEncoded
    every { passwordEncoderMock.matches(changePasswordRequest.newPassword, newPasswordEncoded) } returns true

    // when
    val userSummary = userUpdateService.changePassword(changePasswordRequest)

    // then
    val expectedSummary = UserSummary(
      username = username,
      enabled = true,
      email = email,
      roles = setOf(UserRole.ROLE_USER)
    )
    assertThat(userSummary).isEqualTo(expectedSummary)
  }

  @Test
  fun `should throw exception when trying to change password with incorrect old password provided`() {
    // given
    val changePasswordRequest = ChangePasswordRequest(
      username = username,
      oldPassword = "wrong-old-password",
      newPassword = "brand-n3w-secure_password"
    )
    val newPasswordEncoded = "new-password-encoded"
    every { passwordEncoderMock.encode(changePasswordRequest.newPassword) } returns newPasswordEncoded
    every { passwordEncoderMock.matches(changePasswordRequest.newPassword, newPasswordEncoded) } returns true
    every { passwordEncoderMock.matches(changePasswordRequest.oldPassword, encodedPassword) } returns false

    // when & then
    assertThatExceptionOfType(IncorrectPasswordException::class.java).isThrownBy {
      userUpdateService.changePassword(changePasswordRequest)
    }
  }

  @Test
  fun `should throw exception when trying to change password without old password provided`() {
    // given
    val changePasswordRequest = ChangePasswordRequest(
      username = username,
      oldPassword = null,
      newPassword = "brand-n3w-secure_password"
    )
    val newPasswordEncoded = "new-password-encoded"
    every { passwordEncoderMock.encode(changePasswordRequest.newPassword) } returns newPasswordEncoded
    every { passwordEncoderMock.matches(changePasswordRequest.newPassword, newPasswordEncoded) } returns true
    every { passwordEncoderMock.matches(changePasswordRequest.oldPassword, encodedPassword) } returns false

    // when & then
    assertThatExceptionOfType(IncorrectPasswordException::class.java).isThrownBy {
      userUpdateService.changePassword(changePasswordRequest)
    }
  }

  @Test
  fun `should override user's password as admin`() {
    // given
    val changePasswordRequest = ChangePasswordRequest(
      username = username,
      newPassword = "brand-n3w-secure_password",
      admin = true
    )
    val newPasswordEncoded = "new-password-encoded"
    every { passwordEncoderMock.encode(changePasswordRequest.newPassword) } returns newPasswordEncoded
    every { passwordEncoderMock.matches(changePasswordRequest.newPassword, newPasswordEncoded) } returns true

    // when
    userUpdateService.changePassword(changePasswordRequest)

    // then
    val storedPassword = findPasswordForUser(username)
    assertThat(storedPassword).isEqualTo(newPasswordEncoded)
  }

  @Test
  fun `should not change user's password on update`() {
    // given
    val userUpdateRequest = UpdateUserRequest(
      username = username,
      email = email,
      enabled = false,
      roles = setOf(UserRole.ROLE_USER, UserRole.ROLE_MODERATOR)
    )

    //when
    userUpdateService.update(userUpdateRequest)

    // then
    val storedPassword = findPasswordForUser(username)
    assertThat(storedPassword).isEqualTo(encodedPassword)
  }

  @Test
  fun `should return user's summary on update`() {
    // given
    val userUpdateRequest = UpdateUserRequest(
      username = username,
      email = email,
      enabled = false,
      roles = setOf(UserRole.ROLE_USER, UserRole.ROLE_MODERATOR)
    )

    // when
    val userSummary = userUpdateService.update(userUpdateRequest)

    // then
    val expectedSummary = UserSummary(
      username = username,
      enabled = userUpdateRequest.enabled,
      email = userUpdateRequest.email,
      roles = userUpdateRequest.roles
    )
    assertThat(userSummary).isEqualTo(expectedSummary)
  }

  @Test
  fun `should throw 'UserNotFoundException' when trying to update non-existing user`() {
    // given
    val invalidUpdateRequest = UpdateUserRequest(username = "incorrect-username", email = email)

    // when & then
    assertThatExceptionOfType(UserNotFoundException::class.java)
      .isThrownBy { userUpdateService.update(invalidUpdateRequest) }
  }
}
