import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'reko-animator-vs-participant-dialog',
  templateUrl: './animator-vs-participant-dialog.component.html',
  styleUrls: ['./animator-vs-participant-dialog.component.scss'],
})
export class AnimatorVsParticipantDialogComponent implements OnInit {
  constructor(
    private router: Router,
    private dialogRef: MatDialogRef<AnimatorVsParticipantDialogComponent>
  ) {}

  ngOnInit() {}

  onParticipant() {
    this.router.navigate(['verification', 'add'], { queryParams: { animator: 'false' } });
    this.dialogRef.close();
  }

  onAnimator() {
    this.router.navigate(['verification', 'add'], { queryParams: { animator: 'true' } });
    this.dialogRef.close();
  }
}
