import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndividualRetreatsComponent } from './individual-retreats.component';

describe('IndividualRetreatsComponent', () => {
  let component: IndividualRetreatsComponent;
  let fixture: ComponentFixture<IndividualRetreatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [IndividualRetreatsComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndividualRetreatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
