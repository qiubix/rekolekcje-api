package pl.oaza.waw.rekolekcje.api.parish.domain

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import pl.oaza.waw.rekolekcje.api.parish.dto.ParishNotFoundException
import pl.oaza.waw.rekolekcje.api.parish.dto.ParishPayload

internal class ParishTest {

  private val firstParish = createParish(1L, "First", "Address no 1.")
  private val secondParish = createParish(2L, "Second", "Address no 2.")

  private val parishRepository = InMemoryParishRepository()
  private val parishService = ParishConfiguration().parishService(parishRepository)

  @AfterEach
  fun tearDown() {
    parishService.delete(firstParish.id!!, secondParish.id!!)
  }

  @Test
  fun shouldAddNewParish() {
    parishService.save(firstParish)

    val foundParish = parishService.findOne(firstParish.id!!)
    assertThat(foundParish).isEqualTo(firstParish)
  }

  @Test
  fun shouldGetAllParishes() {
    parishService.save(firstParish)
    parishService.save(secondParish)

    val foundParishes = parishService.findAll()
    assertThat(foundParishes).hasSize(2)
    assertThat(foundParishes).containsOnlyOnce(firstParish, secondParish)
  }

  @Test
  fun shouldGetSingleParish() {
    parishService.save(firstParish)
    parishService.save(secondParish)

    val foundParish = parishService.findOne(secondParish.id!!)
    assertThat(foundParish).isEqualTo(secondParish)
  }

  @Test
  fun shouldThrowExceptionWhenParishNotFound() {
    parishService.save(firstParish)

    assertThatExceptionOfType(ParishNotFoundException::class.java)
      .isThrownBy { parishService.findOne(secondParish.id!!) }
      .withMessageContaining("id " + secondParish.id!!)
  }

  @Test
  fun shouldDeleteParish() {
    parishService.save(firstParish)
    parishService.save(secondParish)

    parishService.delete(firstParish.id!!)

    val remainingParishes = parishService.findAll()
    assertThat(remainingParishes).hasSize(1)
    assertThat(remainingParishes).containsOnly(secondParish)
  }

  @Test
  fun shouldUpdateParish() {
    parishService.save(firstParish)
    parishService.save(secondParish)
    val parishWithUpdatedData = createParish(secondParish.id!!, "New name", "New address")

    parishService.save(parishWithUpdatedData)

    val foundParish = parishService.findOne(secondParish.id!!)
    assertThat(foundParish).isEqualTo(parishWithUpdatedData)
  }

  private fun createParish(id: Long, name: String, address: String): ParishPayload {
    return ParishPayload(id = id, name = name, address = address)
  }
}
