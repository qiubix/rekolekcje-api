import { CommunityComponent } from './_root/community.component';
import { CommunityDialogComponent } from './add-edit/community-dialog/community-dialog.component';
import { CommunityFormComponent } from './add-edit/community-form/community-form.component';
import { CommunityListComponent } from './list/community-list.component';

export const components: any[] = [
  CommunityComponent,
  CommunityDialogComponent,
  CommunityFormComponent,
  CommunityListComponent
];

export * from './_root/community.component';
export * from './add-edit/community-dialog/community-dialog.component';
