package pl.oaza.waw.rekolekcje.api.user.api

import pl.oaza.waw.rekolekcje.api.shared.BaseApiBehaviour
import pl.oaza.waw.rekolekcje.api.shared.Response
import pl.oaza.waw.rekolekcje.api.shared.RestApi
import pl.oaza.waw.rekolekcje.api.user.domain.UserNotFoundException
import pl.oaza.waw.rekolekcje.api.user.endpoint.ChangePasswordRequest
import pl.oaza.waw.rekolekcje.api.user.endpoint.SignUpRequest
import pl.oaza.waw.rekolekcje.api.user.endpoint.UpdateUserRequest
import pl.oaza.waw.rekolekcje.api.user.endpoint.UserEndpoint.Companion.DISABLE_URI
import pl.oaza.waw.rekolekcje.api.user.endpoint.UserEndpoint.Companion.ENABLE_URI
import pl.oaza.waw.rekolekcje.api.user.endpoint.UserEndpoint.Companion.PASSWORD_URI
import pl.oaza.waw.rekolekcje.api.user.endpoint.UserEndpoint.Companion.REGISTER_URI
import pl.oaza.waw.rekolekcje.api.user.endpoint.UserSummary

class UserApiBehaviour(restApi: RestApi, baseUri: String): BaseApiBehaviour<UserSummary>(restApi, baseUri) {

  fun newUserIsRegistered(signUpPayload: SignUpRequest): Response {
    return restApi.performPost(baseUri + REGISTER_URI, signUpPayload)
  }

  fun userIsUpdated(userUpdateRequest: UpdateUserRequest): Response {
    return restApi.performPut(baseUri, userUpdateRequest)
  }

  fun userIsDeleted(username: String): Response {
    return restApi.performDelete("$baseUri/$username")
  }

  fun userIsDisabled(username: String): Response {
    return restApi.performPut("$baseUri$DISABLE_URI", username)
  }

  fun userIsEnabled(username: String): Response {
    return restApi.performPut("$baseUri$ENABLE_URI", username)
  }

  fun passwordIsChanged(changePasswordRequest: ChangePasswordRequest): Response {
    return restApi.performPut("$baseUri$PASSWORD_URI", changePasswordRequest)
  }

  override fun haveTheSameData(first: UserSummary, second: UserSummary): Boolean =
    throw IllegalStateException("Not yet implemented")

  override fun getNotFoundException(payload: UserSummary): RuntimeException = UserNotFoundException(payload.username)

}
