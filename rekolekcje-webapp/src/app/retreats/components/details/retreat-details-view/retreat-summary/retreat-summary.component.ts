import { Component, Input, OnInit } from '@angular/core';
import { RetreatDetails } from '@retreats/models/retreat-details.model';

@Component({
  selector: 'reko-retreat-summary',
  templateUrl: './retreat-summary.component.html',
  styleUrls: ['./retreat-summary.component.scss'],
})
export class RetreatSummaryComponent implements OnInit {
  @Input() retreatDetails: RetreatDetails;

  ngOnInit() {}
}
