package pl.oaza.waw.rekolekcje.api.application.domain

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.junit.jupiter.api.Test
import pl.oaza.waw.rekolekcje.api.application.ApplicationTestFixture
import pl.oaza.waw.rekolekcje.api.application.animatorApplication
import pl.oaza.waw.rekolekcje.api.shared.value.PaymentStatus

internal class PaymentStatusTest : ApplicationTestFixture() {

  @Test
  fun `should create application with 'NOTHING' payment status by default`() {
    // given
    val application = RetreatApplication(
      applicationType = ApplicationType.PARTICIPANT,
      memberId = 123L,
      firstName = "John",
      lastName = "Doe"
    )

    // when
    val savedApplication = commandFacade.submitNewApplication(application)

    // then
    assertThat(savedApplication.paymentStatus).isEqualTo(PaymentStatus.NOTHING)
  }

  @Test
  fun `should change member status to 'DOWN PAYMENT'`() {
    // given
    val applicationId = submitSampleApplication()

    // when
    commandFacade.changePaymentStatus(applicationId, PaymentStatus.DOWN_PAYMENT)

    // then
    val savedApplication = findApplication(applicationId)
    assertThat(savedApplication.paymentStatus).isEqualTo(PaymentStatus.DOWN_PAYMENT)
  }

  @Test
  fun `should change member payment status to 'FULL'`() {
    // given
    val applicationId = submitSampleApplication()

    // when
    commandFacade.changePaymentStatus(applicationId, PaymentStatus.FULL)

    // then
    val savedApplication = findApplication(applicationId)
    assertThat(savedApplication.paymentStatus).isEqualTo(PaymentStatus.FULL)
  }

  @Test
  fun `should throw exception when member does not exist`() {
    val idOfNotExistingApplication = 999L

    assertThatExceptionOfType(IllegalStateException::class.java)
      .isThrownBy { commandFacade.changePaymentStatus(idOfNotExistingApplication, PaymentStatus.FULL) }
  }

  private fun submitSampleApplication(): Long {
    val application = animatorApplication(paymentStatus = PaymentStatus.NOTHING)
    val submittedApplication = commandFacade.submitNewApplication(application)
    return submittedApplication.getId() ?: throw IllegalStateException()
  }

  private fun findApplication(id: Long) = queryFacade.findApplicationById(id) ?: throw IllegalStateException()
}