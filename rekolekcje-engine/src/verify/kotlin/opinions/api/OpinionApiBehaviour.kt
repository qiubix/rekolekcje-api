package pl.oaza.waw.rekolekcje.api.opinions.api

import pl.oaza.waw.rekolekcje.api.opinions.endpoint.OpinionDetailsResponse
import pl.oaza.waw.rekolekcje.api.opinions.endpoint.OpinionSubmittedResponse
import pl.oaza.waw.rekolekcje.api.opinions.endpoint.OpinionsEndpoint.Companion.OPINIONS_URI
import pl.oaza.waw.rekolekcje.api.opinions.endpoint.OpinionsEndpoint.Companion.SUBMIT_URI
import pl.oaza.waw.rekolekcje.api.opinions.endpoint.SubmitOpinionRequest
import pl.oaza.waw.rekolekcje.api.shared.RestApi

class OpinionApiBehaviour(private val restApi: RestApi) {

  private val baseUri = OPINIONS_URI
  private val submitUri = baseUri + SUBMIT_URI

  fun alreadyExistSomeOpinions(opinions: List<SubmitOpinionRequest>): List<OpinionDetailsResponse> {
    return opinions
      .map { restApi.performPost(submitUri, it) }
      .map { it.getContent<OpinionSubmittedResponse>().id }
      .map { restApi.performGet("$baseUri/$it").getContent<OpinionDetailsResponse>() }
  }
}
