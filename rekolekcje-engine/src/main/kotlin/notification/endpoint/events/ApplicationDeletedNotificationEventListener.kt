package pl.oaza.waw.rekolekcje.api.notification.endpoint.events

import org.springframework.context.ApplicationListener
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import pl.oaza.waw.rekolekcje.api.application.endpoint.events.ApplicationDeletedEvent
import pl.oaza.waw.rekolekcje.api.notification.domain.NotificationCommandFacade

@Component
internal class ApplicationDeletedNotificationEventListener(
  private val notificationCommandFacade: NotificationCommandFacade
) : ApplicationListener<ApplicationDeletedEvent> {

  @Transactional
  override fun onApplicationEvent(event: ApplicationDeletedEvent) {
    notificationCommandFacade.deleteByApplication(event.applicationId)
  }
}