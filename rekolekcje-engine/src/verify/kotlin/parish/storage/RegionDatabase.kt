package pl.oaza.waw.rekolekcje.api.parish.storage

import org.springframework.jdbc.core.JdbcTemplate

class RegionDatabase(private val jdbcTemplate: JdbcTemplate) {

  fun clearAll() {
    jdbcTemplate.execute("DELETE FROM regions")
  }
}
