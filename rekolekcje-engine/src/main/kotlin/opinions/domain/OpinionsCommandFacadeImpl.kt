package pl.oaza.waw.rekolekcje.api.opinions.domain

import org.slf4j.LoggerFactory
import pl.oaza.waw.rekolekcje.api.members.infrastructure.encryption.MembersEncryptionService
import pl.oaza.waw.rekolekcje.api.shared.exceptions.MemberNotFoundException
import pl.oaza.waw.rekolekcje.api.members.query.MembersQueryFacade
import pl.oaza.waw.rekolekcje.api.opinions.endpoint.SubmitOpinionRequest

internal class OpinionsCommandFacadeImpl(
    private val writeRepository: OpinionsWriteRepository,
    private val opinionsCreator: OpinionsCreator,
    private val membersQueryFacade: MembersQueryFacade,
    private val membersEncryptionService: MembersEncryptionService
) : OpinionsCommandFacade {

  private val logger = LoggerFactory.getLogger(this::class.java)

  override fun submit(submitRequest: SubmitOpinionRequest): Opinion {
    val memberId = membersEncryptionService.decryptMemberId(submitRequest.encryptedMemberId)
    validateIfMemberExists(memberId)

    val opinion = opinionsCreator.fromSubmitRequest(submitRequest, memberId)

    return writeRepository.save(opinion)
  }

  private fun validateIfMemberExists(memberId: Long) {
    if (!membersQueryFacade.existsById(memberId)) {
      logger.info("Opinion can be submitted only for existing member")
      throw MemberNotFoundException(memberId)
    }
  }
}
