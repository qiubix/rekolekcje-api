import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'reko-rejection-reason-dialog',
  templateUrl: './rejection-reason-dialog.component.html',
  styleUrls: ['./rejection-reason-dialog.component.scss'],
})
export class RejectionReasonDialogComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<RejectionReasonDialogComponent>,
    private _fb: FormBuilder
  ) {}

  form: FormGroup;

  ngOnInit() {
    this.form = this._fb.group({
      reason: '',
    });
  }

  onSubmit() {
    console.log('on submit clicked');
    this.dialogRef.close(this.form.value);
  }

  onCancel() {
    console.log('on cancel clicked');
    this.dialogRef.close();
  }
}
