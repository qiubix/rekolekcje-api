package pl.oaza.waw.rekolekcje.api.members.endpoint

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.multipart.MultipartFile
import pl.oaza.waw.rekolekcje.api.members.domain.MembersCommandFacade
import pl.oaza.waw.rekolekcje.api.members.endpoint.csv.convertMembersFromCsv
import pl.oaza.waw.rekolekcje.api.parish.domain.ParishFacade
import pl.oaza.waw.rekolekcje.api.parish.dto.ParishPayload
import pl.oaza.waw.rekolekcje.api.shared.exceptions.InvalidPayloadException

@Service
@Transactional
internal class MembersCreateService(
  private val membersCommandFacade: MembersCommandFacade,
  private val parishFacade: ParishFacade
) {

  private val logger = LoggerFactory.getLogger(this.javaClass)

  fun createMember(memberDetails: MemberDetails): MemberSummary {
    val parishId = findOrCreateParish(memberDetails.personalData)
    val updatedDetails = memberDetails.copy(personalData = memberDetails.personalData.copy(parishId = parishId))
    val memberToSave = fromDto(updatedDetails)
    val savedMember = membersCommandFacade.save(memberToSave)
    return MemberSummary(savedMember)
  }

  private fun findOrCreateParish(personalData: PersonalDataValue): Long? {
    val newParish = personalData.newParish
    val currentParishId = personalData.parishId

    if (currentParishId != null && newParish != null) {
      throw InvalidPayloadException()
    }

    return when {
      newParish != null -> parishFacade.save(ParishPayload(name = newParish.name, address = newParish.address)).id
      else -> currentParishId
    }
  }

  fun createManyMembers(members: List<MemberDetails>): List<MemberSummary> {
    val membersToSave = members.map { fromDto(it) }
    return membersCommandFacade.saveAll(membersToSave).map { MemberSummary(it) }
  }

  fun importFromCsv(file: MultipartFile): List<MemberSummary> {
    logger.info("File uploaded: ${file.originalFilename}")
    val fileContents = String(file.bytes)
    val membersToSave = convertMembersFromCsv(fileContents).convertedMembers.map { fromDto(it) }
    logger.info("Extracted following members: ${membersToSave.map { "${it.personalData.contactInfo}\n" }}")
    return membersCommandFacade.saveAll(membersToSave).map { MemberSummary(it) }
  }
}