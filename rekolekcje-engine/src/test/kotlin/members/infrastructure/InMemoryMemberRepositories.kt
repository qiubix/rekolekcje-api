package pl.oaza.waw.rekolekcje.api.members.infrastructure

import pl.oaza.waw.rekolekcje.api.members.domain.Member
import pl.oaza.waw.rekolekcje.api.members.domain.MemberReadRepository
import pl.oaza.waw.rekolekcje.api.members.domain.MemberWriteRepository
import pl.oaza.waw.rekolekcje.api.tools.repository.InMemoryReadRepository
import pl.oaza.waw.rekolekcje.api.tools.repository.InMemoryWriteRepository

class InMemoryMemberWriteRepository(storage: HashMap<Long, Member>) : InMemoryWriteRepository<Member>(storage), MemberWriteRepository

class InMemoryMemberReadRepository(storage: Map<Long, Member>) : InMemoryReadRepository<Member>(storage), MemberReadRepository
