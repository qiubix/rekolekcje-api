package pl.oaza.waw.rekolekcje.api.parish.infrastructure

import org.springframework.data.repository.Repository
import pl.oaza.waw.rekolekcje.api.parish.domain.ParishRepository

internal interface PersistedParishRepository : ParishRepository, Repository<PersistedParish, Long>
