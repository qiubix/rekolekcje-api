import { OpinionsComponent } from './_root/opinions.component';
import { OpinionFormComponent } from './opinion-form/opinion-form.component';
import { OpinionSubmitterComponent } from './opinion-form/opinion-submitter/opinion-submitter.component';
import { OpinionContentComponent } from './opinion-form/opinion-content/opinion-content.component';
import { MembersWaitingForOpinionComponent } from './members-waiting-for-opinion/members-waiting-for-opinion.component';
import { OpinionSubmissionPageComponent } from './opinion-submission-page/opinion-submission-page.component';
import { OpinionThankYouPageComponent } from './opinion-thank-you-page/opinion-thank-you-page.component';

export const components: any[] = [
  OpinionsComponent,
  OpinionFormComponent,
  OpinionSubmitterComponent,
  OpinionContentComponent,
  MembersWaitingForOpinionComponent,
  OpinionSubmissionPageComponent,
  OpinionThankYouPageComponent
];

export * from './_root/opinions.component';
export * from './opinion-submission-page/opinion-submission-page.component';
export * from './opinion-thank-you-page/opinion-thank-you-page.component';
