import { NgModule } from '@angular/core';

import { RegistrationRoutingModule } from './registration-routing.module';
import { RegistrationComponent } from './root/registration.component';
import { SharedModule } from '../shared/shared.module';
import { ActiveRetreatsListComponent } from './active-retreats-list/active-retreats-list.component';
import { IndividualRetreatsComponent } from './individual-retreats/individual-retreats.component';

@NgModule({
  declarations: [RegistrationComponent, ActiveRetreatsListComponent, IndividualRetreatsComponent],
  imports: [SharedModule, RegistrationRoutingModule]
})
export class RegistrationModule {}
