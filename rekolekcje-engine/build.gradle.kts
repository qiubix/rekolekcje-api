import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
  id("spring-with-kotlin")
  id("tests-config")
  id("coverage")
  id("sonar")
}

repositories {
  mavenCentral()
  maven(url = "https://dl.bintray.com/kotlin/exposed")
}

version = "1.0.0-SNAPSHOT"
group = "pl.oaza.waw"

java {
  sourceCompatibility = JavaVersion.VERSION_17
  targetCompatibility = JavaVersion.VERSION_17
}

tasks.withType<KotlinCompile> {
  kotlinOptions {
    freeCompilerArgs = listOf("-Xjsr305=strict")
    jvmTarget = "17"
  }
}

dependencies {

  implementation(Libs.liquibase)
  implementation(Libs.json)
  implementation(Libs.jjwt)
  implementation(Libs.gag)
  implementation(Libs.hibernateJava8)
  implementation(Libs.jacksonJsr310)
//  implementation(Libs.exposed)

  implementation(Libs.openApiUi)

  runtimeOnly(RuntimeLibs.postgresql)

  testImplementation(TestLibs.junit)
  testImplementation(TestLibs.assertJ)
  testImplementation(TestLibs.mockk)

  verifyImplementation(TestLibs.postgresqlEmbedded)
  verifyImplementation(TestLibs.testContainersPostgresql)
  verifyImplementation(TestLibs.testContainersJUnit)
  verifyImplementation(TestLibs.greenMail)
  verifyImplementation(TestLibs.greenMailSpring)
  verifyImplementation(TestLibs.archUnit)
  verifyImplementation(TestLibs.awaitility)

}

val packageFrontend = task<Copy>("packageFrontend") {
  dependsOn(":rekolekcje-webapp:buildClient")
  from("${project(":rekolekcje-webapp").projectDir}/dist")
  into("${buildDir}/classes/kotlin/main/static")
}

tasks["bootJar"].dependsOn(packageFrontend)