package pl.oaza.waw.rekolekcje.api.retreats

import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.jdbc.core.JdbcTemplate
import pl.oaza.waw.rekolekcje.api.shared.RestApi
import pl.oaza.waw.rekolekcje.api.retreats.api.RetreatsApiBehaviour
import pl.oaza.waw.rekolekcje.api.retreats.api.RetreatsApiExpectations
import pl.oaza.waw.rekolekcje.api.retreats.api.RetreatsEventsBehaviour
import pl.oaza.waw.rekolekcje.api.retreats.endpoint.RetreatsEndpoint.Companion.RETREATS_API_URI
import pl.oaza.waw.rekolekcje.api.retreats.storage.RetreatsDatabase

@Configuration
@Profile("test")
internal class RetreatsTestContext {

  @Bean
  fun retreatsDatabase(jdbcTemplate: JdbcTemplate): RetreatsDatabase {
    return RetreatsDatabase(jdbcTemplate)
  }

  @Bean
  fun retreatsApiBehaviour(restApi: RestApi): RetreatsApiBehaviour {
    return RetreatsApiBehaviour(restApi, RETREATS_API_URI)
  }

  @Bean
  fun retreatsApiExpectations(restApi: RestApi): RetreatsApiExpectations {
    return RetreatsApiExpectations(restApi, RETREATS_API_URI)
  }

  @Bean
  fun retreatsEventsBehaviour(eventPublisher: ApplicationEventPublisher): RetreatsEventsBehaviour =
    RetreatsEventsBehaviour(eventPublisher)
}
