package pl.oaza.waw.rekolekcje.api.notification.domain.template.extractor

import pl.oaza.waw.rekolekcje.api.notification.domain.Notification

internal class EmailExtractor(
	private val notification: Notification
) : Extractor(ExtractorType.EMAIL) {
	override fun extractValue(): String {
		return notification.targetEmail
	}
}
