package pl.oaza.waw.rekolekcje.api.retreats

import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationType
import pl.oaza.waw.rekolekcje.api.application.endpoint.RetreatApplicationDto
import pl.oaza.waw.rekolekcje.api.retreats.domain.RetreatStatus
import pl.oaza.waw.rekolekcje.api.retreats.domain.Turn
import pl.oaza.waw.rekolekcje.api.retreats.endpoint.RetreatDetails
import pl.oaza.waw.rekolekcje.api.retreats.endpoint.RetreatMemberDto
import pl.oaza.waw.rekolekcje.api.retreats.endpoint.RetreatStatisticsDto
import pl.oaza.waw.rekolekcje.api.retreats.endpoint.RetreatSummary
import pl.oaza.waw.rekolekcje.api.shared.value.PaymentStatus
import pl.oaza.waw.rekolekcje.api.shared.value.Sex
import pl.oaza.waw.rekolekcje.api.shared.value.Stage
import java.time.LocalDate


fun retreatSummary(details: RetreatDetails): RetreatSummary {
  return RetreatSummary(
    id = details.id,
    stage = details.stage,
    status = details.status,
    turn = details.turn,
    localization = details.localization,
    startDate = details.startDate,
    endDate = details.endDate
  )
}

fun retreatDetailsWithSampleData1(): RetreatDetails {
  return RetreatDetails(
    localization = "Warsaw",
    stage = Stage.ONZ_III,
    status = RetreatStatus.ACTIVE,
    turn = Turn.II
  )
}

fun retreatDetailsWithSampleData2(): RetreatDetails {
  return RetreatDetails(
    localization = "Pomiechowek",
    stage = Stage.ODB,
    status = RetreatStatus.HISTORICAL,
    turn = Turn.I,
    startDate = LocalDate.of(2020, 7, 2),
    endDate = LocalDate.of(2020, 7, 17),
    moderatorName = "Jane Jones",
    moderatorPhoneNumber = "777888999",
    priestName = "John Smith",
    priestPhoneNumber = "888999777"
  )
}

fun retreatDetailsWithSampleData(
  stage: Stage = Stage.OND,
  turn: Turn = Turn.I,
  startDate: LocalDate = LocalDate.of(2010, 7, 15)
): RetreatDetails {
  return RetreatDetails(
    stage = stage,
    turn = turn,
    startDate = startDate,
    localization = "Pomiechowek",
    moderatorName = "Jane Jones",
    moderatorPhoneNumber = "777888999",
    priestName = "John Smith",
    priestPhoneNumber = "888999777"
  )
}

fun RetreatSummary.withNumberOfParticipants(number: Int) = this.copy(participantsAmount = number)
fun RetreatSummary.withNumberOfMaleParticipants(number: Int) = this.copy(malesParticipants = number)
fun RetreatSummary.withNumberOfFemaleParticipants(number: Int) = this.copy(femalesParticipants = number)
fun RetreatSummary.withAverageAge(average: Double) = this.copy(averageAge = average)
fun RetreatSummary.withNumberOfUnderageParticipants(number: Int) = this.copy(underageParticipants = number)

fun RetreatDetails.withId(id: Long) = this.copy(id = id)
fun RetreatDetails.withParticipants(participants: Set<RetreatMemberDto>) = this.copy(participants = participants)
fun RetreatDetails.withStatistics(stats: RetreatStatisticsDto) = this.copy(statistics = stats)
fun RetreatDetails.withLimit(limit: Int) = this.copy(participantsLimit = limit)

fun sampleRetreatMember(
  memberId: Long,
  firstName: String = "Sample",
  lastName: String = "Retreat Member",
  sex: Sex = Sex.MALE,
  age: Int = 17,
  paymentStatus: PaymentStatus = PaymentStatus.NOTHING,
  animator: Boolean = false
): RetreatMemberDto = RetreatMemberDto(
  memberId = memberId,
  firstName = firstName,
  lastName = lastName,
  sex = sex,
  age = age,
  paymentStatus = paymentStatus,
  animator = animator
)

fun sampleRetreatMember(application: RetreatApplicationDto): RetreatMemberDto = RetreatMemberDto(
  memberId = application.member.id ?: throw IllegalStateException(),
  firstName = application.member.personalData.firstName,
  lastName = application.member.personalData.lastName,
  sex = application.member.personalData.sex ?: throw IllegalStateException(),
  age = application.member.personalData.age ?: throw IllegalStateException(),
  paymentStatus = application.paymentStatus,
  animator = application.type == ApplicationType.ANIMATOR
)