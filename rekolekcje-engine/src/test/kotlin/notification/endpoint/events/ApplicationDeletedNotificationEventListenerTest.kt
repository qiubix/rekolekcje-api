package notification.endpoint.events

import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import pl.oaza.waw.rekolekcje.api.application.endpoint.events.ApplicationDeletedEvent
import pl.oaza.waw.rekolekcje.api.notification.domain.NotificationCommandFacade
import pl.oaza.waw.rekolekcje.api.notification.endpoint.events.ApplicationDeletedNotificationEventListener

internal class ApplicationDeletedNotificationEventListenerTest {
	private val notificationCommandFacadeMock = mockk<NotificationCommandFacade>(relaxUnitFun = true)
	private val eventListener = ApplicationDeletedNotificationEventListener(notificationCommandFacadeMock)

	private val applicationId = 123L
	private val event = ApplicationDeletedEvent(
		applicationId = applicationId
	)

	@Test
	fun `should delete notifications when member is deleted`() {
		// when
		eventListener.onApplicationEvent(event)

		// then
		verify { notificationCommandFacadeMock.deleteByApplication(applicationId) }
	}
}
