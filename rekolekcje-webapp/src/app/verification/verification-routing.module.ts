import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
  AddEditApplicationPageComponent,
  ApplicationDetailsComponent,
  VerificationComponent
} from '@verification/components';
import { MembersToVerifyGuard } from './guards/members-to-verify.guard';
import { ApplicationDetailsGuard } from './guards/application-details.guard';

const routes: Routes = [
  {
    path: '',
    component: VerificationComponent,
    canActivate: [MembersToVerifyGuard]
  },
  {
    path: 'add',
    component: AddEditApplicationPageComponent,
    data: { editing: false }
  },
  {
    path: ':id',
    component: ApplicationDetailsComponent,
    canActivate: [ApplicationDetailsGuard]
  },
  {
    path: ':id/edit',
    component: AddEditApplicationPageComponent,
    canActivate: [ApplicationDetailsGuard],
    data: { editing: true }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VerificationRoutingModule {}
