package pl.oaza.waw.rekolekcje.api.members

import com.tngtech.archunit.core.domain.JavaClasses
import com.tngtech.archunit.junit.AnalyzeClasses
import com.tngtech.archunit.junit.ArchTest
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition
import com.tngtech.archunit.library.Architectures.onionArchitecture

@AnalyzeClasses(packages = ["pl.oaza.waw.rekolekcje.api.members"])
class MembersArchitectureTest {

  @ArchTest
  fun `domain package should not depend on infrastructure`(importedClasses: JavaClasses) {
    val rule = ArchRuleDefinition
      .noClasses().that().resideInAPackage("..domain..")
      .should().dependOnClassesThat()
      .resideInAPackage("..infrastructure..")
    rule.check(importedClasses)
  }

  @ArchTest
  fun `domain should not depend on adapters`(importedClasses: JavaClasses) {
    val rule = ArchRuleDefinition
      .noClasses().that().resideInAPackage("..domain..")
      .should().dependOnClassesThat()
      .resideInAPackage("..endpoint..")
    rule.check(importedClasses)
  }

  @ArchTest
  fun `domain should not depend on Spring framework`(importedClasses: JavaClasses) {
    val rule = ArchRuleDefinition
      .noClasses().that().resideInAPackage("..domain..")
      .should().dependOnClassesThat()
      .resideInAPackage("org.springframework..")
    rule.check(importedClasses)
  }

  fun `should respect hexagonal architecture`(importedClasses: JavaClasses) {
    val baseMemberPackage = "pl.oaza.waw.rekolekcje.api.members"
    val rule = onionArchitecture()
      .domainModels("$baseMemberPackage.domain")
      .adapter("rest", "$baseMemberPackage.endpoint")
      .adapter("events", "$baseMemberPackage.infrastructure.events")
      .adapter("persistance", "$baseMemberPackage.infrastructure.persistance")
    rule.check(importedClasses)
  }
}