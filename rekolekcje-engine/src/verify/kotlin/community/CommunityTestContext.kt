package pl.oaza.waw.rekolekcje.api.community

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.jdbc.core.JdbcTemplate
import pl.oaza.waw.rekolekcje.api.community.api.CommunityApiBehaviour
import pl.oaza.waw.rekolekcje.api.community.api.CommunityApiExpectations
import pl.oaza.waw.rekolekcje.api.community.storage.CommunityDatabase
import pl.oaza.waw.rekolekcje.api.shared.RestApi

@Configuration
@Profile("test")
internal class CommunityTestContext {

  @Bean
  fun communityStorage(jdbcTemplate: JdbcTemplate): CommunityDatabase {
    return CommunityDatabase(jdbcTemplate)
  }

  @Bean
  fun communityApiBehaviour(restApi: RestApi): CommunityApiBehaviour {
    return CommunityApiBehaviour(
      restApi,
      COMMUNITY_API_URI
    )
  }

  @Bean
  fun communityApiExpectations(restApi: RestApi): CommunityApiExpectations {
    return CommunityApiExpectations(
      restApi,
      COMMUNITY_API_URI
    )
  }
}
