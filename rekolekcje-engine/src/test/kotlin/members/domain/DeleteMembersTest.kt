package pl.oaza.waw.rekolekcje.api.members.domain

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import pl.oaza.waw.rekolekcje.api.members.MemberTestFixture

internal class DeleteMembersTest : MemberTestFixture() {

  private val first = sampleMember("Roger", "Moore", "95030455410")
  private val second = sampleMember("Pierce", "Brosnan", "05230455415")
  private val third = sampleMember("Sean", "Connery", "75093055417")

  @Test
  fun `should delete member from system`() {
    // given
    commandFacade.saveAll(listOf(first, second, third))
    val idToDelete = getIdOfCorrespondingMemberFromSystem(second)

    // when
    commandFacade.delete(idToDelete)

    // then
    assertThat(queryFacade.findAllMembers())
      .hasSize(2)
      .doesNotContain(second.copy(id = idToDelete))
  }

}
