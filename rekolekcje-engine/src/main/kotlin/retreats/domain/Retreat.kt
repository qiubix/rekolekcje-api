package pl.oaza.waw.rekolekcje.api.retreats.domain

import pl.oaza.waw.rekolekcje.api.shared.domain.EntityWithId
import pl.oaza.waw.rekolekcje.api.shared.value.Sex
import pl.oaza.waw.rekolekcje.api.shared.value.Stage
import java.time.LocalDate
import javax.persistence.*

@Entity(name = "retreat")
@Table(name = "retreats")
data class Retreat(

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  @Column(name = "retreat_id")
  private val id: Long? = null,

  @Enumerated(EnumType.STRING)
  val turn: Turn? = null,

  @Enumerated(EnumType.STRING)
  val stage: Stage? = null,

  @Enumerated(EnumType.STRING)
  val status: RetreatStatus? = null,

  val localization: String? = null,

  val startDate: LocalDate? = null,
  val endDate: LocalDate? = null,

  val moderatorName: String? = null,
  val moderatorPhoneNumber: String? = null,
  val priestName: String? = null,
  val priestPhoneNumber: String? = null,

  @OneToMany(cascade = [CascadeType.ALL], orphanRemoval = true)
  @JoinColumn(name = "retreat_id")
  private val retreatMembers: MutableSet<RetreatMember> = mutableSetOf(),

  val participantsLimit: Int? = null,
  val minimumAnimatorsRequired: Int = 0
) : EntityWithId {

  override fun getId(): Long? = id

  override fun withId(id: Long): Retreat = this.copy(id = id)

  fun numberOfParticipants(): Int {
    return participants().size
  }

  fun numberOfMaleParticipants(): Int {
    return participants().count { it.sex == Sex.MALE }
  }

  fun numberOfFemaleParticipants(): Int {
    return participants().count { it.sex == Sex.FEMALE }
  }

  fun averageParticipantsAge(): Double {
    val average = participants().map { it.age }.average()
    return if (average.isNaN()) 0.0 else average
  }

  fun numberOfUnderageParticipants(): Int = participants()
    .map { it.age }
    .filter { it < 18 }
    .count()

  fun participants(): Set<RetreatMember> = retreatMembers.filter { !it.animator }.toSet()

  fun animators(): Set<RetreatMember> = retreatMembers.filter { it.animator }.toSet()

  fun registerMember(member: RetreatMember) {
    if (member.animator) {
      registerAnimator(member)
    }

    if (!member.animator) {
      registerParticipant(member)
    }
  }

  private fun registerAnimator(animator: RetreatMember) {
    retreatMembers.add(animator)
  }

  private fun registerParticipant(participant: RetreatMember) {
    requireParticipantsLimitNotReached()
    retreatMembers.add(participant)
  }

  private fun requireParticipantsLimitNotReached() {
    if (participantsLimit == null) {
      return
    }

    if (numberOfParticipants() >= participantsLimit) {
      throw RetreatParticipantLimitExceededException(
        retreatId = id ?: throw IllegalStateException("To apply, retreat id must be present"),
        limit = participantsLimit
      )
    }
  }

  fun hasMember(memberId: Long): Boolean = this.retreatMembers.any { it.memberId == memberId }

  fun removeMember(memberId: Long) {
    this.retreatMembers.removeIf { it.memberId == memberId }
  }

  fun hasEnoughAnimators() = animators().count() >= minimumAnimatorsRequired
}

