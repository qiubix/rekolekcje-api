import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedMembersTableComponent } from './shared-members-table.component';

describe('SharedMembersTableComponent', () => {
  let component: SharedMembersTableComponent;
  let fixture: ComponentFixture<SharedMembersTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SharedMembersTableComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedMembersTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
