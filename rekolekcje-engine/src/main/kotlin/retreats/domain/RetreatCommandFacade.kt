package pl.oaza.waw.rekolekcje.api.retreats.domain

/**
 * Provides methods for modifying retreats' state.
 */
interface RetreatCommandFacade {

  fun save(retreat: Retreat): Retreat

  fun delete(id: Long)

  fun removeMemberFromAllRetreats(memberId: Long)

  fun registerMember(retreatId: Long, member: RetreatMember): Retreat

}
