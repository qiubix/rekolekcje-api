package pl.oaza.waw.rekolekcje.api.parish.domain

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
internal class ParishConfiguration {

  @Bean
  fun parishService(parishRepository: ParishRepository): ParishFacade {
    return ParishFacade(parishRepository)
  }
}
