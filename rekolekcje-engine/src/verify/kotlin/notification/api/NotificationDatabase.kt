package pl.oaza.waw.rekolekcje.api.notification.api

import org.springframework.jdbc.core.JdbcTemplate
import pl.oaza.waw.rekolekcje.api.notification.dto.NotificationReason
import pl.oaza.waw.rekolekcje.api.notification.dto.NotificationStatus
import pl.oaza.waw.rekolekcje.api.notification.dto.NotificationSummary
import java.sql.ResultSet

class NotificationDatabase(private val jdbcTemplate: JdbcTemplate) {

  fun clear() {
    jdbcTemplate.execute("DELETE FROM notifications")
  }

  internal fun getNotifications(applicationId: Long): List<NotificationSummary> {
    return jdbcTemplate.query("select * from notifications where application_id = $applicationId", notificationSummaryRowMapper())
  }

  private fun notificationSummaryRowMapper(): (ResultSet, Int) -> NotificationSummary {
    return { rs, _ ->
      NotificationSummary(
        id = rs.getLong("id"),
        applicationId = rs.getLong("application_id"),
        reason = NotificationReason.valueOf(rs.getString("reason")),
        status = NotificationStatus.valueOf(rs.getString("status"))
      )
    }
  }
}
