import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { Action, Store } from '@ngrx/store';
import { catchError, map, switchMap } from 'rxjs/operators';
import { Community } from '../models/community.model';
import { CommunityActions } from './community-actions';
import { Communities } from './community-reducer';
import { CommunityService } from '../community.service';
import CreateCommunitySuccess = CommunityActions.CreateCommunitySuccess;
import CreateCommunityFail = CommunityActions.CreateCommunityFail;
import DeleteCommunitySuccess = CommunityActions.DeleteCommunitySuccess;
import DeleteCommunityFail = CommunityActions.DeleteCommunityFail;
import SelectCommunitySuccess = CommunityActions.SelectCommunitySuccess;
import UpdateCommunitySuccess = CommunityActions.UpdateCommunitySuccess;
import UpdateCommunityFail = CommunityActions.UpdateCommunityFail;
import LoadCommunitiesListFail = CommunityActions.LoadCommunitiesListFail;
import LoadCommunitiesListSuccess = CommunityActions.LoadCommunitiesListSuccess;

@Injectable()
export class CommunityEffects {
  constructor(
    private actions: Actions,
    private store: Store<Communities.State>,
    private service: CommunityService
  ) {}

  @Effect()
  CreateCommunity: Observable<Action> = this.actions.pipe(
    ofType(CommunityActions.types.CreateCommunity),
    switchMap((action: CommunityActions.CreateCommunity) =>
      this.service.addNew(action.payload).pipe(
        map(data => new CreateCommunitySuccess(data)),
        catchError(error => of(new CreateCommunityFail(error)))
      )
    )
  );

  @Effect()
  DeleteCommunity: Observable<Action> = this.actions.pipe(
    ofType(CommunityActions.types.DeleteCommunity),
    switchMap((action: CommunityActions.DeleteCommunity) =>
      this.service.deleteOne(action.payload).pipe(
        map(data => new DeleteCommunitySuccess(action.payload)),
        catchError(error => of(new DeleteCommunityFail(error)))
      )
    )
  );

  @Effect()
  LoadCommunitiesList: Observable<Action> = this.actions.pipe(
    ofType(CommunityActions.types.LoadCommunitiesList),
    switchMap((action: CommunityActions.LoadCommunitiesList) =>
      this.service.getAll().pipe(
        map((data: Community[]) => new LoadCommunitiesListSuccess(data)),
        catchError(error => of(new LoadCommunitiesListFail(error)))
      )
    )
  );

  @Effect()
  SelectCommunity: Observable<Action> = this.actions.pipe(
    ofType(CommunityActions.types.SelectCommunity),
    switchMap((action: CommunityActions.SelectCommunity) =>
      of(new SelectCommunitySuccess(action.payload))
    )
  );

  @Effect()
  UpdateCommunity: Observable<Action> = this.actions.pipe(
    ofType(CommunityActions.types.UpdateCommunity),
    switchMap((action: CommunityActions.UpdateCommunity) =>
      this.service.update(action.payload).pipe(
        map(data => new UpdateCommunitySuccess(data)),
        catchError(error => of(new UpdateCommunityFail(error)))
      )
    )
  );
}
