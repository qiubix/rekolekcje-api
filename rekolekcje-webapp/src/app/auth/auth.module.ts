import { NgModule } from '@angular/core';
import { AuthRoutingModule } from './auth-routing-module';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '../shared/shared.module';
import { CorrectAuthRolesDirective } from './components';
import { httpInterceptorProviders } from './http-interceptors';
import * as fromComponents from './components';

@NgModule({
  imports: [SharedModule, HttpClientModule, AuthRoutingModule],
  declarations: [fromComponents.components],
  exports: [CorrectAuthRolesDirective],
  providers: [httpInterceptorProviders]
})
export class AuthModule {}
