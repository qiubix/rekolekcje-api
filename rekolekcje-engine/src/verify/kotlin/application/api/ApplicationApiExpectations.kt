package pl.oaza.waw.rekolekcje.api.application.api

import org.assertj.core.api.Assertions.assertThat
import pl.oaza.waw.rekolekcje.api.application.endpoint.ApplicationEndpoint
import pl.oaza.waw.rekolekcje.api.application.endpoint.ApplicationEndpoint.Companion.ANIMATORS_URI
import pl.oaza.waw.rekolekcje.api.application.endpoint.ApplicationEndpoint.Companion.PARTICIPANTS_URI
import pl.oaza.waw.rekolekcje.api.application.endpoint.ApplicationEndpoint.Companion.PENDING_URI
import pl.oaza.waw.rekolekcje.api.application.endpoint.ApplicationEndpoint.Companion.VERIFIED_URI
import pl.oaza.waw.rekolekcje.api.application.endpoint.ApplicationSummary
import pl.oaza.waw.rekolekcje.api.application.endpoint.RetreatApplicationDto
import pl.oaza.waw.rekolekcje.api.application.endpoint.VerificationStatisticsDto
import pl.oaza.waw.rekolekcje.api.shared.Response
import pl.oaza.waw.rekolekcje.api.shared.RestApi
import pl.oaza.waw.rekolekcje.api.shared.value.PaymentStatus

class ApplicationApiExpectations(private val restApi: RestApi) {

  companion object {
    private const val BASE_URI = ApplicationEndpoint.APPLICATION_URI
    const val BASE_ANIMATOR_URI = "$BASE_URI$ANIMATORS_URI"
    const val BASE_PARTICIPANT_URI = "$BASE_URI$PARTICIPANTS_URI"
    const val BASE_PENDING_URI = "$BASE_URI$PENDING_URI"
    const val BASE_VERIFIED_URI = "$BASE_URI$VERIFIED_URI"
  }

  private val allApplicationDetails: List<RetreatApplicationDto>
    get() = restApi.performGet(BASE_URI).getContentAsList()

  private val allApplicationSummaries: List<ApplicationSummary>
    get() = restApi.performGet(BASE_PENDING_URI).getContentAsList<ApplicationSummary>() +
        restApi.performGet(BASE_VERIFIED_URI).getContentAsList()

  fun exists(expectedApplication: RetreatApplicationDto) {
    val applicationWithMatchingId = allApplicationDetails.find { it.id == expectedApplication.id }
      ?: throw IllegalStateException()
    assertThat(applicationWithMatchingId)
      .usingRecursiveComparison()
      .ignoringFields("surety.id")
      .isEqualTo(expectedApplication)
  }

  fun exists(expectedApplication: ApplicationSummary) {
    assertThat(allApplicationSummaries).contains(expectedApplication)
  }

  fun responseHasAllApplicationsWithMatchingMemberDetails(
    response: Response,
    expectedApplications: List<RetreatApplicationDto>
  ) {
    val applicationsInResponse = response.getContentAsList<RetreatApplicationDto>()
      .map { it.copy(animatorRoles = it.animatorRoles?.toHashSet()) }
    assertThat(applicationsInResponse)
      .usingRecursiveComparison()
      .ignoringFields("member", "surety.id", "animatorRoles")
      .isEqualTo(expectedApplications)
    assertThat(applicationsInResponse.map { it.animatorRoles })
      .isEqualTo(expectedApplications.map { it.animatorRoles })
    assertThat(applicationsInResponse.map { it.member })
      .usingRecursiveComparison()
      .ignoringFields("id", "personalData.sex", "personalData.age", "personalData.birthDate")
      .isEqualTo(expectedApplications.map { it.member })
  }

  fun responseHasApplicationWithMatchingMemberDetails(response: Response, expectedApplication: RetreatApplicationDto) {
    val responsePayload = response.getContent<RetreatApplicationDto>()
    assertThat(responsePayload)
      .usingRecursiveComparison()
      .ignoringFields("surety.id", "member")
      .isEqualTo(expectedApplication)
    assertThat(responsePayload.member)
      .usingRecursiveComparison()
      .ignoringFields("id", "personalData.sex", "personalData.age", "personalData.birthDate")
      .isEqualTo(expectedApplication.member)
  }

  fun thereIsSpecificNumberOfApplications(size: Int) {
    assertThat(allApplicationDetails).hasSize(size)
  }

  fun applicationHasCorrectStatus(applicationId: Long, expectedStatus: PaymentStatus) {
    val application = restApi.performGet("$BASE_URI/$applicationId").getContent<RetreatApplicationDto>()
    assertThat(application.paymentStatus).isEqualTo(expectedStatus)
  }

  fun responseHasCorrectStats(response: Response, expectedStats: VerificationStatisticsDto) {
    response.hasStatusOk()
    val actualStats = response.getContent<VerificationStatisticsDto>()
    assertThat(actualStats).isEqualTo(expectedStats)
  }

  fun responseHasAllApplicationSummaries(response: Response, expectedSummaries: List<ApplicationSummary>) {
    response.hasStatusOk()
    val actualSummaries = response.getContentAsList<ApplicationSummary>()
    assertThat(actualSummaries)
      .usingRecursiveComparison()
      .ignoringFields("id")
      .isEqualTo(expectedSummaries)
  }
}