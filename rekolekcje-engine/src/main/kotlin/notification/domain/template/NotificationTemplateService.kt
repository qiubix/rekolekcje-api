package pl.oaza.waw.rekolekcje.api.notification.domain.template

import pl.oaza.waw.rekolekcje.api.application.domain.RetreatApplication
import pl.oaza.waw.rekolekcje.api.application.query.ApplicationQueryFacade
import pl.oaza.waw.rekolekcje.api.members.query.MembersQueryFacade
import pl.oaza.waw.rekolekcje.api.members.query.OpinionLinkQueryFacade
import pl.oaza.waw.rekolekcje.api.notification.domain.Notification
import pl.oaza.waw.rekolekcje.api.notification.domain.template.extractor.AttachmentLinkExtractor
import pl.oaza.waw.rekolekcje.api.notification.domain.template.extractor.EmailExtractor
import pl.oaza.waw.rekolekcje.api.notification.domain.template.extractor.Extractor
import pl.oaza.waw.rekolekcje.api.notification.domain.template.extractor.ExtractorType
import pl.oaza.waw.rekolekcje.api.notification.domain.template.extractor.NameExtractor
import pl.oaza.waw.rekolekcje.api.notification.domain.template.extractor.OpinionLinkExtractor
import pl.oaza.waw.rekolekcje.api.notification.domain.template.extractor.RejectionReasonExtractor
import pl.oaza.waw.rekolekcje.api.notification.domain.template.extractor.RetreatExtractor
import pl.oaza.waw.rekolekcje.api.retreats.query.RetreatQueryFacade

internal class NotificationTemplateService(
  private val applicationQueryFacade: ApplicationQueryFacade,
  private val membersQueryFacade: MembersQueryFacade,
  private val retreatQueryFacade: RetreatQueryFacade,
  private val opinionLinkQueryFacade: OpinionLinkQueryFacade,
  private val templateProvider: TemplateProvider
) {
  fun fillInTemplate(notification: Notification): TemplateConfiguration {
    val application = applicationQueryFacade.findApplicationById(notification.applicationId)
      ?: throw IllegalStateException("Cannot create template for non-existing application")
    val template = templateProvider.provideTemplate(notification.reason)
    return TemplateConfiguration(
      subject = template.subject,
      template = template.htmlTemplate,
      templateModel = buildTemplateParams(template, notification, application)
    )
  }

  private fun buildTemplateParams(
    template: Template,
    notification: Notification,
    application: RetreatApplication
  ): Map<String, Any> {
    return template.extractors
      .map { getExtractor(it, notification, application) }
      .associate(Extractor::extract)
  }

  private fun getExtractor(
    extractorType: ExtractorType,
    notification: Notification,
    application: RetreatApplication
  ): Extractor {
    return when (extractorType) {
      ExtractorType.EMAIL -> EmailExtractor(notification)
      ExtractorType.NAME -> NameExtractor(application)
      ExtractorType.RETREAT -> RetreatExtractor(application, retreatQueryFacade)
      ExtractorType.OPINION_LINK -> OpinionLinkExtractor(application, opinionLinkQueryFacade)
      ExtractorType.REJECTION_REASON -> RejectionReasonExtractor(notification)
      ExtractorType.ATTACHMENT_LINK -> AttachmentLinkExtractor(application, membersQueryFacade)
    }
  }
}
