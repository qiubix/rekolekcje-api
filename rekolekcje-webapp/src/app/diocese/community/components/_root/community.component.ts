import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Community } from '../../models/community.model';
import { Communities } from '../../store/community-reducer';
import { select, Store } from '@ngrx/store';
import { CommunityActions } from '../../store/community-actions';
import { AppSelectors } from '@core/store/app-selectors';

@Component({
  selector: 'reko-community',
  templateUrl: './community.component.html',
  styleUrls: ['./community.component.scss']
})
export class CommunityComponent implements OnInit {
  communities: Observable<Community[]>;
  communitiesLoading: Observable<boolean>;

  constructor(private store: Store<Communities.State>) {}

  ngOnInit() {
    this.communities = this.store.pipe(select(AppSelectors.getCommunityList));
    this.communitiesLoading = this.store.pipe(select(AppSelectors.getCommunityListLoading));
  }

  onAdd(community: Community): void {
    this.store.dispatch(new CommunityActions.CreateCommunity(community));
  }

  onDelete(id: number): void {
    this.store.dispatch(new CommunityActions.DeleteCommunity(id));
  }

  onEdit(community: Community): void {
    this.store.dispatch(new CommunityActions.UpdateCommunity(community));
  }
}
