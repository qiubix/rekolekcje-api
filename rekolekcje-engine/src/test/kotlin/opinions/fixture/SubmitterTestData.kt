package pl.oaza.waw.rekolekcje.api.opinions.fixture

import pl.oaza.waw.rekolekcje.api.opinions.domain.Submitter
import pl.oaza.waw.rekolekcje.api.opinions.endpoint.SubmitterApiModel

object SubmitterTestData {
  private const val FULL_NAME_DEFAULT = "Jonas Trubadur"
  private const val PHONE_NUMBER_DEFAULT = "604-543-200"
  private const val EMAIL_DEFAULT = "j.teriyaki@gmail.com"
  private val ROLE_DEFAULT = Submitter.Role.ANIMATOR

  fun createSubmitter(
      fullName: String = FULL_NAME_DEFAULT,
      phoneNumber: String = PHONE_NUMBER_DEFAULT,
      email: String = EMAIL_DEFAULT,
      role: Submitter.Role = ROLE_DEFAULT
  ): SubmitterApiModel {
    return SubmitterApiModel(
        fullName = fullName,
        phoneNumber = phoneNumber,
        email = email,
        role = role
    )
  }
}