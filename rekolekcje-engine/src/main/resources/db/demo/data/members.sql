INSERT INTO members (id, first_name, last_name, phone_number, pesel        , parish_id, father_id, mother_id, place_of_birth, christening_date, emergency_contact_id, street     , street_number, flat_number, postal_code, city    , current_treatment, medications    , allergies, other               )
VALUES              (1 , 'Jason'   , 'Tatum'  , '3456346'   , '99020354136', 1        , 1008     , 1007     , 'Boston'      , '2017-08-01'    , 1009                , 'TD Garden', 23           , 4          , 'USA'      , 'Boston', 'None'           , 'Rutinoscorbin', 'Peanuts', 'other medical data');

INSERT INTO retreat_applications(id, application_type, member_id, first_name, last_name, retreat_id, status)
VALUES (101, 'PARTICIPANT', 1, 'Jason', 'Tatum', 4, 'VERIFIED');

INSERT INTO contacts(id, full_name, phone_number)
VALUES (1007, 'Mary Tatum', '436363543'),
       (1008, 'Jacob Tatum', '436363543'),
       (1009, 'Isiah Thomas', '414515511');

INSERT INTO members (id, first_name, last_name, pesel, parish_id)
VALUES (2, 'Jaylen', 'Brown', '96012134149', 2);

INSERT INTO retreat_applications (id, application_type, animator_roles, availability, member_id, first_name, last_name, surety_id)
VALUES  (102, 'ANIMATOR', 'GROUP,MUSICIAN', '1,2,3', 2, 'Jaylen', 'Brown', 1007);

INSERT INTO contacts(id, full_name, phone_number, email)
VALUES (3002, 'Kevin Garnett', '999888777', 'kevin@mail.com'),
       (3003, 'James Rondo', '123123123', 'father@mail.com'),
       (3004, 'Kate Rondo', '321321312', 'mother@mail.com');

INSERT INTO members(id, first_name, last_name, phone_number, email                 , pesel        , parish_id, community_id, street        , street_number, flat_number, city    , postal_code, emergency_contact_id, father_id, mother_id, place_of_birth, christening_date, school_year, name_day, kwc_status, kwc_since   , number_of_communion_days, number_of_prayer_retreats, formation_meetings_in_month, leading_group_to_formation_stage, deuterocatechumenate_year, steps_taken, steps_planned_this_year, celebrations_taken, celebrations_planned_this_year, current_treatment, mental_disorders, medications , allergies    , medical_diet, cannot_hike, illness_history  , has_motion_sickness, other                   )
VALUES             (3 , 'Rajon'   , 'Rondo'  , '600500432' , 'rajon.rondo@mail.com', '87091546316', 2        , 11          , 'Green Valley', 21           , 1          , 'Boston', '12-543'   , 3002                , 3003     , 3004     , 'Kentucky'    , '1988-01-01'    , '1'        , '29-04' , 'MEMBER'  , '2008-02-03', 2                       , 3                        , 4                          , 'ODB'                           , '2003'                   , 9          , 10                     , 4                 , 6                             , 'Knee'           , 'none'          , 'some pills', 'shooting 3s', 'eats all'  , true    , 'no serious ones', false              , 'no other medical issues');

INSERT INTO historical_retreats(id, member_id, stage, year)
VALUES (301, 3, 'ODB', '2009'),
       (302, 3, 'OND', '2011'),
       (303, 3, 'ONZ_I', '2013');

INSERT INTO retreat_applications (id, application_type, animator_roles, availability, member_id, first_name, last_name, surety_id)
VALUES  (103, 'ANIMATOR', 'GROUP', '2,3', 3, 'Rajon', 'Rondo', 1009);

INSERT INTO members (id, first_name, last_name, pesel) VALUES (4, 'Carsen', 'Edwards', '96012134149');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (5, 'Tacko', 'Fall', '01250676953');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (6, 'Javonte', 'Green', '98071655593');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (7, 'Romeo', 'Langford', '99122155394');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (8, 'Aaron', 'Nesmith', '98090841531');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (9, 'Semi', 'Ojeleye', '99070722671');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (10, 'Payton', 'Pritchard', '94041391551');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (11, 'Marcus', 'Smart', '93120619896');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (12, 'Daniel', 'Theis', '93041517277');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (13, 'Tristan', 'Thompson', '10283059851');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (14, 'Kemba', 'Walker', '94060777811');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (15, 'Tremont', 'Waters', '03270871491');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (16, 'Grant', 'Williams', '03221153812');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (17, 'Robert', 'Williams III', '06220127598');

INSERT INTO members (id, first_name, last_name, pesel) VALUES (18, 'Patrick', 'Beverley', '07252692634');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (19, 'Amir', 'Coffey', '09231669314');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (20, 'Paul', 'George', '09292577597');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (21, 'Luke', 'Kennard', '11250339853');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (22, 'Serge', 'Ibaka', '01260571358');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (23, 'Reggie', 'Jackson', '94102569132');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (24, 'Mfiondu', 'Kabengele', '97042217976');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (25, 'Kawhi', 'Leonard', '00313057153');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (26, 'Terance', 'Mann', '02290135154');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (27, 'Daniel', 'Oturu', '08230356779');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (28, 'Marcus', 'Morris', '05230919832');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (29, 'Jay', 'Scrubb', '93061762174');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (30, 'Nicolas', 'Batum', '97030521654');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (31, 'Patrick', 'Patterson', '99122564639');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (32, 'Lou', 'Williams', '03260467976');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (33, 'Ivica', 'Zubac', '01240353877');

INSERT INTO members (id, first_name, last_name, pesel) VALUES (34, 'Kostas', 'Andetokunmbo', '98082476664');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (35, 'Devontae ', 'Cacok', '09300795126');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (36, 'Kentavious ', 'Caldwell-Pope', '97081018145');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (37, 'Alex ', 'Caruso', '04210327467');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (38, 'Quinn', 'Cook', '96120619323');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (39, 'Anthony', 'Davis', '95030861349');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (40, 'Jared', 'Dudley', '03301058682');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (41, 'Marc', 'Gasol', '16321315369');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (42, 'Montrezl', 'Harrell', '96112667125');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (43, 'Talen', 'Horton-Tucker', '93033197287');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (44, 'LeBron', 'James', '09271979787');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (45, 'Kyle', 'Kuzma', '97090982923');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (46, 'Matthews', 'Wesley', '15281164389');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (47, 'Alfonzo', 'McKinnie', '93012789867');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (48, 'Markieff', 'Morris', '91052634228');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (49, 'Dennis', 'Schróder', '93062519881');


INSERT INTO members (id, first_name, last_name, pesel) VALUES (50, 'Red', 'Auerbach', '89072722342');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (51, 'Bill', 'Russell', '74072188272');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (52, 'Tom', 'Heinsohn', '74111157171');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (53, 'Tom', 'Sanders', '84031962197');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (54, 'Dave', 'Covens', '72050256847');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (55, 'Bill', 'Fitch', '91072883235');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (56, 'Chris', 'Ford', '90031362981');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (57, 'M.L.', 'Carr', '76091879566');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (58, 'Rick', 'Pitino', '90071088917');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (59, 'Jim', 'O''Brien', '79120715176');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (60, 'Doc', 'Rivers', '75011135177');
INSERT INTO members (id, first_name, last_name, pesel) VALUES (61, 'Brad', 'Stevens', '76072957555');
