package notification.endpoint.events

import io.mockk.Called
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import pl.oaza.waw.rekolekcje.api.application.domain.RetreatApplication
import pl.oaza.waw.rekolekcje.api.application.endpoint.events.ApplicationRejectedEvent
import pl.oaza.waw.rekolekcje.api.application.query.ApplicationQueryFacade
import pl.oaza.waw.rekolekcje.api.application.sampleApplication
import pl.oaza.waw.rekolekcje.api.members.query.MembersQueryFacade
import pl.oaza.waw.rekolekcje.api.notification.CreateNotificationRequest
import pl.oaza.waw.rekolekcje.api.notification.domain.NotificationCommandFacade
import pl.oaza.waw.rekolekcje.api.notification.dto.NotificationReason
import pl.oaza.waw.rekolekcje.api.notification.endpoint.events.ApplicationRejectedNotificationEventListener

internal class ApplicationRejectedNotificationEventListenerTest {
  private val membersQueryFacadeMock = mockk<MembersQueryFacade>()
  private val applicationQueryFacadeMock = mockk<ApplicationQueryFacade>()
  private val notificationCommandFacadeMock = mockk<NotificationCommandFacade>()
  private val eventListener = ApplicationRejectedNotificationEventListener(
    notificationCommandFacadeMock,
    applicationQueryFacadeMock,
    membersQueryFacadeMock
  )

  private val applicationId = 321L
  private val memberId = 123L
  private val reason = "Some rejection reason"
  private val targetEmail = "some@email"
  private val event = ApplicationRejectedEvent(
    applicationId = applicationId,
    reason = reason
  )

  @BeforeEach
  internal fun setUp() {
    every { notificationCommandFacadeMock.create(any()) } returns mockk()
    every { applicationQueryFacadeMock.findApplicationById(applicationId) } returns
        sampleApplication(memberId = memberId).withId(applicationId) as RetreatApplication
  }

  @Test
  fun `should send notification when application is rejected`() {
    // given
    every { membersQueryFacadeMock.getEmail(memberId) } returns targetEmail

    // when
    eventListener.onApplicationEvent(event)

    // then
    val expectedNotificationRequest = CreateNotificationRequest(
      applicationId = applicationId,
      reason = NotificationReason.APPLICATION_REJECTED,
      targetEmail = targetEmail,
      additionalInfo = event.reason
    )
    verify { notificationCommandFacadeMock.create(expectedNotificationRequest) }
  }

  @Test
  fun `should not send rejection notification when member does not have email`() {
    // given
    every { membersQueryFacadeMock.getEmail(memberId) } returns null

    // when
    eventListener.onApplicationEvent(event)

    // then
    verify { notificationCommandFacadeMock wasNot Called }
  }
}
