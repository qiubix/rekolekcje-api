package pl.oaza.waw.rekolekcje.api.user.domain

import org.springframework.security.crypto.password.PasswordEncoder
import pl.oaza.waw.rekolekcje.api.user.endpoint.SignUpRequest
import pl.oaza.waw.rekolekcje.api.user.endpoint.UserSummary

class UserRegisterService(
  private val userReadRepository: UserReadRepository,
  private val userWriteRepository: UserWriteRepository,
  private val passwordEncoder: PasswordEncoder
) {

  fun registerUser(signUpRequest: SignUpRequest): UserSummary {
    val existingUser = userReadRepository.findByUsername(signUpRequest.username)
    if (existingUser != null) {
      throw UserAlreadyExistsException(existingUser.username)
    } else {
      val user = createUser(signUpRequest)
      val savedUser = userWriteRepository.save(user)
      return savedUser.toDto()
    }
  }

  private fun createUser(signUpRequest: SignUpRequest): User {
    val encryptedPassword = passwordEncoder.encode(signUpRequest.password)
    return User(
      username = signUpRequest.username,
      password = encryptedPassword,
      email = signUpRequest.email,
      roles = mapRoles(signUpRequest)
    )
  }

  private fun mapRoles(signUpRequest: SignUpRequest): Set<Role> =
    (signUpRequest.roles.map { Role.valueOf(it.name) } + listOf(Role.ROLE_USER)).toSet()
}
