package pl.oaza.waw.rekolekcje.api.notification.domain.template.extractor

import pl.oaza.waw.rekolekcje.api.application.domain.RetreatApplication

internal class NameExtractor(
	private val application: RetreatApplication
) : Extractor(ExtractorType.NAME) {
	override fun extractValue(): String = "${application.firstName} ${application.lastName}"
}
