package pl.oaza.waw.rekolekcje.api.community.domain

import org.springframework.data.repository.Repository
import pl.oaza.waw.rekolekcje.api.core.ddd.DomainRepository

@DomainRepository
internal interface CommunityRepository : Repository<Community, Long> {

  fun findById(id: Long): Community?

  fun findAll(): List<Community>

  fun save(community: Community): Community

  fun deleteById(id: Long)
}
