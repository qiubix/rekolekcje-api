--liquibase formatted sql

--changeset init-schema:1 author:qiubix
CREATE TYPE participantkwcstatus AS ENUM (
    'CANDIDATE',
    'MEMBER',
    'NOT_ENGAGED'
);

CREATE SEQUENCE hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

--changeset init-schema:2 author:qiubix
CREATE TABLE authorities (
    id bigint NOT NULL,
    name character varying(50) NOT NULL
);

CREATE TABLE communities (
    id bigint NOT NULL,
    name character varying(100),
    address character varying(255)
);


CREATE TABLE contacts (
    id bigint NOT NULL,
    first_name character varying(50) NOT NULL,
    last_name character varying(50) NOT NULL,
    email character varying(50),
    phone_number character varying(10)
);


CREATE TABLE members (
    id bigint NOT NULL,
    street character varying(255),
    parish_id bigint,
    pesel character varying(11) NOT NULL,
    christening_date date,
    city character varying(255),
    postal_code character varying(255),
    street_number integer,
    flat_number integer,
    current_treatment character varying(255),
    medications character varying(255),
    allergies character varying(255),
    other character varying(255),
    school_year character varying(50),
    name_day character varying(50),
    community_id bigint,
    kwc_since date,
    kwc_status character varying(50),
    number_of_communion_days integer,
    number_of_prayer_retreats integer,
    formation_meetings_in_month integer,
    leading_group_to_formation_stage character varying(50),
    deuterocatechumenate_year integer,
    steps_taken integer,
    steps_planned_this_year integer,
    celebrations_taken integer,
    celebrations_planned_this_year integer,
    mental_disorders character varying(255),
    medical_diet character varying(255),
    can_hike boolean,
    illness_history character varying(255),
    has_motion_sickness boolean,
    mod character varying(255),
    mod_opinion character varying(1500),
    animator_name character varying(255),
    animator_opinion character varying(1500),
    musical_talents character varying(255),
    number_of_odb integer,
    number_of_ond integer,
    number_of_retreats_with_parents integer,
    choir boolean,
    altar_boy boolean,
    flower_girl boolean,
    mission_celebration_year integer,
    call_by_name_celebration_year integer,
    holy_triduum_year integer,
    was_animator_before boolean,
    school_type character varying(15),
    animator boolean,
    animator_role character varying(20),
    status character varying(20),
    retreat_id bigint,
    place_of_birth character varying(50),
    contact_info_id bigint,
    father_id bigint,
    mother_id bigint,
    emergency_contact_id bigint,
    CONSTRAINT pesel_check CHECK (((pesel)::text ~ '[0-9]{11}'::text))
);

CREATE TABLE parishes (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    address character varying(255),
    region bigint
);

CREATE TABLE regions (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    address character varying(255)
);

CREATE TABLE retreat_participant (
    retreat_id bigint,
    participant_id bigint
);

CREATE TABLE retreat_turns (
    id bigint NOT NULL,
    member_id bigint,
    stage character varying(50),
    location character varying(100),
    year integer
);

CREATE TABLE retreats (
    retreat_id bigint NOT NULL,
    turn character varying(3),
    stage character varying(20),
    localization character varying(255),
    start_date date,
    end_date date,
    moderator_name character varying(50),
    moderator_phone_number character varying(15),
    priest_name character varying(50),
    priest_phone_number character varying(15),
    status character varying(20)
);


CREATE TABLE user_authority (
    user_id bigint NOT NULL,
    authority_id bigint NOT NULL
);

CREATE TABLE users (
    id bigint NOT NULL,
    enabled boolean NOT NULL,
    lastpasswordresetdate timestamp(6) without time zone NOT NULL,
    password character varying(100) NOT NULL,
    username character varying(50) NOT NULL
);

--changeset init-schema:3 author:qiubix
INSERT INTO authorities (id, name) VALUES(1, 'ROLE_USER');
INSERT INTO authorities (id, name) VALUES(2, 'ROLE_ADMIN');

INSERT INTO user_authority (user_id, authority_id) VALUES(991, 1);
INSERT INTO user_authority (user_id, authority_id) VALUES(991, 2);
INSERT INTO user_authority (user_id, authority_id) VALUES(992, 1);

INSERT INTO users (id, enabled, lastpasswordresetdate, password, username)
VALUES(991, true, '2016-01-01 00:00:00', '$2a$08$lDnHPz7eUkSi6ao14Twuau08mzhWrL4kyZGGU5xfiGALO/Vxd5DOi', 'admin');
INSERT INTO users (id, enabled, lastpasswordresetdate, password, username)
VALUES(992,	true, '2016-01-01 00:00:00', '$2a$10$6LsCgTA1oeOw3BEF06JN5eZjxKE/1tQIsupZDk6B3oBakC082AtP6', 'user');

--changeset init-schema:4 author:qiubix
ALTER TABLE ONLY authorities
    ADD CONSTRAINT authority_pkey PRIMARY KEY (id);


ALTER TABLE ONLY communities
    ADD CONSTRAINT communities_pk PRIMARY KEY (id);

ALTER TABLE ONLY contacts
    ADD CONSTRAINT contacts_pk PRIMARY KEY (id);

ALTER TABLE ONLY parishes
    ADD CONSTRAINT parish_pk PRIMARY KEY (id);

ALTER TABLE ONLY members
    ADD CONSTRAINT participant_pkey PRIMARY KEY (id);

ALTER TABLE ONLY regions
    ADD CONSTRAINT region_pk PRIMARY KEY (id);

ALTER TABLE ONLY retreat_turns
    ADD CONSTRAINT retreat_turn_pkey PRIMARY KEY (id);

ALTER TABLE ONLY retreats
    ADD CONSTRAINT retreats_pk PRIMARY KEY (retreat_id);

ALTER TABLE ONLY users
    ADD CONSTRAINT uk_r43af9ap4edm43mmtq01oddj6 UNIQUE (username);

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);

ALTER TABLE ONLY user_authority
    ADD CONSTRAINT fkgvxjs381k6f48d5d2yi11uh89 FOREIGN KEY (authority_id) REFERENCES authorities(id);

ALTER TABLE ONLY user_authority
    ADD CONSTRAINT fkhi46vu7680y1hwvmnnuh4cybx FOREIGN KEY (user_id) REFERENCES users(id);

ALTER TABLE ONLY parishes
    ADD CONSTRAINT parish_region_fk FOREIGN KEY (region) REFERENCES regions(id);

ALTER TABLE ONLY retreat_turns
    ADD CONSTRAINT retreat_turn_fkey_participant FOREIGN KEY (member_id) REFERENCES members(id) ON DELETE CASCADE;

SELECT pg_catalog.setval('hibernate_sequence', 13, true);

