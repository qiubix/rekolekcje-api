import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs/internal/Observable';
import { Parish } from '@diocese/parish/models/parish.model';
import { select, Store } from '@ngrx/store';
import { Parishes } from '@diocese/parish/store/parish-reducer';
import { AppSelectors } from '@core/store/app-selectors';
import { filter, take, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'reko-parish-picker',
  templateUrl: './parish-picker.component.html',
  styleUrls: ['./parish-picker.component.scss']
})
export class ParishPickerComponent implements OnInit, OnDestroy {
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  @Input() value: number;
  @Input() inputControl: FormControl;

  parishes$: Observable<Parish[]>;

  constructor(private _store: Store<Parishes.State>) {
    this.parishes$ = this._store.pipe(
      select(AppSelectors.getParishList),
      takeUntil(this.ngUnsubscribe),
      filter(parishes => parishes.length > 0),
      take(1)
    );
  }

  ngOnInit() {}

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  createNewParish(): void {
    // const dialogRef = this.dialog.open(ParishAddEditDialogComponent, {
    //   data: {
    //     dialogTitle: this.translate.instant('parish.dialog.add')
    //   }
    // });
    //
    // dialogRef.afterClosed().subscribe((dialogClose?: { result: Parish }) => {
    //   if (dialogClose) {
    //     this.store.dispatch(new ParishSharedActions.CreateParish(dialogClose.result));
    //     this.addedParish = dialogClose.result;
    //   }
    // });
  }
}
