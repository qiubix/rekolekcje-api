package pl.oaza.waw.rekolekcje.api.retreats.domain

interface RetreatEventPublisher {

  fun memberRegistered(retreatId: Long, memberId: Long)

  fun memberResigned(retreatId: Long, memberId: Long)

  fun memberPutOnWaitingList(retreatId: Long, memberId: Long)

}