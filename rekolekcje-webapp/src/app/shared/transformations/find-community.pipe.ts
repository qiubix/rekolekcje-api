import { OnDestroy, Pipe, PipeTransform } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { AppSelectors } from '@core/store/app-selectors';
import { filter, map, take, takeUntil, tap } from 'rxjs/operators';
import { Community } from '@diocese/community/models/community.model';
import { Communities } from '@diocese/community/store/community-reducer';
import { Observable } from 'rxjs/internal/Observable';
import { Subject } from 'rxjs';

@Pipe({
  name: 'findCommunity'
})
export class FindCommunityPipe implements PipeTransform, OnDestroy {
  private _communities$: Observable<Community[]>;
  private ngUnsubscribe: Subject<void> = new Subject<void>();

  constructor(private _store: Store<Communities.State>) {
    // console.log('findCommunity pipe constructed');
    this._communities$ = this._store.pipe(
      select(AppSelectors.getCommunityList),
      takeUntil(this.ngUnsubscribe),
      // tap(list => {
      //   if (!list || list.length === 0) {
      //     console.log('Dispatching load communities action');
      //     this._store.dispatch(new CommunityActions.LoadCommunitiesList());
      //   }
      // }),
      filter(list => list.length > 0),
      take(1)
    );
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

  transform(communityId: number, args?: any): Observable<Community> {
    // console.log('trying to find community for id: ', communityId);
    return this._communities$.pipe(
      map(communities => communities.find(it => it.id === communityId))
    );
  }
}
