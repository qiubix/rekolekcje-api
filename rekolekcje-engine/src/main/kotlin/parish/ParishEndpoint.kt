package pl.oaza.waw.rekolekcje.api.parish

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import pl.oaza.waw.rekolekcje.api.API_URI
import pl.oaza.waw.rekolekcje.api.parish.domain.ParishFacade
import pl.oaza.waw.rekolekcje.api.parish.dto.ParishPayload

const val PARISH_API_URI = "$API_URI/parish"

/**
 * This is the controller class which provides parishes and regions management. This class allows to
 * perform simple CRUD operations.
 */
@RestController
@RequestMapping(PARISH_API_URI)
class ParishEndpoint internal constructor(private val parishFacade: ParishFacade) {

  /**
   * Returns all parishes in the system as list of [ParishPayload] objects.
   *
   * @return list of ParishPayload objects
   * @see ParishPayload
   */
  @GetMapping
  @ResponseStatus(HttpStatus.OK)
  internal fun allParishes(): List<ParishPayload> {
    return parishFacade.findAll()
  }

  /**
   * Finds parish in repository by id.
   *
   * @param id id of parish to be found
   * @return parish data as ParishPayload or 404 code status, if parish wasn't found
   */
  @GetMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  internal fun singleParish(@PathVariable id: Long?): ParishPayload {
    return parishFacade.findOne(id!!)
  }

  /**
   * This method provides saving parishes to repository.
   *
   * @param newParish parish data to be saved as [ParishPayload] object
   * @return parish data saved in repository as ParishPayload and 201 code status
   */
  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  internal fun createParish(@RequestBody newParish: ParishPayload): ParishPayload {
    return parishFacade.save(newParish)
  }

  /**
   * This method allows to delete parish from repository.
   * The 404 status is returned if there is no parish with the given id.
   *
   * @param id id of parish to be deleted
   */
  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.OK)
  internal fun deleteParish(@PathVariable id: Long?) {
    parishFacade.delete(id!!)
  }


  /**
   * This method allows to update parish data.
   *
   * @param parishWithNewData updated parish data as [ParishPayload] object
   * @return updated parish data as ParishPayload and 200 code status
   */
  @PutMapping
  @ResponseStatus(HttpStatus.OK)
  internal fun updateParish(@RequestBody parishWithNewData: ParishPayload): ParishPayload {
    return parishFacade.save(parishWithNewData)
  }
}
