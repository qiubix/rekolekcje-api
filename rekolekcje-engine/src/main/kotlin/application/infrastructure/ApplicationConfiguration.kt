package pl.oaza.waw.rekolekcje.api.application.infrastructure

import org.springframework.context.ApplicationEventPublisher
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationCommandFacade
import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationNotifier
import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationReadRepository
import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationWriteRepository
import pl.oaza.waw.rekolekcje.api.application.domain.*
import pl.oaza.waw.rekolekcje.api.application.infrastructure.events.ApplicationNotifierImpl
import pl.oaza.waw.rekolekcje.api.application.infrastructure.persistance.ApplicationReadRepositoryImpl
import pl.oaza.waw.rekolekcje.api.application.infrastructure.persistance.ApplicationWriteRepositoryImpl
import pl.oaza.waw.rekolekcje.api.application.infrastructure.persistance.JpaApplicationReadRepository
import pl.oaza.waw.rekolekcje.api.application.infrastructure.persistance.JpaApplicationWriteRepository
import pl.oaza.waw.rekolekcje.api.application.infrastructure.persistance.*
import pl.oaza.waw.rekolekcje.api.application.query.ApplicationQueryFacade

@Configuration
internal class ApplicationConfiguration {

  @Bean
  fun applicationCommandFacade(
    applicationReadRepository: ApplicationReadRepository,
    applicationWriteRepository: ApplicationWriteRepository,
      applicationNotifier: ApplicationNotifier
  ) = ApplicationCommandFacade(
    applicationReadRepository = applicationReadRepository,
    applicationWriteRepository = applicationWriteRepository,
      applicationNotifier = applicationNotifier
  )

  @Bean
  fun applicationQueryFacade(applicationReadRepository: ApplicationReadRepository) =
    ApplicationQueryFacade(applicationReadRepository)

  @Bean
  fun applicationReadRepository(jpaApplicationReadRepository: JpaApplicationReadRepository): ApplicationReadRepository =
    ApplicationReadRepositoryImpl(jpaApplicationReadRepository)

  @Bean
  fun applicationWriteRepository(jpaApplicationWriteRepository: JpaApplicationWriteRepository): ApplicationWriteRepository =
    ApplicationWriteRepositoryImpl(jpaApplicationWriteRepository)

  @Bean
  fun applicationNotifier(eventPublisher: ApplicationEventPublisher, queryFacade: ApplicationQueryFacade): ApplicationNotifier =
    ApplicationNotifierImpl(eventPublisher, queryFacade)
}
