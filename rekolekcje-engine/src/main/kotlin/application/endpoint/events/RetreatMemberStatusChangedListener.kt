package pl.oaza.waw.rekolekcje.api.application.endpoint.events

import org.springframework.context.ApplicationListener
import org.springframework.stereotype.Component
import pl.oaza.waw.rekolekcje.api.application.domain.ApplicationCommandFacade
import pl.oaza.waw.rekolekcje.api.application.domain.ChangeApplicationStatus
import pl.oaza.waw.rekolekcje.api.application.query.ApplicationQueryFacade
import pl.oaza.waw.rekolekcje.api.retreats.endpoint.events.MemberStatusChangedEvent

@Component
class RetreatMemberStatusChangedListener(
  private val applicationQueryFacade: ApplicationQueryFacade,
  private val applicationCommandFacade: ApplicationCommandFacade
) : ApplicationListener<MemberStatusChangedEvent> {

  override fun onApplicationEvent(event: MemberStatusChangedEvent) {
    val applicationId = applicationQueryFacade
      .findByRetreatAndMember(event.retreatId, event.memberId)
      ?.getId() ?: throw IllegalStateException()
    val command = ChangeApplicationStatus(
      id = applicationId,
      status = event.applicationStatus
    )
    applicationCommandFacade.changeApplicationStatus(command)
  }
}