package pl.oaza.waw.rekolekcje.api.parish.domain

import java.util.Objects.requireNonNull


internal class InMemoryParishRepository :
  ParishRepository {
  private val map = HashMap<Long, Parish>()

  override fun findById(id: Long): Parish? {
    return map[id]
  }

  override fun findAll(): List<Parish> {
    return ArrayList(map.values)
  }

  override fun save(parish: Parish): Parish {
    requireNonNull(parish)
    map[parish.dto().id!!] = parish
    return parish
  }

  override fun deleteById(id: Long) {
    map.remove(id)
  }
}
