package pl.oaza.waw.rekolekcje.api.retreats.api

import org.springframework.context.ApplicationEvent
import org.springframework.context.ApplicationEventPublisher

class RetreatsEventsBehaviour(private val eventPublisher: ApplicationEventPublisher) {

  fun eventIsPublished(event: ApplicationEvent) {
    eventPublisher.publishEvent(event)
  }
}