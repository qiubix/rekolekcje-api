import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { UsersComponent } from './components/_root/users.component';
import { AuthGuard } from '../auth/guards/auth-guard.service';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { HasCorrectRoleGuard } from '../auth/guards/has-correct-role.guard';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { UserProfileGuard } from './guards/user-profile.guard';

const usersRoutes: Routes = [
  {
    path: '',
    component: UsersComponent,
    canActivate: [AuthGuard, HasCorrectRoleGuard]
  },
  {
    path: 'sign-up',
    component: SignUpComponent
  },
  {
    path: 'profile',
    component: UserProfileComponent,
    canActivate: [AuthGuard, UserProfileGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(usersRoutes)],
  exports: [RouterModule]
})
export class UsersRoutingModule {}
