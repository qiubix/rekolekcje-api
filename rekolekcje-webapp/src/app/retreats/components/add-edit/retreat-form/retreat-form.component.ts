import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { RetreatDetails } from '../../../models/retreat-details.model';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Stage } from '../../../../shared/models/stage.model';
import { Turns } from '../../../../shared/models/turn.model';
import { RekoValidators } from '../../../../shared/validators/reko-validators';

@Component({
  selector: 'retreat-form',
  templateUrl: './retreat-form.component.html',
  styleUrls: ['./retreat-form.component.scss']
})
export class RetreatFormComponent implements OnInit {
  @Input() retreatData: RetreatDetails;

  @Output() formOutput: EventEmitter<RetreatDetails> = new EventEmitter<RetreatDetails>();

  form: FormGroup;

  stages = Stage;
  stagesKeys = Object.keys(this.stages);
  turns = Turns;
  turnsKeys = Object.keys(this.turns);

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      turn: [this.retreatData ? this.retreatData.turn : '', Validators.required],
      stage: [this.retreatData ? this.retreatData.stage : '', Validators.required],
      localization: [this.retreatData ? this.retreatData.localization : '', Validators.required],
      startDate: [this.retreatData ? this.retreatData.startDate : ''],
      endDate: [this.retreatData ? this.retreatData.endDate : ''],
      priestName: [this.retreatData ? this.retreatData.priestName : ''],
      priestPhoneNumber: [
        this.retreatData ? this.retreatData.priestPhoneNumber : '',
        [RekoValidators.isPhoneNumberInvalid]
      ],
      moderatorName: [this.retreatData ? this.retreatData.moderatorName : ''],
      moderatorPhoneNumber: [
        this.retreatData ? this.retreatData.moderatorPhoneNumber : '',
        [RekoValidators.isPhoneNumberInvalid]
      ]
    });
  }

  cancel(): void {
    this.formOutput.emit();
  }

  submit(): void {
    if (this.form.invalid) {
      this.form.markAsDirty();
      return;
    }

    const retreat = this.form.value;
    retreat.id = this.retreatData.id;
    this.formOutput.emit(retreat);
  }

  get priestPhoneNumber() {
    return this.form.get('priestPhoneNumber') as FormControl;
  }

  get moderatorPhoneNumber() {
    return this.form.get('moderatorPhoneNumber') as FormControl;
  }
}
