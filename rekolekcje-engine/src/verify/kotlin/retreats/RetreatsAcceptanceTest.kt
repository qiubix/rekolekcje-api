package pl.oaza.waw.rekolekcje.api.retreats

import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.test.context.support.WithMockUser
import pl.oaza.waw.rekolekcje.api.members.storage.MembersDatabase
import pl.oaza.waw.rekolekcje.api.retreats.api.RetreatsApiBehaviour
import pl.oaza.waw.rekolekcje.api.retreats.api.RetreatsApiExpectations
import pl.oaza.waw.rekolekcje.api.retreats.storage.RetreatsDatabase
import pl.oaza.waw.rekolekcje.api.shared.BaseIntegrationTest

internal class RetreatsAcceptanceTest : BaseIntegrationTest() {

  @Autowired
  private lateinit var whenInRetreatsApi: RetreatsApiBehaviour

  @Autowired
  private lateinit var thenInRetreatsApi: RetreatsApiExpectations

  @Autowired
  private lateinit var database: RetreatsDatabase

  @Autowired
  private lateinit var membersDatabase: MembersDatabase

  private val retreatOne = retreatDetailsWithSampleData1()
  private val retreatTwo = retreatDetailsWithSampleData2()

  private val retreats = listOf(retreatOne, retreatTwo)

  @BeforeEach
  fun setup() {
    database.clear()
    membersDatabase.clear()
  }

  @AfterAll
  fun tearDown() {
    database.clear()
  }

  @Test
  @WithMockUser
  fun `should add a single retreat`() {
    // when some retreat turn is added
    val response = whenInRetreatsApi.singleIsAdded(retreatOne)

    // then API returns correct response
    response.hasStatusCreated()
    thenInRetreatsApi.responseHasCorrectJsonValues(response, retreatSummary(retreatOne))
  }

  @Test
  @WithMockUser
  fun `should update a single retreat`() {
    // given some retreat has already been added
    whenInRetreatsApi.allAreAdded(listOf(retreatOne))
    val existingRetreatId = whenInRetreatsApi.existsRetreatWithTheSameData(retreatOne)

    // when this retreat is updated
    val updatedRetreat = retreatTwo.copy(id = existingRetreatId)
    val response = whenInRetreatsApi.singleIsUpdated(updatedRetreat)

    // then API returns the same number of retreats with updated data
    val expectedPayload = retreatSummary(updatedRetreat)
    thenInRetreatsApi.responseHasCorrectDataIgnoringId(response, expectedPayload)
    thenInRetreatsApi.thereIsAnExactNumberOfEntities(1)
  }

  @Test
  @WithMockUser
  fun `should return 'not found' when retrieving non-existing retreat`() {
    // given some retreats were already added
    whenInRetreatsApi.allAreAdded(retreats)
    val incorrectId = 654321L

    // when trying to retrieve a retreat with incorrect id
    val response = whenInRetreatsApi.singleIsRequested(incorrectId)

    // then 'not found' is returned
    thenInRetreatsApi.responseStatusIsNotFound(response)
  }

  @Test
  @WithMockUser
  fun `should get one retreat`() {
    // given some retreats were already added
    whenInRetreatsApi.allAreAdded(retreats)
    val existingRetreat = whenInRetreatsApi.existsRetreatWithTheSameData(retreatOne)

    // when trying to retrieve one with correct id
    val response = whenInRetreatsApi.singleIsRequested(existingRetreat)

    // then retreat turn with correct data is returned
    val expectedDetails = retreatOne.copy(id = existingRetreat)
    thenInRetreatsApi.responseHasCorrectRetreatDetails(response, expectedDetails)
  }

  @Test
  @WithMockUser
  fun `should get all retreats as retreatSummary`() {
    // given
    whenInRetreatsApi.allAreAdded(retreats)

    // when
    val response = whenInRetreatsApi.allAreRequested()

    // then
    thenInRetreatsApi.responseHasExactNumberOfElements(response, retreats.size)
    thenInRetreatsApi.responseHasCorrectDataIgnoringId(response, retreats.map { retreatSummary(it) })
  }

  @Test
  @WithMockUser
  fun `should delete a retreat`() {
    // given some retreats were already added
    whenInRetreatsApi.allAreAdded(retreats)
    val existingRetreatId = whenInRetreatsApi.existsRetreatWithTheSameData(retreatOne)

    // when retreat turn is deleted
    whenInRetreatsApi.singleIsDeleted(existingRetreatId)

    // then API returns retreats without the deleted one
    thenInRetreatsApi.thereIsAnExactNumberOfEntities(retreats.size - 1)
    thenInRetreatsApi.doesNotExist(retreatSummary(retreatOne))
  }
}