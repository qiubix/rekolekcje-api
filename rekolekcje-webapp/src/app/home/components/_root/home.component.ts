import { Component, OnInit } from '@angular/core';
import {
  ANIMATORS_NUMBER,
  COMMUNITIES_NUMBER,
  PARTICIPANTS_NUMBER,
  RETREATS_NUMBER,
} from '../../mockSummary';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  participantsNumber: number;
  animatorsNumber: number;
  communitiesNumber: number;
  retreatsNumber: number;

  testEmailForm: FormGroup;

  constructor(
    private router: Router,
    private fb: FormBuilder,
    private http: HttpClient,
    private snackBar: MatSnackBar
  ) {
    this.participantsNumber = PARTICIPANTS_NUMBER;
    this.animatorsNumber = ANIMATORS_NUMBER;
    this.communitiesNumber = COMMUNITIES_NUMBER;
    this.retreatsNumber = RETREATS_NUMBER;
  }

  ngOnInit() {
    this.testEmailForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
    });
  }

  navigate(url: string) {
    this.router.navigateByUrl(url);
  }

  sendTestEmail() {
    if (this.testEmailForm.invalid) {
      return;
    }
    let email = this.testEmailForm.value.email;
    const request = {
      applicationId: 102,
      reason: 'APPLICATION_SUBMITTED',
      targetEmail: email,
    };
    this.http.post('/api/notification', request).subscribe((response) => {
      console.log('response: ', response);
      this.snackBar.open('Wiadomość wysłana na adres', email, { duration: 2000 });
    });
  }
}
