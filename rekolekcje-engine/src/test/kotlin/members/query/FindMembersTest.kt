package pl.oaza.waw.rekolekcje.api.members.query

import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatExceptionOfType
import org.assertj.core.api.BDDAssertions.then
import org.junit.jupiter.api.Test
import pl.oaza.waw.rekolekcje.api.members.MemberTestFixture
import pl.oaza.waw.rekolekcje.api.members.domain.Member
import pl.oaza.waw.rekolekcje.api.members.domain.sampleMember
import pl.oaza.waw.rekolekcje.api.shared.exceptions.MemberNotFoundException

internal class FindMembersTest : MemberTestFixture() {

  private val firstMember = sampleMember("Kevin", "Garnett", "82020354329")
  private val secondMember = sampleMember("Ray", "Allen", "82020312349")

  @Test
  fun `should find all members`() {
    // given
    val members = listOf(firstMember, secondMember)
    commandFacade.saveAll(members)

    // when
    val foundMembers = queryFacade.findAllMembers()

    // then
    assertThat(foundMembers)
      .usingElementComparatorIgnoringFields("id")
      .isEqualTo(members)
  }

  @Test
  fun `should return empty list when no members found`() {
    // when
    val foundMembers = queryFacade.findAllMembers()

    //then
    assertThat(foundMembers).isEmpty()
  }

  @Test
  fun `should find single member`() {
    // given
    val members = listOf(firstMember, secondMember)
    commandFacade.saveAll(members)
    val memberId = getIdOfCorrespondingMemberFromSystem(secondMember)

    // when
    val foundMember = queryFacade.find(memberId)

    // then
    assertThat(foundMember).isEqualTo(secondMember.copy(id = memberId))
  }

  @Test
  fun `should throw exception when no member found`() {
    // given
    val idOfNotExistingMember = 0L

    // when & then
    assertThatExceptionOfType(MemberNotFoundException::class.java)
      .isThrownBy { queryFacade.find(idOfNotExistingMember) }
      .withMessageContaining("id: $idOfNotExistingMember")
  }

  @Test
  fun `should return true when member exists`() {
    // given
    val savedMember = commandFacade.save(firstMember)

    // when
    val existsById = queryFacade.existsById(savedMember.getId()!!)

    // then
    then(existsById).isTrue()
  }

  @Test
  fun `should return false when member doest not exist`() {
    // given
    commandFacade.save(firstMember)

    // when
    val existsById = queryFacade.existsById(99L)

    // then
    then(existsById).isFalse()
  }

  @Test
  fun `should get member's email`() {
    // given
    val email = "sample@email.com"
    val member = sampleMember(email = email)
    val persistedMember = commandFacade.save(member)
    val memberId = persistedMember.getId() ?: throw IllegalStateException()

    // when
    val foundEmail = queryFacade.getEmail(memberId)

    // then
    assertThat(foundEmail).isEqualTo(email)
  }

  @Test
  fun `should test if member is adult`() {
    // given
    val member = sampleMember(pesel = "81112693713")
    val memberId = saveAndGetId(member)

    // when
    val isAdult = queryFacade.isAdult(memberId)

    // then
    assertThat(isAdult).isTrue()
  }

  @Test
  fun `should return false when member is not adult`() {
    // given
    val member = sampleMember(pesel = "21210181783")
    val memberId = saveAndGetId(member)

    // when
    val isAdult = queryFacade.isAdult(memberId)

    // then
    assertThat(isAdult).isFalse()
  }

  private fun saveAndGetId(member: Member): Long = commandFacade.save(member).getId() ?: throw IllegalStateException()
}

